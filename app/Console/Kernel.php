<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     * C:\xampp\htdocs 執行 ＊＊＊　php artisan schedule:run　＊＊＊
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\DailyIssue::class,
        \App\Console\Commands\ExpiredCheck::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // 2018/10/23 有兩個要執行
        // 1.(一次性)填入現有 開通 的值 (就用創造ball的時間當他的開通時間)
        // $schedule->command('insert:insert_all_active_record')->everyMinute();

        // 2.(一次性)填入現有 多顆球的編號
        // $schedule->command('insert:ball_table_insert_ball_index')->everyMinute();

        // 例行
        $schedule->command('expired:check')->everyTenMinutes(); // ->everyMinute();   ->everyTenMinutes(); 
        $schedule->command('daily:issue')->daily(); //->everyMinute();// ->daily(); 

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
