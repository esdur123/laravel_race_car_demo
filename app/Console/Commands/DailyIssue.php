<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\Ball;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\PhoneVerify;
use App\Models\Rate;


use App\Repositories\WalletRepository;
use App\Services\OrganizationService;
use App\Repositories\RateRepository;
use App\Repositories\BerReportRepository;

use Carbon\Carbon;
use File;
use Lock;
use DB;

class DailyIssue extends Command
{
    // 命令名稱
    protected $signature = 'daily:issue';

    // 說明文字
    protected $description = '[每日任務] 清除日累計/靜態獎金';

    public function __construct()
    {
        parent::__construct();
    }

    // Console 執行的程式
    public function handle()
    {

        static::static_bonus();
        static::clear_phone_verify();
        //static::test_log('999'); // ******************* 測試用LOG ********************************

    } // end func


    // 測試用的log 
    private function test_log($info){

            // 檔案紀錄在 storage/test.log
            $log_file_path = storage_path('test.log');


            // 記錄當時的時間
            // $log_info = [
            //     'date'=>date('Y-m-d H:i:s '.$info)
            // ];
            $log_info = '['.Carbon::now().'] '.$info;

            // 記錄 JSON 字串
            $log_info_json = json_encode($log_info) . "\r\n";

            // 記錄 Log
            File::append($log_file_path, $log_info_json);
    }


    // 每清除電話驗證上限
    private function clear_phone_verify(){
        $phone_verify_db = PhoneVerify::where('verify',0)
                    ->get();

        for ($i=0; $i < count($phone_verify_db); $i++) { 
            $phone_verify_db[$i]->wrong_count = 0;
            $phone_verify_db[$i]->apply_count = 0;
            $phone_verify_db[$i]->save();
        }
    }


    // 取得今日倍率 
    private function get_today_static_rate(){

            // 取得所有配套總和
            $level_db = Ball::selectRaw("level,count('level') as lvl_count")->groupBy('level')->get();
                        
            $lvl_1_count = $level_db->where("level",1)->first()->lvl_count;
            $total_set_lvl_1 = (int)$lvl_1_count * (int)OrganizationService::getSetFromLevel(1);

            $lvl_2_count = $level_db->where("level",2)->first()->lvl_count;
            $total_set_lvl_2 = (int)$lvl_2_count * (int)OrganizationService::getSetFromLevel(2);            
            
            $total_set = $total_set_lvl_1 + $total_set_lvl_2;
            if($total_set < 6000000){
                $total_set = rand(5500000, 6500000) ;
            }
            // 丟到 BerReportRepository
            $a_win_times = rand(10,16);
            $bet_report_db = BerReportRepository::create($total_set, $a_win_times);

            // 存入當日rate db紀錄, 每天會記錄在rate table, 當日重複會覆蓋        
            $now = Carbon::now();
            RateRepository::add($now->year, $now->month, $now->day, $bet_report_db->income_ratio);
            //static::test_log((string)$bet_report_db->income_ratio);            

            return $bet_report_db->income_ratio;

            // 舊版
            // $now = Carbon::now();

            // $today_rate = 1;
            // $rate_db = Rate::where('year',$now->year)
            //             ->where('month',$now->month)
            //             ->where('day',$now->day)
            //             ->first();

            // // 創一個1%-2%的上去            
            // if(!$rate_db){
            // // 1-2%的隨機倍率
            // $today_rate = 1 + (rand(0,100) * 0.01);
            // $today_rate = round($today_rate,2); // 許小數點下兩位

            //     // 同一天重複的會覆蓋過去
            //     RateRepository::create($now->year,$now->month,$now->day,$today_rate);
            // } else {
            //     $today_rate = $rate_db->rate;
            // }

    }



    // 每日發靜態獎金 
    private function static_bonus(){

        // 目前獎金 全部依照 BerReportRepository 算出來的值當倍率, 
        $daily_static_rate = static::get_today_static_rate();
        if($daily_static_rate == 0){ // 今日倍率0 ->不發
            return;
        }

        // $daily_static_rate = (static::get_today_static_rate()) / (float)100;
        // (float)PointRules::BONUS_DAILY_STATIC / (float)1000;
        // $bonus_static = (float)$set * $daily_static_rate;
        // $fee_bonus_static = 0;//$bonus_static * (float)PointRules::FEE_DAILY_STATIC / 100;
        
        $ball_db = Ball::all();
        
        for ($i=0; $i < count($ball_db); $i++) { 

            // 每顆球的靜態上限 (自動激活要把上限加上去)
            $static_max_out_quota = 0;
            if($ball_db[$i]->level == 1) {
                $static_max_out_quota = (int)PointRules::SET_LVL_1 * (int)PointRules::STATIC_OUT_RATE_LVL_1;
            }

            if($ball_db[$i]->level == 2) {
                $static_max_out_quota = (int)PointRules::SET_LVL_2 * (int)PointRules::STATIC_OUT_RATE_LVL_2;
            }  

            // wallet
            $find_wallet_db = Wallet::where('user_id',$ball_db[$i]->user_id)
                        ->where('coin','Point')
                        ->first();

            if(is_null($find_wallet_db)){ // wallet有問題者 跳過..
                continue;
            }

            $find_user_id = $find_wallet_db->user_id;

            DB::transaction(function() use($ball_db, $i, $daily_static_rate, $find_user_id, $static_max_out_quota){

                // 到此確定有wallet_db, 鎖每一個錢包
                $lock_wallet_key = 'Wallet@user_id:' .$find_user_id;

                try {
                    Lock::acquire($lock_wallet_key);

                    // Logic
                    $wallet_db = Wallet::where('user_id',$ball_db[$i]->user_id)
                            ->where('coin','Point')
                            ->first();
                                    
                    $set = OrganizationService::getSetFromLevel($ball_db[$i]->level);
                    $active_cost_point = $set;
                    //$active_cost_colorpoint = 0;

                    if($active_cost_point <= 0){ // 未激活者 提示他 active_cost_point

                        $transaction_db = new Transaction;
                        $transaction_db->user_id = $ball_db[$i]->user_id;
                        $transaction_db->wallet_id = $wallet_db->id;
                        $transaction_db->coin =  'Point';
                        $transaction_db->type = TransactionTypes::DAILY_STATIC_INACTIVED;
                        $transaction_db->value = '0';
                        $transaction_db->before = $wallet_db->balance_available + $wallet_db->balance_locked;
                        $transaction_db->after  = $wallet_db->balance_available + $wallet_db->balance_locked;
                        $transaction_db->link = "";
                        $transaction_db->save();
                        //continue;
                        return; // 閉包改return
                    }

                    // 清除每日碰撞上限
                    //$ball_db[$i]->acc_pv_today = 0;
                    $ball_db[$i]->save();

                    // 算出靜態獎金1%
                    //$out_array = explode("/",$ball_db[$i]->out);

                    // $daily_static_rate = (float)PointRules::BONUS_DAILY_STATIC / (float)1000;
                    $bonus_static = (float)$set * $daily_static_rate;
                    $bonus_static = round($bonus_static,7);

                    $fee_bonus_static = 0;//$bonus_static * (float)PointRules::FEE_DAILY_STATIC / 100;
                        
                    // 發獎金前檢查 自動激活 + 爆掉處理
                    $out_result = 'succeed';
                    if((float)$ball_db[$i]->static_current >= (float)$ball_db[$i]->static_max){ // 爆了
                        
                        if((int)$ball_db[$i]->auto_active == 0){   // 沒自動激活  
                            $out_result = TransactionTypes::DAILY_STATIC_OUT;

                        } else {

                            // 檢查wallet/colorwallet夠不夠錢
                            if( (float)$wallet_db->balance_available >= $active_cost_point )
                            {

                                // -$set 交易單
                                $transaction_db = new Transaction;
                                $transaction_db->user_id = $ball_db[$i]->user_id;
                                $transaction_db->wallet_id = $wallet_db->id;
                                $transaction_db->coin =  'Point';
                                $transaction_db->type = TransactionTypes::AUTO_REACTIVE;//'Auto-reActive';
                                $transaction_db->value = $active_cost_point;
                                $transaction_db->before = $wallet_db->balance_available + $wallet_db->balance_locked;
                                $transaction_db->after  = $wallet_db->balance_available + $wallet_db->balance_locked - $active_cost_point;
                                $transaction_db->link = "";
                                $transaction_db->save();

                                $wallet_db->balance_available = (float)$wallet_db->balance_available - $active_cost_point;
                                $wallet_db->save();

                                // 扣款 out歸零
                                //$ball_db[$i]->static_current = 0;
                                $ball_db[$i]->static_max += $static_max_out_quota;
                                $ball_db[$i]->save();

                            } else {

                                $out_result = TransactionTypes::REACTIVE_FAILED;//'Reactive-Failed';
                            }

                        }                
                    }

                    // 靜態出局 發交易單提示, continue
                    if($out_result != 'succeed' ){
                        
                        $transaction_db = new Transaction;
                        $transaction_db->user_id = $ball_db[$i]->user_id;
                        $transaction_db->wallet_id = $wallet_db->id;
                        $transaction_db->coin =  'Point';
                        $transaction_db->type = $out_result;
                        $transaction_db->value = $bonus_static;
                        $transaction_db->before = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                        $transaction_db->after = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                        $transaction_db->link = "";
                        $transaction_db->save();

                        // 超過十萬補一張 沒發到的單
                        if($ball_db[$i]->performance > 100000){
                            $missed_reward = 100;

                            if($ball_db[$i]->performance > 200000) $missed_reward = 200;
                            if($ball_db[$i]->performance > 300000) $missed_reward = 300;
                            if($ball_db[$i]->performance > 400000) $missed_reward = 400;

                            $transaction_db = new Transaction;
                            $transaction_db->user_id = $ball_db[$i]->user_id;
                            $transaction_db->wallet_id = $wallet_db->id;
                            $transaction_db->coin =  'Point';
                            $transaction_db->type = TransactionTypes::DAILY_REWARD_OUT;
                            $transaction_db->value = $missed_reward;
                            $transaction_db->before = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                            $transaction_db->after = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                            $transaction_db->link = "";
                            $transaction_db->save();
                        }

                        //continue;
                         return; // 閉包改return
                    }

                    // 發錢
                    // 兩張單
                    $transaction_db = new Transaction;
                    $transaction_db->user_id = $ball_db[$i]->user_id;
                    $transaction_db->wallet_id = $wallet_db->id;
                    $transaction_db->coin =  'Point';
                    $transaction_db->type = TransactionTypes::DAILY_STATIC;//'Daily-Static';
                    $transaction_db->value = $bonus_static;
                    $transaction_db->before = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                    $transaction_db->after = (float)$wallet_db->balance_available  + (float)$wallet_db->balance_locked + $bonus_static;
                    $transaction_db->link = "";
                    $transaction_db->save();

                    // $transaction_db = new Transaction;
                    // $transaction_db->user_id = $ball_db[$i]->user_id;
                    // $transaction_db->wallet_id = $wallet_db->id;
                    // $transaction_db->coin =  'Point';
                    // $transaction_db->type = TransactionTypes::FEE_DAILY_STATIC;//'Fee-Daily-Static';
                    // $transaction_db->value = $fee_bonus_static;
                    // $transaction_db->before = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                    // $transaction_db->after = (float)$wallet_db->balance_available  + (float)$wallet_db->balance_locked + $bonus_static - $fee_bonus_static;
                    // $transaction_db->link = "";
                    // $transaction_db->save();                

                    $wallet_db->balance_available = (float)$wallet_db->balance_available + $bonus_static - $fee_bonus_static;
                    $wallet_db->save(); // 不管發了會不會爆 都發

                    $add = $ball_db[$i]->static_current + $bonus_static;
                    $add = round($add,7);
                    if((int)$ball_db[$i]->auto_active == 0){
                        $ball_db[$i]->static_current = min( $add, $ball_db[$i]->static_max);
                    }   

                    if((int)$ball_db[$i]->auto_active == 1){
                        $ball_db[$i]->static_current = $add;
                    }   
                    //$ball_db[$i]->static_current = $add;
                    $ball_db[$i]->save();

                    // 超過十萬的獎勵
                    if($ball_db[$i]->performance > 100000){

                        $performance_reward = 100;
                        $performance_transaction_type = TransactionTypes::PERFORMANCE_100K_REWARD;

                        if($ball_db[$i]->performance > 200000){
                            $performance_reward = 200;
                            $performance_transaction_type = TransactionTypes::PERFORMANCE_200K_REWARD;
                        }

                        if($ball_db[$i]->performance > 300000){
                            $performance_reward = 300;
                            $performance_transaction_type = TransactionTypes::PERFORMANCE_300K_REWARD;
                        }

                        if($ball_db[$i]->performance > 400000){
                            $performance_reward = 400;
                            $performance_transaction_type = TransactionTypes::PERFORMANCE_400K_REWARD;
                        }

                        $transaction_db = new Transaction;
                        $transaction_db->user_id = $ball_db[$i]->user_id;
                        $transaction_db->wallet_id = $wallet_db->id;
                        $transaction_db->coin =  'Point';
                        $transaction_db->type = $performance_transaction_type;
                        $transaction_db->value = $performance_reward;
                        $transaction_db->before = (float)$wallet_db->balance_available 
                                                  + (float)$wallet_db->balance_locked;

                        $transaction_db->after = (float)$wallet_db->balance_available  
                                                 + (float)$wallet_db->balance_locked 
                                                 + $performance_reward;

                        $transaction_db->link = "";
                        $transaction_db->save();

                        $ball_db[$i]->static_current += $performance_reward;
                        $ball_db[$i]->save();
                        $wallet_db->balance_available += $performance_reward;
                        $wallet_db->save();
                    }
                    

                    // 再檢查一次自動激活
                    if((float)$ball_db[$i]->static_current >= (float)$ball_db[$i]->static_max){ // 爆了
                        
                        if((int)$ball_db[$i]->auto_active == 0){   // 沒自動激活                          
                            //continue;
                             return; // 閉包改return
                        }

                            // 檢查wallet/colorwallet夠不夠錢
                            if((float)$wallet_db->balance_available >= $active_cost_point)
                            {

                                // -$set 交易單
                                $transaction_db = new Transaction;
                                $transaction_db->user_id = $ball_db[$i]->user_id;
                                $transaction_db->wallet_id = $wallet_db->id;
                                $transaction_db->coin =  'Point';
                                $transaction_db->type = TransactionTypes::AUTO_REACTIVE;//'Auto-reActive';
                                $transaction_db->value = $active_cost_point;
                                $transaction_db->before = $wallet_db->balance_available + $wallet_db->balance_locked;
                                $transaction_db->after = $wallet_db->balance_available + $wallet_db->balance_locked - $active_cost_point;
                                $transaction_db->link = "";
                                $transaction_db->save();

                                $wallet_db->balance_available = (float)$wallet_db->balance_available - $active_cost_point;
                                $wallet_db->save();

                                // 扣款 out歸零
                                //$ball_db[$i]->static_current = 0;
                                $ball_db[$i]->static_max += $static_max_out_quota;
                                $ball_db[$i]->save();


                            } else {
                                //continue;
                                 return; // 閉包改return
                            }
                            
                    }

                } finally {
                    Lock::release($lock_wallet_key);
                }

            }); // end db transaction    

        } // end fo

    } // end func





}