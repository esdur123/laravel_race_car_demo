<?php

namespace App\Console\Commands;
use App\Defined\VerifyTags;
use App\Defined\FrozenTags;

use App\Models\Trading;
use App\Models\User;
use App\Models\Order;
use App\Models\Verify;

use Illuminate\Console\Command;
use File;
use Carbon\Carbon;
use App\Repositories\VerifyRepository;
use App\Services\FrozenService;

class ExpiredCheck extends Command
{
    // 命令名稱
    protected $signature = 'expired:check';

    // 說明文字
    protected $description = '[檢查] 超過8小時取消交易單';

    public function __construct()
    {
        parent::__construct();
    }

    // Console 執行的程式
    public function handle()
    {

        $expire_time = Carbon::now()->subHours(8);
        
        $trading_db = Trading::where("state","waiting")->get();
        for ($i=0; $i < count($trading_db); $i++) { 

            $start_time = $trading_db[$i]->created_at;
            if( $start_time->lt($expire_time) ){
               if($trading_db[$i]->state == 'cancel_order'){
                // 這張order單已被取消
                

               } else {
                 // 正常過期程序
                 $trading_db[$i]->state = 'expired';
                 $trading_db[$i]->save();

                 // order單交易中取消
                 $order_db = Order::withTrashed()->find($trading_db[$i]->order_id);
                 if($order_db){
                    $order_db->trading = $order_db->trading - $trading_db[$i]->amount;
                    $order_db->save();
                 }
               }


               // 凍結買家
               $is_frozen_buyer = true;

               // $confirm_value_array = explode(",", $trading_db[$i]->confirm_value);
               // if(count($confirm_value_array)==2){
               //      if($confirm_value_array[0] == 'buyer-confirm'){
               //          $is_frozen_buyer = false;
               //      }
               // }

               if($is_frozen_buyer){
                  //VerifyRepository::create($trading_db[$i]->buyer_user_id, VerifyTags::USER_FROZEN);
                  $user_db = User::find($trading_db[$i]->buyer_user_id);
                  if($user_db){
                     FrozenService::frozen(FrozenTags::PHONE, '('.$user_db->country_code.')'.$user_db->phone, 1);
                  }
                 
               }


               // check buyer-confirm
               
               // 如果state == waiting 同時 confirm-value 第一格不是buyer-confirm
               // 就凍結買家

            }
            
        }

        // // 檔案紀錄在 storage/test.log
        // $log_file_path = storage_path('test.log');

        // // 記錄當時的時間
        // $log_info = [
        //     'expired:check'=>date('Y-m-d H:i:s')
        // ];

        // // 記錄 JSON 字串
        // $log_info_json = json_encode($log_info) . "\r\n";

        // // 記錄 Log
        // File::append($log_file_path, $log_info_json);
    }
}