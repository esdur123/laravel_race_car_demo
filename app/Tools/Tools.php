<?php

namespace App\Tools;
use File;

class Tools
{

    // base64 解密
    // public static function base64_decoed($data){

    //     $key = '12345612345612345612345612345612';//md5('123456');
    //     $string = base64_decode($data);
    //     $len = strlen($key);
    //     $code = '';   
    //      for ($i = 0; $i < strlen($string); $i++) {  
    //             $k = $i % $len;   
    //             $code .= $string [$i] ^ $key [$k];
    //      }   
    //     $decode = base64_decode($code);
    //     return $decode;
    // }


    public static function getIP()
    {
        $connect_ip = "";
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $connect_ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $connect_ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $connect_ip = $_SERVER["REMOTE_ADDR"];
        }

        return $connect_ip;
    }


    public static function random4()
    {
        $rd4 = substr(strval(rand(10000,19999)),1,4);

        return $rd4;
    }




}