<?php

namespace App\Tools;

class TXPay
{
    /** 超商代碼 */
    const METHOD_CSPN = "CSPM";
    /** ATM虛擬帳號 */
    const METHOD_VBANK = "VBANK";

    /**
     * 正式
     * https://ag.txpay.tw/
     */


    /**
     * 測試
     * https://agtest.txpay.tw/
     */


    static function createOrder($method, $return_url, $trade_id, $amount, $msg, $user_name)
    {
        // 812 跟 822 有個資法問題
        //$banks_id = ['007','812','822'];
        $banks_id = ['007'];
        shuffle($banks_id);

        //$chksum = TXPay::STORE_ID . TXPay::STORE_PASSWORD . $amount;  // env('JWT_KEY')

        $chksum = env('TX_STORE_ID') . env('TX_STORE_PASSWORD') . $amount;
        $data = array(
            /** 商家代號 */
            "StoreID" => env('TX_STORE_ID'),
            /** 交易金額 */
            "Amount" => $amount,
            /** 幣值 */
            "Currency" => 'TWD',
            /** 幣值 */
            "PayMethod" => $method,
            /** 購買說明 */
            "PayInfo" => $msg,
            /** 商家訂單編號 */
            "StoreOrderId" => $trade_id,
            /** 買家姓名 */
            "PayName" => $user_name,
            /** 買家聯絡電話 */
            "PayPhone" => '09',
            /** 買家 Email */
            //"PayEmail" => 'buyer@gmail.com',
            /** 備註 1 */
            //"Note1" => '',
            /** 備註 2 */
            //"Note2" => '',
            /** 過期時間 */
            "ExpireDate" => date('Ymd', strtotime('+1 day')),
            /** 商家用戶編號(自行定義會員編號) */
            "UserId" => $user_name,
            /** 交易檢查碼  md5(StoreID+交易密碼+Amount) */
            "Chksum" => md5($chksum),
            /** 付款完成通知回傳網址 */
            "ReturnURL" => $return_url,
            /** 收單銀行 */
            "Bankid" => $banks_id[0]);

        $result = "<form id='fr' name='fr' action='" . env('TX_URL') . "' type='POST'  >";
        foreach ($data as $key => $value) {
            $result .= "<input type='hidden' name='" . $key . "' value='" . $value . "'>";
        }
        $result .= "</form>";
        $result .= "<script type='text/javascript'>";
        //$result .= "window.onload=function(){window.setTimeout(document.fr.submit(), 3000);};";
        // $result .= "$('#fr').submit(function() {window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');this.target = 'formpopup';});";
        //$result .= "submit_fr();";
        $result .= "$('#fr').submit();";
        // $result .= "form_ready();";
        //$result .= "window.onload=function(){window.setTimeout(form_ready(), 3000);};";
        $result .= "</script>";

        return $result;
    }

    static function verifyReturn($request)
    {
        $chksum = md5(env('TX_STORE_ID') . env('TX_STORE_PASSWORD') . $request->id . $request->Amount . $request->errorcode);
        return $request->Chksum == $chksum;
    }
}