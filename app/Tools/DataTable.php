<?php

namespace App\Tools;

use App\Defined\ApiError;

class DataTable
{
    static function complex($eloquent, $request, $columns)
    {
        $recordsTotal = $eloquent->count();
        /** filter */
        $eloquent = DataTable::filter($eloquent, $request, $columns);
        $recordsFiltered = $eloquent->count();

        /** limit */
        if (isset($request['start']) && $request['length'] != -1) {
            $eloquent = $eloquent->offset(intval($request['start']))->limit(intval($request['length']));
        }

        /** order */
        $order = DataTable::order($request, $columns);
        if (isset($order)) {
            foreach ($order as $key => $value) {
                $eloquent = $eloquent->orderBy($key, $value);
            }
        }

        $data = $eloquent->get();

        $result['error'] = ApiError::SUCCESS;
        $result['draw'] = isset ($request['draw']) ? intval($request['draw']) : 0;
        $result['recordsTotal'] = intval($recordsTotal);
        $result['recordsFiltered'] = intval($recordsFiltered);
        $result['data'] = DataTable::data_output($columns, $data);

        return $result;
    }

    static function order($request, $columns)
    {
        $orderBy = array();
        if (isset($request['order']) && count($request['order'])) {
            $dtColumns = self::pluck($columns, 'dt');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';
                    $orderBy[$column['db']] = $dir;
                }
            }
        }
        return $orderBy;
    }

    static function limit($request)
    {
        $limit = '';
        if (isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . intval($request['start']) . ", " . intval($request['length']);
        }
        return $limit;
    }

    static function filter($eloquent, $request, $columns)
    {
        $filterBySearch = DataTable::filterBySearch($request, $columns);
        if (isset($filterBySearch)) {
            $eloquent->where(function ($eloquent) use ($request, $columns, $filterBySearch) {
                foreach ($filterBySearch as $key => $value) {
                    $eloquent->orWhere($key, 'like', $value);
                }
            });
        }

        $filterByColumns = DataTable::filterByColumns($request, $columns);
        if (isset($filterByColumns)) {
            foreach ($filterByColumns as $key => $value) {
                $eloquent = $eloquent->where($key, 'like', $value);
            }
        }
        return $eloquent;
    }

    /** OR */
    static function filterBySearch($request, $columns)
    {
        $globalSearch = array();
        $dtColumns = self::pluck($columns, 'dt');
        if (isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['searchable'] == 'true' && $column['db'] != '') {
                    $globalSearch[$column['db']] = '%' . $str . '%';
                }
            }
        }
        return $globalSearch;
    }

    /** AND */
    static function filterByColumns($request, $columns)
    {
        $columnSearch = array();
        $dtColumns = self::pluck($columns, 'dt');
        // Individual column filtering
        if (isset($request['columns'])) {
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $str = $requestColumn['search']['value'];
                if ($requestColumn['searchable'] == 'true' &&
                    $str != '') {
                    $columnSearch[$column['db']] = '%' . $str . '%';
                }
            }
        }
        return $columnSearch;
    }

    static function data_output($columns, $data)
    {
        $out = array();

        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    static function pluck($a, $prop)
    {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

}