<?php

namespace App\Tools;

use Illuminate\Support\Facades\Config;

use App\Defined\ApiError;

class SMS
{
    /** 每日發送次數 */
    const DAILY_TIMES = 3;
    /** 每日發送次數 */
    const DAILY_IP_TIMES = 6;
    /** 發送間隔1分鐘 */
    const INTERVAL = 60;

    //台灣簡訊(國內)
    const TWSMS_USERNAME = "sian";
    const TWSMS_PASSWORD = "585858";

    private static function sendByTWSMS($country_code, $phone, $msg)
    {
        /** 只發送台灣 */
        if ($country_code != '886') {
            return false;
        }
        /** 沒0補0 */
        if (!preg_match('/^0/', $phone)) {
            $phone = "0" . $phone;
        }
        /** 只發送手機 */
        if (!preg_match('/^09\d{8}$/', $phone)) {
            return false;
        }

        $data = array(
            "username" => SMS::TWSMS_USERNAME,
            "password" => SMS::TWSMS_PASSWORD,
            "mobile" => $phone,
            "message" => urlencode($msg));

        $post = http_build_query($data, null, '&', PHP_QUERY_RFC3986);
        $url = "http://api.twsms.com/smsSend.php?" . $post;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        /*
        00000 完成
        00001 狀態尚未回復
        00010 帳號或密碼錯誤
        00020 通數不足
        00030 IP 無使用權限
        00040 帳號已停用
        00050 sendtime 格式錯誤
        00060 expirytime 格式錯誤
        00070 popup 格式錯誤
        00080 mo 格式錯誤
        00090 longsms 格式錯誤
        00100 手機號碼格式錯誤
        00110 沒有簡訊內容
        00120 長簡訊不支援國際門號
        00130 簡訊內容超過長度
        00140 drurl 格式錯誤
        00150 sendtime 預約的時間已經超過
        00300 找不到 msgid
        00310 預約尚未送出
        00400 找不到 snumber 辨識碼
        00410 沒有任何 mo 資料
        99998 資料處理異常，請重新發送
        99999 系統錯誤，請通知系統廠商
         */
        $result = array(
            'error' => ApiError::SUCCESS,
            'data' => $array);

        if ($array['code'] != '00000') {
            $result['error'] = ApiError::SMS_ERROR;
        }

        return $result;
    }

    /**
     *寄送註冊簡訊
     *int $account_id 使用者ID
     *string $country_code 電話國碼
     *string $phone 電話
     *string $password 密碼
     */
    public static function send($country_code, $phone, $msg)
    {

        $result = array('error' => ApiError::SUCCESS);
                
        /** Debug模式不發簡訊 */
        // if (Config::get('app.debug')) {
        //     return $result;
        // }

        if(env('APP_DEBUG')){
            return $result;
        }

        if ($country_code == "886") {
            $result = SMS::sendByTWSMS($country_code, $phone, $msg);
        } else {
            $result = array('error' => ApiError::SMS_TIME_IS_TOO_CLOSE);
        }

        //TODO 紀錄發送簡訊錯誤
        return $result;
    }


}