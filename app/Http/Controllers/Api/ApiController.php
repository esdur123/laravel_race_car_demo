<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Defined\ApiError;
use App\Http\Controllers\Controller;
use File;
use App\Models\User;  
use App\Defined\PointRules;


class ApiController extends Controller
{


    // ＊＊ 檢查是否為10的倍數 ＊＊　
    public function check10Multiple($str)
    {
        $return = array('error' => ApiError::SUCCESS);
        if( !ctype_digit( (string)$str ) )
        {
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        if(((int)$str) <=0){
            $return['error'] = ApiError::INT_NOT_10_MULTIPLE;
        }

        $str_10 = ((int)$str) % 10;

        if($str_10 != 0){
            $return['error'] = ApiError::INT_NOT_10_MULTIPLE;
        }
        
        return  $return;
    }


    // ＊＊ 檢查是否為100的倍數 ＊＊　
    public function check100Multiple($str)
    {
        $return = array('error' => ApiError::SUCCESS);
        if( !ctype_digit( (string)$str ) )
        {
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        if(((int)$str) <=0){
            $return['error'] = ApiError::INT_NOT_100_MULTIPLE;
        }

        $str_10 = ((int)$str) % 100;

        if($str_10 != 0){
            $return['error'] = ApiError::INT_NOT_100_MULTIPLE;
        }
        
        return  $return;
    }


    // ＊＊ 檢查參數 ＊＊　
    public function checkRequest(Request $request, $key_list)
    {
        if (!is_array($key_list)) {
            $key_list = array($key_list);
        }

        $return = array('error' => ApiError::SUCCESS);

        for ($i = 0; $i < count($key_list); $i++) {
            $key = $key_list[$i];
            $value = $request->input($key);
            if (!isset($value) || $value == '') {
                $return['error'] = ApiError::UNDEFINED_ARGUMENT;
                $return['msg'] = $key . ' is undefined!!';
                break;
            }
        }
        return $return;
    }
    

    // ＊＊ 檢查正整數 ＊＊　
    public function checkPositiveInteger($str)   
    {
        $return = array('error' => ApiError::SUCCESS);
        if( !ctype_digit( (string)$str ) )
        {
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        return  $return;
    }


    // ＊＊ 檢查正整數 ＊＊　
    public function checkPositiveInteger2($str)   
    {
        $return = array('error' => ApiError::SUCCESS);
        if( !ctype_digit( (string)$str ) )
        {
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        if((int)$str ==0){
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        return  $return;
    }


    // ＊＊ 檢查整數　或　浮點數 ＊＊　
    public function checkIntegerOrFloat($str)   
    {
        $return = array('error' => ApiError::SUCCESS);
        if( !is_numeric((string)$str) )
        {
            $return['error'] = ApiError::ILLEGAL_VALUES;           
        }

        return  $return;
    }

 



}
