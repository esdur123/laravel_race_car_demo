<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\VerifyTags;
use App\Defined\FrozenTags;


use App\Models\OrderWithdraw;
use App\Models\User;


use App\Http\Controllers\Api\ApiController;
use App\Services\WithdrawService;
use App\Services\TagService;
use App\Services\FrozenService;
use App\Services\PhoneVerifyService;


use App\Repositories\OrderWithdrawRepository;



use App\Tools\DataTable;

        

class WithdrawController extends ApiController
{


    // ＊＊ 申請提領 ＊＊　
    public function check_available(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);
        $result['data']['is_tagged'] = TagService::verify(0, VerifyTags::ALL_USERS_LIMITED_WITHDRAW);
        return Response::json($result);
    }


    // ＊＊ 取消提領 ＊＊　
    public function user_cancel(Request $request)
    {

        $check_key = array('withdraw_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = array('error' => ApiError::SUCCESS);      

        // 檢查是不是100的倍數
        $result = static::checkPositiveInteger($request->withdraw_id);
        if ($result['error'] != ApiError::SUCCESS) {            
            return $result;
        }  

        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = WithdrawService::user_cancel($user_account_id, $request->withdraw_id);

        return Response::json($result);
    }


    // ＊＊ 申請提領 ＊＊　
    public function apply(Request $request)
    {

		$check_key = array('amount');
	    $check_request = $this->checkRequest($request, $check_key);
	    if ($check_request['error'] != ApiError::SUCCESS) {
	        return $check_request;
	    }

        $result = array('error' => ApiError::SUCCESS);    	

        // 檢查管理員有沒有關掉
        $is_limited = TagService::verify(0, VerifyTags::ALL_USERS_LIMITED_WITHDRAW);
        if($is_limited == 1){
            $result['error'] = ApiError::WITHDRAW_LIMITED_BY_ADMIN;
            return $result;
        }

        // 檢查user有沒有資格申請
        $user_account_id = Session::get(SessionNames::USER_ID);

        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 檢查銀行綁定狀態
        $is_bound_bank = TagService::verify($user_account_id,VerifyTags::USER_BIND_BANK);
        if($is_bound_bank != 1){
            $result['error'] = ApiError::USER_BANK_UNBOUND;
            return $result;
        }

        // 檢查電話綁定狀態
        $is_bound_phone = PhoneVerifyService::verify($user_db->country_code, $user_db->phone);
        if($is_bound_phone != 1){
            $result['error'] = ApiError::USER_MOBILE_UNBOUND;
            return $result;
        }


        // 是否被凍結
        $result = FrozenService::test_user_id($user_account_id);
        if($result['data']['is_frozen_01']==1){
            if($result['data']['tag'] == FrozenTags::USER_ID){
                $result['error'] = ApiError::USER_FROZEN_BY_ID;
            }

            if($result['data']['tag'] == FrozenTags::BANK_ACCOUNT){
                $result['error'] = ApiError::USER_FROZEN_BY_BANK_ACCOUNT;
            }

            if($result['data']['tag'] == FrozenTags::PHONE){
                $result['error'] = ApiError::USER_FROZEN_BY_PHONE;
            }
                       
            return $result;     
        }

        // 檢查是不是100的倍數
        $result = static::check100Multiple($request->amount);
	    if ($result['error'] != ApiError::SUCCESS) {	    	
	        return $result;
	    }  

        // 申請
        $result = WithdrawService::apply($user_account_id, $request->amount);
        if ($result['error'] != ApiError::SUCCESS) {            
            return $result;
        }  
        
        // $OrderWithdraw_db = OrderWithdrawRepository::create($user_account_id, $request->amount);
        // $result['data'] = $request->amount;
        return Response::json($result);
    }


    // ＊＊ 取得withdraw info資料 ＊＊
    public function get_info(Request $request)
    {
        $user_account_id = Session::get(SessionNames::USER_ID); 

        $columns = array(
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'amount', 'dt' => 'amount'),
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'updated_at', 'dt' => 'updated_at'),            
            array('db' => 'commit', 'dt' => 'commit'),
        );
        
        $eloquent = new OrderWithdraw();
        $eloquent = $eloquent
                    ->where('user_id',$user_account_id)
                    ->orderBy('created_at','desc'); 
                    
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }






}


