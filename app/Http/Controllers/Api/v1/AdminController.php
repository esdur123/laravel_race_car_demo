<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; // 回應?
use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\VerifyTags;
use App\Defined\FrozenTags;
use App\Defined\WithdrawStates;

use App\Models\Ball;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Admin;
use App\Models\Transaction;
use App\Models\Trading;
use App\Models\Order;
use App\Models\System;
use App\Models\Frozen;
use App\Models\OrderWithdraw;
use App\Models\Ask;

use Carbon\Carbon;
use Datetime;

use App\Http\Controllers\Api\ApiController;
use Firebase\JWT\JWT;
use App\Defined\SessionNames;
use App\Services\AdminService;
use App\Services\TagService;
use App\Services\UserService;
use App\Services\TransactionService;
use App\Services\BonusService;
use App\Services\FrozenService;

use App\Repositories\AskRepository;
use App\Repositories\PhoneVerifyRepository;



use App\Tools\DataTable;

class AdminController extends ApiController
{

    // ＊＊ 強制通過電話驗證 ＊＊
    public function force_verify_phone(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('country_code','phone','user_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_country_code = $this->checkPositiveInteger($request->country_code);
        if ($check_country_code['error'] != ApiError::SUCCESS) {
            return $check_country_code;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        $result = AdminService::force_insert($request->user_id, $request->country_code, $request->phone);

        return Response::json($result);
    }

    // ＊＊ 答覆問題 ＊＊
    public function reply_question(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('ask_id','reply');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check = $this->checkPositiveInteger2($request->ask_id);
        if ($check['error'] != ApiError::SUCCESS) {
            return $check;
        }

        $result = AskRepository::reply($request->ask_id, $request->reply);
        //$result['data'] = '666';

        return Response::json($result);
    }



    // ＊＊ 取得 提問明細 ＊＊
    public function get_ask_info(Request $request, $type)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'updated_at', 'dt' => 'updated_at'),
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'user_id', 'dt' => 'user_id'),
            array('db' => 'title', 'dt' => 'title'),
            array('db' => 'comment', 'dt' => 'comment'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'reply', 'dt' => 'reply'),
        );
        

        $eloquent = new Ask();

        if($type=='all'){
            $eloquent = $eloquent  
                        ->orderBy('created_at','desc');
        }

        if($type=='reply'){
            $eloquent = $eloquent 
                        ->where('state', 'reply')      
                        ->orderBy('created_at','desc');  
        }

        if($type=='waiting'){
            $eloquent = $eloquent
                //->where('state', WithdrawStates::Accepting)
                ->where('state', 'waiting') 
                ->orderBy('created_at','desc');    
        }

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 新增分紅倍率 ＊＊
    public function add_dividend_rates(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('year','month','day','rate');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查rate是不是數字 (小數.正負皆可)
        $check = $this->checkIntegerOrFloat($request->rate);
        if ($check['error'] != ApiError::SUCCESS) {
            return $check;
        }

        // 如果日期小於名天　不給過
        // $clicked_datetime = new DateTime(); 
        // $clicked_datetime->setDate((int)$request->year, (int)$request->month, (int)$request->day); 
        // $clicked_datetime->setTime(0, 0, 0); 

        // $expire_time = Carbon::now()->addDays(1);
        // $expire_time->setTime(0, 0, 0);  // 死線就是.. 現在+1天往前退到半夜12點..
        // if( $expire_time->lt($clicked_datetime) ){
        //     $result['error'] = ApiError::DATETIME_EXPIRED;
        //     return $result;
        // }



        $result = AdminService::add_dividend_rates(
                                                  (int)$request->year
                                                  , (int)$request->month
                                                  , (int)$request->day
                                                  , (float)$request->rate
                                                  );

        // 讀取所有資料
        $result = AdminService::get_dividend_rates();
        //$result['data']['aaaa'] = $expire_time;

        return Response::json($result);
    }

    // ＊＊ 取得分紅倍率 ＊＊
    public function get_dividend_rates(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        // $check_key = array('withdraw_id');
        // $check_request = $this->checkRequest($request, $check_key);
        // if ($check_request['error'] != ApiError::SUCCESS) {
        //     return $check_request;
        // }

        $result = AdminService::get_dividend_rates();
        //$result['data'] = '666';

        return Response::json($result);
    }


    // ＊＊ 會員提領狀態 已完成 ＊＊
    public function deal_withdraw(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('withdraw_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = AdminService::deal_withdraw($request->withdraw_id);
        //$result['data'] = '666';

        return Response::json($result);
    }


    // ＊＊ 會員狀態 處理中 ＊＊
    public function accepting_withdraw(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('withdraw_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = AdminService::accepting_withdraw($request->withdraw_id);
        //$result['data'] = '666';

        return Response::json($result);
    }


    // ＊＊ 取得會員儲值開關 ＊＊
    public function checkIsAllLimitWithdraw(Request $request)
    {
         
        $result = array('error' => ApiError::SUCCESS);
        $result['data']['is_tagged'] = TagService::verify(0, VerifyTags::ALL_USERS_LIMITED_WITHDRAW);


        return Response::json($result);
    }
    

    // ＊＊ 限制儲值(開關) ＊＊
    public function switchLimitedWithdraw(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('is_limited_withdraw');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 is_limited_pay 格式 (只能是0或1)
        if( !($request->is_limited_withdraw == 0 || $request->is_limited_withdraw == 1) ){
            $result['error'] = ApiError::ADMIN_SWITCH_LIMIT_PAY_ERROR;
            return $result;
        }

        $result = AdminService::switchLimitedWithdraw($request->is_limited_withdraw);
        return Response::json($result);
    }


    // ＊＊ 管理員撥彩點 ＊＊
    public function send_color_points(Request $request)
    {
        $check_key = array('user_id','color_point');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = array('error' => ApiError::SUCCESS);

        $check = $this->checkPositiveInteger($request->color_point);
        if ($check['error'] != ApiError::SUCCESS) {
            return $check;
        }

        $result = AdminService::depositPoints($request->user_id, $request->color_point,'ColorPoint');
        //$result['data'] = $request->user_id.','.$request->color_point;

        return Response::json($result);
    }


    // ＊＊ 管理員撥點 ＊＊
    public function send_points(Request $request)
    {
        $check_key = array('user_id','point');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = array('error' => ApiError::SUCCESS);

        $check = $this->checkPositiveInteger($request->point);
        if ($check['error'] != ApiError::SUCCESS) {
            return $check;
        }


        //$result['data'] = $request->user_id.','.$request->point;

        $result = AdminService::depositPoints($request->user_id, $request->point,'Point');
        return Response::json($result);
    }



    // ＊＊ 取得凍結明細 ＊＊
    public function getFrozenInfo(Request $request, $user_id)
    {

        $user_db = USer::find($user_id);
        $full_phone = '('.$user_db->country_code.')'.$user_db->phone;

        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'deleted_at', 'dt' => 'deleted_at'),            
            array('db' => 'tag', 'dt' => 'tag'), 
            array('db' => 'info', 'dt' => 'info'), 
        );
        
        // $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Frozen();
    
        //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     // 可包含deleted_at
                    ->whereRaw(" 
                           (tag = '".FrozenTags::PHONE."' AND ( info = '".$full_phone."' ))                        
                        OR (tag = '".FrozenTags::BANK_ACCOUNT."' AND ( info = '".$user_db->bank_account."' )) 
                        OR (tag = '".FrozenTags::USER_ID."' AND ( info = '".$user_id."' )) 
                    ")
                     //->orWhere(['tag'=>FrozenTags::PHONE,'info'=>'123456'])
                     //->orWhere(['tag'=>FrozenTags::BANK_ACCOUNT,'info'=>$user_db->bank_account])
                     ->orderBy('deleted_at','asc');  //->orderBy('id','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 註冊 (無上線註冊) ＊＊
    public function registerNoUpline(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('account','nickname','country_code','phone');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_country_code = $this->checkPositiveInteger($request->country_code);
        if ($check_country_code['error'] != ApiError::SUCCESS) {
            return $check_country_code;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        $result = UserService::registerNoUpline(
                                          $request->account
                                          ,$request->nickname
                                          ,$request->country_code
                                          ,$request->phone
                                          ,'123456'
                                       ); 

        return Response::json($result);
    }




    // ＊＊ 新版註冊 ＊＊
    public function register_new(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('account', 'nickname','country_code','phone','level');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // recommender_account 如果不是空白 再檢查
        $recommender_id = 0;
        if($request->recommender_account != ''){
            $check_key = array('recommender_account');
            $check_request = $this->checkRequest($request, $check_key);
            if ($check_request['error'] != ApiError::SUCCESS) {
                return $check_request;
            }

            // 檢查recommender 是否合法
            $recommender_db = User::Where('account',$request->recommender_account)->first();
            if(!$recommender_db){
                $result['error'] = ApiError::REGISTER_RECOMMENDER_NULL;
                return $result;
            }

            // recommender是否有開通
            $recommender_ball_db = Ball::where('user_id', $recommender_db->id)->where('level','>',0)->first();
            if(!$recommender_ball_db){
                $result['error'] = ApiError::REGISTER_RECOMMENDER_NOT_ACTIVE;
                return $result;   
            }

            $recommender_id = $recommender_db->id;
        }

        // 檢查 country_code格式
        $check_id = $this->checkPositiveInteger($request->country_code);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        // 檢查 phone 格式 , 
        $check_id = $this->checkPositiveInteger($request->phone);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

         // 檢查 phone 格式 (10+ digi)
        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        if( !($request->level == 0 || $request->level == 1 || $request->level == 2) ){
            $return['error'] = ApiError::ILLEGAL_VALUES;
            return $return;
        }


        // 上線存在時
        if($recommender_id != 0){

            // 無配套
            if($request->level == 0){
                $result = UserService::registerFromRecommender(
                    $recommender_id
                    ,$request->account
                    ,$request->nickname
                    ,$request->country_code
                    ,$request->phone
                    ,'123456'
                );   
                      
            } 

            // 有配套
            if($request->level != 0){     

                $result = UserService::register(
                                                  $recommender_id
                                                  ,$request->account
                                                  ,$request->nickname
                                                  ,$request->country_code
                                                  ,$request->phone
                                                  ,$request->level
                                               );      
            
                if ($result['error'] != ApiError::SUCCESS) {
                    return $result;
                }

                $new_ball_id = $result['new_ball_id'];

                // 新註冊那個人直接激活目前點數
                $new_user_id = $result['new_user_id'];// accountID

                $result = TransactionService::activeSet($new_user_id, $new_ball_id, $request->level);
                if ($result['error'] != ApiError::SUCCESS) {
                    return $result;
                }

                // 計算直推獎金
                $result = BonusService::directPush($new_user_id, $new_ball_id, $request->level);
                if ($result['error'] != ApiError::SUCCESS) {
                    return $result;
                }            
            } 

        }

        // 上線不存在時
        if($recommender_id == 0){

            // 無配套
            if($request->level == 0){
                $result = UserService::registerNoUpline(
                                                  $request->account
                                                  ,$request->nickname
                                                  ,$request->country_code
                                                  ,$request->phone
                                                  ,'123456'
                                               ); 
            }

            // 有配套
            if($request->level != 0){
                $result['error'] = ApiError::REGISTER_NEW_NO_RECOMMENDER_INABLE_TO_WITH_LEVEL;
                return $result;
            }
        }

        return Response::json($result);
    }


    // ＊＊ 註冊 ＊＊
    public function registerMaxLeftMaxRight(Request $request)
    {

        $check_key = array('recommender_account','RL', 'account','nickname','country_code','phone','level');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }


        // 檢查recommender 是否合法
        $recommender_db = User::Where('account',$request->recommender_account)->first();
        if(!$recommender_db){
            $result['error'] = ApiError::REGISTER_RECOMMENDER_NULL;
            return $result;
        }

        // recommender是否有開通
        $recommender_ball_db = Ball::where('user_id', $recommender_db->id)->where('level','>',0)->first();
        if(!$recommender_ball_db){
            $result['error'] = ApiError::REGISTER_RECOMMENDER_NOT_ACTIVE;
            return $result;   
        }

        // 檢查 country_code格式
        $check_id = $this->checkPositiveInteger($request->country_code);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        // 檢查 phone 格式
        $check_id = $this->checkPositiveInteger($request->phone);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 RL 格式
        if( !($request->RL == 'R' || $request->RL == 'L') ){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        if( !($request->level == 1 || $request->level == 2 || $request->level == 3) ){
            $return['error'] = ApiError::ILLEGAL_VALUES;
            return $return;
        }

        $result = UserService::register(
                                          $recommender_db->id
                                          ,$request->RL
                                          ,$request->account
                                          ,$request->nickname
                                          ,$request->country_code
                                          ,$request->phone
                                          ,$request->level
                                       );     

        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        $new_ball_id = $result['new_ball_id'];

        // 新註冊那個人直接激活目前點數
        $new_user_id = $result['new_user_id'];// accountID

        $result = TransactionService::activeSet($new_user_id, $new_ball_id, $request->level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // 計算直推獎金
        $result = BonusService::directPush($result, $new_user_id, $request->level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // 計算上面相關pv
        $result = BonusService::calculatePV($result, $new_user_id, $request->level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        return Response::json($result);
    }


    // ＊＊ 註冊 (推薦人連結註冊) ＊＊
    public function registerFromRecommender(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('recommender_account','account','nickname','country_code','phone','pw','pw2');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查recommender 是否合法
        $recommender_db = User::Where('account',$request->recommender_account)->first();
        if(!$recommender_db){
            $result['error'] = ApiError::REGISTER_RECOMMENDER_NULL;
            return $result;
        }

        // recommender是否有開通
        $recommender_ball_db = Ball::where('user_id', $recommender_db->id)->where('level','>',0)->first();
        if(!$recommender_ball_db){
            $result['error'] = ApiError::REGISTER_RECOMMENDER_NOT_ACTIVE;
            return $result;   
        }

        // 檢查兩次密碼
        if ($request->pw != $request->pw2) {
            $result['error'] = ApiError::NEWPASSWORD2_NOT_MATCH;
            return $result;
        }

        // 檢查 country_code格式
        $check_country_code = $this->checkPositiveInteger($request->country_code);
        if ($check_country_code['error'] != ApiError::SUCCESS) {
            return $check_country_code;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        $result = UserService::registerFromRecommender(
                                          $recommender_db->id
                                          ,$request->account
                                          ,$request->nickname
                                          ,$request->country_code
                                          ,$request->phone
                                          ,$request->pw
                                       ); 

        return Response::json($result);
    }



    // ＊＊ 修改密碼 ＊＊
    public function updateAdminPassword(Request $request)
    {
        $check_key = array('old_pw','new_pw','new_pw2');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        if($request->new_pw != $request->new_pw2){
            $result['error'] = ApiError::ADMIN_NEWPASSWORD2_NOT_MATCH;
            return $result;
        }

        $result = AdminService::updateAdminPassword($request->old_pw, $request->new_pw, $request->new_pw2);
        return Response::json($result);
    }



    // ＊＊ 取消委託 ＊＊
    public function cancelOrder(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('order_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 $id 格式
        $check_id = $this->checkPositiveInteger($request->order_id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $result = AdminService::cancelOrder($request->order_id);
        return Response::json($result);
    }


    // ＊＊ 更改每簡訊驗證上限 ＊＊
    public function updateMaxVerifyMessage(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('max_verify_times');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 max_verify_times 格式
        $check_id = $this->checkPositiveInteger($request->max_verify_times);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $result = AdminService::updateMaxVerifyMessage($request->max_verify_times);
        return Response::json($result);
    }


    // ＊＊ 取得每簡訊驗證上限 ＊＊
    public function getMaxVerifyMessage(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $result = AdminService::getMaxVerifyMessage();
        return Response::json($result);
    }


    // ＊＊ 取得委託明細 ＊＊
    public function getOrders(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'user_id', 'dt' => 'user_id'),
            array('db' => 'total', 'dt' => 'total'),
            array('db' => 'deal', 'dt' => 'deal'),            
            array('db' => 'trading', 'dt' => 'trading'),            
        );
        
        //$user_account_id = Session::get(SessionNames::USER_ID); 

        $eloquent = new Order();
        $eloquent = $eloquent
                    // ->where('buyer_user_id', $user_account_id)
                    ->where('state', 'open') 
                    // ->orWhere('state', 'deal') 
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'deal')")
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' )")
                    ->orderBy('created_at','desc');

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取消交易單 ＊＊
    public function cancelTrading(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('trading_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 $id 格式
        $check_id = $this->checkPositiveInteger($request->trading_id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $result = AdminService::cancelTrading($request->trading_id);
        return Response::json($result);
    }




    // ＊＊ 取得提領明細 ＊＊
    public function getWithdrawInfo(Request $request, $type)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'updated_at', 'dt' => 'updated_at'),                
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'user_id', 'dt' => 'user_id'),
            array('db' => 'amount', 'dt' => 'amount'),
            array('db' => 'commit', 'dt' => 'commit'),         
        );
        
        //$user_account_id = Session::get(SessionNames::USER_ID); 

        $eloquent = new OrderWithdraw();
        if($type=='all'){
            $eloquent = $eloquent
                // ->where('buyer_user_id', $user_account_id)
                //->where('state', 'Waiting') 
                // ->orWhere('state', 'deal') 
                //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'deal')")
                //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' )")
                ->orderBy('created_at','desc');    
        }

        if($type==WithdrawStates::ACCEPTING){
            $eloquent = $eloquent
                //->where('state', WithdrawStates::Accepting)
                ->where('state', WithdrawStates::ACCEPTING) 
                ->orderBy('created_at','desc');    
        }

        if($type==WithdrawStates::DEAL){
            $eloquent = $eloquent
                //->where('state', WithdrawStates::Accepting)
                ->where('state', WithdrawStates::DEAL) 
                ->orderBy('created_at','desc');    
        }
        
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得錢包明細 ＊＊
    public function getTradingInfo(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),        
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'amount', 'dt' => 'amount'),
            array('db' => 'seller_info', 'dt' => 'seller_info'),
            array('db' => 'buyer_info', 'dt' => 'buyer_info'),            
            array('db' => 'confirm_value', 'dt' => 'confirm_value'),            
        );
        
        //$user_account_id = Session::get(SessionNames::USER_ID); 

        $eloquent = new Trading();
        $eloquent = $eloquent
                    // ->where('buyer_user_id', $user_account_id)
                    ->where('state', 'waiting') 
                    // ->orWhere('state', 'deal') 
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'deal')")
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' )")
                    ->orderBy('created_at','desc');

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }

    // ＊＊ 取得 recommend 組織圖資料 ＊＊
    public function GetRecommenderInfoByID(Request $request, $id)
    {
        // 檢查 $id 格式
        $check_id = $this->checkPositiveInteger($id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }


        $result = AdminService::GetRecommenderInfoByID($id);
        return Response::json($result);
    }    
    // public function GetRecommenderInfo(Request $request, $id)
    // {

    //     // 檢查 $id 格式
    //     $check_id = $this->checkPositiveInteger($id);
    //     if ($check_id['error'] != ApiError::SUCCESS) {
    //         return $check_id;
    //     }

    //     $result = UserService::getIDFromAccount($account);
    //     if($result['error'] != ApiError::SUCCESS){
    //         return $result;
    //     }

    //     $user_id = $result['id'];
    //     $ball_db = Ball::select("id")->where('user_id', $user_id)->first();
    //     $result = OrganizationService::GetRecommenderInfoByID($ball_db->id);
    //     return Response::json($result);
    // }    



    // // ＊＊ 取得特定branch 組織圖資料 ＊＊
    // public function getBranchInfoByID(Request $request, $id)
    // {
    //     $result = array('error' => ApiError::SUCCESS);
    //     // 檢查 $id 格式
    //     $check_id = $this->checkPositiveInteger($id);
    //     if ($check_id['error'] != ApiError::SUCCESS) {
    //         return $check_id;
    //     }

    //     $result = AdminService::getBranchInfoByID($id);
    //     //$result['data'] = '666';//AdminService::getBranchInfoByID($id);
    //     return Response::json($result);
    // }


    // ＊＊ 取得錢包明細 ＊＊
    public function getWalletInfo(Request $request, $id)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),            
            array('db' => 'type', 'dt' => 'type'), 
            array('db' => 'value', 'dt' => 'value'), 
            array('db' => 'after', 'dt' => 'after'), 
            array('db' => 'link', 'dt' => 'link'), 
        );
        
        // $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Transaction();
        // //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     ->where('user_id', $id)
                     ->where('coin', 'Point')
                     ->orderBy('id','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得錢包明細 ＊＊
    public function getColorpointInfo(Request $request, $id)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),            
            array('db' => 'type', 'dt' => 'type'), 
            array('db' => 'value', 'dt' => 'value'), 
            array('db' => 'after', 'dt' => 'after'), 
            array('db' => 'link', 'dt' => 'link'), 
        );
        
        // $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Transaction();
        // //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     ->where('user_id', $id)
                     ->where('coin', 'ColorPoint')
                     ->orderBy('id','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 修改會員資料 ＊＊　
    public function updateUserInfo(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('user_id','nickname','country_code','phone','new_pw','bank_code','bank_account');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_country_code = $this->checkPositiveInteger($request->country_code);
        if ($check_country_code['error'] != ApiError::SUCCESS) {
            return $check_country_code;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }
        
       $result = AdminService::updateUserInfo(
                                              $request->user_id
                                              , $request->nickname
                                              , $request->country_code
                                              , $request->phone
                                              , $request->new_pw
                                              , $request->bank_code
                                              , $request->bank_branch
                                              , $request->bank_account                                              
                                             );
        return Response::json($result);
    }


    // ＊＊ 取得會員資料 ＊＊　
    public function getUserAccountInfo(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('user_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = AdminService::getUserAccountInfo($request->user_id);
        return Response::json($result);
    }


    // ＊＊ 登入 ＊＊　
    public function login(Request $request)
    {
        $check_key = array('account', 'password');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = AdminService::login($request->account, $request->password);
        return Response::json($result);
    }


    // ＊＊ 登出 ＊＊
    public function logout(Request $request)
    {
        $result = AdminService::logout();
        return Response::json($result);
    }

    // ＊＊ 回收點數 ＊＊　
    // public function recyclePoint(Request $request)
    // {
    //     $check_key = array('wallet_id', 'point', 'user_id');
    //     $check_request = $this->checkRequest($request, $check_key);
    //     if ($check_request['error'] != ApiError::SUCCESS) {
    //         return $check_request;
    //     }

    //     // 確認正整數
    //     $check_point = $this->checkPositiveInteger($request->point);
    //     if ($check_point['error'] != ApiError::SUCCESS) {
    //         return $check_point;
    //     }

    //     $result = AdminService::recyclePoint($request->wallet_id, $request->user_id, $request->point);
    //     return Response::json($result);
    // }

    // ＊＊ 取得 會員列表 ＊＊
    public function getUserAccounts(Request $request)
    {
        $columns = array(
            array('db' => 'country_code', 'dt' => 'country_code'),
            array('db' => 'account', 'dt' => 'account'),            
            array('db' => 'nickname', 'dt' => 'nickname'),
            array('db' => 'phone', 'dt' => 'phone'),            
            array('db' => 'id', 'dt' => 'id'),             
        );
        
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new User();
        $eloquent = $eloquent
                     // ->where('buyer_user_id', $user_account_id)
                     // ->where('state', 'waiting') 
                     // ->orWhere('state', 'deal') 
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'deal')")
                    ->orderBy('created_at','asc');        
        //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        // $eloquent = $eloquent
        //             ->all();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得會員Ball資訊 ＊＊
    public function getUserBallInfo(Request $request, $id)
    {

        $columns = array(
            array('db' => 'id', 'dt' => 'id'),
            // array('db' => 'pv_left', 'dt' => 'pv_left'),
            // array('db' => 'pv_right', 'dt' => 'pv_right'),
            // array('db' => 'acc_pv_today', 'dt' => 'acc_pv_today'),
            // array('db' => 'acc_pv_max', 'dt' => 'acc_pv_max'),      
            array('db' => 'ball_index', 'dt' => 'ball_index'),      
            array('db' => 'static_current', 'dt' => 'static_current'),
            array('db' => 'static_max', 'dt' => 'static_max'),
            array('db' => 'auto_active', 'dt' => 'auto_active'),
            array('db' => 'level', 'dt' => 'level'),
            array('db' => 'created_at', 'dt' => 'created_at'),            
        );
        
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Ball();
        $eloquent = $eloquent
                    ->where('user_id',$id);
                    
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得管理員帳號 ＊＊　
    public function getAdminAccount(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $admin_id = Session::get(SessionNames::ADMIN_ID); // accountID

        $admin_db = Admin::select('account')->where('id',$admin_id)->first();
        if(!$admin_id){
            $result['error'] = ApiError::ADNIN_NULL;
            return $result;
        }

        $result['data'] = $admin_db->account;
        // $check_key = array('wallet_id', 'point', 'user_id');
        // $check_request = $this->checkRequest($request, $check_key);
        // if ($check_request['error'] != ApiError::SUCCESS) {
        //     return $check_request;
        // }

        // // 確認正整數
        // $check_point = $this->checkPositiveInteger($request->point);
        // if ($check_point['error'] != ApiError::SUCCESS) {
        //     return $check_point;
        // }

        return Response::json($result);
    }


    // ＊＊ 檢查現在是否要用彩點 ＊＊　
    public function checkIsColorpoint(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $is_colorpoint = 0;
        $C_COLOR_POINT_PERCENTAGE = 0;// env('COLOR_POINT_PERCENTAGE');
        if($C_COLOR_POINT_PERCENTAGE > 0){
            $is_colorpoint = 1;
        }

        $result['data']['is_colorpoint'] = $is_colorpoint;

        return Response::json($result);
    }


    // ＊＊ 檢查憑證by ID ＊＊　
    public function verifyFrozen_by_id(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('user_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = FrozenService::test_user_id($request->user_id);
        return Response::json($result);
    }


    // ＊＊ 檢查憑證by Phone ＊＊　
    public function verifyFrozen_by_phone(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('user_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $user_db = User::find($request->user_id);
        if(!$user_db){
            $result['error'] = ApiError::ADMIN_FIND_USER_NULL;
            return $result;
        }

        $result = FrozenService::test_phone($user_db->country_code, $user_db->phone);
        return Response::json($result);
    }


    // ＊＊ 檢查憑證by Bank ＊＊　
    public function verifyFrozen_by_bank(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('user_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $user_db = User::find($request->user_id);
        if(!$user_db){
            $result['error'] = ApiError::ADMIN_FIND_USER_NULL;
            return $result;
        }

        $result = FrozenService::test_bank($user_db->bank_account);
        return Response::json($result);
    }


    // ＊＊ 限制交易(凍結) by ID ＊＊
    public function switchFrozenByID(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('user_id','is_frozen');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 is_frozen 格式 (只能是0或1)
        if( !($request->is_frozen == 0 || $request->is_frozen == 1) ){
            $result['error'] = ApiError::ADMIN_SWITCH_LIMIT_PAY_ERROR;
            return $result;
        }

        $user_db = User::find($request->user_id);
        if(!$user_db){
            $result['error'] = ApiError::ADMIN_FIND_USER_NULL;
            return $result;
        }

        $result = FrozenService::frozen(FrozenTags::USER_ID, $request->user_id, $request->is_frozen);
        //$result = AdminService::switchFrozen($request->user_id, $request->is_frozen);
        return Response::json($result);
    }


    // ＊＊ 限制交易(凍結) by Phone ＊＊
    public function switchFrozenByPhone(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('user_id','is_frozen');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 is_frozen 格式 (只能是0或1)
        if( !($request->is_frozen == 0 || $request->is_frozen == 1) ){
            $result['error'] = ApiError::ADMIN_SWITCH_LIMIT_PAY_ERROR;
            return $result;
        }

        $user_db = User::find($request->user_id);
        if(!$user_db){
            $result['error'] = ApiError::ADMIN_FIND_USER_NULL;
            return $result;
        }

        $result = FrozenService::frozen(FrozenTags::PHONE, '('.$user_db->country_code.')'.$user_db->phone, $request->is_frozen);
        //$result = AdminService::switchFrozen($request->user_id, $request->is_frozen);
        return Response::json($result);
    }


    // ＊＊ 限制交易(凍結) by Bank ＊＊
    public function switchFrozenByBank(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('user_id','is_frozen');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 is_frozen 格式 (只能是0或1)
        if( !($request->is_frozen == 0 || $request->is_frozen == 1) ){
            $result['error'] = ApiError::ADMIN_SWITCH_LIMIT_PAY_ERROR;
            return $result;
        }

        $user_db = User::find($request->user_id);
        if(!$user_db){
            $result['error'] = ApiError::ADMIN_FIND_USER_NULL;
            return $result;
        }

        // 銀行帳號為空的情況
        if(strlen($user_db->bank_account) < 1 ){
            $result['error'] = ApiError::BANK_ACCOUNT_WRONG_FORMAT;
            return $result;
        }

        $result = FrozenService::frozen(FrozenTags::BANK_ACCOUNT, $user_db->bank_account, $request->is_frozen);
        //$result = AdminService::switchFrozen($request->user_id, $request->is_frozen);
        return Response::json($result);
    }











} // end class







