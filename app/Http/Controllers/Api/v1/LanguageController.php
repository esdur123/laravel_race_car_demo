<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Http\Controllers\Api\ApiController;
use App\Defined\SessionNames;

// 改變語系
class LanguageController extends ApiController
{

    // ＊＊ 設定語系 ＊＊　
    public function set(Request $request, $lang)
    {
        
        $result = array('error' => ApiError::SUCCESS); // 預設api成功
        $result['data'] = 'lang = '.$lang;

        if(!($lang == 'en' || $lang == 'zh_TW' || $lang == 'zh_CN')){ // 語系不合法            
            $result = array('error' => ApiError::LANGUAGE_ILLEGAL);
            return $result;
        }

        // 根據有/無session 設定語系
        if(Session::exists(SessionNames::LANGUAGE)){
            Session::forget(SessionNames::LANGUAGE);
            Session::put(SessionNames::LANGUAGE, $lang);

        } else {
            Session::put(SessionNames::LANGUAGE, $lang);
        }

        return Response::json($result);
    }


}


