<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Api\ApiController;

use App\Defined\ApiError;
use App\Defined\CoinDefined;
use App\Defined\TransactionDefined;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\Pay;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;

use App\Repositories\TransactionRepository;
use App\Tools\TXPay;
use Lock;

class TXPayController extends ApiController
{


    // ＊＊　產生訂單　＊＊
    public function createTCOrder(Request $request, $pay_id)
    {
     
        $result = array('error' => ApiError::SUCCESS);
        $user_id = Session::get(SessionNames::USER_ID);
        $pay_db = Pay::where('id', $pay_id)->where('user_id', $user_id)->first();
        if (!isset($pay_db) ) {
            $result['error'] = ApiError::PAY_ORDER_ERROR;

        } elseif (isset($pay_db->obtained_at)) {
            $result['error'] = ApiError::PAY_ORDER_ERROR_1;
        } else {

            // echo '產生訂單中...';

            $pay_db->obtained_at = date('Y-m-d H:i:s');
            $pay_db->save();

            $return_domain = route('api.pay.tc.vbank.id.return', ['order_id' => $pay_db->id]);  // "http://13.250.56.38/financial/";
            //$return_domain = 'http://'. env('DOMAIN_NAME') .'/api/v1/user/pay/vbank/' . $pay_db->id . '/return';

            // $result['data']['url'] = 'http://'. env('DOMAIN_NAME') .'/user/pay/vbank/' . $pay_db->id;
            $user_db = User::find($user_id);
            if(!$user_db){
                $result['error'] = ApiError::PAY_ORDER_ERROR_2;
                return Response::json($result);
            }

            $result['data'] = TXPay::createOrder(
                TXPay::METHOD_VBANK,
                $return_domain,
                $pay_db->trade_id,
                $pay_db->total_price,
                '儲值 ' . $pay_db->amount . ' ' . PointRules::PRODUCT_01_NAME . ' ' . ($pay_db->amount * $pay_db->price) . '元，手續費 ' . $pay_db->fee . ' 元，總金額 ' . $pay_db->total_price . ' 元',
                $user_db->id); 
        }
       
        return Response::json($result);
    }



    // ＊＊ 接收已付款api　＊＊
    public function returnTCOrder(Request $request, $pay_id)
    {

/*
        //  手動測試用    
        $result = '0000';
        $PRODUCT_01_PRICE = (int) PointRules::PRODUCT_01_PRICE;
        $pay_db = Pay::where('id', $pay_id)->first();

        // Wallet加錢
        $earn_point = ($pay_db->amount * $pay_db->price) / $PRODUCT_01_PRICE;
        $wallet_db = Wallet::where('user_id',$pay_db->user_id)->first();
        $before = $wallet_db->balance_available;
        $wallet_db->balance_available = $wallet_db->balance_available + $earn_point;
        $wallet_db->save();

        $pay_db->msg = "手動測試";
        $pay_db->finished_at = date('Y-m-d H:i:s');
        $pay_db->save();
        $wallet_db = Wallet::where('user_id',$pay_db->user_id)->first();
        TransactionRepository::create(
                                      $pay_db->user_id
                                      , $wallet_db->id
                                      , 'Point'
                                      , TransactionTypes::TX_ATM_BUY
                                      , $earn_point
                                      , $before
                                      , $wallet_db->balance_available
                                      , ""
            
                                      ); 
        return $result;
*/

        
        
        $result = '0000';
        $find_pay_db = Pay::where('id', $pay_id)->first();
        $find_wallet_db = Wallet::where('user_id', $find_pay_db->user_id)->first();
        if(!$find_pay_db){
            $result = '9991';
            return response($result);
        }

        if(!$find_wallet_db){
            $result = '9992';
            return response($result);
        }

        // define
        $PRODUCT_01_PRICE = (int) PointRules::PRODUCT_01_PRICE; // 35

        // Lock  
        $lock_key = 'Wallet@user_id:' . $find_pay_db->user_id;
        try {
            Lock::acquire($lock_key);

            if (TXPay::verifyReturn($request)) {
                // SQL參數
                $pay_db = Pay::where('id', $pay_id)->where('trade_id', $request->StoreOrderId)->first();
                if (isset($pay_db)) {
                    // 轉換unicode
                    $request->PayInfo = urlencode($request->PayInfo);
                    $pay_db->msg = urldecode(json_encode($request->toArray()));
                    // 付款成功
                    if ($request->errorcode == '00' &&
                        isset($pay_db->obtained_at) &&
                        !isset($pay_db->finished_at)) {

                        $pay_db->finished_at = date('Y-m-d H:i:s');
                        $pay_db->save();

                        $wallet_db = Wallet::where('user_id',$pay_db->user_id)->first();
                        if (isset($wallet_db)) {

                        // Wallet加錢
                        $earn_point = $pay_db->amount;//($pay_db->amount * $pay_db->price) / $PRODUCT_01_PRICE;
                        $before = $wallet_db->balance_available;
                        $wallet_db->balance_available = $wallet_db->balance_available + $earn_point;
                        $wallet_db->save();

                        // 交易紀錄 (提出方)                    
                        TransactionRepository::create(
                                                      $pay_db->user_id
                                                      , $wallet_db->id
                                                      , 'Point'
                                                      , TransactionTypes::TX_ATM_BUY
                                                      , $earn_point //$pay_db->price  // 除35
                                                      , $before 
                                                      , $wallet_db->balance_available 
                                                      , ""
                                                     );                        
                        }

                    } else {
                        $pay_db->save();
                    }
                }
            } else {
                $result = '9999';
            } // end if (TXPay::verifyReturn($request))

        } finally {
            Lock::release($lock_key);
        }

        return response($result);
    }





}// end class

?>