<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;


use App\Models\User;

use App\Services\TransactionService;
use App\Services\UserService;
use App\Services\OrganizationService;
use App\Services\BonusService;


use App\Tools\DataTable;

class UserController extends ApiController
{



    // ＊＊ 註冊球 ＊＊
    public function register_ball(Request $request){

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('level','amount');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // level是否合法
        if( !($request->level == '1' || $request->level == '2') ){
            $result['error'] = ApiError::ILLEGAL_LEVEL;
            return $result;
        }

        // 數量是否合法
        $check_amount = $this->checkPositiveInteger2($request->amount);
        if ($check_amount['error'] != ApiError::SUCCESS) {
            return $check_amount;
        }

        $user_account_id = Session::get(SessionNames::USER_ID);
        // 檢查 user 是否合法
        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 檢查主錢包夠不夠錢給升
        $total_cost = OrganizationService::getSetFromLevel($request->level) * (int)$request->amount;

        $is_wallet_enough = OrganizationService::check_is_main_wallet_enough(
                                                                            $user_account_id
                                                                            , $total_cost
                                                                            );
        if($is_wallet_enough==0){
            $result = array('error' => ApiError::ACTIVE_SET_WALLET_NOT_ENOUGH); 
            return $result;      
        }

        // 創球
        //$aaa = array();
        for ($i=0; $i < (int)$request->amount; $i++) { 
            $result = OrganizationService::create_another_ball($user_account_id); 
            if ($result['error'] != ApiError::SUCCESS) {
                return $result;
            }        

            $new_ball_db_id = $result['data'];
            //$aaa[$i] = $new_ball_db_id;
            $result = TransactionService::activeSet($user_account_id, $new_ball_db_id, $request->level);
            if ($result['error'] != ApiError::SUCCESS) {
                return $result;
            }

            // 計算直推獎金
            $result = BonusService::directPush($user_account_id, $new_ball_db_id, $request->level);
            if ($result['error'] != ApiError::SUCCESS) {
                return $result;
            }

        }

        //$result['data'] = $aaa;

        return Response::json($result);
    }


    // ＊＊ 註冊 (推薦人連結註冊) ＊＊
    public function registerFromRecommender(Request $request, $recommender_account)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('recommender_account','account','nickname','country_code','phone','pw','pw2');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查recommender 是否合法
        $recommender_db = User::Where('account',$request->recommender_account)->first();
        if(!$recommender_db){
            $result['error'] = ApiError::REGISTER_RECOMMENDER_NULL;
            return $result;
        }

        // 檢查兩次密碼
        if ($request->pw != $request->pw2) {
            $result['error'] = ApiError::NEWPASSWORD2_NOT_MATCH;
            return $result;
        }

        // 檢查 country_code格式
        $check_country_code = $this->checkPositiveInteger($request->country_code);
        if ($check_country_code['error'] != ApiError::SUCCESS) {
            return $check_country_code;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        $result = UserService::registerFromRecommender(
                                          $recommender_db->id
                                          ,$request->account
                                          ,$request->nickname
                                          ,$request->country_code
                                          ,$request->phone
                                          ,$request->pw
                                       ); 

        return Response::json($result);
    }


    // ＊＊ 註冊 ＊＊
    public function register(Request $request)
    {

        $check_key = array('account','nickname','country_code','phone','level');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_id = $this->checkPositiveInteger($request->country_code);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        // 檢查 phone 格式
        $check_id = $this->checkPositiveInteger($request->phone);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        if(mb_strlen($request->phone) > 10){
            $result['error'] = ApiError::ILLEGAL_VALUES;
            return $result;
        }

        // 檢查 RL 格式
        // if( !($request->RL == 'R' || $request->RL == 'L') ){
        //     $result['error'] = ApiError::ILLEGAL_VALUES;
        //     return $result;
        // }

        // 檢查 account 格式 不接受底線
        if (strpos($request->account, '_') !== false) {
            $result['error'] = ApiError::REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES;
            return $result;
        }

        if( !($request->level == 1 || $request->level == 2) ){
            $return['error'] = ApiError::ILLEGAL_VALUES;
            return $return;
        }

        $recommender_id = Session::get(SessionNames::USER_ID);

        $result = UserService::register(
                                          $recommender_id
                                          ,$request->account
                                          ,$request->nickname
                                          ,$request->country_code
                                          ,$request->phone
                                          ,$request->level
                                       );     

        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        $new_ball_id = $result['new_ball_id'];
        // 新註冊那個人直接激活目前點數
        $new_user_id = $result['new_user_id'];// accountID

        $result = TransactionService::activeSet($new_user_id, $new_ball_id, $request->level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // 計算直推獎金
        $result = BonusService::directPush($new_user_id, $new_ball_id, $request->level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // // 計算上面相關pv
        // $result = BonusService::calculatePV($result, $new_user_id, $request->level);
        // if ($result['error'] != ApiError::SUCCESS) {
        //     return $result;
        // }

        return Response::json($result);
    }


    // ＊＊ 登入 ＊＊　
    public function login(Request $request)
    {
        $check_key = array('account', 'password');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $result = UserService::login($request->account, $request->password);
        return Response::json($result);
    }


    // ＊＊ 登出 ＊＊
    public function logout(Request $request)
    {
        $result = UserService::logout();
        return Response::json($result);
    }
    

    // ＊＊ 取得會員帳號 ＊＊
    public function getAccount(Request $request)
    {
        $result = UserService::getAccount();
        return Response::json($result);
    }


    // ＊＊ 由輸入ID取得會員帳號 ＊＊
    public function getAccountFromID(Request $request, $id)
    {
        
        // 檢查 id格式
        $check_id = $this->checkPositiveInteger($id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $result = UserService::getAccountFromID($request->id);
        return Response::json($result);
    }

    // ＊＊ 取得會員資訊 ＊＊
    public function getInfo(Request $request)
    {
        $result = UserService::getInfo();
        return Response::json($result);
    }


    // ＊＊ 取得銀行資訊 ＊＊
    public function getBankInfo(Request $request)
    {
        $result = UserService::getBankInfo();
        return Response::json($result);
    }


    // ＊＊ 修改會員資訊 ＊＊
    public function updateInfo(Request $request)
    {
        $check_key = array('nickname','country_code','phone');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_id = $this->checkPositiveInteger($request->country_code);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        $result = UserService::updateInfo($request->nickname, $request->country_code, $request->phone);
        return Response::json($result);
    }


    // ＊＊ 修改銀行資訊 ＊＊
    public function updateBankInfo(Request $request)
    {
        $check_key = array('bank_code', 'bank_account');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_code = $this->checkPositiveInteger($request->bank_code);
        if ($check_code['error'] != ApiError::SUCCESS) {
            return $check_code;
        }

        // 檢查 bank_account 格式
        $check_code = $this->checkPositiveInteger($request->bank_account);
        if ($check_code['error'] != ApiError::SUCCESS) {
            return $check_code;
        }

        // bank_branch 可空白
        $bank_branch = $request->bank_branch;
        if(is_null($bank_branch)){
            $bank_branch = '';
        }

        $result = UserService::updateBankInfo($request->bank_code, $bank_branch, $request->bank_account);
        return Response::json($result);
    }


    // ＊＊ 修改密碼 ＊＊
    public function updatePassword(Request $request)
    {
        $check_key = array('old_pw','new_pw','new_pw2');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        if($request->new_pw != $request->new_pw2){
            $result['error'] = ApiError::NEWPASSWORD2_NOT_MATCH;
            return $result;
        }

        $result = UserService::updatePassword($request->old_pw, $request->new_pw, $request->new_pw2);
        return Response::json($result);
    }





}


