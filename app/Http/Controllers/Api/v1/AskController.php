<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;

use App\Defined\ApiError;
use App\Defined\SessionNames;
// use App\Defined\PointRules;


use App\Models\Ask;

use App\Services\AskService;
// use App\Services\UserService;
// use App\Services\OrganizationService;
// use App\Services\BonusService;


use App\Tools\DataTable;

class AskController extends ApiController
{

        // ＊＊ 提問 ＊＊　
    public function apply(Request $request)
    {
        
        $result = array('error' => ApiError::SUCCESS); // 預設api成功

        $check_key = array('title', 'comment');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = AskService::apply($user_account_id, $request->title, $request->comment);
        //$result['data'] = $request->title.','.$request->comment;
        return Response::json($result);
    }


    // ＊＊ 取得 我的提問明細 ＊＊
    public function get_info(Request $request, $type)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'title', 'dt' => 'title'),
            array('db' => 'comment', 'dt' => 'comment'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'reply', 'dt' => 'reply'),
        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 
        $eloquent = new Ask();

        if($type=='all'){
            $eloquent = $eloquent
                        ->where('user_id', $user_account_id)      
                        ->orderBy('created_at','desc');
        }

        if($type=='reply'){
            $eloquent = $eloquent
                        ->where('user_id', $user_account_id)      
                        ->where('state', 'reply')      
                        ->orderBy('created_at','desc');  
        }

        if($type=='waiting'){
            $eloquent = $eloquent
                ->where('user_id', $user_account_id)      
                ->where('state', 'waiting') 
                ->orderBy('created_at','desc');    
        }

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }

}


