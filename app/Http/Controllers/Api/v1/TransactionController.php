<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;
use App\Tools\DataTable;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\VerifyTags;
use App\Defined\FrozenTags;

use App\Models\Wallet;
use App\Models\Order;
use App\Models\Trading;
use App\Models\Ball;
use App\Models\User;

use App\Services\WalletService;
use App\Services\TransactionService;
use App\Services\UserService;
use App\Services\OrganizationService;
use App\Services\BonusService;
use App\Services\TradingService;
use App\Services\TagService;
use App\Services\PhoneVerifyService;
use App\Services\FrozenService;



use App\Repositories\TradingRepository;
use App\Repositories\WalletRepository;
use App\Repositories\OrderRepository;

class TransactionController extends ApiController
{


    // ＊＊ 確認收款/付款 ＊＊　
    public function confirmTrading(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('trading_id','type');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_trading_id = $this->checkPositiveInteger($request->trading_id);
        if ($check_trading_id['error'] != ApiError::SUCCESS) {
            return $check_trading_id;
        }

        // 如果買/賣方被凍結 -> (相關人員被凍結, 交易失敗)
        $trading_db = Trading::find($request->trading_id);
        if(!$trading_db){
            $result['error'] = ApiError::TRADING_NULL;
            return $result;
        }
        $frozen_result_seller = FrozenService::test_user_id($trading_db->seller_user_id);
        $frozen_result_buyer = FrozenService::test_user_id($trading_db->buyer_user_id);
        if($frozen_result_seller['data']['is_frozen_01']==1 || $frozen_result_buyer['data']['is_frozen_01']==1){
            $result['error']  = ApiError::TRADING_FROZEN_ERROR;
            return $result;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = TradingService::confirmTrading($request->trading_id, $user_account_id, $request->type);
        if( $result['data'] != 'deal'){
            return Response::json($result);
        }

        // 資料庫Trading 的 confirm_value 已標記交易完成, 開始轉帳
        $deal_result = WalletRepository::dealTrading($result['trading_id']); 
        // $result['error'] = $deal_result->error;
        $result['error'] = $deal_result->error;

        //$result['data'] = 'trading_id = '.$request->trading_id.' , user_id = '.$user_account_id.' , type = '.$request->type;

        return Response::json($result);
    }


    // ＊＊ 取消交易 ＊＊　
    public function cancelTrading(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('trading_id','type');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_trading_id = $this->checkPositiveInteger($request->trading_id);
        if ($check_trading_id['error'] != ApiError::SUCCESS) {
            return $check_trading_id;
        }

        if( !($request->type == 'buy' || $request->type == 'sell')){
            $result['error'] =  ApiError::ILLEGAL_VALUES_DANGER;
            return  $result;
        }

        
        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = TradingRepository::cancelTrading($request->trading_id, $user_account_id, $request->type);
        return Response::json($result);
    }


    // ＊＊ 取得 我的購買明細 ＊＊
    public function getBuy(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'amount', 'dt' => 'amount'),
            array('db' => 'seller_info', 'dt' => 'seller_info'),
            array('db' => 'confirm_value', 'dt' => 'confirm_value'),            
        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 

        $eloquent = new Trading();
        $eloquent = $eloquent
                     // ->where('buyer_user_id', $user_account_id)
                     // ->where('state', 'waiting') 
                     // ->orWhere('state', 'deal') 
                    //->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'cancel_order')")
                    ->whereRaw("buyer_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'cancel_order' )")
                    ->orderBy('created_at','desc');

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得 我的賣出明細 ＊＊
    public function getSell(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'state', 'dt' => 'state'),
            array('db' => 'amount', 'dt' => 'amount'),
            array('db' => 'buyer_info', 'dt' => 'buyer_info'),
            array('db' => 'confirm_value', 'dt' => 'confirm_value'),

        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 

        $eloquent = new Trading();
        $eloquent = $eloquent
                     // ->where('seller_user_id', $user_account_id)
                     // ->where('state', 'waiting')
                     // ->orWhere('state', 'deal')
                    //->whereRaw("seller_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'deal' )") 
                    ->whereRaw("seller_user_id = ".$user_account_id." AND ( state = 'waiting' OR state = 'cancel_order')")        
                    ->orderBy('created_at','desc');

        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 買點數From Order單＊＊　
    public function buyPointsFromOrder(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_account_id = Session::get(SessionNames::USER_ID);
        // 檢查綁定
        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }


        // 檢查銀行綁定狀態
        $is_bound_bank = TagService::verify($user_account_id,VerifyTags::USER_BIND_BANK);
        if($is_bound_bank != 1){
            $result['error'] = ApiError::USER_BANK_UNBOUND;
            return $result;
        }

        // if($user_db->binded_bank != 1){
        //     $result['error'] = ApiError::USER_BANK_UNBOUND;
        //     return $result;
        // }

        // 檢查電話綁定狀態
        $is_bound_phone = PhoneVerifyService::verify($user_db->country_code, $user_db->phone);
        if($is_bound_phone != 1){
            $result['error'] = ApiError::USER_MOBILE_UNBOUND;
            return $result;
        }


        // 是否被凍結
        $result = FrozenService::test_user_id($user_account_id);
        if($result['data']['is_frozen_01']==1){
            if($result['data']['tag'] == FrozenTags::USER_ID){
                $result['error'] = ApiError::USER_FROZEN_BY_ID;
            }

            if($result['data']['tag'] == FrozenTags::BANK_ACCOUNT){
                $result['error'] = ApiError::USER_FROZEN_BY_BANK_ACCOUNT;
            }

            if($result['data']['tag'] == FrozenTags::PHONE){
                $result['error'] = ApiError::USER_FROZEN_BY_PHONE;
            }
                       
            return $result;     
        }

        $check_key = array('amount');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_amount = $this->check10Multiple($request->amount);// 檢查10的倍數
        if ($check_amount['error'] != ApiError::SUCCESS) {
            return $check_amount;
        }

        
        $ball_db = Ball::select('level')->where('user_id', $user_account_id)->first();
        if(!$ball_db){
            $result = array('error' => ApiError::USER_ID_NULL); 
            return $result;
        }

        $set = OrganizationService::getSetFromLevel($ball_db->level);
        $result = OrderRepository::buyPointsFromOrder($user_account_id, $request->amount, $set);
        return Response::json($result);
    }


    // ＊＊ 取消Order單＊＊　
    public function cancelOrder(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('order_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_order_id = $this->checkPositiveInteger($request->order_id);
        if ($check_order_id['error'] != ApiError::SUCCESS) {
            return $check_order_id;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = TransactionService::cancelOrder($user_account_id, $request->order_id);
        return Response::json($result);
    }


    // ＊＊ 取得委託明細 ＊＊
    public function getOrder(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),            
            array('db' => 'total', 'dt' => 'total'), 
            array('db' => 'deal', 'dt' => 'deal'),            
            array('db' => 'trading', 'dt' => 'trading'),  
            array('db' => 'deleted_at', 'dt' => 'deleted_at'),  
        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Order();
        // //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     ->withTrashed()
                     ->where('user_id', $user_account_id)
                     ->orderBy('created_at','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 創 Order單＊＊　
    public function createOrder(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_account_id = Session::get(SessionNames::USER_ID);
        // 檢查綁定
        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 檢查銀行綁定狀態
        $is_bound_bank = TagService::verify($user_account_id,VerifyTags::USER_BIND_BANK);
        if($is_bound_bank != 1){
            $result['error'] = ApiError::USER_BANK_UNBOUND;
            return $result;
        }

        // 檢查電話綁定狀態
        $is_bound_phone = PhoneVerifyService::verify($user_db->country_code, $user_db->phone);
        if($is_bound_phone != 1){
            $result['error'] = ApiError::USER_MOBILE_UNBOUND;
            return $result;
        }

        // 是否被凍結
        $result = FrozenService::test_user_id($user_account_id);
        if($result['data']['is_frozen_01']==1){
            if($result['data']['tag'] == FrozenTags::USER_ID){
                $result['error'] = ApiError::USER_FROZEN_BY_ID;
            }

            if($result['data']['tag'] == FrozenTags::BANK_ACCOUNT){
                $result['error'] = ApiError::USER_FROZEN_BY_BANK_ACCOUNT;
            }

            if($result['data']['tag'] == FrozenTags::PHONE){
                $result['error'] = ApiError::USER_FROZEN_BY_PHONE;
            }
                       
            return $result;     
        }
        
        $check_key = array('amount');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_amount_int = $this->check10Multiple($request->amount); // 檢查10的倍數
        if ($check_amount_int['error'] != ApiError::SUCCESS) {
            return $check_amount_int;
        }

        $result = TransactionService::createOrder($request->amount);
        //$result['data'] = '666555';
        return Response::json($result);
    }


    // ＊＊ 轉帳 (Point)＊＊　
    public function depositPoints(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('deposit_account', 'points');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_positive_int = $this->checkPositiveInteger($request->points);
        if ($check_positive_int['error'] != ApiError::SUCCESS) {
            return $check_positive_int;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID

        // 是否被凍結
        $result = FrozenService::test_user_id($user_account_id);
        if($result['data']['is_frozen_01']==1){
            if($result['data']['tag'] == FrozenTags::USER_ID){
                $result['error'] = ApiError::USER_FROZEN_BY_ID;
            }

            if($result['data']['tag'] == FrozenTags::BANK_ACCOUNT){
                $result['error'] = ApiError::USER_FROZEN_BY_BANK_ACCOUNT;
            }

            if($result['data']['tag'] == FrozenTags::PHONE){
                $result['error'] = ApiError::USER_FROZEN_BY_PHONE;
            }
                       
            return $result;     
        }

        // // 取得目標id
        $result = UserService::getIDFromAccount($request->deposit_account);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }
        $target_account_id = $result['id'];

        // 檢查是不是自己推薦下線
        $result = UserService::isRecommendDownline($user_account_id, $target_account_id, 1, false);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        $result = TransactionService::depositPoints('Point',$user_account_id, $target_account_id, $request->points, true);
        return Response::json($result);
    }


    // ＊＊ 轉帳 (彩點)＊＊　
    public function depositColorPoints(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $check_key = array('deposit_account', 'color_points');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // check int
        $check_positive_int = $this->checkPositiveInteger($request->color_points);
        if ($check_positive_int['error'] != ApiError::SUCCESS) {
            return $check_positive_int;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID

        // 是否被凍結
        $result = FrozenService::test_user_id($user_account_id);
        if($result['data']['is_frozen_01']==1){
            if($result['data']['tag'] == FrozenTags::USER_ID){
                $result['error'] = ApiError::USER_FROZEN_BY_ID;
            }

            if($result['data']['tag'] == FrozenTags::BANK_ACCOUNT){
                $result['error'] = ApiError::USER_FROZEN_BY_BANK_ACCOUNT;
            }

            if($result['data']['tag'] == FrozenTags::PHONE){
                $result['error'] = ApiError::USER_FROZEN_BY_PHONE;
            }
                       
            return $result;     
        }

        // // 取得目標id
        $get_id_result = UserService::getIDFromAccount($request->deposit_account);
        if ($get_id_result['error'] != ApiError::SUCCESS) {
            return $get_id_result;
        }

        $target_account_id = $get_id_result['id'];

        // 檢查是不是自己推薦下線
        $result = UserService::isRecommendDownline($user_account_id, $target_account_id, 1, false);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // 贈送彩點!
        $result = TransactionService::depositPoints('ColorPoint',$user_account_id, $target_account_id, $request->color_points, true);
        return Response::json($result);
    }




}


