<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;

use App\Defined\SessionNames;
use App\Defined\ApiError;
use App\Defined\PointRules;
use App\Defined\PayType;
use App\Defined\VerifyTags;

use App\Models\Pay;
use App\Models\User;
// use App\Models\System;

use App\Tools\Tools;
use Carbon\Carbon;
use App\Services\TagService;

class PayController extends ApiController
{


    // ＊＊ 取得商品單價 ＊＊　
    // public function getPrice(Request $request, $product)
    // {

    //     $result = array('error' => ApiError::SUCCESS);

    //     if($product == PointRules::PRODUCT_01_NAME)
    //     {
    //         $result['data']['price'] = PointRules::PRODUCT_01_PRICE;
    //     } else {
    //         $result['error'] = ApiError::PRODUCT_NAME_NULL;
    //         return $result;
    //     }
    
    //     return Response::json($result);
    // }


    // 我們自己創訂單
    public function createOrder(Request $request)
    {
        //---------------------------------------------
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('pay_type', 'amount');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $user_id = Session::get(SessionNames::USER_ID);

        // 今日儲值額度已滿?
        $is_limited_pay = TagService::verify(0, VerifyTags::ALL_USERS_LIMITED_PAY);
        if($is_limited_pay == 1){
            $result['error'] = ApiError::DAILY_PAY_LIMITED;
            return $result;
        }

        // 檢查 amount 正整數
        $check_amount = $this->checkPositiveInteger($request->amount);
        if ($check_amount['error'] != ApiError::SUCCESS) {
            return $check_amount;
        }

        // 檢查 amount 上限
        if($request->amount > (int)PointRules::PRODUCT_01_MAX_AMOUNT){
            $return_error['error'] = ApiError::OVER_MAX_AMOUNT;
            return $return_error;
        }

        //------------------------------------
        
        $amount = (int)$request->amount;
        $PRODUCT_01_MAX_AMOUNT = (int) PointRules::PRODUCT_01_MAX_AMOUNT;  // 5000
        if ($amount < 1 || $amount > $PRODUCT_01_MAX_AMOUNT) {
            $result['error'] = ApiError::ILLEGAL_PAY_PRICE;
        } else if (in_array($request->pay_type, array(
            PayType::ECPAY_ATM,
            PayType::ECPAY_CREDIT,
            PayType::ECPAY_CVS,
            PayType::ECPAY_BARCODE,
            PayType::TXPAY_CSPM,
            PayType::TXPAY_VBANK,
        ))) {
            $price = (int) PointRules::PRODUCT_01_PRICE;  // 35
            $pay_price = $price * $amount;
            $fee = PayType::getFee($request->pay_type, $pay_price);
            $total_price = $pay_price + $fee;

            $pay_db = new Pay();
            $pay_db->user_id = $user_id;
            $pay_db->type = $request->pay_type;
            $pay_db->amount = $amount;
            $pay_db->price = $price;
            $pay_db->fee = $fee;
            $pay_db->total_price = $total_price;
            $pay_db->ip = $request->ip();

            /** 判斷是否有重複單號 */
            do {
                $pay_db->trade_id = time() . Tools::random4();
            } while (Pay::where('trade_id', $pay_db->trade_id)->count() != 0);


            $pay_db->save();

            switch ($request->pay_type) {
                case PayType::ECPAY_ATM:
                case PayType::ECPAY_CREDIT:
                case PayType::ECPAY_CVS:
                case PayType::ECPAY_BARCODE:
                    // 傳給金流
                    //$result['data'] = array('url' => route('api.v1.pay.tc.ecpay.id', ['order_id' => $pay_db->id]));
                    //$result['data'] = $pay_db->id;
                    break;
                case PayType::TXPAY_CSPM:
                case PayType::TXPAY_VBANK:
                    //$result['data']['url'] = 'http://'. env('DOMAIN_NAME') .'/user/pay/vbank/' . $pay_db->id;
                    // http://localhost/user/http//localhost/user/pay/vbank/43
                    $result['data'] = array('url' => route('api.pay.tc.vbank.id', ['pay_id' => $pay_db->id]));
                //$result['data'] = $pay_db->id;
                    break;
                default:
                    break;
            }
        }

        return Response::json($result);
    }
}

?>