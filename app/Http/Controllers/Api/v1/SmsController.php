<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Console\Command;

use App\Defined\SessionNames;
use App\Defined\ApiError;
use App\Defined\PointRules;
use App\Defined\PayType;

use App\Models\Pay;
use App\Models\User;

use App\Services\PhoneVerifyService;
use App\Repositories\PhoneVerifyRepository;
use App\Tools\Tools;
use App\Tools\SMS;

use Carbon\Carbon;

class SmsController extends ApiController
{

    // ＊＊ 傳送驗證簡訊 ＊＊　
    public function create(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('country_code','phone');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查 country_code格式
        $check_id = $this->checkPositiveInteger($request->country_code);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        // 檢查 phone 格式
        $check_phone = $this->checkPositiveInteger($request->phone);
        if ($check_phone['error'] != ApiError::SUCCESS) {
            return $check_phone;
        }

        // user_id
        $user_account_id = Session::get(SessionNames::USER_ID);
        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 已綁定
        $rand_code = Tools::random4();
        $verify_phone_result = PhoneVerifyService::verify_state($request->country_code, $request->phone, $rand_code);
        if($verify_phone_result['value'] == 'bound'){  // empty unverified bound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_1;
            return $result;          
        }

        // 先存入目前的country_code 和 phone
        $user_db->country_code = $request->country_code;
        $user_db->phone = $request->phone;
        $user_db->save();        


        if($verify_phone_result['value'] == 'apply_max'){  // empty unverified bound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_2;
            return $result;          
        }


        $send_rand_txt = "【北京賽車】驗證碼：".$rand_code."，請在5分鐘內輸入，請勿告知他人，謹防上當受騙。";
        $result = SMS::send($user_db->country_code, $user_db->phone, $send_rand_txt);

        // 創 phone_verify_db
        if($verify_phone_result['value'] == 'empty'){  // empty unverified bound
            PhoneVerifyRepository::create($user_db->country_code, $user_db->phone, $rand_code);        
        }



        
        // $user_db->binded_phone = $rand_code . ',' . Carbon::now();
        // $user_db->save();

        $result['data']['debug'] = $verify_phone_result['value'];

        return Response::json($result);
    }


    // ＊＊ 傳送驗證簡訊 ＊＊　
    public function bind(Request $request)
    {

        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('verify_code');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // user_id
        $user_account_id = Session::get(SessionNames::USER_ID);
        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 已綁定
        $verify_phone_result = PhoneVerifyService::verify_code($user_db->country_code, $user_db->phone, $request->verify_code);
        if($verify_phone_result['value'] =='bound'){  // empty unverified bound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_1;
            return $result;          
        }

        // 未申請驗證碼
        if($verify_phone_result['value'] == 'empty' || $verify_phone_result['value'] == 'over_max_wrong'){  //unbound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_3;
            return $result;
        }

        // 錯誤次數超過三次
        if($verify_phone_result['value'] == 'max_wrong'){  //unbound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_6;
            return $result;
        }

        // // 驗證碼錯誤
        if($verify_phone_result['value'] == 'wrong_code'){  //unbound
            $result['error'] = ApiError::PHONE_BINDED_FAILED_5;
            return $result;
        }        

        // 綁定成功
        // if( $verify_phone_result['value'] == 'verified'){
        //     //比自己慢綁定又重複的的 改他的電話
        //     $check_double_phone_db = User::where('country_code', $user_db->country_code)
        //                         ->where('phone', $user_db->phone)
        //                         ->where('id','!=',$user_account_id)
        //                         ->get();

        //     if( $check_double_phone_db){
        //         for ($i=0; $i < count($check_double_phone_db) ; $i++) { 
        //             $check_double_phone_db[$i]->phone = "0";
        //             $check_double_phone_db[$i]->save();
        //         }        
        //     }  
        // }

        $result['data'] = $verify_phone_result['value'];//$user_db->binded_phone;
        
        return Response::json($result);
    }




}

?>