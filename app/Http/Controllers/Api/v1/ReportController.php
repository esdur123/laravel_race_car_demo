<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;

use Illuminate\Http\Request;

use App\Services\ReportService;

class ReportController extends ApiController
{
    public function getBetReport(Request $request)
    {
        $result = ReportService::getBetReport($request);
        return $result;
    }
}