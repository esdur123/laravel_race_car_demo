<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Models\User;
use App\Models\Ball;

use App\Services\OrganizationService;
use App\Services\TransactionService;
use App\Services\BonusService;
use App\Services\UserService;

use App\Tools\DataTable;

class OrganizationController extends ApiController
{

    // ＊＊ 激活配套＊＊　
    public function activeLevel(Request $request)
    {

        $check_key = array('new_level', 'ball_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        // 檢查新level是否合法    
        if( !($request->new_level==1 || $request->new_level==2 || $request->new_level==3) ){
            $result['error'] = ApiError::ILLEGAL_LEVEL;
            return $result;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID
        
        // 檢查ball_id是不是屬於這個人
        $is_ball_owner = OrganizationService::check_is_user_own_ball($user_account_id, $request->ball_id);
        if($is_ball_owner != 1){
            $result['error'] = ApiError::IS_NOT_BALL_OWNER;
            return $result;
        }

        // 開通配套
        $result = TransactionService::activeSet($user_account_id, $request->ball_id, $request->new_level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // 計算直推獎金
        $result = BonusService::directPush($user_account_id, $request->ball_id, $request->new_level);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }

        // // 計算上面相關pv
        // $result = BonusService::calculatePV($result, $user_account_id, $request->level);
        // if ($result['error'] != ApiError::SUCCESS) {
        //     return $result;
        // }

        return Response::json($result);
    }


    // ＊＊ 取得(branch-tree底下用戶) branch資料 ＊＊
    public function getActiveInfo(Request $request)
    {
        $user_account_id = Session::get(SessionNames::USER_ID); 

        $columns = array(
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'ball_index', 'dt' => 'ball_index'),
            array('db' => 'static_current', 'dt' => 'static_current'),
            array('db' => 'static_max', 'dt' => 'static_max'),
            array('db' => 'auto_active', 'dt' => 'auto_active'),
            array('db' => 'level', 'dt' => 'level'),
        );
        
        $ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Ball();
        $eloquent = $eloquent
                    ->where('user_id',$user_account_id);
                    
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得特定branch 組織圖資料 ＊＊
    public function getBranchInfo(Request $request, $account)
    {

        $result = UserService::getIDFromAccount($account);
        if($result['error'] != ApiError::SUCCESS){
            return $result;
        }

        $user_id = $result['id'];
        $ball_db = Ball::select("id")->where('user_id', $user_id)->first();
        $result = OrganizationService::getBranchInfoByID($ball_db->id);
        //$result['data'] = 'account='.$account.',id='.$id;
        return Response::json($result);
    }


    // ＊＊ 切換自動開關 ＊＊
    public function switchActive(Request $request)
    {
        
        // 檢查 $id 格式
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('ball_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_id = $this->checkPositiveInteger($request->ball_id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); 

        $result = OrganizationService::switchActive($user_account_id, $request->ball_id);
        return Response::json($result);
    }


    // ＊＊ 立刻激活 ＊＊
    public function ActiveStatic(Request $request)
    {
        // 檢查 $id 格式
        $result = array('error' => ApiError::SUCCESS);        

        $check_key = array('ball_id');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $check_id = $this->checkPositiveInteger($request->ball_id);
        if ($check_id['error'] != ApiError::SUCCESS) {
            return $check_id;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); 

        $result = OrganizationService::ActiveStatic($user_account_id, $request->ball_id);
        if ($result['error'] != ApiError::SUCCESS) {
            return $result;
        }
                
        $ball_level = $result['ball_level'];

        // 直推獎金
        $result = BonusService::directPush($user_account_id, $request->ball_id, $ball_level);
        return Response::json($result);
    }


    // ＊＊ 取得 recommend 組織圖資料 ＊＊
    public function GetRecommenderInfo(Request $request, $account)
    {
        // 檢查 $id 格式
        // $check_id = $this->checkPositiveInteger($id);
        // if ($check_id['error'] != ApiError::SUCCESS) {
        //     return $check_id;
        // }

        $result = UserService::getIDFromAccount($account);
        if($result['error'] != ApiError::SUCCESS){
            return $result;
        }

        $user_id = $result['id'];
        $ball_db = Ball::select("id")->where('user_id', $user_id)->first();
        $result = OrganizationService::GetRecommenderInfoByID($ball_db->id);
        return Response::json($result);
    }


    // ＊＊ 取得ball ID＊＊
    public function getBallID(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS); 
        $result['data'] = OrganizationService::getBallID();
        return Response::json($result);
    }


    // ＊＊ 取得配套 ＊＊
    public function getSet(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);  
        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result['data'] = OrganizationService::getSet($user_account_id);
        //$result['data'] = $wallet_db->coin;
        return Response::json($result);
    }


    // ＊＊ 計算PV ＊＊
    public function calculatePV(Request $request)
    {
        $user_account_id = Session::get(SessionNames::USER_ID); 
        $result = OrganizationService::calculatePV($user_account_id);
        return Response::json($result);
    }



}


