<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Api\ApiController;

use App\Defined\ApiError;
use App\Defined\SessionNames;

use App\Models\Wallet;
use App\Models\User;

use App\Services\LotteryService;
// use App\Tools\DataTable;
// use App\Services\OrganizationService;
        

class LotteryController extends ApiController
{


    // ＊＊ 九宮格抽抽樂(單次) ＊＊　
    public function play9Block(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $check_key = array('i_click');
        $check_request = $this->checkRequest($request, $check_key);
        if ($check_request['error'] != ApiError::SUCCESS) {
            return $check_request;
        }

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID        
        $result = LotteryService::play9Block($user_account_id, $request->i_click);
        return Response::json($result);
    }


    // ＊＊ 九宮格抽抽樂(十次) ＊＊　
    public function play9Block10Times(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID        
        $result = LotteryService::play9Block10Times($user_account_id);
        return Response::json($result);
    }



}


