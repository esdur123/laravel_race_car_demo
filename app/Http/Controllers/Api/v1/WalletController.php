<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response; 
use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;

use App\Models\Transaction;
use App\Http\Controllers\Api\ApiController;
use App\Services\WalletService;
use App\Tools\DataTable;
// use App\Services\OrganizationService;
        

class WalletController extends ApiController
{


    // ＊＊ 取得彩點點數 ＊＊　
    public function getColorPoint(Request $request)
    {
        $user_account_id = Session::get(SessionNames::USER_ID); // accountID
        $point = WalletService::getBalance($user_account_id,'ColorPoint');
        return Response::json($point);
    }


    // ＊＊ 檢查現在是否要用彩點 ＊＊　
    public function checkIsColorpoint(Request $request)
    {
        $result = array('error' => ApiError::SUCCESS);
        $is_colorpoint = 0;
        $C_COLOR_POINT_PERCENTAGE = 0;//env('COLOR_POINT_PERCENTAGE');
        if($C_COLOR_POINT_PERCENTAGE > 0){
            $is_colorpoint = 1;
        }

        $result['data']['is_colorpoint'] = $is_colorpoint;

        return Response::json($result);
    }


    // ＊＊ 取得點數 ＊＊　
    public function getPoint(Request $request)
    {
        $user_account_id = Session::get(SessionNames::USER_ID); // accountID
        $point = WalletService::getBalance($user_account_id,'Point');
        return Response::json($point);
    }


    // ＊＊ 取得Point明細 ＊＊
    public function getPointInfo(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),            
            array('db' => 'type', 'dt' => 'type'), 
            array('db' => 'value', 'dt' => 'value'), 
            array('db' => 'after', 'dt' => 'after'), 
            array('db' => 'link', 'dt' => 'link'), 
        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Transaction();
        // //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     ->where('coin', 'Point')
                     ->where('user_id', $user_account_id)
                     ->orderBy('id','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


    // ＊＊ 取得ColorPoint明細 ＊＊
    public function getColorPointInfo(Request $request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at'),
            array('db' => 'id', 'dt' => 'id'),            
            array('db' => 'type', 'dt' => 'type'), 
            array('db' => 'value', 'dt' => 'value'), 
            array('db' => 'after', 'dt' => 'after'), 
            array('db' => 'link', 'dt' => 'link'), 
        );
        
        $user_account_id = Session::get(SessionNames::USER_ID); 
        //$ball_id = OrganizationService::getBallID(); // 取得使用者ball id

        $eloquent = new Transaction();
        // //$eloquent = $eloquent->with('recommends'); // 沒用過0.0
        $eloquent = $eloquent
                     ->where('coin', 'ColorPoint')
                     ->where('user_id', $user_account_id)
                     ->orderBy('id','desc');
        //             ->get();
        $result = DataTable::complex($eloquent, $request, $columns);
        return Response::json($result);
    }


}


