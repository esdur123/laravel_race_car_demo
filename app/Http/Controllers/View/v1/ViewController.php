<?php

namespace App\Http\Controllers\View\v1;

use Illuminate\Http\Request;

use App\Defined\ApiError;
use App\Http\Controllers\Controller;
use App;

class ViewController extends Controller
{

    // ** User ** 
    public function home(Request $request)
    {
        return view('home');
    }

    public function register(Request $request, $recommender_account)
    {
        return view('register', [ 'recommender_account'=> $recommender_account ]);
    }

    public function user_news(Request $request)
    {
        return view('_news/user_news');
    }

    public function user_org_branch(Request $request)
    {
        return view('_org/user_org_branch');
    }

    public function user_org_branch_specific(Request $request, $account)
    {
        return view('_org/user_org_branch_specific', [ 'account'=> $account ]);
    }

    public function user_org_recommend_specific(Request $request, $account)
    {
        return view('_org/user_org_recommend_specific', [ 'account'=> $account ]);
    }

    public function user_org_recommand(Request $request)
    {
        return view('_org/user_org_recommend');
    }

    public function user_account(Request $request)
    {
        return view('_account/user_account');
    }

    public function user_active(Request $request)
    {
        return view('_active/user_active');
    }
    
    public function user_transform(Request $request)
    {
        return view('_transform/user_transform');
    }

    public function user_trade(Request $request)
    {
        return view('_trade/user_trade');
    }
    
    public function user_wallet(Request $request)
    {
        return view('_wallet/user_wallet');
    }

    public function user_ask(Request $request)
    {
        return view('_ask/user_ask');
    }

    public function user_pay(Request $request)
    {
        return view('_pay/user_pay');
    }

    public function user_game(Request $request)
    {
        return view('_game/user_game');
    }

    public function intro(Request $request)
    {
        return view('intro');
    }

    public function user_withdraw(Request $request)
    {
        return view('_withdraw/user_withdraw');
    }
    
    public function user_pay_vbank(Request $request, $pay_id)
    {
        return view('_pay/user_pay_vbank', [ 'pay_id'=> $pay_id ]);
    }

    public function bet_report(Request $request)
    {
        return view('_bet_report/index');
    }

    public function test(Request $request)
    {
        return view('test');
    }

    // ** Admin **
    public function admin_home(Request $request)
    {
        return view('_admin/home');
    }

    public function admin_ask(Request $request)
    {
        return view('_admin/ask');
    }

    public function admin_manage_account(Request $request)
    {
        return view('_admin/manage_account');
    }

    public function admin_manage_account_specific(Request $request, $id)
    {
        return view('_admin/manage_account_specific', [ 'id'=> $id ]);
    }

    public function admin_manage_branch(Request $request, $id)
    {
        return view('_admin/manage_branch', [ 'id'=> $id ]);
    }

    public function admin_manage_recommend(Request $request, $id)
    {
        return view('_admin/manage_recommend', [ 'id'=> $id ]);
    }

    public function admin_trade(Request $request)
    {
        return view('_admin/trade');
    }

    public function admin_control(Request $request)
    {
        return view('_admin/control');
    }

    public function admin_info(Request $request)
    {
        return view('_admin/info');
    }

    public function admin_withdraw(Request $request)
    {
        return view('_admin/withdraw');
    }

    public function admin_calender(Request $request)
    {
        return view('_admin/calender');
    }


}
