<?php

namespace App\Http\Middleware\User\View;

use Illuminate\Support\Facades\Session;
use App\Defined\SessionNames;
use App\Models\User;



use Closure;

// # 檢查註冊上線是否合法,
// # 不合法就回首頁
class RegisterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        // 先把依"/"將url分開 找最後一個字串(就是'上線帳號' upline)
        $explodeURL =  explode( "/" , $request->url()); // 將url依 "/" 分成陣列
        $upline = $explodeURL[count($explodeURL)-1]; // 找url的最後一個字串(不可能為空白 "/Register/(空白)", 因為Route-Web會檔)

        // (資料庫) 依account找此會員
        $upline_db = User::Where('account',$upline)->first();

        // 合法註冊: 此上線會員存在 (或是) 路徑為"null" 素人自己申請
        if ($upline_db) { 

            $result = $next($request);

        } else { // 不合法註冊: 無此會員 跳回首頁

            $result = redirect()->route('home');
        }

        return $result;
    }
}
