<?php

namespace App\Http\Middleware\User\Api;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use Illuminate\Support\Facades\Response;

use Closure;

use App\Defined\SessionNames;


// 在 Kernel 中註冊為 apiAuth
class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** 未登入 **/
        if ( !( Session::has(SessionNames::USER_ID) ) ) {
            
            //$result = redirect()->route('home'); 
            $result = Response::json(                
                array('error' => ApiError::SESSION_BE_REVOKED) // api不過 => 由api_mgr.js的 a.send 處理
            );

        } else {

            $result = $next($request);

        }

        return $result;
    }
}
