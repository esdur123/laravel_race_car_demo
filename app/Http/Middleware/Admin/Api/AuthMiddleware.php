<?php

namespace App\Http\Middleware\Admin\Api;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use Illuminate\Support\Facades\Response;

use Closure;

use App\Defined\SessionNames;


// 在 Kernel 中註冊為 apiAuth
class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** 未登入 **/
        if ( !( Session::has(SessionNames::ADMIN_ID) ) ) {
            
            $result = redirect()->route('admin_home'); 

        } else {

            $result = $next($request);

        }

        return $result;
    }
}
