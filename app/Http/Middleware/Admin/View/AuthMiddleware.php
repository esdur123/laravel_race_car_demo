<?php

namespace App\Http\Middleware\Admin\View;

use Illuminate\Support\Facades\Session;
use App\Defined\SessionNames;

use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( Session::has(SessionNames::ADMIN_ID) ){
            /** 登入成功 **/
            $result = $next($request);

        } else {
            /** 未登入 **/
            $result = redirect()->route('admin_home'); 

        }

        return $result;
    }
}
