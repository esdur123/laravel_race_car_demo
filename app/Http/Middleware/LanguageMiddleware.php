<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use App\Defined\SessionNames;
use Closure;
use App;

//use App\MyUtility;



class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        $dafault_language = 'zh_TW';  // 預設值   
        if(Session::exists(SessionNames::LANGUAGE)){
            App::setLocale(Session::get(SessionNames::LANGUAGE));
        } else {
            App::setLocale($dafault_language);
        }

        
        $result = $next($request);
        return $result;
    }
}
