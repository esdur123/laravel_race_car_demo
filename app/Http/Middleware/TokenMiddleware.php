<?php

namespace App\Http\Middleware;
use App\Defined\SessionNames;

use App\Defined\ApiError;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Firebase\JWT\JWT;

use Closure;


// 在 Kernel 中註冊為 token

class TokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     **/

    public function handle($request, Closure $next)
    {
        $authorization = $request->header('Authorization');

        if (isset($authorization)) {
            $token = preg_split('/ /', $authorization);

            if ($token[0] == "Bearer" && isset($token[1])) {     
               // cache:clear(); // config:cache(); // catch失敗xd
                $token_data = (array)JWT::decode($token[1], config('app.JWT_KEY'), array('HS256')); 
                //newbi_key_hahaha213 , env('JWT_KEY') , config('app.JWT_KEY')
                Session::setId($token_data['session']);
                $token_expire = $token_data['expire'];
            }
        }

        if (isset($token_expire) &&  
            $token_expire < strtotime("now")) { // 判斷token是否過期
        
            Session::forget(SessionNames::TOKEN);
            
            $result = Response::json(
                array('error' => ApiError::TOKEN_EXPIRE)
            );
        } 
        else if( !isset($token_expire) ){ // 判斷token是否存在
            $result = Response::json(
                array('error' => ApiError::TOKEN_BE_REVOKED)
            );
        }
        else{ // Token 存在且沒過期

            $result = $next($request);
        }

        return $result;
    }

}
