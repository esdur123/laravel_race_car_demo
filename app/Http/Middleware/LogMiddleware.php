<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use App\Defined\SessionNames;
use Closure;
use App;

use App\Tools\Tools;
use App\Models\Log;

// 停用, 目前只在重要的動作寫在controller呼叫MyUtility紀錄
class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        // $result = $next($request);
        // $request_method = $request->method();

        // // 過濾非POST和PUT
        // if( !(strtoupper($request_method) == "POST" || strtoupper($request_method) == "PUT") ){
        //     return $result;
        // }

        // // 存 methos + url
        // $request_path =  str_replace("api/v1/","",$request->path());
        // $url = $request_method.",".$request_path;
        // MyUtility::RecordApiLog($url); // 存log

        $log_db = new Log();
        $log_db->ip = Tools::getIP();
        $log_db->action = $request->getRequestUri();
        $log_db->device = $request->header('User-Agent');
        $log_db->body = file_get_contents('php://input');
        $log_db->post = json_encode($_POST);
        $log_db->get = json_encode($_GET);
        $log_db->session = json_encode(array('account_id' => Session::get(SessionNames::USER_ID)));
        $log_db->save();

        return $next($request);

        
        // return $result;
    }
}
