<?php

namespace App\Services;
use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Services\Service;
use App\Repositories\WalletRepository;

class WalletService extends Service
{


    // ＊＊ 取得點數 ＊＊　
    public static function getBalance($user_account_id, $coin)
    {
        $result = array('error' => ApiError::SUCCESS);
        
        $wallet_db = Wallet::where('user_id', $user_account_id)
                ->where('coin', $coin)
                ->first();        

        if(!$wallet_db){

            $ball_db = Ball::where('user_id',$user_account_id)->first();
            if(!$ball_db){
                $result = array('error' => ApiError::USER_ID_NULL); 
                return $result;
            }

            // 沒有就創給他, lock 寫在 WalletRepository 
            $wallet_db = WalletRepository::create($user_account_id, $ball_db->id ,$coin);
        }
        
        $result['data'] = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
        return $result;
    }


    // ＊＊ 註冊時創 Wallet ＊＊　
    public static function register($user_account_id, $ball_id, $coin)
    {
        $result = array('error' => ApiError::SUCCESS);
        
        $wallet_db = WalletRepository::create(
                                              $user_account_id
                                              , $ball_id
                                              , $coin
                                             );
        return $wallet_db->id;
    }

}