<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\FrozenTags;

use App\Models\Frozen;
use App\Models\User;

use App\Repositories\FrozenRepository;
use App\Services\Service;


class FrozenService extends Service
{


    // ＊＊ 檢查 phone 是否被凍結 ＊＊　
    public static function test_phone($country_code, $phone){
        
        $result = array('error' => ApiError::SUCCESS);
        $result['data']['is_frozen_01'] = 0;
        $result['data']['tag'] = '';
        $result['data']['tag_phone'] = 0;

        $frozen_phone_db = Frozen::where('tag', FrozenTags::PHONE) 
                            ->where('info', '('.$country_code.')'.$phone)
                            ->first();

        if($frozen_phone_db){
            $result['data']['is_frozen_01'] = 1;
            $result['data']['tag'] = FrozenTags::PHONE;
            $result['data']['tag_phone'] = 1;
            return $result;
        }    

        return $result;
    } 


    // ＊＊ 檢查 bank 是否被凍結 ＊＊　
    public static function test_bank($bank_account){
        
        $result = array('error' => ApiError::SUCCESS);
        $result['data']['is_frozen_01'] = 0;
        $result['data']['tag'] = '';
        $result['data']['tag_bankaccount'] = 0;

        $frozen_bank_db = Frozen::where('tag', FrozenTags::BANK_ACCOUNT) 
                            ->where('info', $bank_account)
                            ->first();

        if($frozen_bank_db){
            $result['data']['is_frozen_01'] = 1;
            $result['data']['tag'] = FrozenTags::BANK_ACCOUNT;
            $result['data']['tag_bankaccount'] = 1;
            return $result;
        }    

        return $result;
    } 


    // ＊＊ 凍結/解凍 by (tag & info) ＊＊
    public static function frozen($tag, $info, $is_frozen_01)
    {
        $result = array('error' => ApiError::SUCCESS);
        if($is_frozen_01){
            $result['data']['is_frozen_01'] = FrozenRepository::create($tag, $info);
        } else {
            $result['data']['is_frozen_01'] = FrozenRepository::delete($tag, $info);
        }
        
        return $result;
    }


    // ＊＊ 檢查 account id 是否被凍結 ＊＊　
    public static function test_user_id($user_id){
        
        $result = array('error' => ApiError::SUCCESS);
        $result['data']['is_frozen_01'] = 0;
        $result['data']['tag'] = '';
        $result['data']['tag_userid'] = 0;
        $result['data']['tag_phone'] = 0;
        $result['data']['tag_bankaccount'] = 0;
        // 先檢查tag user id

        $user_db = User::find($user_id);
        if(!$user_db){
            return $result; // 老實講 我不知道怎麼處理這種情形.. 應該走不進來吧0.0!
        }

        $frozen_id_db = Frozen::where('tag', FrozenTags::USER_ID)
                        ->where('info',$user_id)
                        ->first();

        if($frozen_id_db){
            $result['data']['is_frozen_01'] = 1;
            $result['data']['tag'] = FrozenTags::USER_ID;
            $result['data']['tag_userid'] = 1;
            return $result;
        }    

        $frozen_phone_db = Frozen::where('tag', FrozenTags::PHONE) 
                            ->where('info', '('.$user_db->country_code.')'.$user_db->phone)
                            ->first();

        if($frozen_phone_db){
            $result['data']['is_frozen_01'] = 1;
            $result['data']['tag'] = FrozenTags::PHONE;
            $result['data']['tag_phone'] = 1;
            return $result;
        }    

        $frozen_bank_db = Frozen::where('tag', FrozenTags::BANK_ACCOUNT) 
                            ->where('info', $user_db->bank_account)
                            ->first();

        if($frozen_bank_db){
            $result['data']['is_frozen_01'] = 1;
            $result['data']['tag'] = FrozenTags::BANK_ACCOUNT;
            $result['data']['tag_bankaccount'] = 1;
            return $result;
        } 

        return $result;
    } 

}