<?php

namespace App\Services;
use Lock;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response; 

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\VerifyTags;
use App\Defined\WithdrawStates;
use App\Defined\TransactionTypes;



use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;

use App\Models\Transaction;
use App\Models\Admin;
use App\Models\Trading;
use App\Models\Order;
use App\Models\Rate;
use App\Models\System;
use App\Models\OrderWithdraw;

use App\Services\Service;
use App\Services\BonusService;
use App\Services\OrganizationService;

use App\Repositories\TradingRepository;
use App\Repositories\TagRepository;
use App\Repositories\WalletRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\RateRepository;
use App\Repositories\PhoneVerifyRepository;


use Carbon\Carbon;

class AdminService extends Service
{


    // ＊＊ 強制通過電話驗證 ＊＊
    public static function force_insert($user_id, $country_code, $phone)
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);

        $user_db = User::find($user_id);
        if(!$user_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        $user_db->country_code = $country_code;
        $user_db->phone = $phone;
        $user_db->save();

        // 強制寫入一組電話通過驗證
        PhoneVerifyRepository::force_insert($country_code, $phone);

        //$result['data'] = $ask_id.','.$reply;

        return $result;
    }




    // ＊＊ 回答問題 ＊＊
    public static function reply_question($ask_id, $reply)
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);

        $result['data'] = $ask_id.','.$reply;

        return $result;
    }


    // ＊＊ 取得分紅倍率 ＊＊
    public static function add_dividend_rates($year,$month,$day,$rate)
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);

        // 前端日曆月份是0-11...
        $plus_month = $month+1;

        // 加一筆資料
        RateRepository::add($year, $plus_month, $day, $rate);
        
        return $result;
    }


    // ＊＊ 取得分紅倍率 ＊＊
    public static function get_dividend_rates()
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);
        

        $rate_db = Rate::select('year','month','day','rate')->get();
        if(!$rate_db){
            $result['error'] = ApiError::RATE_DATABASE_NULL;
            return $result;
        }

        $obj_array = array();

        for ($i=0; $i < count($rate_db) ; $i++) { 
            $obj = new \stdClass();
            $obj->rate = $rate_db[$i]->rate;
            $obj->year = $rate_db[$i]->year;
            $obj->month = $rate_db[$i]->month-1; // 前端日曆月份是0-11...所以要-1 xd
            $obj->day = $rate_db[$i]->day;

             $obj_array[$i] = $obj;
            # code...
        }

        $result['data'] = $obj_array;
  
        return $result;
    }


    // ＊＊ 處理中 ＊＊
    public static function deal_withdraw($withdraw_id)
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);

        $find_withdraw_db = OrderWithdraw::find($withdraw_id);
        if(!$find_withdraw_db){  
            $result['error'] = ApiError::WITHDRAW_ID_ERROR;
            return $result;
        }

        // 如果狀態已改變(會員撤銷之類的..) 那就不會進入處理中
        if($find_withdraw_db->state != WithdrawStates::ACCEPTING){
            $result['error'] = ApiError::WITHDRAW_ID_ERROR;
            return $result;
        }

        $user_id = $find_withdraw_db->user_id;

        $find_wallet_db = Wallet::where('user_id', $user_id)
                        ->first();

        if(!$find_wallet_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        DB::transaction(function() use($withdraw_id, $user_id){
            // 確認兩個db都存在, 進入lock , 第一層 Withdraw
            $lock_withdraw_key = 'Withdraw@user_id:' . $user_id;
            try {
                Lock::acquire($lock_withdraw_key);

                // 第二層 Wallet
                $lock_wallet_key = 'Wallet@user_id:' . $user_id;
                try {
                    Lock::acquire($lock_wallet_key);

                    $withdraw_db = OrderWithdraw::find($withdraw_id);
                    $withdraw_db->state = WithdrawStates::DEAL;
                    $withdraw_db->save();

                    // Wallet
                    $wallet_db = Wallet::where('user_id', $user_id)
                                ->first();

                    $before = $wallet_db->balance_available + $wallet_db->balance_locked;                
                    $wallet_db->balance_locked -= $withdraw_db->amount;  
                    $wallet_db->save();
                    $after = $wallet_db->balance_available + $wallet_db->balance_locked;

                    // 交易紀錄 (提出方)
                    TransactionRepository::create(
                                                  $user_id
                                                  , $wallet_db->id
                                                  , 'Point'
                                                  , TransactionTypes::USER_WITHDRAW
                                                  , $withdraw_db->amount
                                                  , $before
                                                  , $after
                                                  , ""
                                                 );


                } finally {
                    Lock::release($lock_wallet_key);
                }


            } finally {
                Lock::release($lock_withdraw_key);
            }

        }); // end db transaction    


        $result['data'] = 'DEAL';
        return $result;
    }


    // ＊＊ 處理中 ＊＊
    public static function accepting_withdraw($withdraw_id)
    {
        //VerifyTags::ALL_USERS_LIMITED_PAY
        $result = array('error' => ApiError::SUCCESS);

        $find_withdraw_db = OrderWithdraw::find($withdraw_id);
        if(!$find_withdraw_db){  
            $result['error'] = ApiError::WITHDRAW_ID_ERROR;
            return $result;
        }

        // 如果狀態已改變(會員撤銷之類的..) 那就不會進入處理中
        if($find_withdraw_db->state != WithdrawStates::WAITING){
            $result['error'] = ApiError::WITHDRAW_ID_ERROR;
            return $result;
        }

        // 第一層 Withdraw
        DB::transaction(function() use($find_withdraw_db, $withdraw_id){
            $lock_withdraw_key = 'Withdraw@user_id:' . $find_withdraw_db->user_id;
            try {
                Lock::acquire($lock_withdraw_key);

                $withdraw_db = OrderWithdraw::find($withdraw_id);
                $withdraw_db->state = WithdrawStates::ACCEPTING;
                $withdraw_db->save();


            } finally {
                Lock::release($lock_withdraw_key);
            }
        }); // end db transaction

        $result['data'] = 'ACCEPTING';
        return $result;
    }



    // ＊＊ 贈送彩點 ＊＊　
    // public static function depositColorPoints($to_account_id, $value)
    // {

    //     $coin = 'ColorPoint';
    //     $type = 'Admin-Deposit-Color-Point';

    //     $result = array('error' => ApiError::SUCCESS);



    //     $to_wallet_db = Wallet::Select("balance_available","id")
    //             ->where("user_id", $to_account_id)
    //             ->where('coin',$coin)
    //             ->first();

    
    //     if( !$to_wallet_db ){
    //         $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
    //         return $result;
    //     }


    //     $to_wallet_before = $to_wallet_db->balance_available + $to_wallet_db->balance_locked; 
    //     $to_wallet_db_id = $to_wallet_db->id;
       
    //     // 收點
    //     $lock_to_wallet_db = WalletRepository::deposit_by_userID($to_account_id, $value, $coin);
    //     $to_wallet_after = $lock_to_wallet_db->balance_available + $lock_to_wallet_db->balance_locked; 


    //     // 交易單 to                  
    //     TransactionRepository::create(
    //                           $to_account_id
    //                           , $to_wallet_db_id
    //                           , $coin
    //                           , $type
    //                           , $value
    //                           , $lock_to_wallet_db->before
    //                           , $lock_to_wallet_db->after
    //                           , ''
    //                          );

    //     return $result;
    // }


    // ＊＊ 贈送點數 ＊＊　
    public static function depositPoints($to_account_id, $value, $coin)
    {

        $type = 'Admin-Deposit-Point';

        $result = array('error' => ApiError::SUCCESS);

        $to_wallet_db = Wallet::Select("balance_available","id")
                ->where("user_id", $to_account_id)
                ->where('coin', $coin)
                ->first();
    
        if( !$to_wallet_db ){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        $to_wallet_before = $to_wallet_db->balance_available + $to_wallet_db->balance_locked; 
        $to_wallet_db_id  = $to_wallet_db->id;

        // 收點
        $lock_to_wallet_db = WalletRepository::deposit_by_userID($to_account_id, $value,$coin);
        $to_wallet_after = $lock_to_wallet_db->balance_available + $lock_to_wallet_db->balance_locked; 
                    

        TransactionRepository::create(
                              $to_account_id
                              , $to_wallet_db_id
                              , $coin
                              , $type
                              , $value
                              , $lock_to_wallet_db->before
                              , $lock_to_wallet_db->after
                              , ''
                             );
       
        return $result;
    }



    // ＊＊ 取消委託 (lock) ＊＊
    private static function cancelOrderTradings($find_order_db, $admin_id)
    {

        $obj = new \stdClass();
        $obj->error = ApiError::SUCCESS;

        $order_id = $find_order_db->id;

        // 根據order單找出seller的 $user_id  // $order_id
        $user_id = $find_order_db->user_id;
        
        $obj = DB::transaction(function() use($obj, $order_id, $admin_id, $user_id){
            // Lock
            $lock_key_wallet = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_key_wallet);

                // logic
                // 先確定找的到, 後面會另外開1個order db在try裡面        
                $order_db = Order::where('id', $order_id)
                    ->first();

                $wallet_db = Wallet::where('user_id', $user_id)
                    ->where('coin','Point')
                    ->first();

                if($order_db && $wallet_db){       

                    // 這邊應該要把 $order_db 關起來了? (暫時不做這件事)

                    $lock_key_order = 'Order@order_id:' . $order_id;
                    try {
                            Lock::acquire($lock_key_order);

                            // 另外開一個order db 實作
                            $locked_order_db = Order::where('id', $order_id)
                                ->where('user_id', $user_id)
                                ->first();

                            // 把未交易的point存回去
                            $value = $locked_order_db->total - $locked_order_db->deal;
                            $wallet_db->balance_available = $wallet_db->balance_available + $value;
                            $wallet_db->balance_locked    = $wallet_db->balance_locked - $value;
                            $wallet_db->save();

                            $locked_order_db->delete();
                            if(!$locked_order_db->trashed()){
                                $obj->error = array('error' => ApiError::DELETE_ORDER_FAILED);
                            }

                            // 把這張單的相關交易中Trading取消
                            //TradingRepository::updateState($order_id, 'cancel', 'admin-'.$admin_id.'-cancel');
                            $trading_db = Trading::where('order_id', $order_id)
                                    ->where('state','!=','deal')
                                    ->get();

                            $cur_time = Carbon::now();
                            for ($i=0; $i < count($trading_db) ; $i++) { 
                                $trading_db[$i]->state = 'cancel';
                                $trading_db[$i]->confirm_value = 'admin-'.$admin_id.'-cancel,'.$cur_time;
                                $trading_db[$i]->save();
                            }


                    } finally {
                        Lock::release($lock_key_order);
                    }        

                } else {
                    $obj->error = ApiError::ORDER_BUSY;
                }

            } finally {
                Lock::release($lock_key_wallet);
            }

            return $obj;
        }); // end db transaction    

        return $obj;
    }

    // ＊＊ 取消委託 ＊＊　
    public static function cancelOrder($order_id)
    {
        $result = array('error' => ApiError::SUCCESS);

        $order_db = Order::find($order_id);
        if(!$order_db){
            $result = array('error' => ApiError::ADMIN_FIND_ORDER_NULL); 
            return $result;              
        }

        // 把相關的trading單刪除
        $admin_id = Session::get(SessionNames::ADMIN_ID);
        static::cancelOrderTradings($order_db, $admin_id);

        // 刪除order
        $order_db->state = 'admin-'.$admin_id.'-cancel';
        $order_db->save();
        $order_db->delete();

        return $result;
    }


    // ＊＊ 取消交易單 ＊＊　
    public static function cancelTrading($trading_id)
    {
        $result = array('error' => ApiError::SUCCESS);

        $trading_db = Trading::find($trading_id);
        if(!$trading_db){
            $result = array('error' => ApiError::ADMIN_FIND_TRADING_NULL); 
            return $result;              
        }
        
        $admin_id = Session::get(SessionNames::ADMIN_ID);

        $cur_time = Carbon::now();
        $trading_db->state = 'admin-'.$admin_id.'-cancel';
        $trading_db->save();

        return $result;
    }


    // ＊＊ 取得 Recommend Tree ＊＊
    public static function GetRecommenderInfoByID($_target_ball_id)
    {
        $result = array('error' => ApiError::SUCCESS);

        //$user_ball_id = static::getBallID();// $user_db->id;
        $target_ball_id = $_target_ball_id;   
        $find_other_non_recommender_ball_db = 'none';
        // 樹頂
        if($target_ball_id == 0){  
            $find_top_ball_db = Ball::where('recommender_id',0)->first(); // 第一個recommender_id=0的就是一開始創的
            if(!$find_top_ball_db){
                $result = array('error' => ApiError::ADMIN_FIND_USER_ID_NULL); 
                return $result;
            }

            $target_ball_id = $find_top_ball_db->id;

            // 先把其他沒有上線的的帳號丟進來
            $find_other_non_recommender_ball_db = Ball::where('recommender_id',0)
                                        ->where('id','!=',$target_ball_id)
                                        ->get();
        } 

        $user_ball_id = $target_ball_id;// $user_db->id;

        $target_db = Ball::select("recommend_tree")
                ->where('id', $target_ball_id)
                ->first();

        // 無此target_ball_id
        if(!$target_db){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        // 檢查user有沒有權限看這個人
        $permission_pass = false;

        $target_tree_array = explode(',' , (string)$target_db->recommend_tree); // 無法信任like+%, 自己比對0.0!
        for ($i=0; $i < count($target_tree_array); $i++) { 
            if( $user_ball_id == $target_tree_array[$i])
            {
                $permission_pass = true;
                break;
            }
        }

        if( $user_ball_id == $target_ball_id){ // 自己看自己 也給權限
            $permission_pass = true;
        }

        if(!$permission_pass){
            $result = array('error' => ApiError::USER_GET_SPECIFIC_ID_ILLEGAL);
            return $result;
        }

        $first_db = Ball::where('id', $target_ball_id)
            ->first();

        $first_account_db = User::select("nickname","account")
                    ->where("id", $first_db->user_id)
                    ->first();
                    

        $obj_array = array(); // 頂端 root 自己key
        $index = 0;
        $obj_array[$index] = 
            OrganizationService::createRecommenderObj
                (
                $target_ball_id
                , $first_db->recommender_id
                //, $first_db->pv_left.'/'.$first_db->pv_right
                , 0
                , $first_db->performance // ($first_db->total_left + $first_db->total_right) 
                , $first_account_db->nickname
                , OrganizationService::getSetFromLevel($first_db->level)
                , $first_account_db->account
                , $first_db->ball_index
                );  // root 

        $index++;

        $brahcn_after =static::_getBranchAfter($obj_array, $index, $target_ball_id);
        $obj_array = $brahcn_after['array'];
        $index = $brahcn_after['index'];

        // 如果是看全部, 再把游離的帳號丟進來(也是recommendr id = 0 只顯示一層)
        if($find_other_non_recommender_ball_db != 'none'){

            for ($i=0; $i < count($find_other_non_recommender_ball_db); $i++) { 

                $find_account_db = User::select("nickname","account")
                ->where('id',$find_other_non_recommender_ball_db[$i]->user_id)
                ->first();

                $obj_array[$index] = OrganizationService::createRecommenderObj(
                        $find_other_non_recommender_ball_db[$i]->id      
                        ,$find_other_non_recommender_ball_db[$i]->recommender_id
                        ,0
                        ,$find_other_non_recommender_ball_db[$i]->performance
                        ,$find_account_db->nickname
                        ,OrganizationService::getSetFromLevel($find_other_non_recommender_ball_db[$i]->level)
                        ,$find_account_db->account
                        ,$find_other_non_recommender_ball_db[$i]->ball_index
                    );                

                $index++;

                $brahcn_after =static::_getBranchAfter($obj_array, $index, $find_other_non_recommender_ball_db[$i]->id);
                $obj_array = $brahcn_after['array'];
                $index = $brahcn_after['index'];                 

            }

        }


        // 再整理一次, 把同一個人底下的ball編號列出來
        // for ($i=0; $i < count($obj_array); $i++) { 

        //     $index = 0;
        //     for ($j=0; $j < count($obj_array); $j++) { 

        //         if($i == $j){  //自己不用比對
        //             continue; 
        //         }

        //         // 加編號
        //         if(
        //             $obj_array[$i]->nickname == $obj_array[$j]->nickname
        //             &&
        //             $obj_array[$i]->account == $obj_array[$j]->account
        //           )
        //         {            
        //             $index ++;        
        //             $obj_array[$j]->nickname = $obj_array[$j]->nickname.'('.$index.')';                    
        //         }

        //     }
        // }

        $result['data']['tree'] = json_encode($obj_array);

        return $result;
    }    


    // 取得下線資訊
    private static function _getBranchAfter($obj_array, $index, $target_ball_id){

        // 權限有了, 用like+%初步列出組織圖名單, 我不信任like+%
        $like_db = Ball::where('recommend_tree', 'like', '%'.$target_ball_id.'%')
                //->where('level', '>', '0')
                ->get();

                // 用逗號切成陣列,掃 recommend_tree, 比對target_ball_id有沒有在陣列中
        for ($i=0; $i < count($like_db) ; $i++) { 
            $like_db_tree_array = explode(',',(string)$like_db[$i]->recommend_tree);
            $count_tree = count($like_db_tree_array);
            for ($j=0; $j < $count_tree; $j++) { 

                // (idR || idL)  = branch_tree的最後一格
                if( $target_ball_id == $like_db_tree_array[$j] )
                {
                    $floor = $count_tree - $j;

                    // 秀三層內
                    if($floor <= 3){ 
                        $like_account_db = User::select("nickname","account")
                                ->where('id',$like_db[$i]->user_id)
                                ->first();

                        $obj_array[$index] = OrganizationService::createRecommenderObj(
                                                $like_db[$i]->id      
                                                ,$like_db[$i]->recommender_id
                                                //,$like_db[$i]->pv_left.'/'.$like_db[$i]->pv_right
                                                ,$floor
                                                ,$like_db[$i]->performance//($like_db[$i]->total_left + $like_db[$i]->total_right)
                                                ,$like_account_db->nickname
                                                ,OrganizationService::getSetFromLevel($like_db[$i]->level)
                                                ,$like_account_db->account
                                                ,$like_db[$i]->ball_index  
                                            );
                        $index++;
                    }
                } // RL
            }
        }

        $function_result['array'] = $obj_array;
        $function_result['index'] = $index;

        return $function_result;
    }

    // ＊＊ 取得 target_id 照規格排列的組織圖 + 極左極右資訊 ＊＊
    public static function getBranchInfoByID($_target_id)
    {
        $result = array('error' => ApiError::SUCCESS);         


        $target_id = $_target_id;        

        // 樹頂
        if($target_id == 0){  
            $find_top_ball_db = Ball::where('level','>',0)->where('branch_tree','')->first();         
            if(!$find_top_ball_db){
                $result = array('error' => ApiError::ADMIN_FIND_USER_ID_NULL); 
                return $result;
            }

            $target_id = $find_top_ball_db->id;
        } 


        // 開始沿用user那邊的寫法..
        $target_db = Ball::select("branch_tree")
                ->where('id', $target_id)
                ->first();

        // 無此target_id
        if(!$target_db){
            $result = array('error' => ApiError::ADMIN_FIND_USER_ID_NULL); 
            return $result;
        }

        // 沒有排序的樹(3層)和極左極右資訊
        $info = OrganizationService::getBranchInfoByID_withoutOrder($target_id, $target_id);
        $obj_array = $info['obj_array'];
        //$max_array = $info['max_array'];        
        
        $order_array = OrganizationService::setOrder15Branch($obj_array); // 15格順序排列, 空的填滿empty
        $order_array = OrganizationService::getCorrectLengthArray($order_array); // 上線是empty的修掉
        //$max_info = OrganizationService::getMaxRL($order_array, $max_array, $ball_id, $target_id); // max相關

        $result['data']['map'] = json_encode($order_array);
        // $result['data']['max_L_id'] = json_encode($max_info['max_L_id']);
        // $result['data']['max_R_id'] = json_encode($max_info['max_R_id']);
        // $result['data']['log'] = json_encode($max_info['log']);
        // $result['data']['is_user_root'] = json_encode($max_info['is_user_root']);

        return $result;
    }


    // ＊＊ 修改會員資料 ＊＊　
    public static function updateUserInfo($user_id, $nickname, $country_code, $phone, $new_pw, $bank_code, $bank_branch, $bank_account)
    {

        $result = array('error' => ApiError::SUCCESS);

        $user_db = User::find($user_id);
        if(!$user_db){
            $result = array('error' => ApiError::TARGET_USER_NULL); 
            return $result;              
        }

        $user_db->nickname = $nickname;
        $user_db->country_code = $country_code;
        $user_db->phone = $phone;

        //var_dump("AAAAAAAAAAAAAAAA "+$new_pw);

        if($new_pw != 0){
            $user_db->password = password_hash($new_pw, PASSWORD_DEFAULT); 
        }
        $user_db->bank_code = $bank_code;
        $user_db->bank_branch = $bank_branch;
        $user_db->bank_account = $bank_account;
        $user_db->save();

        return $result;
    }


    // ＊＊ 限制儲值(開關) ＊＊
    public static function switchLimitedWithdraw($is_limited)
    {

        $result = array('error' => ApiError::SUCCESS);

        // $system_db = System::orderBy('id','asc')
        //         ->first();

        // $system_db->is_limited_pay = $is_limited_pay;
        // $system_db->save();

        $is_tagged = 0;
        if($is_limited==1){
            $is_tagged = TagRepository::create(0, VerifyTags::ALL_USERS_LIMITED_WITHDRAW);
        } 

        if($is_limited==0){
            $is_tagged = TagRepository::delete(0, VerifyTags::ALL_USERS_LIMITED_WITHDRAW);
        } 

        $result['data'] = $is_tagged;
        return $result;
    }



    // ＊＊ 取得會員資訊 ＊＊　
    public static function getUserAccountInfo($user_id)
    {
        $result = array('error' => ApiError::SUCCESS);
        
        $user_db = User::all()->where('id', $user_id)
                    ->first();    
        
        if(!$user_db){
            $result = array('error' => ApiError::TARGET_USER_NULL); 
            return $result;  
        }

        $obj = new \stdClass();
        $obj->uid = $user_db->id;
        $obj->account = $user_db->account;
        $obj->nickname = $user_db->nickname;
        $obj->country_code = $user_db->country_code;
        $obj->phone = $user_db->phone;
        $obj->is_limited_pay = $user_db->is_limited_pay;
        $obj->bank_code = $user_db->bank_code;
        $obj->bank_branch = $user_db->bank_branch;
        $obj->bank_account = $user_db->bank_account;
        $obj->is_frozen = $user_db->is_frozen;



        $result['data'] = json_encode($obj);

        return $result;
    }

    // ＊＊ 登入 ＊＊　
    public static function login($account, $password)
    {
        $result = array('error' => ApiError::SUCCESS);
        
        $admin_db = Admin::select("id","account","password") // 找此會員
                    ->where('account', strtolower($account))
                    ->first();    
        
        if(!(isset($admin_db) && password_verify($password, $admin_db->password))){ // 密碼不符 / 會員不存在
            $result = array('error' => ApiError::ADMIN_LOGIN_ERROR); 
            return $result;
        }

        Session::put(SessionNames::ADMIN_ID, $admin_db->id);  // 成功 jobs
        static::createToken();
        $obj = new \stdClass();
        $obj->id = $admin_db->id;
        $result['data'] = json_encode($obj);
        //Response::Json($result);
        return $result;
    }


    // ＊＊ 登出 ＊＊
    public static function logout()
    {
        Session::forget(SessionNames::ADMIN_ID);
        Session::forget(SessionNames::TOKEN);
        $result = array('error' => ApiError::SUCCESS); // 目前沒有想到有其他錯誤方式..
        return $result;
    }


    // ＊＊ 更改每簡訊驗證上限 ＊＊
    public static function updateMaxVerifyMessage($max_verify_times)
    {
        $result = array('error' => ApiError::SUCCESS);

        $system_db = System::orderBy('id','asc')
                ->first();

        $system_db->daily_max_phone_verify = $max_verify_times;
        $system_db->save();
        return $result;
    }


    // ＊＊ 取得每簡訊驗證上限 ＊＊
    public static function getMaxVerifyMessage()
    {
        $result = array('error' => ApiError::SUCCESS);

        $system_db = System::orderBy('id','asc')
                ->first();

        $result['data']['value'] = $system_db->daily_max_phone_verify;

        return $result;
    }


    // ＊＊ 回收點數 ＊＊　
    public static function recyclePoint($wallet_id, $user_id, $point)
    {
        $result = array('error' => ApiError::SUCCESS);
        $result['data'] = $wallet_id.','.$point;

        $wallet_db = Wallet::all()
                ->where('id', $wallet_id)
                ->where('coin','Point')
                ->first();

        if(!$wallet_db){
            $result = array('error' => ApiError::USER_ID_NULL); 
            return $result;
        }

        // 檢查點數夠不夠
        if((float)$point > (float)$wallet_db->balance_available){
            $result = array('error' => ApiError::POINT_NOT_ENOUGH); 
            return $result;
        }

        $before =  $wallet_db->balance_available;
        $wallet_db->balance_available = $wallet_db->balance_available - $point;
        $wallet_db->save();

        // 寫單
        $transaction_db = new Transaction;
        $transaction_db->user_id = $user_id;
        $transaction_db->wallet_id = $wallet_id;
        $transaction_db->coin =  'Point';
        $transaction_db->type = 'Admin-Recycle-Point';
        $transaction_db->value = $point;
        $transaction_db->before = $before;
        $transaction_db->after = $wallet_db->balance_available;
        $transaction_db->link = "";
        $transaction_db->save();

        // $admin_db = Admin::select("id","account","password") // 找此會員
        //             ->where('account', strtolower($account))
        //             ->first();    
        
        // if(!(isset($admin_db) && password_verify($password, $admin_db->password))){ // 密碼不符 / 會員不存在
        //     $result = array('error' => ApiError::ADMIN_LOGIN_ERROR); 
        //     return $result;
        // }

        // Session::put(SessionNames::ADMIN_ID, $admin_db->id);  // 成功 jobs
        // static::createToken();
        // $obj = new \stdClass();
        // $obj->id = $admin_db->id;
        // $result['data'] = json_encode($obj);
        //Response::Json($result);
        return $result;
    }



    // ＊＊ 修改會員密碼 ＊＊
    public static function updateAdminPassword($old_pw, $new_pw, $new_pw2)
    {
        $result = array('error' => ApiError::SUCCESS);

        $admin_id = Session::get(SessionNames::ADMIN_ID); 
        $admin_db = Admin::find($admin_id);

        if(!$admin_db){
            $result = array('error' => ApiError::ADMIN_NULL); // 登入者ID不存在
            return $result;
        }

        if( !password_verify($old_pw, $admin_db->password)){ // 密碼錯誤
            $result = array('error' => ApiError::ADMIN_PASSWORD_FAILED); 
            return $result;
        }   

        $admin_db->password = password_hash($new_pw, PASSWORD_DEFAULT); 
        $admin_db->save();

        $result['data'] = 'updatePW ok';//json_encode($user_db);
        return $result;
    }



}