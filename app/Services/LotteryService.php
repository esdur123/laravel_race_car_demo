<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Models\Transaction;

use App\Services\Service;
use App\Services\WalletService;
use App\Services\OrganizationService;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;

class LotteryService extends Service
{



    // ＊＊ 九宮格抽抽樂 (10次) ＊＊
    public static function play9Block10Times($user_account_id)
    {

        $result = array('error' => ApiError::SUCCESS);
        $total_times = 10;

        $wallet_db = Wallet::where('user_id', $user_account_id)
                    ->where('coin','Point')
                    ->first();

        if(!$wallet_db){
            $result['error'] = ApiError::TARGET_USER_NULL;
            return $result;
        }

        if($wallet_db->balance_available < 50){
           $result['error'] = ApiError::POINT_NOT_ENOUGH;
            return $result; 
        }

        $total_win_point = 0;
        $i_prize_array = array();
        for ($i=0; $i < $total_times; $i++) { 
            $i_prize_array[$i] = static::getPrize();
            $total_win_point += $i_prize_array[$i];
            if($i_prize_array[$i] != 0){
                $prize[$i] = "point" . $i_prize_array[$i];//"point4"; // 獲得獎項
            } else {
                $prize[$i] = "thankyou";//"point4"; // 獲得獎項
            }
        }

        $i_random_array = array();
        for ($i=0; $i < $total_times; $i++) { 
            $i_random_array[$i] = rand(0,8);
        }


        $obj_array = array(); 
        for ($i=0; $i < $total_times ; $i++) {  

            // 塞8個假的
            $box[0] = "thankyou";
            $box[1] = "point".rand(1,10);
            $box[2] = "point".rand(1,10);
            $box[3] = "point".rand(1,10);
            $box[4] = "point".rand(1,10);
            $box[5] = "japan1";
            $box[6] = "gogoro2";
            $box[7] = "iphonex";
            $count_box = 8;

            $obj = new \stdClass();
            for ($j=0; $j < 9; $j++) { 
                
                if( $j == $i_random_array[$i]){
                     $obj->block[$j] = $prize[$i];
                } else{            

                     $j_chosen = rand(0,$count_box-1);

                     $obj->block[$j] = $box[$j_chosen];
                    
                     // // 把box拿掉選過的
                     unset($box[$j_chosen]);
                     $box = array_values($box);
                     $count_box--;
                     continue;
                }
            }

            $obj_array[$i] = $obj;
        }


        $result['data']['i_random_array'] = json_encode($i_random_array);
        $result['data']['obj_array']      = json_encode($obj_array);
        $result['data']['prize_array']    = json_encode($i_prize_array);//substr($prize, -1);
        //$result['data']['wallet_point']    = $wallet_db->balance_available + $total_win_point - 50;

        // 算點+錢包紀錄
        $wallet_db = WalletRepository::play9Block10Times($user_account_id, $total_win_point);
        $result['data']['wallet_point'] = $wallet_db->balance_available;


        return $result;
    }


    // ＊＊ 九宮格抽抽樂 (單次) ＊＊
    public static function play9Block($user_account_id, $i_click)
    {

        $result = array('error' => ApiError::SUCCESS);

        $wallet_db = Wallet::where('user_id', $user_account_id)
                    ->where('coin','Point')
                    ->first();
                    
        if(!$wallet_db){
            $result['error'] = ApiError::TARGET_USER_NULL;
            return $result;
        }

        if($wallet_db->balance_available < 5){
           $result['error'] = ApiError::POINT_NOT_ENOUGH;
            return $result; 
        }

        //$result['data'] = $user_account_id;
        $i_prize = static::getPrize();
        if($i_prize != 0){
            $prize = "point" . $i_prize;//"point4"; // 獲得獎項
        } else {
            $prize = "thankyou";//"point4"; // 獲得獎項
        }
        

        // 塞8個假的
        $box[0] = "thankyou";
        $box[1] = "point".rand(1,10);
        $box[2] = "point".rand(1,10);
        $box[3] = "point".rand(1,10);
        $box[4] = "japan1";
        $box[5] = "thankyou";
        $box[6] = "gogoro2";
        $box[7] = "iphonex";
        $count_box = 8;

        $obj = new \stdClass();

        for ($i=0; $i < 9; $i++) { 
            if( $i == $i_click){
                $obj->block[$i] = $prize;
            } else{
                $ichosen = rand(0,$count_box-1);

                $obj->block[$i] = $box[$ichosen];
                
                // 把box拿掉選過的
                unset($box[$ichosen]);
                $box = array_values($box);
                $count_box--;
                continue;
            }
            # code...
        }

        $result['data']['obj'] = json_encode($obj);
        $result['data']['prize'] = $i_prize;//substr($prize, -1);

        // 算點+錢包紀錄
        $wallet_db = WalletRepository::lottery_9Block($user_account_id, $i_prize);
        $result['data']['wallet_point'] = $wallet_db->balance_available;

        return $result;
    }


    // ＊＊ 獲得獎項 ＊＊
    private static function getPrize()
    {
        $prize[0]  = 1;
        $prize[1]  = 2;
        $prize[2]  = 3;
        $prize[3]  = 4;
        $prize[4]  = 5;
        $prize[5]  = 6;
        $prize[6]  = 7;
        $prize[7]  = 8;
        $prize[8]  = 9;
        $prize[9]  = 10;
        $prize[10] = 4; 
        $prize[11] = 4;
        $prize[12] = 4;
        $prize[13] = 4;
        $prize[14] = 3;
        $prize[15] = 3;
        $prize[16] = 2;
        $prize[17] = 1;
        $prize[18] = 0; // 銘謝惠顧
        $prize[19] = 10;
        // 90 期望值 4.5

        $ichosen = rand(0,19);

        return $prize[$ichosen]; 
    }
}