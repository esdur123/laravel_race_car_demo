<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

use App\Defined\ApiError;
use App\Defined\SessionNames;

use App\Models\Trading;
use App\Models\User;

use App\Services\Service;
use App\Services\UserService;

class TradingService extends Service
{

    // ＊＊ 確認收款/付款 ＊＊　
    public static function confirmTrading($trading_id, $user_id, $type)
    {
        $result = array('error' => ApiError::SUCCESS);
        $result['data'] = 'un-deal';
        
        $trading_db = Trading::find($trading_id);
        
        if(!$trading_db){ // 確認交易存在
            $result['error'] = ApiError::TRADING_NULL;
            return $result;
        }

        if($trading_db->state == 'cancel'){ // 確認交易仍進行中
            $result['error'] = ApiError::TRADING_NULL;
            return $result;
        }

        if($type == 'buy'){ // 確認買家/賣家 id正確
            if($trading_db->buyer_user_id != $user_id){
                $result['error'] = ApiError::CONFIRM_TRADING_ILLEGAL;
                return $result;
            }
        } else if($type == 'sell'){
            if($trading_db->seller_user_id != $user_id){
                $result['error'] = ApiError::CONFIRM_TRADING_ILLEGAL;
                return $result;
            }
        } else {
            $result['error'] = ApiError::CONFIRM_TRADING_ILLEGAL;
            return $result; 
        }

        
        $result['trading_id'] = $trading_db->id;
        $confirm_array = explode(",",$trading_db->confirm_value); 


        if($type == 'buy'){
          
            if($confirm_array[0] == 'none'){
                $trading_db->confirm_value = 'buyer-confirm'.','.Carbon::now();
            }

            if($confirm_array[0] == 'seller-confirm'){
                $trading_db->confirm_value = 'deal'.','.Carbon::now();
                $trading_db->state = 'deal';
                $result['data'] = 'deal';               
            }            
        }

        if($type == 'sell'){
            
            if($confirm_array[0] == 'none'){
                $trading_db->confirm_value = 'seller-confirm'.','.Carbon::now();           
            }

            if($confirm_array[0] == 'buyer-confirm'){
                $trading_db->confirm_value = 'deal'.','.Carbon::now();
                $trading_db->state = 'deal';
                $result['data'] = 'deal';               
            }            
        }

        $trading_db->save();

        return $result;
    }
}