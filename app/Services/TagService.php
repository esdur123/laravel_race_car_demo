<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\Tag;

use App\Services\Service;
use App\Repositories\TagRepository;

class TagService extends Service
{



    // ＊＊ 檢查認證 ＊＊
    public static function verify($user_account_id, $tag)
    {

        $result = array('error' => ApiError::SUCCESS);

        $is_tagged = 1;
        $verify_db = Tag::where('user_id', $user_account_id)
                        ->where('tag',$tag)
                        ->first();
        if(!$verify_db){
            $is_tagged = 0;
        }

        return $is_tagged;
    }




}