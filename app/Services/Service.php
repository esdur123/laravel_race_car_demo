<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use App\Defined\SessionNames;
use Firebase\JWT\JWT;

class Service
{

    // ＊＊ 建Token ＊＊　
    public static function createToken(){

        $key = env('JWT_KEY');

        $token = array(
            "session" => Session::getId(),
            "expire" => strtotime("30 min"),
            "issueAt" => strtotime("now")
        );

        $token = JWT::encode($token, $key);

        Session::put(SessionNames::TOKEN, $token);
    }


    // ＊＊ 檢查正整數 ＊＊　
    public static function checkPositiveInteger($str)   
    {
        $return = array('error' => ApiError::SUCCESS);
        if(0 === strcmp($str   ,   (int)$str)){           
            if((int)$str < 0){
                $return['error'] = ApiError::ILLEGAL_VALUES;
            }

        } else {
            $return['error'] = ApiError::ILLEGAL_VALUES;
        }

        return  $return;
    }




    // ＊＊ 將字串部分內容替換成星號或其他符號 ＊＊　
    public static function replace_symbol_text($string, $symbol, $begin_num = 0, $end_num = 0){

        $string_length = strlen($string);
        $begin_num = (int)$begin_num;
        $end_num = (int)$end_num;
        $string_middle = '';

        $check_reduce_num = $begin_num + $end_num;

        if($check_reduce_num >= $string_length){
        for ($i=0; $i < $string_length; $i++) {
        $string_middle .= $symbol;
        }
        return $string_middle;
        }

        $symbol_num = $string_length - ($begin_num + $end_num);
        $string_begin = substr($string, 0,$begin_num);
        $string_end = substr($string, $string_length-$end_num);

        for ($i=0; $i < $symbol_num; $i++) {
        $string_middle .= $symbol;
        }

        return $string_begin.$string_middle.$string_end;
    }







}
