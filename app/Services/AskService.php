<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;
use App\Defined\AskRules;

use App\Models\Ask;

use App\Services\Service;
use App\Repositories\AskRepository;
use Carbon\Carbon;


class AskService extends Service
{



    // ＊＊ 檢查認證 ＊＊
    public static function apply($user_id, $title, $comment)
    {

        $result = array('error' => ApiError::SUCCESS);

        // 檢查今日提問次數
        $check_limit_db = Ask::where('user_id', $user_id)
                    ->whereDate('created_at', Carbon::today())
                    ->get();

        $daily_count = 0;
        if($check_limit_db){
            $daily_count = count($check_limit_db);
        }
        
        if($daily_count >= (int)AskRules::DAILY_MAX_QUESTIONS){
            $result['error'] = ApiError::OVER_DAILY_MAX_QUESTION_ASK;
            return $result;
        }

        $ask_db = AskRepository::create($user_id, $title, $comment);

        return $result;
    }




}