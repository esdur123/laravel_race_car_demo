<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;
use App\Defined\WithdrawStates;

use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Models\OrderWithdraw;

use App\Services\Service;
use App\Services\WalletService;
use App\Services\OrganizationService;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;
use App\Repositories\OrderWithdrawRepository;
use Lock;

class WithdrawService extends Service
{

/*
    const WAITING = 'Waiting';
    const DEAL = 'Deal';    
    const USER_CANCEL = 'User-Canceled';
    const ADMIN_CANCEL = 'Admin-Canceled';   

*/

    // ＊＊ 使用者撤銷 ＊＊
    public static function user_cancel($user_id, $withdraw_id)
    {

        $result = array('error' => ApiError::SUCCESS);

        // Lock前, 先確認withdraw db 和 wallet db存在
        $find_withdraw_db = OrderWithdraw::where('id', $withdraw_id)
                        ->where('user_id', $user_id)
                        ->first();

        if(!$find_withdraw_db){
            $result['error'] = ApiError::WITHDRAW_ID_ERROR;
            return $result;
        }

        $find_wallet_db = Wallet::where('user_id', $user_id)
                        ->first();

        if(!$find_wallet_db){
            $result['error'] = ApiError::USER_ID_NULL;
            return $result;
        }

        // 第一層 Withdraw
        $lock_withdraw_key = 'Withdraw@user_id:' . $user_id;
        try {
            Lock::acquire($lock_withdraw_key);

            // 第二層 Wallet
            $lock_wallet_key = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_wallet_key);

                // Withdraw
                $withdraw_db = OrderWithdraw::where('id', $withdraw_id)
                            ->where('user_id', $user_id)
                            ->first();

                $withdraw_db->state = WithdrawStates::USER_CANCEL;
                $withdraw_db->save();

                // Wallet
                $wallet_db = Wallet::where('user_id', $user_id)
                            ->first();

                $wallet_db->balance_locked -= $withdraw_db->amount;  
                $wallet_db->balance_available += $withdraw_db->amount;
                $wallet_db->save();


            } finally {
                Lock::release($lock_wallet_key);
            }

        } finally {
            Lock::release($lock_withdraw_key);
        }

        return $result;       
    }


    // ＊＊ 申請提領 ＊＊
    public static function apply($user_id, $amount)
    {
    	$C_WITHDRAW_FEE = 10; // 手續費10

        $result = array('error' => ApiError::SUCCESS);   

       	// Lock前, 先確認wallet db存在
        $find_wallet_db = Wallet::where('user_id', $user_id)
        				->where('coin','Point')
    					->first();

        if(!$find_wallet_db){
        	$result['error'] = ApiError::USER_ID_NULL;
        	return $result;
        }

        // 檢查可用餘額夠不夠0.0
        if($find_wallet_db->balance_available < $amount){
        	$result['error'] = ApiError::POINT_NOT_ENOUGH;
        	return $result;
        }

        // Lock 把錢鎖住先
        $lock_key = 'Wallet@user_id:' . $user_id;
        try {
            Lock::acquire($lock_key);

            // logic
            $wallet_db = Wallet::where('user_id', $user_id)
        				->where('coin','Point')
    					->first();

            $wallet_db->balance_available -= $amount;
            $wallet_db->balance_locked += $amount;
            $wallet_db->save();

        } finally {
            Lock::release($lock_key);
        }

        $OrderWithdraw_db = OrderWithdrawRepository::create($user_id, $amount);

        //$result['data'] = $amount;
        return $result;       
    }




}