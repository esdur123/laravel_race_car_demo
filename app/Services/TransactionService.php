<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\TransactionTypes;
use App\Defined\PointRules;


use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Models\ActiveRecord;

use App\Models\Transaction;
use App\Models\Trading;
use App\Models\Order;

use App\Services\Service;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;
use App\Repositories\ActiveRecordRepository;



use App\Repositories\OrderRepository;

class TransactionService extends Service
{


    // ＊＊ 撤回Order單＊＊　
    public static function cancelOrder($user_id, $order_id)
    {
        $result = array('error' => ApiError::SUCCESS);

        // 如果這張order單目前有交易 不給calcel
        // $trading_db = Trading::where('order_id', $order_id)
        //                 ->where('state','waiting')
        //                 ->first();

        // if($trading_db){
        //     $result['error'] = ApiError::STILL_HAVE_TRADING_ON_THIS_ORDER;
        //     return  $result;
        // }

        // order 不存在或已撤銷
        $order_db = Order::find($order_id);
        if(!$order_db){
            $result['error'] = ApiError::ORDER_NULL;
            return  $result;
        }

        if($order_db->deleted_at){
          $result['error'] = ApiError::ORDER_DELETED;
            return  $result;  
        }


        $create_result = WalletRepository::cancel_order($user_id, $order_id);
        $result['error'] = $create_result->error;
        //$result['data'] = 'user_id = '.$user_id.' , value = '.$value;
    
        return $result;
    }


    // ＊＊ 創 Order單＊＊　
    public static function createOrder($amount)
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_id = Session::get(SessionNames::USER_ID); // 登入者ID

        // 檢查wallet戶頭剩多少

        $create_result = WalletRepository::create_order($user_id, $amount);
        $result['error'] = $create_result->error;
        //$result['data'] = 'user_id = '.$user_id.' , value = '.$value;
    
        return $result;
    }


    // ＊＊ 開通配套 (升級) ＊＊　
    public static function activeSet($user_account_id, $ball_id, $new_lvl)
    {

        // 捨棄 $set 0.0
        $result = array('error' => ApiError::SUCCESS);

        $user_db = User::find($user_account_id);
        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL); 
            return $result;
        }

        $ball_db = Ball::find($ball_id);
        // $ball_db = Ball::all()
        // ->where('user_id', $user_account_id)
        // ->first();


        // 檢查目前配套是否低於要求
        $current_lvl = $ball_db->level;

        if($current_lvl >= $new_lvl){
            $result = array('error' => ApiError::ACTIVE_SET_TOO_LOW); 
            return $result;
        }

        // 檢查點數是否足夠        
        $wallet_db = Wallet::select("id","balance_available","balance_locked")
        ->where('user_id', $user_account_id)
        ->where('coin','Point')
        ->first();

        // 真正要扣的point 
        //$C_COLOR_POINT_PERCENTAGE = 0;//(float)(env('COLOR_POINT_PERCENTAGE'))*0.01; // 彩點比例>0時, 10%用彩點
        $new_set = OrganizationService::getSetFromLevel($new_lvl);
        $old_set = OrganizationService::getSetFromLevel($current_lvl);

        $pay_point =  $new_set - $old_set; 
        //$pay_colorpoint = 0;
        // if($C_COLOR_POINT_PERCENTAGE > 0){ // 彩點情況
        //     $pay_colorpoint = $C_COLOR_POINT_PERCENTAGE * $pay_point;
        //     $pay_point = (1-$C_COLOR_POINT_PERCENTAGE) * $pay_point;
        // }        

        if($wallet_db->balance_available < $pay_point){
            $result = array('error' => ApiError::ACTIVE_SET_WALLET_NOT_ENOUGH); 
            return $result;
        }

        // 未激活補 branch 和 branch_tree
        if($current_lvl == -1){
            
            // 篩選掉無上線的情況
            if($ball_db->recommender_id != 0){
                // 計算這個人 "所有相關recommender" 的業績
                //$recommender_ball_id, $target_ball_id, $add_set                
                // $branch_info = OrganizationService::getInactivedBranch($user_db->recommender_id);
                // $ball_db->branch_tree = $branch_info['branch_tree'];
                // $ball_db->branch = $branch_info['branch'];
            }

            
            $ball_db->created_at = Carbon::now();// 開通時間
            $ball_db->save();
        }

        // 更新所有影響上線的業績
        OrganizationService::update_upline_performance($ball_db->recommend_tree, $pay_point);

        // 到此ok
        // 交易紀錄
        $type = TransactionTypes::STATIC_SET;
        $before = $wallet_db->balance_available;
        $lock_wallet_db = WalletRepository::deposit_by_userID($user_account_id, -$pay_point, 'Point');

        // 交易紀錄 (提出方)
        TransactionRepository::create(
                                      $user_account_id
                                      , $wallet_db->id
                                      , 'Point'
                                      , $type
                                      , $pay_point
                                      , $lock_wallet_db->before
                                      , $lock_wallet_db->after
                                      , ""
                                     );

        $ball_db->level = $new_lvl;

        // 改出局上限 (預計提升的靜態額度=新額度-舊額度)
        $add_limit = 0;
        if($new_lvl == 1) {
            $add_limit = $new_set * (int)PointRules::STATIC_OUT_RATE_LVL_1;            
        }

        if($new_lvl == 2) {
            $add_limit
              = ($new_set * (int)PointRules::STATIC_OUT_RATE_LVL_2 )- ($old_set * (int)PointRules::STATIC_OUT_RATE_LVL_1);
        }

        $ball_db->static_max += $add_limit;
        $ball_db->save();

        // 紀錄開通時間和金額        
        // $find_presence_active_record = ActiveRecord::where('ball_id', $ball_id)->first();  // 若是第一次開通, 要多一個首次開通時間
        // if(!$find_presence_active_record){ 
        //     $active_db_first_create = ActiveRecordRepository::create($user_account_id, $ball_id, 0);
        // }
        
        $active_db = ActiveRecordRepository::create($user_account_id, $ball_id, $pay_point);

        return $result;
    }


    // ＊＊ 贈送點數 ＊＊　
    public static function depositPoints($coin, $from_account_id, $to_account_id, $value, $is_check_point_enough)
    {

        $type = 'Gift';

        $result = array('error' => ApiError::SUCCESS);

        // 檢查點數是否足夠
        if($is_check_point_enough){
            // 檢查recommender目前Wallet點數夠不夠
            $recommender_wallet_db = Wallet::select("balance_available")
                                ->where('coin', $coin)
                                ->where('user_id', $from_account_id)
                                ->first();

            // 檢查自己點數夠不夠
            if($recommender_wallet_db->balance_available < $value){
                $return['error'] = ApiError::POINT_NOT_ENOUGH;
                return $return;
            }
        }

        $from_wallet_db = Wallet::Select("balance_available","id")
                        ->where("user_id", $from_account_id)
                        ->where('coin', $coin)
                        ->first();

        $to_wallet_db = Wallet::Select("balance_available","id")
                        ->where("user_id", $to_account_id)
                        ->where('coin', $coin)
                        ->first();

        if($coin == 'ColorPoint'){
            if(is_null($from_wallet_db) ){

                $ball_db = Ball::where('user_id', $from_account_id)->first();
                if(!$ball_db){
                    $result = array('error' => ApiError::FROM_ACCOUNT_ERROR); 
                    return $result;
                }

                // 沒有就創給他, lock 寫在 WalletRepository 
                $from_wallet_db = WalletRepository::create($from_account_id, $ball_db->id ,$coin);
            }

            if(is_null($to_wallet_db)){

                $ball_db = Ball::where('user_id', $to_account_id)->first();
                if(!$ball_db){
                    $result = array('error' => ApiError::TO_ACCOUNT_ERROR); 
                    return $result;
                }

                // 沒有就創給他, lock 寫在 WalletRepository 
                $to_wallet_db = WalletRepository::create($to_account_id, $ball_db->id ,$coin);
            }            
        }

        if( is_null($from_wallet_db) ){
            $result = array('error' => ApiError::FROM_ACCOUNT_ERROR);
            return $result;
        }
    
        if( is_null($to_wallet_db) ){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        $from_wallet_before = $from_wallet_db->balance_available;
        $from_wallet_after = $from_wallet_db->balance_available; // 給初值
        $from_wallet_db_id = $from_wallet_db->id;

        $to_wallet_before = $to_wallet_db->balance_available; 
        $to_wallet_after = $to_wallet_db->balance_available;  // 給初值
        $to_wallet_db_id = $to_wallet_db->id;

        // 扣款
        $lock_from_wallet_db = WalletRepository::deposit_by_userID($from_account_id, -$value, $coin);
        $from_wallet_after = $lock_from_wallet_db->balance_available;
       
        // 收點
        $lock_to_wallet_db = WalletRepository::deposit_by_userID($to_account_id, $value, $coin);
        $to_wallet_after = $lock_to_wallet_db->balance_available; 

        // 交易單 from
        $obj_from = new \stdClass(); 
        $obj_from->connect_id = $to_account_id;
        $from_account_db = User::Select("account","id")
                ->where("id", $to_account_id)
                ->first();

        $from_ball_db = Ball::select('id')
                ->where('user_id', $from_account_db->id)
                ->first();                       
        $obj_from->comment =$from_account_db->account.','.$from_ball_db->id;
        TransactionRepository::create(
                              $from_account_id
                              , $from_wallet_db_id
                              , $coin
                              , $type.'-From'
                              , $value
                              , $lock_from_wallet_db->before
                              , $lock_from_wallet_db->after
                              , json_encode($obj_from)
                             );

        // 交易單 to
        $obj_to = new \stdClass(); 
        $obj_to->connect_id = $from_account_id;
        $to_account_db = User::Select("account","id")
                ->where("id", $from_account_id)
                ->first();
                
        $to_ball_db = Ball::select('id')
                ->where('user_id', $to_account_db->id)
                ->first();                     

        $obj_to->comment = $to_account_db->account.','.$to_ball_db->id;  
        TransactionRepository::create(
                              $to_account_id
                              , $to_wallet_db_id
                              , $coin
                              , $type.'-To' //
                              , $value
                              , $lock_to_wallet_db->before
                              , $lock_to_wallet_db->after
                              , json_encode($obj_to)
                             );
        return $result;
    }


}