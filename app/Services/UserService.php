<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\VerifyTags;

use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;

use App\Models\Trading;

use App\Services\Service;
use App\Services\OrganizationService;
use App\Services\TransactionService;
use App\Services\WalletService;

use App\Services\TagService;
use App\Services\PhoneVerifyService;


use App\Repositories\TradingRepository;
use App\Repositories\UserRepository;
use App\Repositories\BallRepository;
use App\Repositories\WalletRepository;
use App\Repositories\TagRepository;
use App\Repositories\ActiveRecordRepository;

class UserService extends Service
{



    // ＊＊ 無上線註冊 ＊＊
    public static function registerNoUpline($account, $nickname, $country_code, $phone, $pw)
    {
        $result = array('error' => ApiError::SUCCESS);

        $_account = strtolower($account); // 強制存小寫

        $check_account_db = User::all()
        ->where('account', $_account)
        ->first();

        if($check_account_db){ // 帳號重複
            $return['error'] = ApiError::USER_ACCOUNT_DOUBLE_APPLY;
            return $return;
        }

        // User db
        $user_db = UserRepository::create(
                                         0 //$recommender_id
                                         , $account
                                         , $nickname
                                         , $country_code
                                         , $phone
                                         , $pw
                                         );

        $ball_id        = OrganizationService::registerNoUpline($user_db->id);
        $wallet_id      = WalletService::register($user_db->id, $ball_id,'Point');
        //$colorwallet_id = WalletService::register($user_db->id, $ball_id, 'ColorPoint');


        if( is_null($ball_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_02;
            return $return;
        }

        if( is_null($wallet_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_03;
            return $return;
        }

        $user_db->ball_id = $ball_id;
        $user_db->save();

        // 紀錄開通時間和金額
        //$active_db = ActiveRecordRepository::create($user_db->id, $ball_id, 0);

        $result['data'] = 'new_user_id='.$user_db->id;

        return $result;
    }


    // ＊＊ 推薦人註冊(配套一律為0) ＊＊
    public static function registerFromRecommender($recommender_id, $account, $nickname, $country_code, $phone, $pw)
    {
        $result = array('error' => ApiError::SUCCESS);

        $_account = strtolower($account); // 強制存小寫

        $check_account_db = User::all()
        ->where('account', $_account)
        ->first();

        if($check_account_db){ // 帳號重複
            $return['error'] = ApiError::USER_ACCOUNT_DOUBLE_APPLY;
            return $return;
        }

        // User db
        $user_db = UserRepository::create(
                                         $recommender_id
                                         , $account
                                         , $nickname
                                         , $country_code
                                         , $phone
                                         , $pw
                                         );

        $ball_id        = OrganizationService::registerFromRecommender($user_db->id, $recommender_id);
        $wallet_id      = WalletService::register($user_db->id, $ball_id,'Point');


        if( is_null($ball_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_02;
            return $return;
        }

        if( is_null($wallet_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_03;
            return $return;
        }

        $user_db->ball_id = $ball_id;
        $user_db->save();

        // 紀錄開通時間和金額
        //$active_db = ActiveRecordRepository::create($user_db->id, $ball_id, 0);

        $result['data'] = 'new_user_id='.$user_db->id;

        return $result;
    }


    // ＊＊ 取得要存入Trading單的個人資烙格式 ＊＊　
    public static function getTradingUserInfo($user_id)
    {
        $return_info = "-,-,-,-,-,-,-";
        $user_db = User::find($user_id);

        if(!$user_db){
            return $return_info;
        }

        $return_info = $user_db->id.','.$user_db->nickname.','.$user_db->country_code.','
                .$user_db->phone.','.$user_db->bank_code.','.$user_db->bank_branch.','
                .$user_db->bank_account;
        return $return_info;
    }


    // ＊＊ 註冊(極左極右) ＊＊
    public static function register($recommender_id, $account, $nickname, $country_code, $phone, $level)
    {
        $result = array('error' => ApiError::SUCCESS);

        $set_value = OrganizationService::getSetFromLevel($level);

        $_account = strtolower($account); // 強制存小寫

        $check_account_db = User::all()
        ->where('account', $_account)
        ->first();

        if($check_account_db){ // 帳號重複
            $return['error'] = ApiError::USER_ACCOUNT_DOUBLE_APPLY;
            return $return;
        }

        // 未激活帳號不開放註冊下線
        $recommender_ball_db = Ball::select('level')
                            ->where('user_id', $recommender_id)
                            ->first();

        if($recommender_ball_db->level <= 0){
            $return['error'] = ApiError::INACTIVED_ACCOUNT_ILLEGAL_REGISTER_DOWNLINE;
            return $return;
        }

        // 檢查recommender目前Wallet點數夠不夠
        $recommender_wallet_db = Wallet::select("id","balance_available")
                            ->where('user_id', $recommender_id)
                            ->where('coin','Point')
                            ->first();

        if($recommender_wallet_db->balance_available < $set_value){
            $return['error'] = ApiError::POINT_NOT_ENOUGH;
            return $return;
        }

        // 註冊
        $new_user_db = UserRepository::create(
                                             $recommender_id
                                             , $_account // 已強制小寫
                                             , $nickname
                                             , $country_code
                                             , $phone
                                             , '123456'
                                             );

        if(!$new_user_db){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_01;
            return $return;
        }

        $new_ball_id   = OrganizationService::register($new_user_db->id, $recommender_id);
        $new_wallet_id = WalletService::register($new_user_db->id, $new_ball_id,'Point');
        //$colorwallet_id = WalletService::register($new_user_db->id, $new_ball_id,'ColorPoint');

        if( is_null($new_ball_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_02;
            return $return;
        }

        if( is_null($new_wallet_id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_03;
            return $return;
        }


        $new_user_db->ball_id = $new_ball_id;
        $new_user_db->save();

        if( $recommender_id ==  $new_user_db->id){
            $return['error'] = ApiError::TRANSATION_SAME_ACCOUNT;
            return $return;
        }

        $result = TransactionService::depositPoints('Point',$recommender_id, $new_user_db->id, $set_value, false);

        $result['new_user_id'] = $new_user_db->id;
        $result['new_ball_id'] = $new_ball_id;
        $result['data'] = $new_ball_id;
        return $result;
    }


    // ＊＊ 登入 ＊＊　
    public static function login($account, $password)
    {

        $result = array('error' => ApiError::SUCCESS);

        $user_db = User::select("id","account","password") // 找此會員
                    ->where('account', strtolower($account))
                    ->first();

        if(!(isset($user_db) && password_verify($password, $user_db->password))){ // 密碼不符 / 會員不存在
            $result = array('error' => ApiError::USER_LOGIN_ERROR);
            return $result;
        }

        Session::put(SessionNames::USER_ID, $user_db->id);  // 成功 jobs
        static::createToken();
        $obj = new \stdClass();
        $obj->id = $user_db->id;
        $result['data'] = json_encode($obj);
        //Response::Json($result);
        return $result;
    }


    // ＊＊ 登出 ＊＊
    public static function logout()
    {
        Session::forget(SessionNames::USER_ID);
        Session::forget(SessionNames::TOKEN);
        $result = array('error' => ApiError::SUCCESS); // 目前沒有想到有其他錯誤方式..
        return $result;
    }


    // ＊＊ 檢查是不是推薦下線 ＊＊
    public static function isRecommendDownline($user_id, $target_id, $allow_floors, $is_allow_same_account)
    {

        $result = array('error' => ApiError::SUCCESS);

        if(!$is_allow_same_account){
            if($user_id == $target_id){
                $return['error'] = ApiError::TARGET_SAME_ACCOUNT;
                return $return;
            }
        }

        $user_ball_db = Ball::select("id","recommend_tree")
                   ->where("user_id", $user_id)
                   ->first();

        if(!$user_ball_db){
            $result = array('error' => ApiError::FROM_ACCOUNT_ERROR);
            return $result;
        }

        $target_ball_db = Ball::select("id","recommend_tree")
                   ->where("user_id", $target_id)
                   ->first();

        if(!$target_ball_db){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        $is_downline = false;
        //$floor = 0;  // 下線代數不符需要的 先留者
        $recommend_tree_array = explode(",", $target_ball_db->recommend_tree);
        for ($i=0; $i < count($recommend_tree_array); $i++) {

            if($recommend_tree_array[$i] == $user_ball_db->id){

                // 下線代數不符
                // $floor = count($recommend_tree_array) - $i;
                // if($floor > $allow_floors){
                //     $return['error'] = ApiError::DOWNLINE_FLOOR_ERROR;
                //     return $return;   
                // }

                $is_downline = true;
                break;
            }
        }
        //可以往上轉
        if (!$is_downline) {
            $is_downline = strrpos($user_ball_db->recommend_tree, (string)$target_ball_db->id) !== false;
        }


        if(!$is_downline){
            $return['error'] = ApiError::RECOMMEND_DOWNLINE_ERROR;
            return $return;
        }

        $result['data'] = $target_ball_db->recommend_tree.', user_id = '.$user_ball_db->id;

        return $result;
    }


    // ＊＊ 取得ID ＊＊
    public static function getIDFromAccount($target_account)
    {
        $result = array('error' => ApiError::SUCCESS);
        // 取得目標id
        $target_account_db = User::select("id")
                   ->where("account", $target_account)
                   ->first();

        if(!$target_account_db){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        $result['id'] = $target_account_db->id;
        return $result;
    }


    // ＊＊ 取得帳號 ＊＊
    public static function getAccount()
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_id = Session::get(SessionNames::USER_ID); // 登入者ID
        $user_db = User::select("account")
                ->where('id', $user_id)
                ->first();

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $result['data']['account'] = $user_db->account;

        return $result;
    }


    // ＊＊ 由指定ID取得帳號 ＊＊
    public static function getAccountFromID($user_id)
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_db = User::select("account")
                ->where('id', $user_id)
                ->first();

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $result['data']['account'] = $user_db->account;

        return $result;
    }


    // ＊＊ 取得會員資訊 ＊＊
    public static function getInfo()
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_account_id = Session::get(SessionNames::USER_ID); // 登入者ID
        $user_db = User::all()
                ->where('id', $user_account_id)
                ->first();

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $is_bound_phone = 0;
        if($user_db->country_code != '' && $user_db->phone != ''){
            $is_bound_phone = PhoneVerifyService::verify($user_db->country_code, $user_db->phone);
        }

        $set = OrganizationService::getSet($user_account_id);

        // 未激活 uid 回傳 0
        $uid = $user_account_id;
        if($set==0){
            $uid = 0;
        }

        $obj = new \stdClass();
        $obj->uid = $uid;
        $obj->set = $set;
        $obj->recommender_id = $user_db->recommender_id;
        $obj->nickname = $user_db->nickname;
        $obj->account = $user_db->account;
        $obj->country_code = $user_db->country_code;
        $obj->phone = $user_db->phone;
        $obj->binded_phone = $is_bound_phone;
        $result['data'] = json_encode($obj);
        return $result;
    }


    // ＊＊ 取得=銀行資訊 ＊＊
    public static function getBankInfo()
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_id = Session::get(SessionNames::USER_ID); // 登入者ID
        $user_db = User::select("bank_code","bank_branch","bank_account")
                ->where('id', $user_id)
                ->first();

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $obj = new \stdClass();
        $obj->bank_code = $user_db->bank_code;
        $obj->bank_branch = $user_db->bank_branch;
        $obj->bank_account = $user_db->bank_account;
        $obj->binded_bank = TagService::verify($user_id,VerifyTags::USER_BIND_BANK);

        $result['data'] = json_encode($obj);
        return $result;
    }


    // ＊＊ 修改會員資訊 ＊＊
    public static function updateInfo($nickname, $country_code, $phone)
    {
        $result = array('error' => ApiError::SUCCESS);
        $user_id = Session::get(SessionNames::USER_ID); // 登入者ID
        $user_db = User::find($user_id);

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $user_db->nickname = $nickname;
        if($user_db->binded_phone != 'bound'){
            $user_db->country_code = $country_code;
            $user_db->phone = $phone;
        }
        $user_db->save();

        TradingRepository::updateTradingUserInfo($user_id); // 交易單相關資料
        // 尋找所有Trading單buyer或seller id 的這個人的 buyer_info 和 serller_info
        $result['data'] = 'updateInfo ok';//json_encode($user_db);
        return $result;
    }


    // ＊＊ 修改銀行資訊 ＊＊
    public static function updateBankInfo($bank_code, $bank_branch, $bank_account)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_id = Session::get(SessionNames::USER_ID); // 登入者ID
        $user_db = User::find($user_id);

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL);
            return $result;
        }

        $is_verify_bind_bank = TagService::verify($user_id,VerifyTags::USER_BIND_BANK);
        if($is_verify_bind_bank == 1){
            $result = array('error' => ApiError::USER_BANK_BINDED);
            return $result;
        }

//        $obj->binded_bank = VerifyService::verify($user_id,VerifyTags::USER_BIND_BANK);
        TagRepository::create($user_id, VerifyTags::USER_BIND_BANK);
        $user_db->bank_code = $bank_code;
        $user_db->bank_branch = $bank_branch;
        $user_db->bank_account = $bank_account;
        //$user_db->binded_bank = 1;    
        $user_db->save();

        TradingRepository::updateTradingUserInfo($user_id); // 交易單相關資料

        $result['data'] = 'updateInfo ok';//json_encode($user_db);
        return $result;
    }


    // ＊＊ 修改會員密碼 ＊＊
    public static function updatePassword($old_pw, $new_pw, $new_pw2)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_id = Session::get(SessionNames::USER_ID);
        $user_db = User::find($user_id);

        if(!$user_db){
            $result = array('error' => ApiError::USER_ID_NULL); // 登入者ID不存在
            return $result;
        }

        if( !password_verify($old_pw, $user_db->password)){ // 密碼錯誤
            $result = array('error' => ApiError::PASSWORD_FAILED);
            return $result;
        }

        $user_db->password = password_hash($new_pw, PASSWORD_DEFAULT);
        $user_db->save();

        $result['data'] = 'updatePW ok';//json_encode($user_db);
        return $result;
    }





}