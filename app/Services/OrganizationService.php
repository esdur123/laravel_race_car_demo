<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response; 

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\TransactionTypes;
use App\Defined\PointRules;

use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Models\Transaction;

use App\Services\Service;
use App\Services\BonusService;
use App\Services\TransactionService;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;
use App\Repositories\OrganizationRepository;

class OrganizationService extends Service
{



    // ＊＊ 立刻激活 ＊＊
    public static function ActiveStatic($user_account_id, $ball_id)
    {

        $result = array('error' => ApiError::SUCCESS);         

        $ball_db = Ball::find($ball_id);

        $wallet_db = Wallet::all()
                ->where('user_id', $user_account_id)
                ->where('coin','Point')
                ->first();
    
        // 無此target_id
        if(!$ball_db || !$wallet_db){
            $result = array('error' => ApiError::FROM_ACCOUNT_ERROR); 
            return $result;
        }

        $cost = static::getSetFromLevel($ball_db->level); // 激活$同等配套


        if((float)$wallet_db->balance_available < $cost){ // 錢要夠
            $result = array('error' => ApiError::POINT_NOT_ENOUGH); 
            return $result;
        }

        // 到此可以激活
        // 改靜態上限
        $max_out_quota = 0;
        if($ball_db->level == 1) $max_out_quota = (int)PointRules::SET_LVL_1 * (int)PointRules::STATIC_OUT_RATE_LVL_1;
        if($ball_db->level == 2) $max_out_quota = (int)PointRules::SET_LVL_2 * (int)PointRules::STATIC_OUT_RATE_LVL_2;
        //if($ball_db->level == 3) $max_out_quota = (int)PointRules::SET_LVL_3 * (int)PointRules::STATIC_OUT_RATE_LVL_3;    
        //$ball_db->static_current = 0;
        $ball_db->static_max += $max_out_quota;
        $ball_db->save();

        // 裡面用的是Lock
        $wallet_before = $wallet_db->balance_available;
        $lock_wallet_db = WalletRepository::deposit_by_userID($user_account_id, -$cost, 'Point');
        $wallet_after = $lock_wallet_db->balance_available;

        // 寫交易單
        TransactionRepository::create(
                              $user_account_id
                              , $wallet_db->id
                              , 'Point'
                              , TransactionTypes::STATIC_RE_ACTIVE
                              , $cost
                              , $lock_wallet_db->before
                              , $lock_wallet_db->after
                              , ""
                             );    

        $result['ball_level'] = $ball_db->level;
        return $result;
    }


    // ＊＊ 檢查這個ball_id是不是這個user的 ＊＊ 
    public static function check_is_user_own_ball($user_id, $ball_id){
		
    	// 新建人員資訊
    	$is_ball_owner = 0;
    	$ball_db = Ball::find($ball_id);
    	if(!$ball_db){
			return $is_ball_owner;
    	}

    	if($ball_db->user_id != $user_id){
    		return $is_ball_owner;
    	}

    	$is_ball_owner = 1;    	
		return $is_ball_owner;
    }

    // ＊＊ 檢查錢包夠不夠這個數字 ＊＊
    public static function check_is_main_wallet_enough($user_id, $check_amount)
    {

        $is_enough = 1;
        $wallet_db = Wallet::where('user_id', $user_id)
        ->where('coin','Point')
        ->first(); 

        if(!$wallet_db){
            $is_enough = 0;
            return $is_enough;
        }
        // $new_set = OrganizationService::getSetFromLevel($new_lvl);     

        if($wallet_db->balance_available < $check_amount){
            $is_enough = 0;
            return $is_enough;
        }

        return $is_enough;
    }



    // ＊＊ 註冊新球 ＊＊
    public static function create_another_ball($user_id)
    {

        $result = array('error' => ApiError::SUCCESS);

        // 未激活帳號不開放註冊新ball
        $main_ball_db = Ball::where('user_id', $user_id)
                            ->orderBy('id', 'asc')  // 每個人的主要ball就是他ball id序列最少的那顆
                            ->first();   

        // 主球未開通 不能申請新球
        if($main_ball_db->level <= 0){
            $return['error'] = ApiError::INACTIVED_ACCOUNT_ILLEGAL_REGISTER_BALL;
            return $return;
        }



        // 加一顆球
        $recommend_tree = static::NewRecommenderTree($main_ball_db->recommend_tree, $main_ball_db->id);

        $new_ball_db = OrganizationRepository::create(
                                                    $user_id
                                                    , $main_ball_db->id
                                                    , $recommend_tree
                                                    , ''//$branch_tree
                                                    , ''//$branch
                                                    , 0
                                                   );

        if( is_null($new_ball_db->id) ){
            $return['error'] = ApiError::USER_REGISTER_FAILED_ISSUE_02;
            return $return;
        }
        
        //$result = TransactionService::depositPoints('Point',$recommender_id, $new_user_db->id, $set_value, false);     

        $result['data'] = $new_ball_db->id;
        return $result;
    }


    // ＊＊ 無上線註冊 ＊＊ 
    public static function registerNoUpline($target_account_id){
		
    	// 新建人員資訊
    	$ball_db = OrganizationRepository::create(
    												$target_account_id
    												, 0
    												, ''
    												, ''
    												, ''
    												, -1
    											   );

		return $ball_db->id;
    }


    // ＊＊ 取得會員ball id ＊＊ 
    public static function getBallID(){

        $user_account_id = Session::get(SessionNames::USER_ID); // accountID
        $ball_db = Ball::select("id")
				->where('user_id', $user_account_id)
				->first();

		if(!$ball_db){
            return 0;
		}

		$ball_id = $ball_db->id;
		return $ball_id;
    }


    // ＊＊ 取得目前Set ＊＊ 
    public static function getSet($user_account_id){
        
        $ball_db = Ball::select("level")
				->where('user_id', $user_account_id)
				->first();

        if(!$ball_db){
            $result = array('error' => ApiError::USER_ID_NULL); 
            return $result;
        }

        $set = static::getSetFromLevel($ball_db->level);
		return $set;
    }


    // ＊＊ 根據Level取得目前Set ＊＊ 
    public static function getSetFromLevel($_lvl){
        
        $set = 0;
        if($_lvl <= 0) $set = (int)PointRules::SET_LVL_0;
        if($_lvl == 1) $set = (int)PointRules::SET_LVL_1;
        if($_lvl == 2) $set = (int)PointRules::SET_LVL_2;
        //if($_lvl == 3) $set = (int)PointRules::SET_LVL_3;

		return $set;
    }


    // ＊＊ 根據Set取得目前Level ＊＊ 
    public static function getLevelFromSet($_set){
        
        $lvl = -1;
        if($_set == (int)PointRules::SET_LVL_0) $lvl = 0;
        if($_set == (int)PointRules::SET_LVL_1) $lvl = 1;
        if($_set == (int)PointRules::SET_LVL_2) $lvl = 2;
        //if($_set == (int)PointRules::SET_LVL_3) $lvl = 3;

		return $lvl;
    }



    // ＊＊ 新增推薦連結 ＊＊ 
    public static function registerFromRecommender($target_account_id, $recommender_account_id){
		
		// 推薦人資訊
    	$recommender_db = Ball::select('id','recommend_tree')
    					 ->where('user_id',$recommender_account_id)
    					 ->first();

    	if(!$recommender_db){
			$result = array('error' => ApiError::USER_REGISTER_RECOMMENDER_ID_ERROR);
			return $result;
    	}

    	// 新建人員資訊
    	$recommend_tree = static::NewRecommenderTree($recommender_db->recommend_tree, $recommender_db->id);
    	$ball_db = OrganizationRepository::create(
    												$target_account_id
    												, $recommender_db->id
    												, $recommend_tree
    												, ''
    												, ''
    												, -1
    											   );

		return $ball_db->id;
    }


    // ＊＊ 推薦連結取得Branch ＊＊ 
    public static function getInactivedBranch($recommender_account_id){
		
		$branch_result['branch'] = '';
		$branch_result['branch_tree'] = '';


		// 推薦人資訊
    	$recommender_db = Ball::select('id','recommend_tree')
    					 ->where('user_id',$recommender_account_id)
    					 ->first();

    	if(!$recommender_db){
			$result = array('error' => ApiError::USER_REGISTER_RECOMMENDER_ID_ERROR);
			return $result;
    	}

    	// 看目前Rceommender RL哪個少
    	// 找到極左or極右branch id	
		$info = static::getBranchInfoByID_withoutOrder($recommender_db->id, $recommender_db->id);
		$max_array = $info['max_array'];
		$obj_array = $info['obj_array'];
		$total_R = $info['total_R'];
		$total_L = $info['total_L'];
		$RL='';

		if($total_R >= $total_L) $RL = 'L'; // 相等取L
		else $RL = 'R';

		$max_info = static::getMaxRL($obj_array, $max_array, $recommender_db->id, $recommender_db->id); // max相關
		$RL_str = 'max_'.$RL.'_id';
		$up_tree_id = $max_info[$RL_str];	

		// // 未來頭上那個人的db
    	$up_tree_db = Ball::select('branch_tree','branch')
    					->where('id', $up_tree_id)
    					->first();

    	if(!$up_tree_db){
			$result = array('error' => ApiError::USER_REGISTER_UPER_ID_ERROR);
			return $result;
    	}


		if($up_tree_db->branch_tree == ''){ // 安置上家是頂端
			$branch_result['branch_tree'] = $up_tree_id.$RL;
			$branch_result['branch']      = $up_tree_id.$RL;
		} else {
			// 根據RL比對up_tree_db的branch_tree 找出新的branch
			$newBranchInfo = static::getNewBranchInfo($RL, $up_tree_db->branch_tree, $up_tree_db->branch, $up_tree_id);
			$branch_result['branch_tree'] = $newBranchInfo['branch_tree'];
			$branch_result['branch']      = $newBranchInfo['branch'];
		}

		return $branch_result;
    }


    // ＊＊ create-branch-obj ＊＊ 
    public static function register($target_account_id, $recommender_account_id){
		
		// 推薦人資訊
    	$recommender_db = Ball::select('id','recommend_tree')
    					 ->where('user_id',$recommender_account_id)
    					 ->first();

    	if(!$recommender_db){
			$result = array('error' => ApiError::USER_REGISTER_RECOMMENDER_ID_ERROR);
			return $result;
    	}

  //   	// 新建人員資訊
     	$recommend_tree = static::NewRecommenderTree($recommender_db->recommend_tree, $recommender_db->id);

    	$target_db = OrganizationRepository::create(
    												$target_account_id
    												, $recommender_db->id
    												, $recommend_tree
    												, ''//$branch_tree
    												, ''//$branch
    												, 0
    											   );

		$new_ball_id = $target_db->id;
		return $new_ball_id;
    }


    // ＊＊ 計算 所有上線的新業績 ＊＊
    public static function update_upline_performance($recommend_tree, $add_set){

		$upline_array = explode(',',(string)$recommend_tree);
		for ($i=0; $i < count($upline_array) ; $i++) { 

				$upline_db = Ball::find($upline_array[$i]);
				if(!$upline_db){
					continue;  // 暫時不處理找不到此上線的情況					
				}
				
				$upline_db->performance += $add_set;
				$upline_db->save();

			# code...
		}


  //   			// 權限有了, 用like+%初步列出組織圖名單, 我不信任like+%
		// $like_db = Ball::where('recommend_tree', 'like', '%'.$recommender_ball_id.'%')
		// 		//->where('level', '>', '0')
		// 		->get();

		// // 用逗號切成陣列,掃 recommend_tree, 比對target_ball_id有沒有在陣列中
		// for ($i=0; $i < count($like_db) ; $i++) { 
		// 	$like_db_tree_array = explode(',',(string)$like_db[$i]->recommend_tree);
		// 	$count_tree = count($like_db_tree_array);
		// 	for ($j=0; $j < $count_tree; $j++) { 

		// 			if($target_ball_id == $like_db_tree_array[$j]){
		// 				$like_db[$i]->performance += $add_set;
		// 				$like_db[$i]->save();
		// 			}						
		// 	}
		// }

    }


    // ＊＊ 新創帳號的 recommend_tree ＊＊
    public static function NewRecommenderTree($recommender_recommend_tree, $recommender_recommend_id){

    	$return_tree = '';

    	if($recommender_recommend_tree == ''){
			$return_tree = $recommender_recommend_id;
    	} else {
    		$return_tree = $recommender_recommend_tree.','.$recommender_recommend_id;
    	}

    	return $return_tree;
    }


    // ＊＊ 取得 target_ball_id 照規格排列的組織圖 + 極左極右資訊 ＊＊
    public static function GetRecommenderInfoByID($target_ball_id)
    {
        $result = array('error' => ApiError::SUCCESS);

        $user_ball_id = static::getBallID();// $user_db->id;

        $target_db = Ball::select("recommend_tree")
                ->where('id', $target_ball_id)
                ->first();

        // 無此target_ball_id
        if(!$target_db){
            $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
            return $result;
        }

        // 檢查user有沒有權限看這個人
        $permission_pass = false;

        $target_tree_array = explode(',' , (string)$target_db->recommend_tree); // 無法信任like+%, 自己比對0.0!
        for ($i=0; $i < count($target_tree_array); $i++) { 
            if( $user_ball_id == $target_tree_array[$i])
            {
                $permission_pass = true;
                break;
            }
        }

        if( $user_ball_id == $target_ball_id){ // 自己看自己 也給權限
            $permission_pass = true;
        }

        if(!$permission_pass){
            $result = array('error' => ApiError::USER_GET_SPECIFIC_ID_ILLEGAL);
            return $result;
        }

		$first_db = Ball::where('id', $target_ball_id)
			->first();

		$first_account_db = User::select("nickname","account")
					->where("id", $first_db->user_id)
					->first();
					
		// 權限有了, 用like+%初步列出組織圖名單, 我不信任like+%
		$like_db = Ball::where('recommend_tree', 'like', '%'.$target_ball_id.'%')
				->where('level', '>', '0')
				->get();

        $obj_array = array(); // 頂端 root 自己key
        $index = 0;
		$obj_array[$index] = 
			static::createRecommenderObj
				(
				$target_ball_id
				, $first_db->recommender_id
				//, $first_db->pv_left.'/'.$first_db->pv_right
				, 0
				, $first_db->performance // ($first_db->total_left + $first_db->total_right) 
				, $first_account_db->nickname
				, static::getSetFromLevel($first_db->level)
				, $first_account_db->account
                , $first_db->ball_index
				);  // root 
		$index++;

		// 極左極右相關陣列
		$index_max = 0;

		// 用逗號切成陣列,掃 recommend_tree, 比對target_ball_id有沒有在陣列中
		for ($i=0; $i < count($like_db) ; $i++) { 
			$like_db_tree_array = explode(',',(string)$like_db[$i]->recommend_tree);
			$count_tree = count($like_db_tree_array);
			for ($j=0; $j < $count_tree; $j++) { 

				// (idR || idL)  = branch_tree的最後一格
				if( $target_ball_id == $like_db_tree_array[$j] 
					|| 
					$target_ball_id == $like_db_tree_array[$j])
				{
					$floor = $count_tree - $j;

					// 秀三層內
					if($floor <= 3){ 
						$like_account_db = User::select("nickname","account")
								->where('id',$like_db[$i]->user_id)
								->first();

						$obj_array[$index] = static::createRecommenderObj(
												$like_db[$i]->id      
												,$like_db[$i]->recommender_id
												//,$like_db[$i]->pv_left.'/'.$like_db[$i]->pv_right
												,$floor
												,$like_db[$i]->performance //($like_db[$i]->total_left + $like_db[$i]->total_right)
												,$like_account_db->nickname
												,static::getSetFromLevel($like_db[$i]->level)
												,$like_account_db->account
                                                ,$like_db[$i]->ball_index
											);
						$index++;
					}

				} // RL
			}
		}



        // 再整理一次, 把同一個人底下的ball編號列出來
        // for ($i=0; $i < count($obj_array); $i++) { 

        //     $index = 0;
        //     for ($j=0; $j < count($obj_array); $j++) { 

        //         if($i == $j){  //自己不用比對
        //             continue; 
        //         }

        //         // 加編號
        //         if(
        //             $obj_array[$i]->nickname == $obj_array[$j]->nickname
        //             &&
        //             $obj_array[$i]->account == $obj_array[$j]->account
        //           )
        //         {            
        //             $index ++;        
        //             $obj_array[$j]->nickname = $obj_array[$j]->nickname.'('.$index.')';                    
        //         }


        //     }
        // }

        
		$result['data']['tree'] = json_encode($obj_array);

        return $result;
    }


    // ＊＊ (private) create-recommender-obj ＊＊ 
    public static function createRecommenderObj($account_id, $recommender, $floor, $sun_downline, $nickname, $set, $account, $ball_index)
    {
		$obj = new \stdClass(); 
		$obj->id = $account_id;
		$obj->recommender = $recommender;
		//$obj->pv_rl = $pv_rl;		
		$obj->floor = $floor;
		$obj->sun_downline = $sun_downline;
		$obj->nickname = $nickname;
		$obj->set = $set;
		$obj->account = $account;
        $obj->ball_index = $ball_index;
		return $obj;
    }


    // ＊＊ (private) create-branch-obj ＊＊ 
    private static function createBranchObj($user_id,$branch_upline,$branch_tree,$branch,$order,$floor,$is_empty,$pv_rl,$set,$r_plus_l,$r,$l,$nickname,$pv,$account){
		$obj = new \stdClass(); 
		$obj->id = $user_id;
		$obj->branch_upline = $branch_upline;
		$obj->branch_tree = $branch_tree; 
		$obj->branch = $branch; 	
		$obj->order = $order;				
		$obj->floor = $floor; 	
		$obj->is_empty = $is_empty;
		$obj->pv_rl = $pv_rl; 
		$obj->set = $set; 
		$obj->r_plus_l = $r_plus_l; 
		$obj->r = $r;
		$obj->l = $l;
		$obj->nickname = $nickname; 
		$obj->pv = $pv; 
		$obj->account = $account; 
		return $obj;
    }


    // ＊＊ 取得 target_id 照規格排列的組織圖 + 極左極右資訊 ＊＊
    public static function getBranchInfoByID($target_ball_id)
    {
        $result = array('error' => ApiError::SUCCESS);	       

		$ball_id = static::getBallID();// $user_db->id;

		$target_db = Ball::select("branch_tree")
				->where('id', $target_ball_id)
				->first();

		// 無此target_id
		if(!$target_db){
            $result = array('error' => ApiError::USER_GET_SPECIFIC_BRANCH_ID_NULL); 
            return $result;
		}

		// 檢查user有沒有權限看這個id , target_id 的 branch_tree 內含 (ball_id+R/L)
		$permission_pass = false;

		$target_branch_tree_array = explode(',',(string)$target_db->branch_tree); // 無法信任like+%, 自己比對0.0!
		for ($i=0; $i < count($target_branch_tree_array); $i++) { 
			if( $ball_id."R" === $target_branch_tree_array[$i] 
				|| 
				$ball_id."L" === $target_branch_tree_array[$i])
			{
				$permission_pass = true;
				break;
			}
		}

		if( $ball_id == $target_ball_id){ // 自己看自己 也給權限
			$permission_pass = true;
		} 

		if(!$permission_pass){
            $result = array('error' => ApiError::USER_GET_SPECIFIC_ID_ILLEGAL); 
            return $result;
		}

		// 沒有排序的樹(3層)和極左極右資訊
		$info = static::getBranchInfoByID_withoutOrder($target_ball_id, $ball_id);
		$obj_array = $info['obj_array'];
		$max_array = $info['max_array'];		
		
		$order_array = static::setOrder15Branch($obj_array); // 15格順序排列, 空的填滿empty
		$order_array = static::getCorrectLengthArray($order_array); // 上線是empty的修掉
		$max_info = static::getMaxRL($order_array, $max_array, $ball_id, $target_ball_id); // max相關

        $result['data']['map'] = json_encode($order_array);
        $result['data']['max_L_id'] = json_encode($max_info['max_L_id']);
        $result['data']['max_R_id'] = json_encode($max_info['max_R_id']);
        $result['data']['log'] = json_encode($max_info['log']);
        $result['data']['is_user_root'] = json_encode($max_info['is_user_root']);

        return $result;
    }


    // ＊＊ 切換自動機活開關 ＊＊
    public static function switchActive($user_account_id, $ball_id)
    {
        $result = array('error' => ApiError::SUCCESS);	       

		$target_db = Ball::find($ball_id);
				// ->where('user_id', $user_account_id)
				// ->first();

		// 無此target_id
		if(!$target_db){
            $result = array('error' => ApiError::USER_GET_SPECIFIC_BRANCH_ID_NULL); 
            return $result;
		}

		$value_before =  $target_db->auto_active;
		$value_after = -1;

		if($value_before == 1) $value_after = 0;
		if($value_before == 0) $value_after = 1;

		$target_db->auto_active = $value_after;
		$target_db->save();

        $result['data'] = $value_after;
        //'user_id='.$target_db->user_id.', before = '.$value_before.' ,after = '.$value_after;

        return $result;
    }


	// ＊＊ (private) 回傳極左極右 無左右排序和空白排序＊＊
    public static function getBranchInfoByID_withoutOrder($target_id, $user_id){


    	$first_db = Ball::where('id', $target_id)
				->first();

		$first_account_db = User::select("nickname","account")
					->where('id',$first_db->user_id)
					->first();

		// 權限有了, 用like+%初步列出組織圖名單, 我不信任like+%
		$like_db = Ball::where('branch_tree', 'like', '%'.$target_id.'%')
				->get();

        $obj_array = array(); // 頂端 root 自己key
        $index = 0;
		$obj_array[$index] = 
			static::createBranchObj(
				$target_id
				, ''
				, ''
				, ''
				, 0
				, 0
				, 'false'
				, $first_db->pv_left.'/'.$first_db->pv_right
				, static::getSetFromLevel($first_db->level)
				, ($first_db->total_left + $first_db->total_right)
				, $first_db->total_right
				, $first_db->total_left
				, $first_account_db->nickname
				, $first_db->pv
				, $first_account_db->account
			);
		$index++;

		// 極左極右相關陣列
		$max_array = array();
		$index_max = 0;
		$total_R = 0;
		$total_L = 0;

		// 用逗號切成陣列, 掃branch_tree, 比對target_id+R/L有沒有在陣列中
		for ($i=0; $i < count($like_db) ; $i++) { 
			$like_db_branch_tree_array = explode(',',(string)$like_db[$i]->branch_tree);
			$count_tree = count($like_db_branch_tree_array);
			for ($j=0; $j < $count_tree; $j++) { 

				// (idR || idL)  = branch_tree的最後一格
				if( $target_id."R" === $like_db_branch_tree_array[$j] 
					|| 
					$target_id."L" === $like_db_branch_tree_array[$j])
				{
					$floor = $count_tree - $j;

					// 秀三層內
					if($floor <= 3){ 
						$branch_upline = $like_db_branch_tree_array[$count_tree-1];
						$branch_upline = substr_replace($branch_upline,'',-1);
							$j_user_db = User::select('nickname','account')
									->where('id', $like_db[$i]->user_id)
									->first();

						$obj_array[$index] = static::createBranchObj(
												$like_db[$i]->id      // user_id
												, $branch_upline              // branch_upline
												, $like_db[$i]->branch_tree // branch tree
												, $like_db[$i]->branch      // branch
												, -1							 // order
												, $floor                      // floor
												, 'false'                     // is_empty
												, $like_db[$i]->pv_left.'/'.$like_db[$i]->pv_right
												, static::getSetFromLevel($like_db[$i]->level) 
												, ($like_db[$i]->total_left + $like_db[$i]->total_right)
												, $like_db[$i]->total_right
												, $like_db[$i]->total_left
												, $j_user_db->nickname
												, $like_db[$i]->pv
												, $j_user_db->account
											);
						$index++;
					}

					// 不限層數, 這邊存極左極右候選名單
					if($user_id == $target_id){
						$max_array[$index_max] = static::createMaxObj(
																$like_db[$i]->id   		// user_id
																,$like_db[$i]->branch   // branch
																,$count_tree            // branch_tree length
															 );
						$index_max ++;						
					}

					// 算總RL各有多少
					if($target_id."R" === $like_db_branch_tree_array[$j]){
						$total_R++;
					}

					if($target_id."L" === $like_db_branch_tree_array[$j]){
						$total_L++;
					}

				} // RL
			}
		}

		$result['obj_array'] = $obj_array;
		$result['max_array'] = $max_array;
		$result['total_R'] = $total_R;
		$result['total_L'] = $total_L;

		return $result;
    }


	// ＊＊ (private) 回傳極左極右 ＊＊
    public static function getMaxRL($order_array, $max_array, $user_id, $target_id){

    	$return_info['max_L_id'] = -1;
    	$return_info['max_R_id'] = -1;
    	$return_info['is_user_root'] = false;
    	$return_info['log'] = '';

    	if($user_id != $target_id){ // 自己不是組織圖的頂端, 不給查max
	    	return $return_info;
    	}


    	$left_mark = '';
    	$right_mark = '';

		$target_db = Ball::select("branch","id") // (用like粗算)
				->where('id', $target_id)
				->first();

     	// 找極左極右要查的RL標記
		$root_RL = '';
     	if($target_db->branch == ''){ // 先看root個有沒有上線
	    	$left_mark = $order_array[0]->id.'L';
	    	$right_mark = $order_array[0]->id.'R';
	    	$root_RL = 'TOP';
     	} else {

			if(substr($target_db->branch, -1) == "L"){
				$root_RL = 'L';
				$left_mark = $target_db->branch;
				$right_mark = $target_db->id."R";
			}

			if(substr($target_db->branch, -1) == "R"){
				$root_RL = 'R';
				$left_mark = $target_db->id."L";	
				$right_mark = $target_db->branch;			
			}
     	}

    	// 極左 branch_tree 有 $left_mark 其中陣列最長的人
		$max_length_L = 0;
		$max_length_R = 0;
		$max_L_id = '';
		$max_R_id = '';

		for ($i=0; $i < count($max_array); $i++) { // 極左id
			if((string)$max_array[$i]->branch == (string)$left_mark){
				if( $max_array[$i]->length > $max_length_L){
					$max_length_L = $max_array[$i]->length;
					$max_L_id = $max_array[$i]->id;
				}
			}
		}

		for ($i=0; $i < count($max_array); $i++) { // 極右id
			if($max_array[$i]->branch == $right_mark){
				if( $max_array[$i]->length > $max_length_R){
					$max_length_R = $max_array[$i]->length;
					$max_R_id = $max_array[$i]->id;
				}					
			}
		}


		if($max_L_id == ''){
			$max_L_id = $target_id;
		}

		if($max_R_id == ''){
			$max_R_id = $target_id;
		}

		$return_info['log'] = 'left_mark = '.$left_mark.', right_mark = '.$right_mark.', root_RL = '.$root_RL.', $max_L_id = '.$max_L_id.', max_R_id = '.$max_R_id; // $max_array;

    	$return_info['max_L_id'] = (int)$max_L_id;
    	$return_info['max_R_id'] = (int)$max_R_id;
    	$return_info['is_user_root'] = true;

		return $return_info;//array((int)$max_L_id,(int)$max_R_id);
    }


	// ＊＊ (private) 上線是empty的修掉 ＊＊
    public static function getCorrectLengthArray($order_array){
    	$return_array = array();
    	$index = 0;
    	for ($i=0; $i < count($order_array); $i++) {
			if($order_array[$i]->branch_upline != 'empty'){
				$return_array[$index] = $order_array[$i];
				$index++;
			}
    	}

		return $return_array;
    }


    // ＊＊ (private) T15格順序排列, 空的填滿empty ＊＊ 
    public static function setOrder15Branch($obj_array){
		$order_array = array(); 

    	// 先塞滿15個empty    	
		$floor = 0;
		$branch = '';
		for ($i=0; $i < 15; $i++) { 
			if($i==0){
				$floor = 0;
			}

			if($i > 0 && $i < 3){
				$floor = 1;
			}

			if($i > 2 && $i < 7){
				$floor = 2;
			}

			if($i > 6){
				$floor = 3;
			}

			$order_array[$i] = static::createBranchObj(
				'empty'.$i
				,''
				,''
				,$branch
				,$i
				,$floor
				,'true'
				,'0/0'
				,'empty'
				,'-1'
				,'-1'
				,'-1'
				,''
				,''
				,''
			);
		}

		// 把現有的放到正確的陣列格 0 - 14
     	$count_obj_array = count($obj_array);

		for ($i=0; $i < $count_obj_array; $i++) {  // floor 0 1
			if($obj_array[$i]->floor == 0){
				$order_array[0] = $obj_array[$i]; // root
			}

			if($obj_array[$i]->floor == 1){
				if(substr($obj_array[$i]->branch, -1) == "L"){
					$order_array[1] = $obj_array[$i];
					$order_array[1]->order = 1;
				} else {
					$order_array[2] = $obj_array[$i];
					$order_array[2]->order = 2;
				}
			}
		}
     	

		for ($i=0; $i < $count_obj_array; $i++) {  // floor 2

			if($obj_array[$i]->floor == 2){
				$upline_order = static::getUplineOrder($obj_array[$i]->branch_upline, $obj_array);
				if($upline_order == 1){
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[3] = $obj_array[$i];
						$order_array[3]->order = 3;
					} else {
						$order_array[4] = $obj_array[$i];
						$order_array[4]->order = 4;
					}
				} else {
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[5] = $obj_array[$i];
						$order_array[5]->order = 5;
					} else {
						$order_array[6] = $obj_array[$i];
						$order_array[6]->order = 6;
					}
				}						
			}
		}

		for ($i=0; $i < $count_obj_array; $i++) {  // floor 3

			if($obj_array[$i]->floor == 3){
				$upline_order = static::getUplineOrder($obj_array[$i]->branch_upline, $obj_array);
				if($upline_order == 3){
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[7] = $obj_array[$i];
						$order_array[7]->order = 7;
					} else {
						$order_array[7] = $obj_array[$i];
						$order_array[7]->order = 7;
					}
				} 		

				if($upline_order == 4){
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[9] = $obj_array[$i];
						$order_array[9]->order = 9;
					} else {
						$order_array[10] = $obj_array[$i];
						$order_array[10]->order = 10;
					}
				} 

				if($upline_order == 5){
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[11] = $obj_array[$i];
						$order_array[11]->order = 11;
					} else {
						$order_array[12] = $obj_array[$i];
						$order_array[12]->order = 12;
					}
				} 

				if($upline_order == 6){
					if(substr($obj_array[$i]->branch, -1) == "L"){
						$order_array[13] = $obj_array[$i];
						$order_array[13]->order = 13;
					} else {
						$order_array[14] = $obj_array[$i];
						$order_array[14]->order = 14;
					}
				} 
			}
		}

		// 處理empty的上線 (改成砍掉empty元素)
		$count = count($order_array);
		for ($i=0; $i < $count; $i++) { 
			if($order_array[$i]->is_empty == 'true'){
				$upline_id = static::getNotEmptyUplineID($order_array[$i]->order, $order_array);
				$order_array[$i]->branch_upline = $upline_id;		  	
				//unset($order_array[$i]); // *
			}
		}

		// 重新整理拿掉empty後的順序和長度 // *
		//$new_order_array = array_values($order_array); 
		//return $new_order_array; // *

 		return $order_array;
    }


    // ＊＊ (private) 存極左極右需要的資訊 ＊＊ 
    private static function createMaxObj($id, $branch, $length){
		$obj = new \stdClass(); 
		$obj->id = $id;
		$obj->branch = $branch;
		$obj->length = $length;
		return $obj;
    }

    // ＊＊ (private) create-branch-obj ＊＊ 
    private static function getNotEmptyUplineID($order, $order_array){
    	$upline = 'empty';
    	$upline_order = -1;
    	if($order == 1) $upline_order = 0;
    	if($order == 2) $upline_order = 0;
    	if($order == 3) $upline_order = 1;
    	if($order == 4) $upline_order = 1;
    	if($order == 5) $upline_order = 2;
    	if($order == 6) $upline_order = 2;
    	if($order == 7) $upline_order = 3;
    	if($order == 8) $upline_order = 3;
    	if($order == 9) $upline_order = 4;
    	if($order == 10) $upline_order = 4;
    	if($order == 11) $upline_order = 5;
    	if($order == 12) $upline_order = 5;
    	if($order == 13) $upline_order = 6;
    	if($order == 14) $upline_order = 6;

    	for ($i=0; $i < count($order_array); $i++) { 
    		if((string)$order_array[$i]->order == (string)$upline_order){
    			if($order_array[$i]->is_empty == 'false'){
					$upline = $order_array[$i]->id;
					break;
    			}
    		}
    	}
		return $upline;
    }


    // ＊＊ (private) getUplineOrder ＊＊ 
    private static function getUplineOrder($upline_id, $obj_array){
    	$order = -1;
    	for ($i=0; $i < count($obj_array); $i++) { 
    		if((string)$obj_array[$i]->id == (string)$upline_id){
				$order = $obj_array[$i]->order;
				break;
    		}
    	}
		return $order;
    }


	// ＊＊ (private) 回傳branch_tree和branch ＊＊
    private static function getNewBranchInfo($RL, $uper_branch_tree, $uper_branch, $uper_id){
    
    	$tree_array = explode(",",$uper_branch_tree);
    	$uper_last_branch = $tree_array[count($tree_array)-1];

		$return_branch_tree = $uper_branch_tree.','.$uper_id.$RL;
		$return_branch = '';

    	// 比對最後一個的RL和自己的RL
    	if(substr($uper_last_branch, -1) == $RL){
    		$return_branch = $uper_branch;    		
    	} else {
    		$return_branch = $uper_id.$RL;
    	}

    	$return['branch_tree'] = $return_branch_tree;
    	$return['branch'] = $return_branch;
		return $return;
    }






}