<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\System;

use App\Services\Service;


class SystemService extends Service
{


    // ＊＊ 檢查認證 ＊＊
    public static function getDailyMaxPhoneVerify()
    {

        $system_db = System::orderBy('id', 'asc')
                ->first();

        return $system_db->daily_max_phone_verify;
    }



}