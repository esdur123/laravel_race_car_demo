<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\User;
use App\Models\Ball;
use App\Models\Wallet;
use App\Models\Transaction;

use App\Services\Service;
use App\Services\WalletService;
use App\Services\OrganizationService;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;

class BonusService extends Service
{

    // ＊＊ 直推獎金 ＊＊
    public static function directPush($user_account_id, $new_ball_id, $user_new_level)
    {

        $C_MAX_GENERATION = 4; // 最多四代
        $user_account_id = (int)$user_account_id;
        $result = array('error' => ApiError::SUCCESS);

        // LVL 1 一代直推獎金 = 8%
        $C_LVL_1_BONUS_GENERATION_PERCENTAGE[1] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_1))*0.01;  
        // LVL 1 二代直推獎金 = 6%
        $C_LVL_1_BONUS_GENERATION_PERCENTAGE[2] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_2))*0.01;  
        // LVL 1 三代直推獎金 = 4%
        $C_LVL_1_BONUS_GENERATION_PERCENTAGE[3] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_3))*0.01;  
        // LVL 1 四代直推獎金 = 2%
        $C_LVL_1_BONUS_GENERATION_PERCENTAGE[4] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_4))*0.01;         
        // LVL 2 一代直推獎金 = 10%
        $C_LVL_2_BONUS_GENERATION_PERCENTAGE[1] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_1))*0.01;  
        // LVL 2 二代直推獎金 = 8%
        $C_LVL_2_BONUS_GENERATION_PERCENTAGE[2] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_2))*0.01;  
        // LVL 2 三代直推獎金 = 6%
        $C_LVL_2_BONUS_GENERATION_PERCENTAGE[3] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_3))*0.01;  
        // LVL 2 四代直推獎金 = 4%
        $C_LVL_2_BONUS_GENERATION_PERCENTAGE[4] = ((float)(PointRules::BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_4))*0.01;        
        // 明細
        $C_TRANSACTION_TYPE[1] = TransactionTypes::DIRECT_PUSH_BONUS_GENERATION_1;
        $C_TRANSACTION_TYPE[2] = TransactionTypes::DIRECT_PUSH_BONUS_GENERATION_2;
        $C_TRANSACTION_TYPE[3] = TransactionTypes::DIRECT_PUSH_BONUS_GENERATION_3;
        $C_TRANSACTION_TYPE[4] = TransactionTypes::DIRECT_PUSH_BONUS_GENERATION_4;

        $C_OUT_TRANSACTION_TYPE[1] = TransactionTypes::DIRECT_PUSH_BONUS_OUT_1;
        $C_OUT_TRANSACTION_TYPE[2] = TransactionTypes::DIRECT_PUSH_BONUS_OUT_2;
        $C_OUT_TRANSACTION_TYPE[3] = TransactionTypes::DIRECT_PUSH_BONUS_OUT_3;
        $C_OUT_TRANSACTION_TYPE[4] = TransactionTypes::DIRECT_PUSH_BONUS_OUT_4;

        // 先找我們新註冊的那顆ball
        $user_ball_db = Ball::where('id',$new_ball_id)->first();          

        if(!$user_ball_db){
            $result = array('error' => ApiError::RECOMMENDER_ID_ERROR);            
            return $result;
        }

        // 若此人無上線
        if($user_ball_db->recommender_id == 0){ 
            return $result;
        }

        // 若此人上線tree為空 (保險起見多檔一次0.0)
        if($user_ball_db->recommend_tree == ''){  
            return $result;
        }

        // // 先return測試一下
        // $result['data'] = 'user_account_id = '.$user_account_id;
        // return $result;  
        
        // 把自己上三代的推薦人找出來
        $upline_array = explode(',',(string)$user_ball_db->recommend_tree);
        $generation = 0;
        // 把三代以內的上線ball_id 丟到 $upline_id_generation[]
        for ($i=count($upline_array)-1; $i >= 0; $i--) { 

            $generation++; // (generation從1開始喔!!! 只有 1 2 3 4 @@!!)
            if($generation > $C_MAX_GENERATION){ // 最多幾(4)代
                break;
            }

            $upline_ball_db = Ball::find($upline_array[$i]);
            if(!$upline_ball_db) { // 這個上限不存在 (安全起見檔一下) 暫時先往下一個找..            
                continue;
            }

            if($upline_ball_db->level < 1){ // 如果這代上限未激活 (安全起見檔一下)
                continue;
            }

            // 二代以上必須level 2才能領
            // if($generation > 1 && $upline_ball_db->level < 2){
            //     continue;
            // }
            
            // 代數獎金
            $user_new_set = (float)OrganizationService::getSetFromLevel($user_new_level);

            $bonus = 0; // 初值
            if($upline_ball_db->level == 1){ // level 1 領的獎金
                $bonus = (float)$C_LVL_1_BONUS_GENERATION_PERCENTAGE[$generation] * (float)$user_new_set;
            }

            if($upline_ball_db->level == 2){ // level 2 領的獎金
                $bonus = (float)$C_LVL_2_BONUS_GENERATION_PERCENTAGE[$generation] * (float)$user_new_set;
            }

            // 交易單需要的obj
            $obj = new \stdClass(); 
            $obj->connect_id = $user_account_id;
            $from_account_db = User::find($user_account_id);             
            $obj->comment = $from_account_db->account.','.$user_ball_db->id;

            // 若這代已靜態出局
            if($upline_ball_db->static_current >= $upline_ball_db->static_max){
                // 要給提示
                $wallet_db = Wallet::where('user_id',$upline_ball_db->user_id)->first();
                if(!$wallet_db){ // 如果wallet_db不存在 (安全起見檔一下)
                    continue;
                }

                //($user_id, $wallet_id, $coin, $type, $value, $before, $after, $link)
                TransactionRepository::create(
                              $upline_ball_db->user_id
                              , $wallet_db->id
                              , 'Point'
                              , $C_OUT_TRANSACTION_TYPE[$generation]
                              , $bonus
                              , $wallet_db->balance_available + $wallet_db->balance_locked
                              , $wallet_db->balance_available + $wallet_db->balance_locked
                              , json_encode($obj)
                             );            
                continue;
            }

            // 領取獎金           
            $upline_wallet_db = WalletRepository::deposit_by_userID($upline_ball_db->user_id, $bonus, 'Point');

            // 交易單

            TransactionRepository::create(
                                  $upline_ball_db->user_id
                                  , $upline_wallet_db->id
                                  , 'Point'
                                  , $C_TRANSACTION_TYPE[$generation]
                                  , $bonus
                                  , $upline_wallet_db->before
                                  , $upline_wallet_db->after
                                  , json_encode($obj)
                                 );            


        }

        return $result;
    }


    // // ＊＊ 計算PV ＊＊
    // public static function calculatePV($result, $user_account_id, $level)
    // {
    //     //$set
    //     // 先找自己的branch_tree
    //     $ball_db = Ball::select("branch_tree","pv_left","pv_right","total_left","total_right")
    //              ->where("user_id", $user_account_id)
    //              ->first();

    //     if( $ball_db->branch_tree==""){ // root 沒有 branch_tree
    //         return $result;
    //     }

    //     $branch_tree_array = explode(",", $ball_db->branch_tree);

    //     // 根據每個人的RL 左/右做一次重新計算
    //     for ($i=0; $i < count($branch_tree_array) ; $i++) { 

    //         // 根據每個人的RL 做一次重新計算左或右
    //         $like_db = Ball::select("level","id","user_id") // (用like粗算)
    //             ->where('branch_tree', 'like', '%'.$branch_tree_array[$i].'%')
    //             ->get();
        
    //         $RL = '';
    //         if(substr($branch_tree_array[$i], -1) == "R"){
    //             $RL = 'R';
    //         }

    //         if(substr($branch_tree_array[$i], -1) == "L"){
    //             $RL = 'L';
    //         }

    //         $total_set = 0;
    //         for ($j=0; $j < count($like_db); $j++) { 
                
    //             $j_set = OrganizationService::getSetFromLevel($like_db[$j]->level);
    //             // if($j_set == 'inactivated'){
    //             //     $j_set = 0;
    //             // }
    //             $total_set += $j_set;
    //             //$test = $test.',['.$branch_tree_array[$i].']'.$like_db[$j]->id.$RL;
    //         }


    //         $calculate_id = substr_replace($branch_tree_array[$i],'',-1);
    //         $calculate_db = Ball::find($calculate_id); // (用like粗算)

    //         if(substr($branch_tree_array[$i], -1) == "R"){
    //             $calculate_db->total_right = $total_set;
    //             $calculate_db->save();
                
    //         }

    //         if(substr($branch_tree_array[$i], -1) == "L"){
    //             $calculate_db->total_left = $total_set;
    //             $calculate_db->save();
    //         }

    //         // ↑↑↑ 到這邊為止, 左右新total已存入 ↑↑↑

    //         // 左右先都扣掉上次pv 取小
    //         $last_time_pv = $calculate_db->pv;
    //         $left  = $calculate_db->total_left  - $last_time_pv;
    //         $right = $calculate_db->total_right - $last_time_pv;
    //         $this_time_pv = min($left, $right);
    //         if($this_time_pv > $last_time_pv){ // 超過上次才碰

    //             $calculate_db->pv = $this_time_pv;
    //             //$calculate_db->pv_rl = $left.'/'.$right;
    //             $calculate_db->pv_left = $left;
    //             $calculate_db->pv_right = $right;
    //             $calculate_db->save();

    //             // 然後發獎金    
    //             static::pvBonus($calculate_db->user_id, $this_time_pv, $user_account_id, $level);
    //         }

    //     }
        
    //     //$result['data'] = "test = ".$test;
    //     return $result;
    // }


    // // ＊＊ 對碰獎金 ＊＊　
    // private static function pvBonus($target_account_id, $pv_value, $active_id, $level)
    // {

    //     $BONUS_PV_PERCENTAGE_LVL_1 = ((float)(PointRules::BONUS_PV_PERCENTAGE_LVL_1))*0.01; // level-1 對碰獎金
    //     $BONUS_PV_PERCENTAGE_LVL_2 = ((float)(PointRules::BONUS_PV_PERCENTAGE_LVL_2))*0.01; // level-2 對碰獎金
    //     $BONUS_PV_PERCENTAGE_LVL_3 = ((float)(PointRules::BONUS_PV_PERCENTAGE_LVL_3))*0.01; // level-3 對碰獎金

    //     // $bonus_pv == ?
    //     $bonus_pv_rate = 0;
    //     if($level == 1) $bonus_pv_rate = $BONUS_PV_PERCENTAGE_LVL_1;
    //     if($level == 2) $bonus_pv_rate = $BONUS_PV_PERCENTAGE_LVL_2;
    //     if($level == 3) $bonus_pv_rate = $BONUS_PV_PERCENTAGE_LVL_3;

    //     $coin = 'Point';
    //     $type = TransactionTypes::PV_BONUS; //'pvBonus';

    //     $result = array('error' => ApiError::SUCCESS);

    //     $to_wallet_db = Wallet::Select("balance_available","id")
    //             ->where("user_id", $target_account_id)
    //             ->where('coin','Point')
    //             ->first();
    
    //     if( !$to_wallet_db ){
    //         $result = array('error' => ApiError::TO_ACCOUNT_ERROR);
    //         return $result;
    //     }

    //     $to_wallet_before = $to_wallet_db->balance_available + $to_wallet_db->balance_locked;
    //     $to_wallet_after = $to_wallet_db->balance_available; // 先給個初值
    //     $to_wallet_id = $to_wallet_db->id;
        
    //     // 看今天acc_pv有沒有超過
    //     $ball_db = Ball::All()
    //             ->where("user_id", $target_account_id)
    //             ->first();
    //     //$acc_array = explode("/", $ball_db->acc_pv_today);

    //     //$quota = (float)$acc_array[1] - (float)$acc_array[0];  
    //     $quota = (float)$ball_db->acc_pv_max - (float)$ball_db->acc_pv_today;  
    //     $is_over_quota = false;  
    //     if($quota <= 0){
    //         $is_over_quota = true;   // 對碰封頂了
    //         $type = TransactionTypes::PV_BONUS_FULL;
    //     }

    //     // 算獎金
    //     $bonus = (float)$pv_value * $bonus_pv_rate; // 對碰獎金10%

    //     if(!$is_over_quota){ // 沒有封頂才發獎金

    //         //$acc_array[0] = (float)$acc_array[0] + $bonus;
    //         $ball_db->acc_pv_today = (float)$ball_db->acc_pv_today + $bonus;
    //         //$ball_db->acc_pv_today = $acc_array[0].'/'.$acc_array[1];
    //         $ball_db->save();

    //         // 收點
    //         $lock_wallet_db = WalletRepository::deposit_by_userID($target_account_id, $bonus, 'Point');
    //         $to_wallet_after = $lock_wallet_db->after;
    //     }

    //     // 交易單 to (封頂也要填寫)
    //     $obj = new \stdClass(); 
    //     $to_account_db = User::Select("account","id")
    //             ->where("id", $active_id)
    //             ->first();

    //     $to_ball_db = Ball::select('id')
    //             ->where('user_id', $to_account_db->id)
    //             ->first();

    //     $obj->comment = $to_account_db->account.','.$to_ball_db->id;  
              
    //     TransactionRepository::create(
    //                           $target_account_id
    //                           , $to_wallet_id
    //                           , $coin
    //                           , $type
    //                           , $bonus
    //                           , $to_wallet_before
    //                           , $to_wallet_after
    //                           , json_encode($obj)
    //                          );

    //     return $result;

    // }


}