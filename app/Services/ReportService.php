<?php

namespace App\Services;

use Cache;

use App\Models\BetReport;

use App\Tools\DataTable;

class ReportService extends Service
{
    public static function getBetReport($request)
    {
        $columns = array(
            array('db' => 'created_at', 'dt' => 'created_at',
                'formatter' => function ($d, $row) {
                    return strtotime($d);
                }),
            array('db' => 'total_amount', 'dt' => 'total_amount'),
            array('db' => 'a_bet_amount', 'dt' => 'a_bet_amount'),
            array('db' => 'a_payout_amount', 'dt' => 'a_payout_amount'),
            array('db' => 'a_balance', 'dt' => 'a_balance'),
            array('db' => 'a_profit', 'dt' => 'a_profit'),
            array('db' => 'b_bet_amount', 'dt' => 'b_bet_amount'),
            array('db' => 'b_payout_amount', 'dt' => 'b_payout_amount'),
            array('db' => 'b_balance', 'dt' => 'b_balance'),
            array('db' => 'b_commission', 'dt' => 'b_commission'),
            array('db' => 'total_balance', 'dt' => 'total_balance'),
            array('db' => 'income_ratio', 'dt' => 'income_ratio'),
        );

        $eloquent = new BetReport();
        $eloquent = $eloquent->where('created_at', '<', now());
        $result = DataTable::complex($eloquent, $request, $columns);

        return $result;
    }
}