<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

use App\Defined\ApiError;
use App\Defined\SessionNames;
use App\Defined\PointRules;
use App\Defined\TransactionTypes;

use App\Models\PhoneVerify;
use App\Models\System;

use App\Services\Service;
use App\Services\SystemService;

class PhoneVerifyService extends Service
{

    // ＊＊ 檢查認證 ＊＊
    public static function verify($country_code, $phone)
    {
        
        $is_verify = 0;  //  empty unverified bound
        $phone_verify_db = PhoneVerify::where('country_code',$country_code)
                        ->where('phone',$phone)
                        ->first();
        
        if($phone_verify_db){
            if($phone_verify_db->verify == 1){
                $is_verify = 1;
            } 
                
        }

        return $is_verify;
    }


    // ＊＊ 檢查認證 ＊＊
    public static function verify_state($country_code, $phone, $rand)
    {
        
        $result['value'] = 'empty';  //  1.無此資料(可申請)
        $phone_verify_db = PhoneVerify::where('country_code',$country_code)
                        ->where('phone',$phone)
                        ->first();
        
        if($phone_verify_db){
            if($phone_verify_db->verify == 1){ // 2.已驗證
                $result['value'] = 'bound';
            }  else {
                $result['value'] = 'not_empty'; 
                $daily_max = SystemService::getDailyMaxPhoneVerify();

                if($phone_verify_db->apply_count +1 > $daily_max){
                    $result['value'] = 'apply_max'; // 3.超過驗證次數 叫他等明天
                } else {
                    $result['value'] = 're_apply'; //  4.未超過驗證次數 洗掉錯誤換4碼告訴他申請成功
                    $phone_verify_db->verify_code = $rand;
                    $phone_verify_db->wrong_count = 0;
                    $phone_verify_db->apply_count = $phone_verify_db->apply_count + 1;
                    $phone_verify_db->save();

                }
            }            
        }

        return $result;
    }


    // ＊＊ 檢查認證 ＊＊
    public static function verify_code($country_code, $phone, $verify_code)
    {
        
        $result['value'] = 'empty';  //  empty unverified bound
        $phone_verify_db = PhoneVerify::where('country_code',$country_code)
                        ->where('phone',$phone)
                        ->first();
        
        if($phone_verify_db){
            if($phone_verify_db->verify == 1){
                $result['value'] = 'bound';
            } else {

                if($phone_verify_db->wrong_count >= 3){
                    $result['value'] = 'over_max_wrong';
                } else if ( $verify_code == $phone_verify_db->verify_code) {                    
                    $phone_verify_db->verify = 1;
                    $result['value'] = 'verified';
                    $phone_verify_db->save();
                } else {
                    $result['value'] = 'wrong_code';
                    $phone_verify_db->wrong_count = $phone_verify_db->wrong_count + 1;
                    $phone_verify_db->save();

                    if($phone_verify_db->wrong_count >= 3){
                         $result['value'] = 'max_wrong';
                        // $phone_verify_db->delete();
                    }
                }
                
               
            }            
        }

        return $result;
    }



}