<?php

namespace App\Defined;

abstract class ApiError
{
    
    // ＊＊ No Error ＊＊　    
    const SUCCESS = 0;


    // ＊＊ ApiController ＊＊　
    // 未定义的参数
    const UNDEFINED_ARGUMENT = 101;
    // 數值不合規範
    const ILLEGAL_VALUES = 102;
    // 參數不合法 (應該都是惡意的攻擊)
    const ILLEGAL_VALUES_DANGER = 103;
    // 必須為10的倍數
    const INT_NOT_10_MULTIPLE = 104;
    // 超過數量上限
    const OVER_MAX_AMOUNT = 105;
    // 必須是100的倍數
    const INT_NOT_100_MULTIPLE = 106;


    // ＊＊ General ＊＊　
    // 帳號或密碼錯誤
    const USER_LOGIN_ERROR = 201;    
    // 找不到分帳號
    const USER_GET_SPECIFIC_BRANCH_ID_NULL = 202; 
    // 查詢分帳號權限不足
    const USER_GET_SPECIFIC_ID_ILLEGAL = 203; 
    // 帳號錯誤
    const USER_ID_NULL = 204;     
    // 帳號重複
    const USER_ACCOUNT_DOUBLE_APPLY = 205;     
    // 註冊失敗(創user db失敗)
    const USER_REGISTER_FAILED_ISSUE_01 = 206;
    // 註冊推薦id有誤
    const USER_REGISTER_RECOMMENDER_ID_ERROR = 207;
    // 註冊推薦上家id有誤
    const USER_REGISTER_UPER_ID_ERROR = 208;
    // 註冊帳號不能有底線
    const REGISTER_ACCOUNT_INCLUDE_BOTTOM_LINES = 209;
    // 兩次輸入密碼不同
    const NEWPASSWORD2_NOT_MATCH = 210;
    // 密碼錯誤
    const PASSWORD_FAILED = 211;
    // 註冊失敗-02(創ball db失敗)
    const USER_REGISTER_FAILED_ISSUE_02 = 212;
    // 註冊失敗-03(創wallet db失敗)
    const USER_REGISTER_FAILED_ISSUE_03 = 213;
    // 新的配套必須高於現有配套
    const ACTIVE_SET_TOO_LOW = 214;
    // 激活配套點數不足
    const ACTIVE_SET_WALLET_NOT_ENOUGH = 215;
    // 點數不足
    const POINT_NOT_ENOUGH = 216;
    // 帳號有誤
    const GIFT_POINT_ACCOUNT_ERROR = 217;
    // 目標帳戶和來源帳戶相同
    const TARGET_SAME_ACCOUNT = 218;
    // 必須是自己的推薦下線
    const RECOMMEND_DOWNLINE_ERROR = 219;
    // 來源帳號有誤
    const FROM_ACCOUNT_ERROR = 220;
    // 目標帳號有誤
    const TO_ACCOUNT_ERROR = 221;
    // 下線代數不符
    const DOWNLINE_FLOOR_ERROR = 222;
    // 推薦人帳號有誤
    const RECOMMENDER_ID_ERROR = 223;
    // 目前無法激活
    const STATIC_QUOTA_ENOUGH = 224;
    // 委託-點數不足
    const ORDER_POINT_NOT_ENOUGH = 225;
    // 委託-撤銷失敗
    const DELETE_ORDER_FAILED = 226;    
    // 委託-系統忙碌中
    const ORDER_BUSY = 227;  
    // 交易已不存在
    const TRADING_NULL = 228;  
    // 取消交易-不合法
    const CANCEL_TRADING_ILLEGAL = 229;  
    // 確認交易-不合法
    const CONFIRM_TRADING_ILLEGAL = 230; 
    // 轉帳失敗-單據有誤
    const DEAL_TRADING_NULL = 231; 
    // 轉帳失敗-委託有誤
    const DEAL_ORDER_NULL = 232; 
    // 轉帳失敗-賣方帳號有誤
    const DEAL_SELLER_NULL = 233; 
    // 轉帳失敗-買方帳號有誤
    const DEAL_BUYER_NULL = 234; 
    // 轉帳失敗-延遲過久
    const DEAL_LOCK_DELAY = 235; 
    // 取消交易失敗-延遲過久
    const CANCEL_TRADING_DELAY = 236;
    // 推薦人不存在
    const REGISTER_RECOMMENDER_NULL = 237;
    // 未激活帳號不開放註冊下線
    const INACTIVED_ACCOUNT_ILLEGAL_REGISTER_DOWNLINE = 238;
    // 未激活帳號-限制交易700點
    const INACTIVE_BUY_POINT_LIMIT_700 = 239;
    // 未激活帳號-一次只能一單交易
    const INACTIVE_BUY_POINT_LIMIT_1_TRADING = 240;
    // 對象不存在
    const TARGET_USER_NULL = 241;
    // 商品名稱不存在
    const PRODUCT_NAME_NULL = 242;
    // 配套不合法
    const ILLEGAL_LEVEL = 243;
    // 今日儲值額度已滿
    const DAILY_PAY_LIMITED = 244;
    // 銀行已綁定
    const USER_BANK_BINDED = 245;
    // 尚未儲存國碼
    const USER_COUNTRYCODE_NULL = 246;
    // 尚未儲存電話
    const USER_PHONE_NULL = 247;
    // 綁定狀態錯誤(1) (目前不處於unbound狀態)
    const PHONE_BINDED_FAILED_1 = 248;
    // 超過每日申請次數
    const PHONE_BINDED_FAILED_2 = 249;
    // 請先申請驗證碼
    const PHONE_BINDED_FAILED_3 = 250;
    // 綁定狀態錯誤(4) User->binded_phone 不是 (verify_code,datatime) 的格式
    const PHONE_BINDED_FAILED_4 = 251;
    // 驗證碼不符 
    const PHONE_BINDED_FAILED_5 = 252;
    // 銀行未綁定
    const USER_BANK_UNBOUND = 253;
    // 手機未綁定 
    const USER_MOBILE_UNBOUND = 254;
    // 凍結 無法交易
    const USER_FROZEN = 255;
    // 註冊失敗-04(創colorwallet db失敗)
    const USER_REGISTER_FAILED_ISSUE_04 = 256;
    // 彩點不足
    const ACTIVE_SET_COLORWALLET_NOT_ENOUGH = 257;
    // 驗證錯誤超過三次 
    const PHONE_BINDED_FAILED_6 = 258;
    // 無點可買
    const NO_POINT_ON_THE_MARKET = 259;
    // 交易開啟8小時候才能取消
    const SELLER_CANCEL_NEED_8_HOURS = 260;
    // 目前仍有交易進行中
    const STILL_HAVE_TRADING_ON_THIS_ORDER = 261;
    // 委託不存在
    const ORDER_NULL = 262;
    // 委託已撤銷
    const ORDER_DELETED = 263;
    // 推薦人尚未開通帳號
    const REGISTER_RECOMMENDER_NOT_ACTIVE = 264;
    // 帳號凍結
    const USER_FROZEN_BY_ID = 265;
    // 相關銀行帳戶凍結
    const USER_FROZEN_BY_BANK_ACCOUNT = 266;
    // 相關電話凍結
    const USER_FROZEN_BY_PHONE = 267;
    // 相關人員被凍結
    const TRADING_FROZEN_ERROR = 268;
    // 提領編號錯誤
    const WITHDRAW_ID_ERROR = 269;
    // 今日提領額度已滿
    const WITHDRAW_LIMITED_BY_ADMIN = 270;
    // 未激活帳號不開放註冊球
    const INACTIVED_ACCOUNT_ILLEGAL_REGISTER_BALL = 271;
    // 帳號不擁有這顆球
    const IS_NOT_BALL_OWNER = 272;
    // 超過今日最高提問次數
    const OVER_DAILY_MAX_QUESTION_ASK = 273;




    // ＊＊ Language ＊＊　
    // 語系不合法
    const LANGUAGE_ILLEGAL= 301;


    // ＊＊ Token ＊＊
    // token到期
    const TOKEN_EXPIRE = 401;
    // token被拒绝
    const TOKEN_BE_REVOKED = 402;
    // session被拒绝
    const SESSION_BE_REVOKED = 403;


    // ＊＊ Admin ＊＊
    // amind帳號或密碼錯誤
    const ADMIN_LOGIN_ERROR = 501;
    // admin不存在
    const ADMIN_NULL = 502;
    // 對象不存在
    const ADMIN_FIND_USER_ID_NULL = 503;
    // 交易不存在
    const ADMIN_FIND_TRADING_NULL = 504;  
    // 委託不存在
    const ADMIN_FIND_ORDER_NULL = 505;    
    // 請求錯誤
    const ADMIN_SWITCH_LIMIT_PAY_ERROR = 506; 
    // 密碼錯誤
    const ADMIN_PASSWORD_FAILED = 507;    
    // 兩次輸入密碼不同
    const ADMIN_NEWPASSWORD2_NOT_MATCH = 508;   
    // 找不到此用戶
    const ADMIN_FIND_USER_NULL = 509;
    // 銀行帳號為空值
    const BANK_ACCOUNT_WRONG_FORMAT = 510;
    // 推薦帳號為空時, 配套必須為0
    const REGISTER_NEW_NO_RECOMMENDER_INABLE_TO_WITH_LEVEL = 511;
    // 分紅資料有誤
    const RATE_DATABASE_NULL = 512;
    // 日期不合法
    const DATETIME_EXPIRED = 513;
    // 提問編號錯誤
    const ASK_ID_NULL = 514;

    // ＊＊ Pay ＊＊    
    // 金額有誤
    const ILLEGAL_PAY_PRICE = 601;  
    // 產生訂單發生錯誤
    const PAY_ORDER_ERROR = 602; 
    const PAY_ORDER_ERROR_1 = 603; // obtained_at重複, 一定會發生在金流(按回上頁), 此時頁面自動返回儲值頁
    const PAY_ORDER_ERROR_2 = 604; 
  


    // ＊＊ 簡訊 ＊＊ 
    // 簡訊錯誤  
    const SMS_ERROR = 701;
    // 請稍後再傳 
    const SMS_TIME_IS_TOO_CLOSE = 702;

}