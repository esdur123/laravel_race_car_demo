<?php

namespace App\Defined;

abstract class SessionNames
{
    const USER_ID = 'user_id';
    const TOKEN = 'token';
    const ADMIN_ID = 'admin_id';
    const LANGUAGE = 'language';
}