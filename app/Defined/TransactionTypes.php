<?php

namespace App\Defined;

abstract class TransactionTypes
{

	const DIRECT_PUSH_BONUS_GENERATION_1  = 'Direct-Push-Bonus-Generation-1'; // 一代直推獎金
	const DIRECT_PUSH_BONUS_GENERATION_2  = 'Direct-Push-Bonus-Generation-2'; // 二代直推獎金
	const DIRECT_PUSH_BONUS_GENERATION_3  = 'Direct-Push-Bonus-Generation-3'; // 三代直推獎金
	const DIRECT_PUSH_BONUS_GENERATION_4  = 'Direct-Push-Bonus-Generation-4'; // 四代直推獎金	
	const PV_BONUS               = 'pvBonus';  		   	     // 對碰獎金
	const PV_BONUS_FULL          = 'pvBonus-Full';  	     // 對碰封頂
	const STATIC_RE_ACTIVE       = 'Static-reActive';  	     // 手動激活 靜態out
	const STATIC_SET             = 'Active-Set';  	         // 升級Set (激活)
	const MARKET_SELL            = 'Market-Sell';  	         // 市場賣點
	const MARKET_BUY             = 'Market-Buy';  	         // 市場買點	
	const TX_ATM_BUY        	 = 'TX-ATM-Buy';             // 鈦鑫ATM購點
	const TX_ATM_BUY_COLOR_POINT = 'TX-ATM-Buy-Color-Point'; // 鈦鑫ATM購點(彩點)
	const DAILY_STATIC_INACTIVED = 'Daily-Static-Inactived'; // 未開通-靜態獎金
	const DAILY_STATIC_OUT       = 'Daily-Static-Out';       // (轉入失敗) 靜態出局
	const AUTO_REACTIVE          = 'Auto-reActive';          // 自動激活
	const REACTIVE_FAILED        = 'Reactive-Failed';        // 自動激活失敗
	const DAILY_STATIC           = 'Daily-Static';           // 每日靜態獎金
	const DAILY_STATIC_RL_10K_BONUS  = 'Daily-Static-10K-Bonus'; // 每日靜態獎金	
	const FEE_DAILY_STATIC       = 'Fee-Daily-Static';       // 靜態獎金手續費
	const AUTO_ACTIVE            = 'Auto-Active';            // 自動激活靜態出局
	const LOTTERY_9BLOCKS_BET        = 'Lottery-9Blocks-Bet';            // 抽抽樂下單注
	const LOTTERY_9BLOCKS_WIN_POINTS = 'Lottery-9Blocks-WinPoints';      // 抽抽樂單注贏錢
	const LOTTERY_9BLOCKS_10TIMES_BET        = 'Lottery-9Blocks-10times-Bet';            // 抽抽樂下10注
	const LOTTERY_9BLOCKS_10TIMES_WIN_POINTS = 'Lottery-9Blocks-10times-WinPoints';      // 抽抽樂10注贏錢
	const USER_WITHDRAW = 'User-Withdraw';      // 提領
	const DIRECT_PUSH_BONUS_OUT_1       = 'Direct-Push-Bonus-Generation-1-Out';       // (轉入失敗) 一代直推獎金-靜態出局
	const DIRECT_PUSH_BONUS_OUT_2       = 'Direct-Push-Bonus-Generation-2-Out';       // (轉入失敗) 二代直推獎金-靜態出局
	const DIRECT_PUSH_BONUS_OUT_3       = 'Direct-Push-Bonus-Generation-3-Out';       // (轉入失敗) 三代直推獎金-靜態出局
	const DIRECT_PUSH_BONUS_OUT_4       = 'Direct-Push-Bonus-Generation-4-Out';       // (轉入失敗) 四代直推獎金-靜態出局

	const PERFORMANCE_100K_REWARD  = 'Performance-100K-Reward';  // (每日獎金10萬) 
	const PERFORMANCE_200K_REWARD  = 'Performance-200K-Reward';  // (每日獎金20萬) 
	const PERFORMANCE_300K_REWARD  = 'Performance-300K-Reward';  // (每日獎金30萬) 
	const PERFORMANCE_400K_REWARD  = 'Performance-400K-Reward';  // (每日獎金40萬) 
	const DAILY_REWARD_OUT         = 'Daily-Reward-Out';       // (轉入失敗) 每日獎金



}