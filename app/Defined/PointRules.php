<?php

namespace App\Defined;

abstract class PointRules
{

	const SET_LVL_0 = '0' ;  // 未激活
	const SET_LVL_1 = '700';  // 配套1
	const SET_LVL_2 = '7000'; // 配套2
	//const SET_LVL_3 = ''; // 配套3


	const PRODUCT_01_NAME = '點數'; // 幸運草 最大交易量
	const PRODUCT_01_PRICE = '5';        // 幸運草 價格0.0
	const PRODUCT_01_MAX_AMOUNT = '6000'; // 幸運草 最大交易量

	const STATIC_OUT_RATE_LVL_1 = '3';  // Lv1 靜態3倍出局
	const STATIC_OUT_RATE_LVL_2 = '3';  // Lv2 靜態3倍出局
	//const STATIC_OUT_RATE_LVL_3 = '5';  // 靜態5倍出局

   // const BONUS_DIRECT_PUSH_PERCENTAGE_GENERATION_1 = '10'; // 一代直推獎金 = 10%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_1 = '8';  // LVL 1 二代直推獎金 = 8%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_2 = '6';  // LVL 1 二代直推獎金 = 6%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_3 = '4';  // LVL 1 三代直推獎金 = 4%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_1_GENERATION_4 = '2';  // LVL 1 三代直推獎金 = 2%

   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_1 = '10';  // LVL 2 二代直推獎金 = 10%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_2 = '8';   // LVL 2 二代直推獎金 = 8%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_3 = '6';   // LVL 2 三代直推獎金 = 6%
   const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2_GENERATION_4 = '4';   // LVL 2 三代直推獎金 = 4%


   // const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_2 = '15'; // 100 USD SET 直推獎金 = 15%
   // const BONUS_DIRECT_PUSH_PERCENTAGE_LVL_3 = '20'; // 500 USD SET 直推獎金 = 20%

    const BONUS_DAILY_STATIC = '10'; // 靜態獎金 (1% * SET)/日, 程式要除以1000
    const FEE_DAILY_STATIC   = '0';   // 靜態獎金手續費 30% (靜態獎金*30%)

    // const BONUS_PV_PERCENTAGE_LVL_1 = '5';  // level-1 對碰獎金 = 5%
    // const BONUS_PV_PERCENTAGE_LVL_2 = '5';  // level-2 對碰獎金 = 5%
    //const BONUS_PV_PERCENTAGE_LVL_3 = '10'; // level-3 對碰獎金 = 10%     

    const COST_STATIC_RE_ACTIVE_LVL_1 = '700';  // 扣款-靜態激活 level-1 
    const COST_STATIC_RE_ACTIVE_LVL_2 = '7000'; // 扣款-靜態激活 level-2 
    //const COST_STATIC_RE_ACTIVE_LVL_3 = '500'; // 扣款-靜態激活 level-3 

}