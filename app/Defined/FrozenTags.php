<?php

namespace App\Defined;

abstract class FrozenTags
{

	const USER_ID = 'USER_ID' ; // 依會員id
	const PHONE = 'PHONE' ;       // 依電話
	const BANK_ACCOUNT = 'BANK_ACCOUNT' ; // 依銀行帳號
	
}