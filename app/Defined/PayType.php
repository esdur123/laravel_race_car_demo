<?php

namespace App\Defined;

abstract class PayType
{
    const ECPAY_ATM = "ECPAY_ATM";
    const ECPAY_CREDIT = "ECPAY_CREDIT";
    const ECPAY_CVS = "ECPAY_CVS";
    const ECPAY_BARCODE = "ECPAY_BARCODE";

    const TXPAY_CSPM = "TXPAY_CSPM";
    const TXPAY_VBANK = "TXPAY_VBANK";


    static function getFee($pay_type, $pay_price)
    {
        $fee_list = array(
            PayType::ECPAY_ATM => 20,
            PayType::ECPAY_CREDIT => ceil($pay_price * 0.03),
            PayType::ECPAY_CVS => 20,
            PayType::ECPAY_BARCODE => 26,

            PayType::TXPAY_CSPM => 26,
            PayType::TXPAY_VBANK => 26,
        );

        return $fee_list[$pay_type];
    }
}
