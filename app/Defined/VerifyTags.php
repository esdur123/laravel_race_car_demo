<?php

namespace App\Defined;

abstract class VerifyTags
{

	const USER_FROZEN = 'USER_FROZEN' ;       // 會員凍結
	const USER_BIND_BANK = 'USER_BIND_BANK' ; // 會員綁定銀行
	const ALL_USERS_LIMITED_PAY = 'ALL_USERS_LIMITED_PAY' ;  // 管理員停止所有會員儲值功能:今日儲值額度已滿
	const ALL_USERS_LIMITED_WITHDRAW = 'ALL_USERS_LIMITED_WITHDRAW' ;  // 管理員停止所有會員提領功能:今日提領額度已滿


}