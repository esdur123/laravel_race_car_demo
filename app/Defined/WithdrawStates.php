<?php

namespace App\Defined;

abstract class WithdrawStates
{
    const ACCEPTING = 'Accepting';
    const WAITING = 'Waiting';
    const DEAL = 'Deal';    
    const USER_CANCEL = 'User-Canceled';

}