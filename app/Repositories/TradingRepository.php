<?php

namespace App\Repositories;
use Lock;
use DB;
use App\Defined\ApiError;
use App\Defined\VerifyTags;
use App\Defined\FrozenTags;

use App\Models\Trading;
use App\Models\User;
use App\Models\Order;
use App\Models\Wallet;

use App\Services\UserService;
use App\Services\FrozenService;

use Carbon\Carbon;


class TradingRepository extends Repository
{

    // ＊＊ 取消交易 (lock) ＊＊　
    public static function cancelTrading($trading_id, $canceller_id, $type)
    {

        if($type=='sell') $type = 'seller-cancel';
        if($type=='buy') $type = 'buyer-cancel';

        $result = array('error' => ApiError::SUCCESS);

        $find_trading_db = Trading::find($trading_id);
        if(!$find_trading_db){
            $result['error'] = ApiError::TRADING_NULL;
            return $result;
        }

        if($find_trading_db->state == 'cancel'){
            $result['error'] = ApiError::TRADING_NULL;
            return $result;    
        }

        // 賣家8小時候才能取消
        if($type == 'seller-cancel'){
            $expire_time = Carbon::now()->subHours(8);
            $start_time = $find_trading_db->created_at;
            if( !($start_time->lt($expire_time)) ){
                $result['error'] = ApiError::SELLER_CANCEL_NEED_8_HOURS;
                return $result;
            } else { // 過8小時, 就把買家凍結 然後往下繼續
                // 取得user電話
                $user_db = User::find($find_trading_db->buyer_user_id);
                if($user_db){
                    FrozenService::frozen(FrozenTags::PHONE, '('.$user_db->country_code.')'.$user_db->phone, 1);
                }

            }
        }

        // 非買家或賣家 有問題 可能被攻擊0.0
        if( !($find_trading_db->seller_user_id == $canceller_id || $find_trading_db->buyer_user_id == $canceller_id) ){
            $result['error'] = ApiError::CANCEL_TRADING_ILLEGAL;
            return $result;
        }

        // 先把order單的 trading 加回去
        // 如果這張單背後的委託單已經被軟刪除, 那錢包的lock要退回這個數字
        DB::transaction(function() use($find_trading_db, $type, $trading_id){
            $lock_order_key = 'Order@order_id:' . $find_trading_db->order_id;
            try {
                Lock::acquire($lock_order_key);

                // 第二層 seller-wallet
                $lock_seller_key = 'Wallet@user_id:' . $find_trading_db->seller_user_id;
                try {
                    Lock::acquire($lock_seller_key);

                    // logic
                    // 處理trading_db
                    $trading_db = Trading::find($trading_id);
                    $state = 'cancel';
                    $trading_db->state = $state;
                    $trading_db->confirm_value = $type;
                    $trading_db->save();

                    // 處理order_db
                    $order_db = Order::withTrashed()->
                                    find($trading_db->order_id);

                    $order_db->trading -= $trading_db->amount;
                    $order_db->save();

                    // 如果此order單已被軟刪除, 那wallet的lock部分要在這邊補回來
                    if($order_db->deleted_at){
                        $wallet_db = Wallet::where('user_id', $trading_db->seller_user_id)
                                ->where('coin','Point')
                                ->first();
                        $wallet_db->balance_locked    -= $trading_db->amount;
                        $wallet_db->balance_available += $trading_db->amount;
                        $wallet_db->save();
                    }

                    
                    //$result['error'] = ApiError::SUCCESS;

                } finally {
                    Lock::release($lock_seller_key);
                }                        

            } finally {
                Lock::release($lock_order_key);
            }
        }); // end db transaction

        return $result;
    }


    // ＊＊ 修改某使用者在交易中Trading單的個人資訊 ＊＊　
    public static function updateTradingUserInfo($user_id)
    {

        $trading_db = Trading::Where('seller_user_id', $user_id)
                    ->orWhere('buyer_user_id', $user_id)
                    ->get();

        $new_trading_info = UserService::getTradingUserInfo($user_id);

        for ($i=0; $i < count($trading_db) ; $i++) { 

            if($trading_db[$i]->state != 'waiting'){
                continue;
            }

            if($trading_db[$i]->seller_user_id == $user_id){
                $trading_db[$i]->seller_info = $new_trading_info;
                $trading_db[$i]->save();
            }

            if($trading_db[$i]->buyer_user_id == $user_id){
                $trading_db[$i]->buyer_info = $new_trading_info;
                $trading_db[$i]->save();
            }
            # code...
        }

        return true;
    }


     // ＊＊ 改狀態 open(交易中) 至 close(交易完成) 或 cancel(委託取消) ＊＊
    public static function updateState($order_id, $state, $confirm_value)
    {
        $trading_db = Trading::where('order_id', $order_id)
                ->where('state','!=','deal')
                ->where('state','!=','cancel')
                ->get();

        for ($i=0; $i < count($trading_db) ; $i++) { 
            $trading_db[$i]->state = $state;
            $trading_db[$i]->save();
        }

        return $trading_db;
    }


     // ＊＊ 創 交易紀錄單 ＊＊
    public static function create($order_id, $amount, $seller_user_id, $buyer_user_id)
    {
        $trading_db = new Trading;
        $trading_db->order_id = $order_id;
        $trading_db->amount = $amount;
        $trading_db->seller_user_id = $seller_user_id;        
        $trading_db->seller_info = UserService::getTradingUserInfo($seller_user_id);
        $trading_db->buyer_user_id = $buyer_user_id;            
        $trading_db->buyer_info = UserService::getTradingUserInfo($buyer_user_id);
        $trading_db->save();

        return $trading_db;
    }

}