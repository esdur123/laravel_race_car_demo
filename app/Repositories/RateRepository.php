<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\Rate;

use App\Repositories\Repository;



class RateRepository extends Repository
{

    // ＊＊ 新增一筆（若重複　就覆蓋過去）＊＊　
    public static function add($year, $month, $day, $rate)
    {
        $return_db = null;
        $rate_db = Rate::where('year', $year)
                        ->where('month', $month)
                        ->where('day', $day)
                        ->first();

        // 如果這天已經設定, 那就覆蓋過去
        if($rate_db){
            $rate_db->year  = $year;
            $rate_db->month = $month;
            $rate_db->day   = $day;
            $rate_db->rate  = $rate;
            $rate_db->save();

            $return_db = $rate_db;
        } else { // 如果沒有　就新創一筆
            $return_db = static::create($year, $month, $day, $rate);
        }

        return $return_db;
    }


    // ＊＊ 創 Rate ＊＊　
    public static function create($year, $month, $day, $rate)
    {
        $rate_db = new Rate;
        $rate_db->year = $year;
        $rate_db->month = $month;
        $rate_db->day = $day;
        $rate_db->rate = $rate;
        $rate_db->save();

        return $rate_db;
    }

}