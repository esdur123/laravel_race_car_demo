<?php


namespace App\Repositories;

use App\Models\BetReport;

class BerReportRepository extends Repository
{
    public static function create($total_amount, $a_win_times, $bet_times = 175)
    {
        /** 固定公式 */
        $bet_amount = round($total_amount / $bet_times / 10 / 2);
        $a_bet_amount = round(-1 * $bet_amount * $bet_times);
        $a_payout_amount = round($bet_amount * $a_win_times * 9.9);
        $a_balance = $a_bet_amount + $a_payout_amount;
        $a_profit = round(-1 * $a_balance * 0.8);
        $b_bet_amount = round(-1 * $bet_amount * $bet_times * 9);
        $b_payout_amount = round($bet_amount * ($bet_times - $a_win_times) * 9.9);
        $b_balance = $b_bet_amount + $b_payout_amount;
        $b_commission = round(-1 * $b_bet_amount * 0.008);
        $total_balance = $a_balance + $a_profit + $b_balance + $b_commission;
        $income_ratio = round($total_balance / $total_amount * 10000) / 10000;
        if ($income_ratio < 0) {
            $income_ratio = 0;
        }

        $bet_report_db = new BetReport();

        $bet_report_db->total_amount = $total_amount;
        $bet_report_db->bet_amount = $bet_amount;
        $bet_report_db->bet_times = $bet_times;
        $bet_report_db->a_win_times = $a_win_times;
        $bet_report_db->a_bet_amount = $a_bet_amount;
        $bet_report_db->a_payout_amount = $a_payout_amount;
        $bet_report_db->a_balance = $a_balance;
        $bet_report_db->a_profit = $a_profit;
        $bet_report_db->b_bet_amount = $b_bet_amount;
        $bet_report_db->b_payout_amount = $b_payout_amount;
        $bet_report_db->b_balance = $b_balance;
        $bet_report_db->b_commission = $b_commission;
        $bet_report_db->total_balance = $total_balance;
        $bet_report_db->income_ratio = $income_ratio;
        $bet_report_db->save();

        return $bet_report_db;
    }
}