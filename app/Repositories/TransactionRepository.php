<?php

namespace App\Repositories;


use App\Models\Transaction;
use App\Models\Wallet;

class TransactionRepository extends Repository
{

     // ＊＊ 創 交易紀錄單 ＊＊
    public static function create($user_id, $wallet_id, $coin, $type, $value, $before, $after, $link)
    {

        if($value == 0){
            return;
        }

        $transaction_db = new Transaction;
        $transaction_db->user_id = $user_id;
        $transaction_db->wallet_id = $wallet_id;
        $transaction_db->coin =  $coin;
        $transaction_db->type = $type;
        $transaction_db->value = $value;
        $transaction_db->before = $before;
        $transaction_db->after = $after;
        $transaction_db->link = $link;
        $transaction_db->save();

        return $transaction_db;
    }


         // ＊＊ 創 交易紀錄單 ＊＊
    public static function create_with_walletdb(Wallet $wallet_db, $user_id, $coin, $type, $value, $link)
    {

        if($value == 0){
            return;
        }

        $before = $wallet_db->balance_available + $wallet_db->balance_locked;
        $after = $before + $value;
        $wallet_db->balance_available += $value;
        $wallet_db->save();

        $transaction_db = new Transaction;
        $transaction_db->user_id = $user_id;
        $transaction_db->wallet_id = $wallet_db->id;
        $transaction_db->coin =  $coin;
        $transaction_db->type = $type;
        $transaction_db->value = $value;
        $transaction_db->before = $before;
        $transaction_db->after = $after;
        $transaction_db->link = $link;
        $transaction_db->save();

        return $transaction_db;
    }

    /**  創建交易明細順便異動錢包金額 */
    public static function createByWallet($wallet_db, $type, $value, $link = null)
    {
        if ($value == 0) {
            return;
        }

        $transaction = new Transaction();
        $transaction->user_id = $wallet_db->user_id;
        $transaction->wallet_id = $wallet_db->id;
        $transaction->coin = $wallet_db->coin;
        $transaction->value = $value;
        $transaction->type = $type;
        if (isset($link)) {
            if (!is_string($link)) {
                $link = json_encode($link);
            }
            $transaction->link = $link;
        }

        /** 錢包交易鎖 */
        $lock_key = 'Wallet@user_id:' . $wallet_db->id;
        try {
            Lock::acquire($lock_key);
            $transaction->before = $transaction->wallet->balance_available;
            $transaction->after = $transaction->wallet->balance_available + $transaction->value;
            $transaction->wallet->increment('balance_available', $transaction->value);
            $transaction->save();
        } finally {
            Lock::release($lock_key);
        }

        return $transaction;
    }
}