<?php


namespace App\Repositories;
use Lock;
use DB;
use App\Defined\ApiError;
use App\Defined\TransactionTypes;
use App\Models\Wallet;
use App\Models\Order;
use App\Models\Trading;
use App\Models\User;
use App\Models\Ball;
use App\Repositories\Repository;
use App\Repositories\TradingRepository;
use App\Repositories\TransactionRepository;
use App\Services\FrozenService;

// ＊＊ 有牽扯到Wallet點數的一律寫在這＊＊　
class WalletRepository extends Repository
{


    // ＊＊ 創 Wallet ＊＊　
    public static function create($user_id, $ball_id, $coin)
    {
        $return_db = null;
        // Lock
        $return_db = DB::transaction(function() use($user_id, $ball_id, $coin, $return_db){

            $lock_key = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_key);

                // logic
                $wallet_db = new Wallet;
                $wallet_db->user_id = $user_id;
                $wallet_db->ball_id = $ball_id;
                $wallet_db->coin = $coin;
                $wallet_db->balance_available = 0;
                $wallet_db->balance_locked = 0;
                $wallet_db->save();
                $return_db = $wallet_db;

            } finally {
                Lock::release($lock_key);
            }

            return $return_db;
        }); // end db transaction

        return $return_db;
    }


    // ＊＊ 完成市場交易 (lock) ＊＊
    public static function dealTrading($trading_id)
    {

        $obj = new \stdClass();
        $obj->error = ApiError::SUCCESS;
        $obj->data = '-';
        

        $find_trading_db = Trading::find($trading_id);

        if(!$find_trading_db){
            $obj->error = ApiError::DEAL_TRADING_NULL;
            return $obj;
        }


        $find_order_db = Order::withTrashed()->
                            find($find_trading_db->order_id);

        if(!$find_order_db){
            $obj->error = ApiError::DEAL_ORDER_NULL;
            return $obj;
        }

        $find_seller_wallet_db = Wallet::where('user_id', $find_trading_db->seller_user_id)
                    ->where('coin','Point')
                    ->first();

        if(!$find_seller_wallet_db){
            $obj->error = ApiError::DEAL_SELLER_NULL;
            return $obj;
        }

        $find_buyer_wallet_db = Wallet::where('user_id', $find_trading_db->buyer_user_id)
                    ->where('coin','Point')
                    ->first();

        if(!$find_buyer_wallet_db){
            $obj->error = ApiError::DEAL_BUYER_NULL;
            return $obj;
        }        


       // DB::transaction(function() use($find_trading_db){
            // 準備鎖 order 和 buyer-wallet 和 seller-wallet
            // 第一層 order
        $obj = DB::transaction(function() use($find_trading_db, $obj ){

            $lock_order_key = 'Order@order_id:' . $find_trading_db->order_id;
            try {
                Lock::acquire($lock_order_key);

                // 第二層 seller-wallet
                $lock_seller_key = 'Wallet@user_id:' . $find_trading_db->seller_user_id;
                try {
                    Lock::acquire($lock_seller_key);

                    // 第三層 buyer-wallet
                    $lock_buyer_key = 'Wallet@user_id:' . $find_trading_db->buyer_user_id;
                    try {
                        Lock::acquire($lock_buyer_key);

                        // Logic
                        $order_db = Order::withTrashed()
                                            ->find($find_trading_db->order_id);
                        $order_db->deal = $order_db->deal + $find_trading_db->amount;
                        $order_db->trading = $order_db->trading - $find_trading_db->amount;
                        $order_db->save();

                        // 這個欄位有必要嗎?
                        if($order_db->deal == $order_db->total){
                            $order_db->state = 'done';
                            $order_db->save();
                        }

                        // Seller
                        $seller_wallet_db = Wallet::where('user_id',$find_trading_db->seller_user_id)->where('coin','Point')->first();   
                        $seller_wallet_before = $seller_wallet_db->balance_available + $seller_wallet_db->balance_locked;        
                        $seller_wallet_after = $seller_wallet_before - $find_trading_db->amount;

                        $obj_seller = new \stdClass(); 
                        $obj_seller->connect_id = $find_trading_db->buyer_user_id;
                        $buyer_account_db = User::Select("account","id")
                                ->where("id", $find_trading_db->buyer_user_id)
                                ->first();
                        $buyer_ball_db = Ball::select('id')
                                ->where('user_id', $find_trading_db->buyer_user_id)
                                ->first();                    
                        $obj_seller->comment = $buyer_account_db->account.','.$buyer_ball_db->id;

                        TransactionRepository::create(
                                                      $find_trading_db->seller_user_id
                                                      , $seller_wallet_db->id
                                                      , 'Point'
                                                      , TransactionTypes::MARKET_SELL
                                                      , $find_trading_db->amount
                                                      , $seller_wallet_before
                                                      , $seller_wallet_after
                                                      , json_encode($obj_seller)                                               
                                                     );
                        $seller_wallet_db->balance_locked -= $find_trading_db->amount; //balance_locked
                        $seller_wallet_db->save();


                        // Buyer
                        $buyer_wallet_db = Wallet::where('user_id',$find_trading_db->buyer_user_id)->where('coin','Point')->first();
                        $buyer_wallet_before = $buyer_wallet_db->balance_available + $buyer_wallet_db->balance_locked;
                        $buyer_wallet_after = $buyer_wallet_before + $find_trading_db->amount;

                        $obj_buyer = new \stdClass(); 
                        $obj_buyer->connect_id = $find_trading_db->seller_user_id;
                        $seller_account_db = User::Select("account","id")
                                ->where("id", $find_trading_db->seller_user_id)
                                ->first();
                        $seller_ball_db = Ball::select('id')
                                ->where('user_id', $find_trading_db->seller_user_id)
                                ->first();                    
                        $obj_buyer->comment = $seller_account_db->account.','.$seller_ball_db->id;

                        TransactionRepository::create(
                                                      $find_trading_db->buyer_user_id
                                                      , $buyer_wallet_db->id
                                                      , 'Point'
                                                      , TransactionTypes::MARKET_BUY
                                                      , $find_trading_db->amount
                                                      , $buyer_wallet_before
                                                      , $buyer_wallet_after
                                                      , json_encode($obj_buyer)                                              
                                                     );                    
                        $buyer_wallet_db->balance_available += $find_trading_db->amount;
                        $buyer_wallet_db->save();
                        
                        $obj->error = ApiError::SUCCESS;


                    } finally {
                        Lock::release($lock_buyer_key);
                    }


                } finally {
                    Lock::release($lock_seller_key);
                }

            } finally {
                Lock::release($lock_order_key);
            }

            return $obj;
        }); // end db transaction            
       // }
        return $obj;
    }


    // ＊＊ 取消委託 (lock) ＊＊
    public static function cancel_order($user_id, $order_id)
    {

        $obj = new \stdClass();
        $obj->error = ApiError::SUCCESS;

        $obj = DB::transaction(function() use($user_id, $order_id, $obj ){
            // Lock
            $lock_key_wallet = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_key_wallet);

                // logic
                // 先確定找的到, 後面會另外開1個order db在try裡面        
                $order_db = Order::where('id', $order_id)
                    ->first();

                $wallet_db = Wallet::where('user_id', $user_id)
                    ->where('coin','Point')     
                    ->first();

                if($order_db && $wallet_db){       

                    // 這邊應該要把 $order_db 關起來了? (暫時不做這件事)

                    $lock_key_order = 'Order@order_id:' . $order_id;
                    try {
                            Lock::acquire($lock_key_order);

                            // 另外開一個order db 實作
                            $locked_order_db = Order::where('id', $order_id)
                                ->where('user_id', $user_id)
                                ->first();

                            // 如果這張order還有綁交易
                            $trading_db = Trading::where('order_id', $order_id)
                                            ->where('state','waiting')
                                            ->get();

                            $reserved_value = 0; // lock的錢先保持lock
                            if($trading_db){    
                                for ($i=0; $i < count($trading_db); $i++) { 
                                    $reserved_value += $trading_db[$i]->amount;
                                }
                            }

                            // 把未交易的point存回去
                            $value = $locked_order_db->total - $locked_order_db->deal - $reserved_value;
                            $wallet_db->balance_available = $wallet_db->balance_available + $value;
                            $wallet_db->balance_locked    = $wallet_db->balance_locked - $value;
                            $wallet_db->save();

                            $locked_order_db->delete();
                            if(!$locked_order_db->trashed()){
                                $obj->error = array('error' => ApiError::DELETE_ORDER_FAILED);
                            }

                            // 把這張單的相關交易中Trading取消
                            TradingRepository::updateState($order_id, 'cancel_order', 'seller-cancel');

                    } finally {
                        Lock::release($lock_key_order);
                    }        

                } else {
                    $obj->error = ApiError::ORDER_BUSY;
                }

            } finally {
                Lock::release($lock_key_wallet);
            }
         
           return $obj;
        }); // end db transaction    

        return $obj;
    }


    // ＊＊ 委託 (lock) ＊＊
    public static function create_order($user_id, $order_amount)
    {

        $obj = new \stdClass();
        $obj->error = ApiError::SUCCESS;

        
        $obj = DB::transaction(function() use($user_id, $order_amount, $obj ){
            // Lock
            $lock_key = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_key);

                // logic
                $wallet_db = Wallet::where('user_id', $user_id)
                ->where('coin','Point')
                ->first();

                if($wallet_db){       

                    if($wallet_db->balance_available >= $order_amount){

                        $wallet_db->balance_available = $wallet_db->balance_available - $order_amount;
                        $wallet_db->balance_locked    = $wallet_db->balance_locked + $order_amount;
                        $wallet_db->save();

                        $order_db = new Order;
                        $order_db->user_id = $user_id;
                        $order_db->total = $order_amount;
                        $order_db->deal = 0;
                        $order_db->save();

                    } else {
                        $obj->error = ApiError::ORDER_POINT_NOT_ENOUGH;
                    }

                } else {
                    $obj->error = ApiError::USER_ID_NULL;
                }

            } finally {
                Lock::release($lock_key);
            }

            return $obj;
        }); // end db transaction                
               
        return $obj;
    }


    // ＊＊ 轉帳 (lock) ＊＊
    public static function deposit_by_userID($user_id, $value, $coin)
    {
        $before = -1;
        $wallet_db = null;

        $wallet_db = DB::transaction(function() use($user_id, $value, $coin, $wallet_db, $before){
            // Lock
            $lock_key = 'Wallet@user_id:' . $user_id;
            try {
                Lock::acquire($lock_key);

                // logic
                $wallet_db = Wallet::where('user_id', $user_id)
                ->where('coin',$coin)
                ->first();

                if($wallet_db){            
                    $before = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                    $wallet_db->balance_available += (float)$value;
                    
                    // 如果超過999.9999萬
                    if($wallet_db->balance_available >= 10000000){
                        $wallet_db->balance_available = (float)9999999;
                    }
                    
                    $wallet_db->save();
                    $wallet_db->before = $before;
                    $wallet_db->after = (float)$wallet_db->balance_available + (float)$wallet_db->balance_locked;
                } 

            } finally {
                Lock::release($lock_key);
            }
            
            return $wallet_db;
        }); // end db transaction    

        return $wallet_db;
    }


    // ＊＊ 遊戲九宮格 10連抽 (lock) ＊＊
    // public static function play9Block10Times($user_id, $prize_value)
    // {
    //     $bet_value = 50;
    //     $wallet_db = null;

    //     // Lock
    //     $lock_key = 'Wallet@user_id:' . $user_id;
    //     try {
    //         Lock::acquire($lock_key);

    //         // logic
    //         $wallet_db = Wallet::where('user_id', $user_id)
    //         ->where('coin','Point')
    //         ->first();

    //         if($wallet_db){        

    //             // 壓50點扣款
    //             TransactionRepository::create_with_walletdb(
    //                                                         $wallet_db
    //                                                         , $user_id
    //                                                         , 'Point'
    //                                                         , TransactionTypes::LOTTERY_9BLOCKS_10TIMES_BET
    //                                                         , -$bet_value
    //                                                         , ""
    //                                                        );

    //             // 贏回點數
    //             TransactionRepository::create_with_walletdb(
    //                                                         $wallet_db
    //                                                         , $user_id
    //                                                         , 'Point'
    //                                                         , TransactionTypes::LOTTERY_9BLOCKS_10TIMES_WIN_POINTS
    //                                                         , $prize_value
    //                                                         , ""
    //                                                        );

    //         } 


    //     } finally {
    //         Lock::release($lock_key);
    //     }
        
    //     return $wallet_db;
    // }


    // ＊＊ 遊戲九宮格抽抽樂 (lock) ＊＊
    // public static function lottery_9Block($user_id, $prize_value)
    // {
    //     $bet_value = 5;
    //     $wallet_db = null;

    //     // Lock
    //     $lock_key = 'Wallet@user_id:' . $user_id;
    //     try {
    //         Lock::acquire($lock_key);

    //         // logic
    //         $wallet_db = Wallet::where('user_id', $user_id)
    //         ->where('coin','Point')
    //         ->first();

    //         if($wallet_db){   

    //             // 壓5點扣款
    //             TransactionRepository::create_with_walletdb(
    //                                                         $wallet_db
    //                                                         , $user_id
    //                                                         , 'Point'
    //                                                         , TransactionTypes::LOTTERY_9BLOCKS_BET
    //                                                         , -$bet_value
    //                                                         , ""
    //                                                        );  

    //             // 贏回點數
    //             TransactionRepository::create_with_walletdb(
    //                                                         $wallet_db
    //                                                         , $user_id
    //                                                         , 'Point'
    //                                                         , TransactionTypes::LOTTERY_9BLOCKS_WIN_POINTS
    //                                                         , $prize_value
    //                                                         , ""
    //                                                        );



    //         } 


    //     } finally {
    //         Lock::release($lock_key);
    //     }
        
    //     return $wallet_db;
    // }


}