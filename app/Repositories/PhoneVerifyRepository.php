<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\PhoneVerify;

use App\Repositories\Repository;
use App\Services\PhoneVerifyService;
use App\Tools\Tools;


class PhoneVerifyRepository extends Repository
{

    // ＊＊ 創 Verify ＊＊　
    public static function force_insert($country_code, $phone)
    {

        $is_verified = PhoneVerifyService::verify($country_code, $phone);

        // verify 存在
        if($is_verified == 1){
            return 1;
        }

        $phone_verify_db = new PhoneVerify;
        $phone_verify_db->ip = Tools::getIP();
        $phone_verify_db->country_code = $country_code;
        $phone_verify_db->phone = $phone;
        $phone_verify_db->apply_count = 1;
        $phone_verify_db->wrong_count = 0;
        $phone_verify_db->verify_code = 'force_verified';
        $phone_verify_db->verify = 1;
        $phone_verify_db->save();

        return 1;
    }


    // ＊＊ 創 Verify ＊＊　
    public static function create($country_code, $phone, $verify_code)
    {

        $is_verified = PhoneVerifyService::verify($country_code, $phone);

        // verify 存在
        if($is_verified == 1){
            return 1;
        }

        $phone_verify_db = new PhoneVerify;
        $phone_verify_db->ip = Tools::getIP();
        $phone_verify_db->country_code = $country_code;
        $phone_verify_db->phone = $phone;
        $phone_verify_db->apply_count = 1;
        $phone_verify_db->wrong_count = 0;
        $phone_verify_db->verify_code = $verify_code;
        $phone_verify_db->verify = 0;
        $phone_verify_db->save();

        return 1;
    }


    // ＊＊ 刪 Verify ＊＊　
    public static function delete($user_id, $tag)
    {

        $is_tagged = 1;
        $verify_db = Verify::where('user_id', $user_id)
                        ->where('tag', $tag)
                        ->first();

        if($verify_db){
            $verify_db->delete();

            if(!$verify_db->trashed()){
                $is_tagged = 1;
            } else {
                $is_tagged = 0;
            }
        }
      
        return $is_tagged;
    }

}