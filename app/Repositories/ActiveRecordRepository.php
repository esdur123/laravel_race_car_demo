<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\ActiveRecord;

use App\Repositories\Repository;



class ActiveRecordRepository extends Repository
{

    // ＊＊ 創 active_record ＊＊　
    public static function create($user_id, $ball_id, $value)
    {
        $active_db = new ActiveRecord;
        $active_db->user_id = $user_id;
        $active_db->ball_id = $ball_id;
        $active_db->value = $value;
        $active_db->save();

        return $active_db;
    }

}