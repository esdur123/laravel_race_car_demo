<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\User;

use App\Repositories\Repository;



class UserRepository extends Repository
{

    // ＊＊ 創 User＊＊　
    public static function create($recommender_id, $account, $nickname, $country_code, $phone, $password)
    {
        $user_db = new User;
        $user_db->recommender_id = $recommender_id;
        $user_db->account = $account;
        $user_db->nickname = $nickname;
        $user_db->country_code = $country_code;
        $user_db->phone = $phone;
        $user_db->password = password_hash($password, PASSWORD_DEFAULT);
        $user_db->save();

        return $user_db;
    }

}