<?php

namespace App\Repositories;
use Lock;
use DB;
use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\Order;
use App\Models\Trading;

use App\Repositories\TradingRepository;
use App\Services\FrozenService;


class OrderRepository extends Repository
{

    // ＊＊ 從委託單 找點+買點 (lock) ＊＊
    public static function buyPointsFromOrder($user_id, $request_amount, $buyer_set)
    {

        $C_INACTIVED_USER_LIMIT_AMOUNT = 700;
        $return_obj = new \stdClass();
        $return_obj->error = ApiError::SUCCESS;

        // 未激活者的限制(1), Trading只能有一張單 已成交也算
        if($buyer_set<=0){
            //$inactived_trading_db = Trading::whereRaw("buyer_user_id = ".$user_id." AND ( state = 'waiting' OR state = 'deal' )")
            $inactived_trading_db = Trading::whereRaw("buyer_user_id = ".$user_id." AND ( state = 'waiting' OR state = 'deal' )")
                ->first();

            if($inactived_trading_db){
                $return_obj->error = ApiError::INACTIVE_BUY_POINT_LIMIT_1_TRADING;
                return $return_obj;
            }  
        }

        // 先找到我要的order單
        $find_order_db = Order::where('state', 'open')
                ->orderBy('id','asc')
                ->get();

        // 把我們需要的點數/訂單號 丟到一個array
        $offer_array = Array(); // 開一個array接我們在 (第幾單 order_id 需要拿幾點 points)
        $hungry_amount = $request_amount; // 需要這麼多點數.. 減到0就停止
        for ($i=0; $i < count($find_order_db) ; $i++) { 

            if($user_id == $find_order_db[$i]->user_id){ // 過濾自己的單
                continue;
            }

            // 過濾凍結者的單
            $frozen_result = FrozenService::test_user_id($find_order_db[$i]->user_id);
            if( $frozen_result['data']['is_frozen_01']==1){
                continue;
            }


            $i_got_points = $find_order_db[$i]->total - $find_order_db[$i]->deal - $find_order_db[$i]->trading; // 這單能提供的點數

            if($i_got_points <= 0){ // 此單目前無空點(也許早就交易完成 或是  正在交易中已滿)
                continue;
            }

            if($buyer_set <= 0){  // 未激活者的限制(2)

                if($request_amount != $C_INACTIVED_USER_LIMIT_AMOUNT){   // 只能買700點
                    $return_obj->error = ApiError::INACTIVE_BUY_POINT_LIMIT_700;
                    return $return_obj;
                    break;
                }        

                if($i_got_points < $C_INACTIVED_USER_LIMIT_AMOUNT){  // 不能給足700點的單跳過
                    continue;
                } else {
                    $obj2 = new \stdClass();
                    $obj2->order_id = $find_order_db[$i]->id;
                    $obj2->amount = $C_INACTIVED_USER_LIMIT_AMOUNT;
                    $obj2->seller =$find_order_db[$i]->user_id;
                    $offer_array[0] = $obj2;
                    $hungry_amount = 0;
                    break;
                }
                
            }

            if($hungry_amount >= $i_got_points ){  // 這張單可以全吃
                
                $obj2 = new \stdClass();
                $obj2->order_id = $find_order_db[$i]->id;
                $obj2->amount = $i_got_points;
                $obj2->seller =$find_order_db[$i]->user_id;
                $offer_array[count($offer_array)] = $obj2;

                $hungry_amount -= $i_got_points;

                if($hungry_amount > 0) { 
                    continue;
                } else {
                    break;
                }                
            }

            if($hungry_amount < $i_got_points){ // 這張單太大 只需要一部分點數

                $obj2 = new \stdClass();
                $obj2->order_id = $find_order_db[$i]->id;
                $obj2->amount = $hungry_amount;
                $obj2->seller =$find_order_db[$i]->user_id;                
                $offer_array[count($offer_array)] = $obj2;
                $hungry_amount = 0;
                break;
            }    

        }

        // 取得需要的單號和明細 準備開單+鎖
        $return_obj->data = $offer_array;//$user_id.','.$request_amount.',[0]id='.$find_order_db[0]->id;

        for ($i=0; $i < count($offer_array); $i++) { 

            // 每個分散的單都要 1.鎖住 2.改trading值 3.填交易中表單(Trading)
            DB::transaction(function() use($offer_array, $i, $user_id){
                $lock_key_order = 'Order@order_id:' . $offer_array[$i]->order_id;
                try {
                        Lock::acquire($lock_key_order);

                        // 改交易中的數值
                        $order_db = Order::find($offer_array[$i]->order_id);
                        $order_db->trading += $offer_array[$i]->amount;
                        $order_db->save();

                        TradingRepository::create(
                                                    $offer_array[$i]->order_id
                                                    , $offer_array[$i]->amount
                                                    , $offer_array[$i]->seller
                                                    , $user_id
                                                 );


                } finally {
                    Lock::release($lock_key_order);
                }  
            }); // end db transaction

        } // end for
            

        // 無點可買
        if($hungry_amount == $request_amount){
            //if($buyer_set > 0){ // 擋掉未激活買700會進來這邊
                $return_obj->error = ApiError::NO_POINT_ON_THE_MARKET;
                return $return_obj;
            //} 

        }  
        return $return_obj;
    }


    // ＊＊ 創 Point Order單＊＊　
    public static function create($user_id, $amount)
    {
        $point_order_db = new PointOrder;
        $point_order_db->user_id = $user_id;
        $point_order_db->amount = $amount;
        $point_order_db->deal =  0;
        $point_order_db->save();

        return $point_order_db;
    }

}