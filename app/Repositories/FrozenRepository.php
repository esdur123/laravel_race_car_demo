<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Defined\FrozenTags;
use App\Models\Frozen;

use App\Repositories\Repository;



class FrozenRepository extends Repository
{

    // ＊＊ 創 Frozen ＊＊　
    public static function create($tag, $info)
    {
        
        $frozen_db = Frozen::where('tag', $tag)
                        ->where('info', $info)
                        ->first();

        // Frozen 存在
        if($frozen_db){
            return 1;
        }

        $frozen_db = new Frozen;
        $frozen_db->info = $info;
        $frozen_db->tag = $tag;
        $frozen_db->save();

        return 1;

    }


    // ＊＊ 刪 Frozen ＊＊　
    public static function delete($tag, $info)
    {

        $is_tagged = 1;
        $frozen_db = Frozen::where('info', $info)
                        ->where('tag', $tag)
                        ->first();

        if($frozen_db){
            $frozen_db->delete();

            if(!$frozen_db->trashed()){
                $is_tagged = 1;
            } else {
                $is_tagged = 0;
            }
        }
      
        return $is_tagged;
    }

}