<?php

namespace App\Repositories;
use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\Ball;
use App\Repositories\Repository;

class OrganizationRepository extends Repository
{

    // ＊＊ 創 Ball ＊＊　
    public static function create($user_id, $recommender_id, $recommend_tree, $branch_tree, $branch, $level)
    {
        $ball_db = new Ball;
        $ball_db->user_id = $user_id;
        $ball_db->recommender_id = $recommender_id;
        $ball_db->recommend_tree = $recommend_tree;
        $ball_db->performance = 0;
        $ball_db->level = $level;

        // 新增 ball_index
        $find_ball_db = Ball::All();
        $max_index = -1;
        for ($i=0; $i < count($find_ball_db); $i++) { 

            if($find_ball_db[$i]->user_id != $user_id){
                continue;
            }

            if($find_ball_db[$i]->ball_index <= $max_index){
                continue;
            }

            // 掃一圈 找出最大值 再+1就是了
            $max_index = $find_ball_db[$i]->ball_index;
            # code...
        }

        $ball_db->ball_index = $max_index + 1;
        $ball_db->save();

        return $ball_db;
    }

}