<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\Ask;

use App\Repositories\Repository;



class AskRepository extends Repository
{

    // ＊＊ 創 Ask ＊＊　
    public static function create($user_id, $title, $comment)
    {

        $ask_db = new Ask;
        $ask_db->user_id = $user_id;
        $ask_db->title = $title;
        $ask_db->comment = $comment;
        $ask_db->state = 'waiting';    
        $ask_db->reply = '';
        $ask_db->save();

        return $ask_db;
    }


    // ＊＊ 回答問題 ＊＊　
    public static function reply($ask_id, $reply)
    {
        $result = array('error' => ApiError::SUCCESS);

        $ask_db = Ask::find($ask_id);
        if(!$ask_db){
            $result['error'] = ApiError::ASK_ID_NULL;
            return $result;
        }

        $ask_db->reply = $reply;
        $ask_db->state = 'reply';    
        $ask_db->save();

        return $result;
    }
    // // ＊＊ 刪 Tag ＊＊　
    // public static function delete($user_id, $tag)
    // {

    //     $is_tagged = 1;
    //     $tag_db = Tag::where('user_id', $user_id)
    //                     ->where('tag', $tag)
    //                     ->first();

    //     if($tag_db){
    //         $tag_db->delete();

    //         if(!$tag_db->trashed()){
    //             $is_tagged = 1;
    //         } else {
    //             $is_tagged = 0;
    //         }
    //     }
      
    //     return $is_tagged;
    // }

}