<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Defined\WithdrawStates;
use App\Models\OrderWithdraw;

use App\Repositories\TradingRepository;
use App\Services\FrozenService;


class OrderWithdrawRepository extends Repository
{


    // ＊＊ 創 -提領Order- 單＊＊　
    public static function create($user_id, $amount)
    {
        $OrderWithdraw_db = new OrderWithdraw;
        $OrderWithdraw_db->user_id = $user_id;
        $OrderWithdraw_db->amount = $amount;
        $OrderWithdraw_db->commit = '';
        $OrderWithdraw_db->state = WithdrawStates::WAITING;
        $OrderWithdraw_db->save();

        return $OrderWithdraw_db;
    }




}