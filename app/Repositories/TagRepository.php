<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;
use App\Defined\ApiError;
use App\Models\Tag;

use App\Repositories\Repository;



class TagRepository extends Repository
{

    // ＊＊ 創 Verify ＊＊　
    public static function create($user_id, $tag)
    {

        $check_db = Tag::where('user_id', $user_id)
                        ->where('tag', $tag)
                        ->first();

        // verify 存在
        if($check_db){
            return 1;
        }

        $tag_db = new Tag;
        $tag_db->user_id = $user_id;
        $tag_db->tag = $tag;
        $tag_db->save();

        return 1;
    }


    // ＊＊ 刪 Tag ＊＊　
    public static function delete($user_id, $tag)
    {

        $is_tagged = 1;
        $tag_db = Tag::where('user_id', $user_id)
                        ->where('tag', $tag)
                        ->first();

        if($tag_db){
            $tag_db->delete();

            if(!$tag_db->trashed()){
                $is_tagged = 1;
            } else {
                $is_tagged = 0;
            }
        }
      
        return $is_tagged;
    }

}