<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActiveRecord extends Model
{
   use SoftDeletes;
   protected $dates = ['deleted_at'];
   
   protected $table = 'active_record';
}
