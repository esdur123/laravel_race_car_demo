<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function () { // api
 Route::group(['namespace' => 'v1', 'prefix' => 'v1'], function () {  // v1

	// 切換語系
	Route::put('/language/{lang}', 'LanguageController@set')->name("api.lang.set");
	// 依id取得account
	Route::get('/user/account/{id}', 'UserController@getAccountFromID')->name("api.user.account.id");
	// 推薦人註冊
	Route::post('/user/register/{recommender_account}', 'UserController@registerFromRecommender')->name("api.user.register.recommend")->middleware('api.log');
	// 會員登入
	Route::post('/user/login', 'UserController@login')->name('api.user.login')->middleware('api.log');
	// 回傳金流order	
	Route::post('/user/pay/vbank/{pay_id}/return', 'TXPayController@returnTCOrder')->name('api.pay.tc.vbank.id.return')->middleware('api.log');

	// Auth-會員
    Route::group(['middleware' => ['token','apiUser']], function () { 

		Route::post('/create/pay/order', 'PayController@createOrder')->name("createPayOrder")->middleware('api.log');  // ＊＊　訂單　＊＊
		Route::post('/create/tc/order/{id}', 'TXPayController@createTCOrder')->name("createTCOrder")->middleware('api.log');	 //＊＊　訂單　＊＊
		Route::post('/sms/bind', 'SmsController@bind')->name("sms.bind")->middleware('api.log'); // 簡訊		
		Route::post('/user/logout', 'UserController@logout')->name("logout"); // 登出
		Route::post('/user/register', 'UserController@register')->name("register")->middleware('api.log');
		Route::post('/user/info', 'UserController@updateInfo')->name("updateUserInfo")->middleware('api.log');
		Route::post('/user/pw', 'UserController@updatePassword')->name("updatePassword")->middleware('api.log');
		Route::post('/user/bank', 'UserController@updateBankInfo')->name("updateBankInfo")->middleware('api.log');
		Route::post('/user/level', 'OrganizationController@activeLevel')->name("activeLevel")->middleware('api.log');
		Route::post('/org/active/auto', 'OrganizationController@switchActive')->name("switchActive")->middleware('api.log');
		Route::post('/org/active/static', 'OrganizationController@ActiveStatic')->name("activeStatic")->middleware('api.log');
		Route::post('/txn/deposit/point', 'TransactionController@depositPoints')->name("depositPoints")->middleware('api.log');
		Route::post('/txn/deposit/colorpoint', 'TransactionController@depositColorPoints')->name("depositColorPoints")->middleware('api.log');
		Route::post('/txn/order/create', 'TransactionController@createOrder')->name("createOrder")->middleware('api.log');
		Route::post('/txn/order/cancel', 'TransactionController@cancelOrder')->name("cancelOrder")->middleware('api.log');
		Route::post('/txn/order/buy', 'TransactionController@buyPointsFromOrder')->name("buyPointsFromOrder")->middleware('api.log');	
		Route::post('/txn/trading/cancel', 'TransactionController@cancelTrading')->name("cancelTrading")->middleware('api.log');			
		Route::post('/txn/trading/confirm', 'TransactionController@confirmTrading')->name("confirmTrading")->middleware('api.log');		
		Route::post('/lottery/block9', 'LotteryController@play9Block')->name("play9Block")->middleware('api.log');	
		Route::post('/lottery/block9/ten_times', 'LotteryController@play9Block10Times')->name("play9Block10Times")->middleware('api.log');	
		Route::post('/withdraw/apply', 'WithdrawController@apply')->name("withdraw.apply")->middleware('api.log');	
		Route::post('/withdraw/user/cancel', 'WithdrawController@user_cancel')->name("withdraw.user.cancel")->middleware('api.log');
        Route::post('/register/ball', 'UserController@register_ball')->name("user.register.ball")->middleware('api.log');		Route::post('/register/ball', 'UserController@register_ball')->name("user.register.ball")->middleware('api.log');
		Route::post('/ask/apply', 'AskController@apply')->name("user.ask.apply")->middleware('api.log');








        Route::get('/user/bank', 'UserController@getBankInfo')->name("getBankInfo");
		Route::get('/user/account', 'UserController@getAccount')->name("getAccount");
		Route::get('/user/info', 'UserController@getInfo')->name("getUserInfo");		
		Route::get('/org/active/info', 'OrganizationController@getActiveInfo')->name("getActiveInfo");
		//Route::get('/org/branch/{account}', 'OrganizationController@getBranchInfo')->name("getBranchInfo");
		Route::get('/org/recommend/{account}', 'OrganizationController@GetRecommenderInfo')->name("GetRecommenderInfo");
		Route::get('/org/set', 'OrganizationController@getSet')->name("getSet");
		Route::get('/org/ball/id', 'OrganizationController@getBallID')->name("getBallID");
		Route::get('/wallet/point', 'WalletController@getPoint')->name("getUserPoint");
		Route::get('/wallet/point/info', 'WalletController@getPointInfo')->name("get.wallet.point.info");	// datatable
		Route::get('/wallet/colorpoint/is_positive', 'WalletController@checkIsColorpoint')->name("checkIsColorpoint");		
		Route::get('/wallet/colorpoint', 'WalletController@getColorPoint')->name("get.color.point");
		Route::get('/wallet/colorpoint/info', 'WalletController@getColorPointInfo')->name("get.wallet.colorpoint.info");  // datatable
		Route::get('/txn/order', 'TransactionController@getOrder')->name("getOrder"); // datatable
		Route::get('/txn/buy', 'TransactionController@getBuy')->name("getBuy"); // datatable
		Route::get('/txn/sell', 'TransactionController@getSell')->name("getSell"); // datatable			
		Route::get('/sms/create', 'SmsController@create')->name("sms.create"); // 簡訊
		Route::get('/withdraw/info', 'WithdrawController@get_info')->name("get.withdraw.info");	// datatable
		Route::get('/withdraw/check', 'WithdrawController@check_available')->name("get.withdraw.available");	// datatable
        Route::get('/report/bet', 'ReportController@getBetReport')->name("report.bet");
		Route::get('/ask/info/{type}', 'AskController@get_info')->name("get.ask.info");	// datatable


		//Route::get('/phone/verify', 'PhoneVerifyController@verify')->name("phone.verify"); // datatable	

    });



	// 管理員登入
	Route::post('/admin/login', 'AdminController@login')->name("AdminLogin");
		
    // Auth-管理員
    Route::group(['middleware' => ['token','apiAdmin']], function () {

		Route::post('/user/info/by/admin', 'AdminController@updateUserInfo')->name("updateUserInfoByAdmin")->middleware('api.log');		
		Route::post('/admin/pw', 'AdminController@updateAdminPassword')->name("admin.update.pw")->middleware('api.log');
		Route::post('/admin/logout', 'AdminController@logout')->name("AdminLogout")->middleware('api.log');		
		Route::post('/trading/cancel', 'AdminController@cancelTrading')->name("AdminCancelTrading")->middleware('api.log');			
		Route::post('/order/cancel', 'AdminController@cancelOrder')->name("AdminCancelOrder")->middleware('api.log');		
		Route::post('/limit/withdraw', 'AdminController@switchLimitedWithdraw')->name("AdminSwitchLimitedPay")->middleware('api.log');
		Route::post('/admin/switch/frozen/by/id', 'AdminController@switchFrozenByID')->name("admin.switch.frozen.id")->middleware('api.log');
		Route::post('/admin/switch/frozen/by/phone', 'AdminController@switchFrozenByPhone')->name("admin.switch.frozen.phone")->middleware('api.log');
		Route::post('/admin/switch/frozen/by/bank', 'AdminController@switchFrozenByBank')->name("admin.switch.frozen.bank")->middleware('api.log');		
		Route::post('/admin/max/verify/message', 'AdminController@updateMaxVerifyMessage')->name("admin.update.max.verify.msg")->middleware('api.log');
		Route::post('/admin/register/user/recommender', 'AdminController@registerFromRecommender')->name("admin.register.user.recommender")->middleware('api.log');
		Route::post('/admin/register/user/max_l_max_r', 'AdminController@registerMaxLeftMaxRight')->name("admin.register.user.max_l_max_r")->middleware('api.log');
		Route::post('/admin/register/user/no_upline', 'AdminController@registerNoUpline')->name("admin.register.user.no_upline")->middleware('api.log');
		Route::post('/admin/send/point', 'AdminController@send_points')->name("admin.register.send.point")->middleware('api.log');
		Route::post('/admin/send/colorpoint', 'AdminController@send_color_points')->name("admin.register.send.colorpoint")->middleware('api.log');
		Route::post('/admin/register/new', 'AdminController@register_new')->name("admin.register.new")->middleware('api.log');
		Route::post('/admin/accepting/withdraw', 'AdminController@accepting_withdraw')->name("admin.accepting.withdraw")->middleware('api.log');		
		Route::post('/admin/deal/withdraw', 'AdminController@deal_withdraw')->name("admin.deal.withdraw")->middleware('api.log');
		Route::post('/admin/dividend/rates', 'AdminController@add_dividend_rates')->name("admin.add.dividend.rates");		
		Route::post('/admin/ask/reply', 'AdminController@reply_question')->name("admin.reply.question");
		Route::post('/admin/force/verify/phone', 'AdminController@force_verify_phone')->name("admin.force.verify.phone");




		Route::get('/admin/account', 'AdminController@getAdminAccount')->name("getAdminAccount");	
		Route::get('/user/accounts/by/admin', 'AdminController@getUserAccounts')->name("getUserAccounts"); // datatable
		Route::get('/user/info/by/admin', 'AdminController@getUserAccountInfo')->name("getUserAccountInfo");
		Route::get('/user/ball/{user_id}/by/admin', 'AdminController@getUserBallInfo')->name("getUserBallInfo"); // datatable		
		Route::get('/wallet/info/{user_id}/by/admin', 'AdminController@getWalletInfo')->name("adminGetUserWalletInfo");	
		Route::get('/admin/wallet/colorpoint/info/{user_id}', 'AdminController@getColorpointInfo')->name("admin.get.colorpoint.info");// datatable
		Route::get('/org/branch/{user_id}/by/admin', 'AdminController@getBranchInfoByID')->name("adminGetBranchByID");
		Route::get('/org/recommend/{user_id}/by/admin', 'AdminController@GetRecommenderInfoByID')->name("adminGetRecommenderInfoByID");		
		Route::get('/txn/trading/by/admin', 'AdminController@getTradingInfo')->name("adminGetTradingInfo");	
		Route::get('/orders/by/admin', 'AdminController@getOrders')->name("adminGetOrders");
		Route::get('/admin/limit/withdraw', 'AdminController@checkIsAllLimitWithdraw')->name("admin.limit.withdraw");
		Route::get('/admin/verify/frozen/by/id', 'AdminController@verifyFrozen_by_id')->name("admin.verify.frozen.id")->middleware('api.log');
		Route::get('/admin/verify/frozen/by/phone', 'AdminController@verifyFrozen_by_phone')->name("admin.verify.frozen.phone")->middleware('api.log');
		Route::get('/admin/verify/frozen/by/bank', 'AdminController@verifyFrozen_by_bank')->name("admin.verify.frozen.bank")->middleware('api.log');
		Route::get('/admin/wallet/colorpoint/is_positive', 'AdminController@checkIsColorpoint')->name("admin.colorwallet.is_positive");
		Route::get('/admin/max/verify/message', 'AdminController@getMaxVerifyMessage')->name("admin.get.max.verify.msg")->middleware('api.log');	
		Route::get('/admin/frozen/info/{user_id}', 'AdminController@getFrozenInfo')->name("admin.get.frozen.info");// datatable
		Route::get('/admin/withdraw/info/{type}', 'AdminController@getWithdrawInfo')->name("admin.get.withdraw.info");	// datatable
		Route::get('/admin/dividend/rates', 'AdminController@get_dividend_rates')->name("admin.get.dividend.rates");	
		Route::get('/admin/ask/info/{type}', 'AdminController@get_ask_info')->name("admin.get.ask.info");	// datatable


    });


 }); // end v1
}); // end api





