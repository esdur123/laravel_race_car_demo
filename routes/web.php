<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// *** v1 ***
Route::group(['namespace' => 'View\v1'], function () { 
	Route::group(['middleware' => 'language'], function () { // 語系

		Route::get('/', 'ViewController@home')->name('home');
		Route::get('/admin', 'ViewController@admin_home')->name('admin_home');								
		Route::get('/register/{recommender_account}', 'ViewController@register')->middleware('register')->name('register');
        Route::get('/intro', 'ViewController@intro')->name('front.intro');
        Route::get('/report', 'ViewController@bet_report')->name('bet_report');

        //Route::get('/test', 'ViewController@test')->name('test');

		// User
		Route::group(['middleware' => 'viewUser'], function () {
		    Route::get('/user/news', 'ViewController@user_news')->name('user_news'); 
		    //Route::get('/user/org/branch/{account}', 'ViewController@user_org_branch_specific')->name('user_org_branch_specific'); 
		    Route::get('/user/org/recommend/{account}', 'ViewController@user_org_recommend_specific')->name('user_org_recommend_specific');
		    Route::get('/user/account', 'ViewController@user_account')->name('user_account');
		    Route::get('/user/active', 'ViewController@user_active')->name('user_active');
		    Route::get('/user/transform', 'ViewController@user_transform')->name('user_transform');
		    Route::get('/user/trade', 'ViewController@user_trade')->name('user_trade');
		    Route::get('/user/wallet', 'ViewController@user_wallet')->name('user_wallet');		  
		    Route::get('/user/game', 'ViewController@user_game')->name('user_game');
		    Route::get('/user/withdraw', 'ViewController@user_withdraw')->name('user_withdraw');		    
		    Route::get('/user/ask', 'ViewController@user_ask')->name('user_ask');		  
		    Route::get('/user/pay', 'ViewController@user_pay')->name('user_pay');
		    Route::get('/user/pay/vbank/{pay_id}', 'ViewController@user_pay_vbank')->name('api.pay.tc.vbank.id');		      

		});
		// Admin
		Route::group(['middleware' => 'viewAdmin'], function () {
			Route::get('/admin/control', 'ViewController@admin_control')->name('admin_control');
			Route::get('/admin/manage/account', 'ViewController@admin_manage_account')->name('admin_manage_account');
			Route::get('/admin/manage/account/{user_id}', 'ViewController@admin_manage_account_specific')->name('admin_manage_account_specific');
			Route::get('/admin/manage/branch/{user_id}', 'ViewController@admin_manage_branch')->name('admin_manage_branch');
			Route::get('/admin/manage/recommend/{user_id}', 'ViewController@admin_manage_recommend')->name('admin_manage_recommend');			
			Route::get('/admin/trade', 'ViewController@admin_trade')->name('admin_trade');
			Route::get('/admin/info', 'ViewController@admin_info')->name('admin_info');
			Route::get('/admin/withdraw', 'ViewController@admin_withdraw')->name('admin_withdraw');
			//Route::get('/admin/calender', 'ViewController@admin_calender')->name('admin_calender'); // admin 倍率日曆
			Route::get('/admin/ask', 'ViewController@admin_ask')->name('admin.ask');
		});



	}); // end middleware language
}); // end v1