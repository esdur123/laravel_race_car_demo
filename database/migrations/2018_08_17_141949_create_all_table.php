<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTable extends Migration
{
    /**
     * Run the migrations.
     * php artisan migrate
     * php artisan make:migration create_ask_table --create=ask
     * @return void
     */
    public function up()
    {

        Schema::create('admin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account', 50);
            $table->string('password', 200);
            $table->integer('level')->default(-1);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        });  

        Schema::create('ball', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 50);
            $table->integer('level')->default(-1);
            $table->integer('performance')->default(0);                
            $table->string('recommender_id', 50);
            $table->string('recommend_tree', 1000);
            $table->string('static_current', 50)->default('0');
            $table->string('static_max', 50)->default('0');
            $table->integer('auto_active')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        });        

        Schema::create('frozen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag', 50);
            $table->string('info', 50);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();     
        });

        Schema::create('log', function (Blueprint $table) {
            $table->increments('id');
            $table->text('ip');
            $table->text('action');
            $table->text('device')->nullable();
            $table->text('body');
            $table->text('post');
            $table->text('get');
            $table->text('session');                                                                        
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        });    

        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('state', 50)->default('open');
            $table->integer('total')->default(0);
            $table->integer('deal')->default(0);
            $table->integer('trading')->default(0);                                                                      
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('pay', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('type', 50);
            $table->integer('amount');
            $table->integer('price');
            $table->integer('fee');
            $table->string('ip', 50);
            $table->text('msg')->nullable();
            $table->integer('total_price');
            $table->string('trade_id', 50);
            $table->dateTime('finished_at')->nullable();
            $table->dateTime('obtained_at')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::create('phone_verify', function (Blueprint $table) {
            $table->increments('id');
            $table->text('ip');
            $table->text('country_code');
            $table->text('phone');
            $table->integer('wrong_count')->default(0);
            $table->integer('apply_count')->default(0);
            $table->text('verify_code');
            $table->integer('verify')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('system', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daily_max_phone_verify')->default(3);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('trading', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->default(0);
            $table->integer('amount')->default(0);
            $table->integer('seller_user_id');
            $table->integer('buyer_user_id');        
            $table->string('confirm_value', 50)->default('none');
            $table->string('seller_info', 200);
            $table->string('buyer_info', 200);
            $table->string('state', 50)->default('waiting');
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('wallet_id');
            $table->text('coin');
            $table->text('type');
            $table->decimal('value', 15, 8);
            $table->decimal('before', 15, 8);
            $table->decimal('after', 15, 8);
            $table->text('link');                     
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recommender_id')->default(0);
            $table->string('account', 50);
            $table->integer('ball_id')->default(0);
            $table->string('nickname', 50);
            $table->string('country_code', 20);
            $table->string('phone', 20)->nullable();
            $table->string('bank_code', 16)->nullable();
            $table->string('bank_name', 50)->nullable();
            $table->string('bank_branch', 50)->nullable();
            $table->string('bank_account', 50)->nullable();
            $table->string('password', 250);                                                                                
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('tag', 30);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        }); 

        Schema::create('wallet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recommender_id')->default(0);
            $table->integer('user_id')->nullable();
            $table->integer('ball_id')->nullable();
            $table->text('coin')->nullable();
            $table->decimal('balance_available', 15, 8)->default(0);
            $table->decimal('balance_locked', 15, 8)->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        }); 

        Schema::create('order_withdraw', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('state', 50)->default('waiting');
            $table->integer('amount')->default(0);          
            $table->text('commit');                                                           
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();       
        });

        Schema::create('rate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year')->default(0);
            $table->integer('month')->default(0);
            $table->integer('day')->default(0);
            $table->string('rate', 30);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('admin');
        Schema::dropIfExists('ball');
        Schema::dropIfExists('frozen');
        Schema::dropIfExists('log');
        Schema::dropIfExists('order');
        Schema::dropIfExists('pay');
        Schema::dropIfExists('phone_verify');    
        Schema::dropIfExists('system');
        Schema::dropIfExists('trading');
        Schema::dropIfExists('transaction');  
        Schema::dropIfExists('user');
        Schema::dropIfExists('tag');  
        Schema::dropIfExists('wallet');
        Schema::dropIfExists('order_withdraw');
        Schema::dropIfExists('rate');        
  
    }
}
