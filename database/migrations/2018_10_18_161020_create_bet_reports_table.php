<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_amount')->comment('總資金量');
            $table->integer('bet_amount')->comment('單注金額');
            $table->integer('bet_times')->comment('下注次數');
            $table->integer('a_win_times')->comment('A贏的次數');
            $table->integer('a_bet_amount')->comment('A下注金額');
            $table->integer('a_payout_amount')->comment('A派彩');
            $table->integer('a_balance')->comment('A輸贏');
            $table->integer('a_profit')->comment('A佔成');
            $table->integer('b_bet_amount')->comment('B下注金額');
            $table->integer('b_payout_amount')->comment('B派彩');
            $table->integer('b_balance')->comment('B輸贏');
            $table->integer('b_commission')->comment('B退水');
            $table->integer('total_balance')->comment('總輸贏');
            $table->decimal('income_ratio', 15, 8)->comment('收入比例');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_reports');
    }
}
