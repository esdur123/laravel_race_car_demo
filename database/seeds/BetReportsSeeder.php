<?php

use Illuminate\Database\Seeder;

use App\Repositories\BerReportRepository;

class BetReportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bet_reports = [
            ['date' => '2018-10-1', 'total_amount' => 1000000, 'a_win_times' => 19],
            ['date' => '2018-10-2', 'total_amount' => 1097801, 'a_win_times' => 17],
            ['date' => '2018-10-3', 'total_amount' => 1207653, 'a_win_times' => 16],
            ['date' => '2018-10-4', 'total_amount' => 1329864, 'a_win_times' => 12],
            ['date' => '2018-10-5', 'total_amount' => 1470462, 'a_win_times' => 14],
            ['date' => '2018-10-6', 'total_amount' => 1622594, 'a_win_times' => 13],
            ['date' => '2018-10-7', 'total_amount' => 1792309, 'a_win_times' => 12],
            ['date' => '2018-10-8', 'total_amount' => 1981795, 'a_win_times' => 11],
            ['date' => '2018-10-9', 'total_amount' => 2193553, 'a_win_times' => 15],
            ['date' => '2018-10-10', 'total_amount' => 2418018, 'a_win_times' => 16],
            ['date' => '2018-10-11', 'total_amount' => 2662716, 'a_win_times' => 15],
            ['date' => '2018-10-12', 'total_amount' => 2935191, 'a_win_times' => 18],
            ['date' => '2018-10-13', 'total_amount' => 3225580, 'a_win_times' => 14],
            ['date' => '2018-10-14', 'total_amount' => 3559303, 'a_win_times' => 13],
            ['date' => '2018-10-15', 'total_amount' => 3931576, 'a_win_times' => 15],
            ['date' => '2018-10-16', 'total_amount' => 4333887, 'a_win_times' => 11],
            ['date' => '2018-10-17', 'total_amount' => 4796976, 'a_win_times' => 17],
            ['date' => '2018-10-18', 'total_amount' => 5276990, 'a_win_times' => 14],
        ];
        foreach ($bet_reports as $bet_report) {
            $bet_report_db = BerReportRepository::create($bet_report['total_amount'], $bet_report['a_win_times']);
            $bet_report_db->created_at = $bet_report['date'];
            $bet_report_db->save();
        }
    }
}
