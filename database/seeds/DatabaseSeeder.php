<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * php artisan db:seed
     * php artisan migrate:refresh --seed
     *
     * @return void
     */
    public function run()
    {
        $this->call(Init::class);
        $this->call(BetReportsSeeder::class);
    }
}
