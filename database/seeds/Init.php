<?php

use Illuminate\Database\Seeder;

class Init extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            'account' => 'soltest',
            'password' => password_hash('123456', PASSWORD_DEFAULT),
            'level' => '1',
            'created_at' =>  date('Y-m-d H:i:s'),
        ]); 

        DB::table('user')->insert([
			'recommender_id' => '0',        	
            'account' => 'soltest',
			'ball_id' => '1',
			'nickname' => 'soltest',
			'country_code' => '886',
			'phone' => '0900000000',
			'bank_code' => '',
			'bank_name' => '',
			'bank_branch' => '',
			'bank_account' => '',	
            'password' => password_hash('123456', PASSWORD_DEFAULT),
			'created_at' => date('Y-m-d H:i:s'),

        ]); 

        DB::table('ball')->insert([
			'user_id' => '1',        	
			'level' => '1',     
			'performance' => '0',     
			'recommender_id' => '0',
			'recommend_tree' => '',	
			'static_current' => '0',																		
			'static_max' => '2100',
			'auto_active' => '0',
			'created_at' =>  date('Y-m-d H:i:s'),												
        ]); 

        DB::table('wallet')->insert([
			'user_id' => '1',        	
            'ball_id' => '1',
			'coin' => 'Point',
			'balance_available' => '0',
			'balance_locked' => '0',	
			'created_at' =>  date('Y-m-d H:i:s'),												
        ]); 


        DB::table('system')->insert([
			'daily_max_phone_verify' => '3',
			'created_at' =>  date('Y-m-d H:i:s'),												
        ]); 

        DB::table('tag')->insert([
        	'user_id' => '0',     
			'tag' => 'ALL_USERS_LIMITED_WITHDRAW',
			'created_at' =>  date('Y-m-d H:i:s'),												
        ]); 



    }
}
