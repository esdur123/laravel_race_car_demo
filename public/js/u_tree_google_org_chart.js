

var p_tree_target_id = ''; // tree root id
var p_tree_admin_id = '';
var m_tree_obj = [];  // *** 存放api撈回資料的陣列, 刷新層數時可重複利用 ***
var m_tree_mapdata = [];           // 暫存畫出tree的資訊(每次改層數都會更動)
var m_tree_chart = null;  // google.visualization.DataTable



// ＊＊Ready Function ＊＊　
$(function(){

}) 



// ＊＊整理m_tree_obj 然後畫出來 ＊＊　
function OrganizationTreeByObjAndDraw_Branch(){

          var showTitleStyle = ''; // 最後要顯示的title顏色
          var id = '';
          var branch_upline = '';
          var pv_rl = '';
          var r_plus_l = ''
          var nickname = ''
          var show_txt = ''
          var set = 0;

          // 把收到的資料, 填到google-chart要的陣列
          for (var i = m_tree_obj.length - 1; i >= 0; i--) {

              set = m_tree_obj[i].set;   
              branch_upline = (m_tree_obj[i].branch_upline.toString()).toLowerCase();

              // 未激活頁面 自己+兩小灰人共3人
              if( m_tree_obj.length == 3 && branch_upline == '' && set <= 0){
                m_tree_mapdata = []; 
                break;
              }

              pv_rl = m_tree_obj[i].pv_rl;        
              
              r_plus_l = m_tree_obj[i].r_plus_l;
              if(r_plus_l==-1){
                r_plus_l = '';
              }
              nickname = m_tree_obj[i].nickname;              

              id = (m_tree_obj[i].id.toString()).toLowerCase();  
                    
              var is_empty = m_tree_obj[i].is_empty;

              // 把上線null 改成空白
              if(branch_upline.toLowerCase() == 'null'){
                  branch_upline='';
              }

              var setColor = 'white';
              if(set == '0'){
                setColor = '#77DDFF'; // 藍色
              }

              if(set == '500'){
                setColor = '#FFB7DD'; // 粉紅
              }

              if(set == '100'){
                setColor = '#00FF99'; // 綠
              }

              if(set == '10'){
                setColor = '#FFAA33'; // 橘黃
              }

              if(set == 'empty'){
                setColor = 'gray';
              }
              
              if(i==0){
                 // var split_pv_rl = pv_rl.split("/");
                 // var new_pv_rl = '';
                 // if(split_pv_rl.length > 1){
                  
                 //    var min_rl = Math.min(split_pv_rl[0], split_pv_rl[1]);
                 //    split_pv_rl[0] = split_pv_rl[0] - min_rl;
                 //    split_pv_rl[1] = split_pv_rl[1] - min_rl;
                 //    new_pv_rl = split_pv_rl[0]+'/'+split_pv_rl[1];
                 // }

                 show_txt = pv_rl+'<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'+nickname+'<br />'
                    +m_lang_set+':'+m_tree_obj[i].set+'<br />PV:'+m_tree_obj[i].pv+'<br />'+m_lang_performance_total+':'+r_plus_l+'<br />('+m_lang_performance_L+')'+m_tree_obj[i].l+'&nbsp;&nbsp;&nbsp;('+m_lang_performance_R+')'+m_tree_obj[i].r;
              } else {
                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +r_plus_l+'<br />'+nickname;
              }
            
              // m_tree_mapdata 接陣列資料
              m_tree_mapdata[i] = 
                [ { v:id, 
                    f:(show_txt)},
                  branch_upline, 
                  ''
                ]
          }
        
          drawChart('branch'); // 畫出
}


// ＊＊整理m_tree_obj 然後畫出來 ＊＊　
function OrganizationTreeByObjAndDraw_Recommend(){

          var id = '';
          var upline = '';
          //var pv_rl = '';
          var floor = '';
          var show_txt = ''
          var sun_downline = ''
          var nickname = ''
          var set = '';
          var ball_index = '';

          // 把收到的資料, 填到google-chart要的陣列
          for (var i = m_tree_obj.length - 1; i >= 0; i--) {

              upline = m_tree_obj[i].recommender.toString();
              
              // 未激活頁面
              if(m_tree_obj[i].set <= 0){
                m_tree_mapdata = []; 
                break;
              }
              
              id = m_tree_obj[i].id.toString();
              sun_downline = m_tree_obj[i].sun_downline;

              ball_index = m_tree_obj[i].ball_index;
              if(ball_index=='0'){
                nickname = m_tree_obj[i].nickname;
              } else {
                nickname = m_tree_obj[i].nickname+'('+ball_index+')';
              }

              //nickname = m_tree_obj[i].nickname;//+'('+id+')';
              //pv_rl = m_tree_obj[i].pv_rl.toString();
              floor = m_tree_obj[i].floor.toString();
              set = m_tree_obj[i].set;
                    
              // 把上線null 改成空白
              if(upline == '0'){
                  upline='';
              }

              if(floor == '0'){
                upline='';
              }

              var setColor = 'white';
              if(set == '0'){
                setColor = '#00FF99'; // 綠 00FF99
              }

              if(set == '7000'){
                setColor = '#FFB7DD'; // 粉紅
              }

              if(set == '700'){
                setColor = '#77DDFF'; // 藍色
              }

              // if(set == '10'){
              //   setColor = '#FFAA33'; // 橘黃
              // }

              if(set == 'empty'){
                setColor = 'gray';
              }
              
              if(i==0){
                 // var split_pv_rl = pv_rl.split("/");
                 // var new_pv_rl = '';
                 // if(split_pv_rl.length > 1){

                 //    var min_rl = Math.min(split_pv_rl[0], split_pv_rl[1]);
                 //    split_pv_rl[0] = split_pv_rl[0] - min_rl;
                 //    split_pv_rl[1] = split_pv_rl[1] - min_rl;
                 //    new_pv_rl = split_pv_rl[0]+'/'+split_pv_rl[1];
                 // }

                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +sun_downline+'<br /><span style="color:gray;">'+nickname+'</span>';
              } else {
                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +sun_downline+'<br /><span style="color:gray;">'+nickname+'</span>';
              }

              // m_tree_mapdata 接陣列資料
              m_tree_mapdata[i] = 
                [ { v:id, 
                    f:(show_txt)},
                  upline, 
                  ''
                ]
          }
        
          drawChart('recommend'); // 畫出
}


// ＊＊畫出GoogleChart, 資料是接全域陣列 m_tree_mapdata ＊＊　
function drawChart(br_type) {

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Manager');
  data.addColumn('string', 'ToolTip');

  data.addRows(m_tree_mapdata); // 用 m_tree_mapdata這個陣列顯示

  // Create the chart.
  var container = document.getElementById('show_tree_div');
  m_tree_chart = new google.visualization.OrgChart(container);

  // Click開關
  container.addEventListener('click', function (e) {
    e.preventDefault();
    var selection = m_tree_chart.getSelection();
    if(selection != ''){
      var row = selection[0].row;
      jumpSpecificUrl(row, br_type);
      
    }

      m_tree_chart.setSelection([]);
      return false;
    }, false);

  var options = {
      allowCollapse: true,
      allowHtml: true,
      color: 'rgba(0, 0, 0, 0)'
  };

  // 畫出樹狀圖
  m_tree_chart.draw(data, options);

}


// ＊＊ 跳至點擊會員頁 ＊＊　
function jumpSpecificUrl(id, br_type) {

  if(id ==0){ // root不能返回
    return false;
  }

  var user_id = m_tree_obj[id].id;
  var user_account = m_tree_obj[id].account;

  if(br_type == 'branch'){
    if(m_tree_obj[id].is_empty.toString() == 'true'){ // empty
      return false;
    }
  }

  //location.href = "/user/org/"+br_type+"/"+user_id.toString();
  location.href = "/user/org/"+br_type+"/"+user_account.toString();
  
  return false;
}


// ＊＊整理m_tree_obj 然後畫出來 ＊＊　
function OrganizationTreeByObjAndDraw_AdminBranch(){

          var showTitleStyle = ''; // 最後要顯示的title顏色
          var id = '';
          var branch_upline = '';
          var pv_rl = '';
          var r_plus_l = ''
          var nickname = ''
          var show_txt = ''
          var set = 0;

          // 把收到的資料, 填到google-chart要的陣列
          for (var i = m_tree_obj.length - 1; i >= 0; i--) {

              set = m_tree_obj[i].set;   
              branch_upline = (m_tree_obj[i].branch_upline.toString()).toLowerCase();

              // 未激活頁面 自己+兩小灰人共3人
              if( m_tree_obj.length == 3 && branch_upline == '' && set == 0){
                m_tree_mapdata = []; 
                break;
              }

              pv_rl = m_tree_obj[i].pv_rl;        
              
              r_plus_l = m_tree_obj[i].r_plus_l;
              if(r_plus_l==-1){
                r_plus_l = '';
              }
              nickname = m_tree_obj[i].nickname;

              

              id = (m_tree_obj[i].id.toString()).toLowerCase();  
                    
              var is_empty = m_tree_obj[i].is_empty;

              // 把上線null 改成空白
              if(branch_upline.toLowerCase() == 'null'){
                  branch_upline='';
              }

              var setColor = 'white';
              if(set == '0'){
                setColor = '#77DDFF'; // 藍色
              }

              if(set == '500'){
                setColor = '#FFB7DD'; // 粉紅
              }

              if(set == '100'){
                setColor = '#00FF99'; // 綠
              }

              if(set == '10'){
                setColor = '#FFAA33'; // 橘黃
              }

              if(set == 'empty'){
                setColor = 'gray';
              }
              
              if(i==0){
                 // var split_pv_rl = pv_rl.split("/");
                 // var new_pv_rl = '';
                 // if(split_pv_rl.length > 1){
                  
                 //    var min_rl = Math.min(split_pv_rl[0], split_pv_rl[1]);
                 //    split_pv_rl[0] = split_pv_rl[0] - min_rl;
                 //    split_pv_rl[1] = split_pv_rl[1] - min_rl;
                 //    new_pv_rl = split_pv_rl[0]+'/'+split_pv_rl[1];
                 // }

                 show_txt = pv_rl+'<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'+nickname+'<br />'
                    +m_lang_set+':'+m_tree_obj[i].set+'<br />PV:'+m_tree_obj[i].pv+'<br />'+m_lang_performance_total+':'+r_plus_l+'<br />('+m_lang_performance_L+')'+m_tree_obj[i].l+'&nbsp;&nbsp;&nbsp;('+m_lang_performance_R+')'+m_tree_obj[i].r;
              } else {
                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +r_plus_l+'<br />'+nickname;
              }
              
              // m_tree_mapdata 接陣列資料
              m_tree_mapdata[i] = 
                [ { v:id, 
                    f:(show_txt)},
                  branch_upline, 
                  ''
                ]
          }
        
          drawChartAdmin('branch'); // 畫出
}


// ＊＊畫出GoogleChart, 資料是接全域陣列 m_tree_mapdata ＊＊　
function drawChartAdmin(br_type) {

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Manager');
  data.addColumn('string', 'ToolTip');

  data.addRows(m_tree_mapdata); // 用 m_tree_mapdata這個陣列顯示

  // Create the chart.
  var container = document.getElementById('show_tree_div');
  m_tree_chart = new google.visualization.OrgChart(container);

  // Click開關
  container.addEventListener('click', function (e) {
    e.preventDefault();
    var selection = m_tree_chart.getSelection();
    if(selection != ''){
      var row = selection[0].row;
      jumpSpecificUrlAdmin(row, br_type);
      
    }

      m_tree_chart.setSelection([]);
      return false;
    }, false);

  var options = {
      allowCollapse: true,
      allowHtml: true,
      color: 'rgba(0, 0, 0, 0)'
  };

  // 畫出樹狀圖
  m_tree_chart.draw(data, options);

}


// ＊＊ 跳至點擊會員頁 ＊＊　
function jumpSpecificUrlAdmin(id, br_type) {

  if(id ==0){ // root不能返回
    return false;
  }

  var user_id = m_tree_obj[id].id;

  if(br_type == 'branch'){
    if(m_tree_obj[id].is_empty.toString() == 'true'){ // empty
      return false;
    }
  }

  location.href = "/admin/manage/"+br_type+"/"+user_id.toString();
  
  return false;
}



// ＊＊整理m_tree_obj 然後畫出來 ＊＊　
function OrganizationTreeByObjAndDraw_AdminRecommend(){

          var id = '';
          var upline = '';
          //var pv_rl = '';
          var floor = '';
          var show_txt = ''
          var sun_downline = ''
          var nickname = ''
          var set = '';
          var ball_index = '';
          

          // 把收到的資料, 填到google-chart要的陣列
          for (var i = m_tree_obj.length - 1; i >= 0; i--) {

              upline = m_tree_obj[i].recommender.toString();

              
              // 未激活頁面
              // if(m_tree_obj[i].set <= 0){
              //   m_tree_mapdata = []; 
              //   break;
              // }

              
              id = m_tree_obj[i].id.toString();
              sun_downline = m_tree_obj[i].sun_downline;
              //          var ball_index = '';
              ball_index = m_tree_obj[i].ball_index;
              if(ball_index=='0'){
                nickname = m_tree_obj[i].nickname;
              } else {
                nickname = m_tree_obj[i].nickname+'('+ball_index+')';
              }
              
              //pv_rl = m_tree_obj[i].pv_rl.toString();
              floor = m_tree_obj[i].floor.toString();
              set = m_tree_obj[i].set;
                
              // 把上線null 改成空白
              if(upline == '0'){
                  upline='';
              }

              if(floor == '0'){
                upline='';
              }

              var setColor = 'white';
              if(set == '0'){
                setColor = '#bbb'; // 綠 00FF99
              }

              if(set == '7000'){
                setColor = '#FFB7DD'; // 粉紅
              }

              if(set == '700'){
                setColor = '#77DDFF'; // 藍色
              }

              // if(set == '10'){
              //   setColor = '#FFAA33'; // 橘黃
              // }

              
              if(i==0){
                 // var split_pv_rl = pv_rl.split("/");
                 // var new_pv_rl = '';                 
                 // if(split_pv_rl.length > 1){

                 //    var min_rl = Math.min(split_pv_rl[0], split_pv_rl[1]);
                 //    split_pv_rl[0] = split_pv_rl[0] - min_rl;
                 //    split_pv_rl[1] = split_pv_rl[1] - min_rl;
                 //    new_pv_rl = split_pv_rl[0]+'/'+split_pv_rl[1];
                 // }

                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +sun_downline+'<br /><span style="color:gray;">'+nickname+'</span>';
              } else {
                 show_txt = '<div style="font-size:25px;color:'+setColor+';"><i class="fas fa-user"></i></div>'
                    +sun_downline+'<br /><span style="color:gray;">'+nickname+'</span>';
              }

              // m_tree_mapdata 接陣列資料
              m_tree_mapdata[i] = 
                [ { v:id, 
                    f:(show_txt)},
                  upline, 
                  ''
                ]
          }
        
          drawChartAdmin('recommend'); // 畫出
}