

var p_ApiMgr_admin = {};

(function (a) {
    a.showError = function (error_code) {
        var msg = {
            101: "錯誤參數",
            102: "數值不合規範",

        
            // user
            201: "帳號或密碼錯誤",    
            202: "找不到分帳號",
            203: "安置帳號未激活,或查詢權限不足",
            204: "帳號錯誤",
            205: "帳號重複",
            206: "註冊失敗01",
            207: "註冊安置-推薦ID有誤",
            208: "註冊安置-上家ID有誤",
            209: "註冊安置-帳號不能有底線",
            210: "兩次輸入密碼不同",            
            211: "密碼錯誤",              
            212: "註冊失敗02",
            213: "註冊失敗03",
            214: "必須高於現有配套",
            215: "開通配套-點數不足",
            216: "可流動點數不足",
            217: "轉點帳號有誤",
            218: "目標帳戶和來源帳戶相同",
            219: "必須是自己的推薦下線",
            220: "來源帳號有誤",
            221: "目標帳號有誤",
            222: "下線代數不符",
            223: "推薦人帳號有誤",
            224: "目前無法激活",            
            225: "委託-點數不足",
            226: "委託-撤銷失敗",            
            227: "委託-系統忙碌中",
            228: "交易已不存在",
            229: "取消交易-不合法",            
            230: "確認交易-不合法",                
            231: "轉帳失敗-單據有誤",
            232: "轉帳失敗-委託有誤",            
            235: "轉帳失敗-延遲過久",
            236: "取消交易失敗-延遲過久",            
            237: "推薦人不存在",   
            238: "未激活帳號-不開放註冊下線",             
            239: "未激活帳號-限制交易100點", 
            240: "未激活帳號-一次只能一單交易", 
            241: "對象不存在", 
            242: "商品名稱不存在",                         
            243: "配套不合法",   
            244: "今日儲值額度已滿",
            245: "銀行已綁定",
            246: "尚未儲存國碼",
            247: "尚未儲存電話",
            248: "此電話已綁定",
            249: "超過每日申請次數",
            250: "請先申請 手機驗證碼",
            251: "綁定狀態錯誤(4)",    
            252: "驗證碼不符",             
            253: "銀行未綁定",    
            254: "手機未綁定",            
            255: "凍結 無法交易",  
            256: "註冊失敗04",
            257: "彩點不足",
            258: "驗證錯誤超過三次 請重新申請驗證碼",
            259: "目前市場無點數可買",            
            260: "交易開啟8小時候才能取消", 
            261: "目前仍有交易進行中", 
            262: "委託不存在", 
            263: "委託已撤銷", 
            264: "推薦人帳號未開通", 
            265: "帳號凍結", 
            266: "相關銀行帳戶凍結", 
            267: "相關電話凍結", 
            268: "相關人員被凍結", 
            269: "提領編號錯誤", 


            // language
            301: "語系不合法",
                                
            // 回主頁
            401: "token到期",
            402: "token被拒绝",
            403: "session被拒绝",  

            501: "帳號或密碼錯誤",  
            502: "管理員不存在",
            503: "對象不存在",            
            504: "交易不存在",              
            505: "委託不存在", 
            506: "請求錯誤",            
            507: "原密碼錯誤",              
            508: "兩次輸入密碼不同", 
            509: "找不到此用戶", 
            510: "銀行帳號為空值", 
            511: "推薦帳號為空時, 配套必須為0", 
            512: "分紅資料有誤", 
            513: "非可設定日期範圍",             
            514: "提問編號錯誤",               



        };
        return msg[error_code];
    };


    a.send = function (type, url, callback, target, data, errorCallback, errorCallbackTarget) {
        $.ajax({
            type: type,
            url: url,
            data: data,
            headers: { // 傳給TokenMiddleware.php
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': 'Bearer ' + $('meta[name="auth-token"]').attr('content') 
            },
            success: function (result) {
                if (result['error'] == "0") {
                    if (callback) {
                        callback.call(target, result['data']);
                    }
                } else if (result['error'] == "241"){ // 對象不存在, 跳回帳號列表
                    errorMsg = a.showError(result['error']);
                    alert( errorMsg );
                    location.href = "/admin/manage/account";
                }
                else if ( // 回主頁
                    result['error'] == "401" ||
                    result['error'] == "402" ||                    
                    result['error'] == "403") {
                    location.href = "/admin";
                } else { // 會跳alert
                    errorMsg = a.showError(result['error']);
                    alert( errorMsg );
                    console.log( errorMsg );
                    if (errorCallback) {
                        errorCallback.call(errorCallbackTarget, result['error'])
                    }
                }
            }
        });
    };


    // ＊＊ 登入 ＊＊
    a.login = function (account, password, callback, target, errorCallback, errorCallbackTarget) {
          
        a.send('post', '/api/v1/admin/login', callback, target, {
            account:account,
            password:password,
        }, errorCallback, errorCallbackTarget)
    };

    a.getBranchs = function (account, password, callback, target, errorCallback, errorCallbackTarget) {
            
        a.send('post', '/api/v1/admin/branch', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ get server-side datatable ＊＊
    a.getDataTableAjax = function (type, url) {
        return {
            url: url,
            type: type,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                xhr.setRequestHeader("Authorization", 'Bearer ' + $('meta[name="auth-token"]').attr('content'));
            }
        };
    };

    // ＊＊ 設定語言 ＊＊
    a.setLanguage = function (lang, callback, target, errorCallback, errorCallbackTarget) {
        a.send('put', '/api/v1/language/'+lang.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 回收點數 ＊＊
    a.recyclePoint = function (wallet_id, user_id, point, callback, target, errorCallback, errorCallbackTarget) {

        a.send('post', '/api/v1/admin/recycle/point', callback, target, {
          wallet_id:wallet_id,
          user_id:user_id,
          point:point,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 登出 ＊＊
    a.logout = function (callback, target, errorCallback, errorCallbackTarget) {
        //alert('123');
        a.send('post', '/api/v1/admin/logout', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得帳號 ＊＊
    a.getAdminAccount = function (callback, target, errorCallback, errorCallbackTarget) {
        //alert('123');
        a.send('get', '/api/v1/admin/account', callback, target, {}, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 取得會員資料 ＊＊
    a.getUserAccountInfo = function (user_id, callback, target, errorCallback, errorCallbackTarget) {

        a.send('get', '/api/v1/user/info/by/admin', callback, target, {
          user_id:user_id,
        }, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 更新會員資料 ＊＊
    a.updateUserInfo = function (user_id, nickname, country_code, phone, new_pw, bank_code,bank_branch,bank_account,callback, target, errorCallback, errorCallbackTarget) {

        a.send('post', '/api/v1/user/info/by/admin', callback, target, {
          user_id:user_id,
          nickname:nickname,
          country_code:country_code,
          phone:phone,
          new_pw:new_pw,     
          bank_code:bank_code,
          bank_branch:bank_branch,
          bank_account:bank_account,                                               
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得會員branch tree ＊＊
    a.getBranchSpecific = function (user_id, callback, target, errorCallback, errorCallbackTarget) {

        a.send('get', '/api/v1/org/branch/'+user_id.toString()+'/by/admin', callback, target, {
                                       
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得會員branch tree ＊＊
    a.getRecommenderSpecific = function (user_id, callback, target, errorCallback, errorCallbackTarget) {

        a.send('get', '/api/v1/org/recommend/'+user_id.toString()+'/by/admin', callback, target, {
                                       
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取消交易單 ＊＊
    a.cancelTrading = function (trading_id, callback, target, errorCallback, errorCallbackTarget) {

        a.send('post', '/api/v1/trading/cancel', callback, target, {
          trading_id:trading_id,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取消委託 ＊＊
    a.cancelOrder = function (order_id, callback, target, errorCallback, errorCallbackTarget) {

        a.send('post', '/api/v1/order/cancel', callback, target, {
          order_id:order_id,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得會員儲值開關 ＊＊
    a.checkIsAllLimitWithdraw = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/limit/withdraw', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 限制儲值(開關) ＊＊
    a.switchLimitedWithdraw = function (is_limited_withdraw, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/limit/withdraw', callback, target, {
          is_limited_withdraw:is_limited_withdraw,
        }, errorCallback, errorCallbackTarget)
    };    

    // ＊＊ 檢查現在是否要用彩點 ＊＊
    a.checkIsColorpoint = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/wallet/colorpoint/is_positive', callback, target, {}, errorCallback, errorCallbackTarget)
    };  

    // ＊＊ 更改每日簡訊上限 ＊＊
    a.updateDailyMessageLimit = function ( max_verify_times ,callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/max/verify/message', callback, target, {
          max_verify_times:max_verify_times,
        }, errorCallback, errorCallbackTarget)
    };  

    // ＊＊ 取得每日簡訊上限 ＊＊
    a.getDailyVerifyMessageLimit = function ( callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/max/verify/message', callback, target, {}, errorCallback, errorCallbackTarget)
    };  

    // ＊＊ 修改管理員密碼 ＊＊
    a.UpdateAdminPassword = function (old_pw, new_pw, new_pw2, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/pw', callback, target, {
            old_pw:old_pw,
            new_pw:new_pw,
            new_pw2:new_pw2,
        }, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 推薦人註冊 ＊＊
    a.registerFromRecommender = function (recommender_account, account, nickname, country_code, phone, pw, pw2, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/register/user/recommender', callback, target, {
            recommender_account : recommender_account,
            account : account,
            nickname : nickname,
            country_code : country_code,
            phone : phone,
            pw : pw,
            pw2 : pw2,
        }, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 極左極右註冊 ＊＊
    a.registerMaxLeftMaxRight = function (recommender_account, RL, account, nickname, country_code, phone, level, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/register/user/max_l_max_r', callback, target, {
            recommender_account : recommender_account,
            RL : RL,
            account : account,
            nickname : nickname,
            country_code : country_code,
            phone : phone,
            level : level,
        }, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 無上線註冊 ＊＊
    a.registerNoUpline = function (account, nickname, country_code, phone, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/register/user/no_upline', callback, target, {
            account : account,
            nickname : nickname,
            country_code : country_code,
            phone : phone,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 檢查id是否被凍結 ＊＊
    a.verifyFrozen_byID = function (user_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/verify/frozen/by/id', callback, target, {
            user_id:user_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 檢查phone是否被凍結 ＊＊
    a.verifyFrozen_byPhone = function (user_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/verify/frozen/by/phone', callback, target, {
            user_id:user_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 檢查bank是否被凍結 ＊＊
    a.verifyFrozen_byBank = function (user_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/verify/frozen/by/bank', callback, target, {
            user_id:user_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 限制交易byID (切換) ＊＊
    a.switchFrozenByID = function (user_id, is_frozen, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/switch/frozen/by/id', callback, target, {
          user_id:user_id,
          is_frozen:is_frozen,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 限制交易byPhone (切換) ＊＊
    a.switchFrozenByPhone = function (user_id, is_frozen, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/switch/frozen/by/phone', callback, target, {
          user_id:user_id,
          is_frozen:is_frozen,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 限制交易byPhone (切換) ＊＊
    a.switchFrozenByBank = function (user_id, is_frozen, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/switch/frozen/by/bank', callback, target, {
          user_id:user_id,
          is_frozen:is_frozen,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 贈點 ＊＊
    a.send_points = function (user_id, point, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/send/point', callback, target, {
          user_id:user_id,
          point:point,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 贈彩點 ＊＊
    a.send_color_points = function (user_id, color_point, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/send/colorpoint', callback, target, {
          user_id:user_id,
          color_point:color_point,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 新版註冊 ＊＊
    a.registerNew = function (recommender_account, account, nickname, country_code, phone, level, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/register/new', callback, target, {
          recommender_account:recommender_account,
          account:account,
          nickname:nickname,
          country_code:country_code,
          phone:phone,
          level:level,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 提領處理中 ＊＊
    a.acceptingWithdraw = function (withdraw_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/accepting/withdraw', callback, target, {
          withdraw_id:withdraw_id,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 提領完成 ＊＊
    a.dealWithdraw = function (withdraw_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/deal/withdraw', callback, target, {
          withdraw_id:withdraw_id,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得目前已存倍率 ＊＊
    a.getDividendRates = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/admin/dividend/rates', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 新增一筆日倍率 ＊＊
    a.addDividendRates = function (year, month, day, rate, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/dividend/rates', callback, target, {
            year:year,
            month:month,
            day:day,
            rate:rate,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 確認回覆留言 ＊＊
    a.confirmReply = function (ask_id, reply, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/ask/reply', callback, target, {
            ask_id:ask_id,
            reply:reply,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 強制通過電話驗證 ＊＊
    a.forceVerifyMobile = function (user_id, country_code, phone, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/admin/force/verify/phone', callback, target, {
            user_id:user_id,
            country_code:country_code,
            phone:phone,
        }, errorCallback, errorCallbackTarget)
    };



   //  // ＊＊ 設定語言 ＊＊
   //  a.setLanguage = function (lang, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('put', '/api/v1/language/'+lang.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 取得特定 branch 組織圖資料 ＊＊
   //  a.getBranchSpecific = function (id ,callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/org/branch/'+id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 取得特定 recommender 組織圖資料 ＊＊
   //  a.getRecommenderSpecific = function (id ,callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/org/recommend/'+id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
   //  };



   //  // ＊＊ 註冊 ＊＊
   //  a.register = function (RL, account, country_code, phone, coin, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/user/register', callback, target, {
   //          RL:RL,
   //          account:account,
   //          country_code:country_code,
   //          phone:phone,
   //          coin:coin,
   //      }, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 取得帳號 ＊＊
   //  a.getAccount = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/user/account', callback, target, {}, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 取得會員資訊 ＊＊
   //  a.getUserInfo = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/user/info', callback, target, {}, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 修改會員資訊 ＊＊
   //  a.updateUserInfo = function (nickname, country_code, phone, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/user/info', callback, target, {
   //          nickname:nickname,
   //          country_code:country_code,
   //          phone:phone,
   //      }, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 修改會員密碼 ＊＊
   //  a.updateUserPassword = function (old_pw, new_pw, new_pw2, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/user/pw', callback, target, {
   //          old_pw:old_pw,
   //          new_pw:new_pw,
   //          new_pw2:new_pw2,
   //      }, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 修改銀行資訊 ＊＊
   //  a.updateBankInfo = function (bank_code, bank_branch, bank_account, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/user/bank', callback, target, {
   //          bank_code:bank_code,
   //          bank_branch:bank_branch,
   //          bank_account:bank_account,
   //      }, errorCallback, errorCallbackTarget)
   //  };

   //  // ＊＊ 取得銀行資訊 ＊＊
   //  a.getBankInfo = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/user/bank', callback, target, {}, errorCallback, errorCallbackTarget)
   //  };

   // // ＊＊ 取得錢包點數 ＊＊
   //  a.getPoint = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/wallet/point', callback, target, {}, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ 取得目前配套 ＊＊
   //  a.getSet = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('get', '/api/v1/org/set', callback, target, {}, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ 激活配套 ＊＊
   //  a.activeSet = function (set, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/user/set', callback, target, {
   //          set : set,
   //      }, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ 轉出點數 ＊＊
   //  a.depositPoints = function (deposit_account, points, callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/txn/point', callback, target, {
   //          deposit_account : deposit_account,
   //          points : points,
   //      }, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ 切換機活 ＊＊
   //  a.switchActive = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/org/active/auto', callback, target, {}, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ 切換機活 ＊＊
   //  a.activeStaticOut = function (callback, target, errorCallback, errorCallbackTarget) {
   //      a.send('post', '/api/v1/org/active', callback, target, {}, errorCallback, errorCallbackTarget)
   //  }; 

   //  // ＊＊ PV ＊＊
   //  // a.calculatePV = function (callback, target, errorCallback, errorCallbackTarget) {
   //  //     a.send('post', '/api/v1/org/pv', callback, target, {}, errorCallback, errorCallbackTarget)
   //  // }; 

   //  // ＊＊ 贈與 ＊＊
   //  // a.givePoint = function (target, value, callback, target, errorCallback, errorCallbackTarget) {
   //  //     a.send('post', '/api/v1/txn/give/point', callback, target, {
   //  //         value : value,
   //  //     }, errorCallback, errorCallbackTarget)
   //  // }; 

})(p_ApiMgr_admin);
