

	$(document).ready(function() {
		getAdminAccount();
	    //$("#nav_show_login").css("opacity","1");
		$("#tag_logout").on("click", logout);		
	  //$("#text_show_login").text("999");
	});// end ready func


	function pageMgrAccount(){
	  location.href = "/admin/manage/account";
	}

	// function pageMgrBranch(){
	//   location.href = "/admin/manage/branch/0";
	// }

	function pageMgrRecommend(){
	  location.href = "/admin/manage/recommend/0";
	}

	function pageTrade(){
	  location.href = "/admin/trade";
	}

	function pageWithdraw(){
	  location.href = "/admin/withdraw";
	}

	function pageControl(){
	  location.href = "/admin/control";
	}

	function pageInfo(){
	  location.href = "/admin/info";
	}

	function pageCalender(){
	  location.href = "/admin/calender";
	}
	
	function pageAsk(){
	  location.href = "/admin/ask";
	}


	// logout
	function logout(){

		  p_ApiMgr_admin.logout(
		        function (result) {		       
		          location.href = "/admin";
		        },this,
		        function (error) {},this
		  );

	  return false;
	}

	// getAdminAccount
	function getAdminAccount(){

		  p_ApiMgr_admin.getAdminAccount(
		        function (result) {		       
	          	  $("#text_show_login").text(result.toString());
		        },this,
		        function (error) {},this
		  );

	  return false;
	}
