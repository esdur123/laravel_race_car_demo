var C_SET_LEVEL_0 = 0;
var C_SET_LEVEL_1 = 700;
var C_SET_LEVEL_2 = 7000;
var C_SET_LEVEL_3 = 99999;

    
// ＊＊ 根據等級取得配套 ＊＊
function getSetFromLevel(_lvl) { 

    var s = -1;
    if(_lvl ==  0) s = C_SET_LEVEL_0;
    if(_lvl ==  1) s = C_SET_LEVEL_1;
    if(_lvl ==  2) s = C_SET_LEVEL_2;
    if(_lvl ==  3) s = C_SET_LEVEL_3;

    return s; 
} 



// ＊＊ 根據配套取得等級 ＊＊
function getLevelFromSet(_set) { 

    var lvl = -1;
    if(_set ==  C_SET_LEVEL_0) lvl = 0;
    if(_set ==  C_SET_LEVEL_1) lvl = 1;
    if(_set ==  C_SET_LEVEL_2) lvl = 2;
    if(_set ==  C_SET_LEVEL_3) lvl = 3;

    return lvl; 
} 