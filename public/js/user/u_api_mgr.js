var p_ApiMgr = {};

(function (a) {
    a.showError = function (error_code) {
        var msg = {
            101: "錯誤參數",
            102: "請輸入正整數",
            103: "參數不合法",
            104: "必須為10的倍數",            
            105: "超過數量上限",       
            106: "必須為100的倍數",            
        
            // user
            201: "帳號或密碼錯誤",    
            202: "找不到分帳號",
            203: "安置帳號未激活,或查詢權限不足",
            204: "帳號錯誤",
            205: "帳號重複",
            206: "註冊失敗01",
            207: "註冊安置-推薦ID有誤",
            208: "註冊安置-上家ID有誤",
            209: "註冊安置-帳號不能有底線",
            210: "兩次輸入密碼不同",            
            211: "密碼錯誤",              
            212: "註冊失敗02",
            213: "註冊失敗03",
            214: "必須高於現有配套",
            215: "開通配套-點數不足",
            216: "可流動點數不足",
            217: "轉點帳號有誤",
            218: "目標帳戶和來源帳戶相同",
            219: "必須是自己的推薦下線",
            220: "來源帳號有誤",
            221: "目標帳號有誤",
            222: "下線代數不符",
            223: "推薦人帳號有誤",
            224: "目前無法激活",            
            225: "委託-點數不足",
            226: "委託-撤銷失敗",            
            227: "委託-系統忙碌中",
            228: "交易已不存在",
            229: "取消交易-不合法",            
            230: "確認交易-不合法",                
            231: "轉帳失敗-單據有誤",
            232: "轉帳失敗-委託有誤",            
            235: "轉帳失敗-延遲過久",
            236: "取消交易失敗-延遲過久",            
            237: "推薦人不存在",   
            238: "未激活帳號-不開放註冊下線",             
            239: "未激活帳號-限制交易700點", 
            240: "未激活帳號-一次只能一單交易", 
            241: "對象不存在", 
            242: "商品名稱不存在",                         
            243: "配套不合法",   
            244: "今日儲值額度已滿",
            245: "銀行已綁定",
            246: "尚未儲存國碼",
            247: "尚未儲存電話",
            248: "此電話已綁定",
            249: "超過每日申請次數",
            250: "請先申請 手機驗證碼",
            251: "綁定狀態錯誤(4)",    
            252: "驗證碼不符",             
            253: "銀行未綁定",    
            254: "手機未綁定",            
            255: "凍結 無法交易",  
            256: "註冊失敗04",
            257: "彩點不足",
            258: "驗證錯誤超過三次 請重新申請驗證碼",
            259: "目前市場無點數可買",            
            260: "交易開啟8小時候才能取消", 
            261: "目前仍有交易進行中", 
            262: "委託不存在", 
            263: "委託已撤銷", 
            264: "推薦人帳號未開通", 
            265: "帳號凍結", 
            266: "相關銀行帳戶凍結", 
            267: "相關電話凍結", 
            268: "相關人員被凍結", 
            269: "提領編號錯誤", 
            270: "今日提領額度已滿", 
            271: "未激活帳號不開放註冊球", 
            272: "帳號不擁有這顆球", 
            273: "超過今日最高提問次數", 


            // ＊＊　語系　＊＊
            // 取消交易失敗-延遲過久
            301: "語系不合法",
                    
            // ＊＊　Token　＊＊
            // 回主頁
            401: "token到期",
            402: "token被拒绝",
            403: "session被拒绝",            

            // ＊＊　儲值　＊＊
            601: "金額有誤", 
            602: "產生訂單發生錯誤(1)", 
            603: "產生訂單發生錯誤(2)",
            604: "產生訂單發生錯誤(3)",                        

            // ＊＊　簡訊　＊＊
            701: "簡訊錯誤", 
            702: "請稍後再傳送", 

        };
        return msg[error_code];
    };


    a.send = function (type, url, callback, target, data, errorCallback, errorCallbackTarget) {
        $.ajax({
            type: type,
            url: url,
            data: data,
            headers: { // 傳給TokenMiddleware.php
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': 'Bearer ' + $('meta[name="auth-token"]').attr('content') 
            },
            success: function (result) {
                if (result['error'] == "0") {
                    if (callback) {
                        callback.call(target, result['data']);
                    }
                } else if ( // 回主頁
                    result['error'] == "401" ||
                    result['error'] == "402" ||                    
                    result['error'] == "403" ) {
                    location.href = "/";
                } else if(result['error'] == "602"){ // 會跳alert + 回首頁
                    errorMsg = a.showError(result['error']);
                    alert( errorMsg );
                    location.href = "/";
                }  else if(result['error'] == "603"){ // 從金流按上一頁回來的時候, 直接跳回儲值頁
                    location.href = "/user/pay";

                } else if(result['error'] == "228"){ // 交易已不存在, 跳回交易頁
                    errorMsg = a.showError(result['error']);
                    alert( errorMsg );                    
                    location.href = "/user/trade";

                    //228
                } else { // 會跳alert
                    errorMsg = a.showError(result['error']);
                    alert( errorMsg );
                    console.log( errorMsg );
                    if (errorCallback) {
                        errorCallback.call(errorCallbackTarget, result['error'])
                    }
                }
            }
        });
    };


    // ＊＊ 登入 ＊＊
    a.login = function (account, password, callback, target, errorCallback, errorCallbackTarget) {
       
        a.send('post', '/api/v1/user/login', callback, target, {
            account:account,
            password:password,
        }, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 登出 ＊＊
    a.logout = function (callback, target, errorCallback, errorCallbackTarget) {

        a.send('post', '/api/v1/user/logout', callback, target, {}, errorCallback, errorCallbackTarget)
    };


    // ＊＊ 設定語言 ＊＊
    a.setLanguage = function (lang, callback, target, errorCallback, errorCallbackTarget) {
        a.send('put', '/api/v1/language/'+lang.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };


    // ＊＊ get server-side datatable ＊＊
    a.getDataTableAjax = function (type, url) {
        return {
            url: url,
            type: type,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                xhr.setRequestHeader("Authorization", 'Bearer ' + $('meta[name="auth-token"]').attr('content'));
            }
        };
    };


    // ＊＊ 取得特定 branch (id版) 組織圖資料 ＊＊
    // a.getBranchSpecific_by_id = function (id ,callback, target, errorCallback, errorCallbackTarget) {
    //     a.send('get', '/api/v1/org/branch/'+id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    // };

    // ＊＊ 取得特定 recommender 組織圖資料 ＊＊
    // a.getRecommenderSpecific = function (id ,callback, target, errorCallback, errorCallbackTarget) {
    //     a.send('get', '/api/v1/org/recommend/'+id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    // };

    // ＊＊ 取得特定 branch (account版) 組織圖資料 ＊＊
    a.getBranchSpecific_by_account = function (account ,callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/org/branch/'+account.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };    

    // ＊＊ 取得特定 recommender 組織圖資料 ＊＊
    a.getRecommenderSpecific_by_account = function (account ,callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/org/recommend/'+account.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 註冊 ＊＊
    a.register = function (account, nickname, country_code, phone, level, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/register', callback, target, {
            account:account,
            nickname:nickname,
            country_code:country_code,
            phone:phone,
            level:level,
        }, errorCallback, errorCallbackTarget)
    };

    
    // ＊＊ 取得帳號 ＊＊
    a.getAccount = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/user/account', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得會員資訊 ＊＊
    a.getUserInfo = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/user/info', callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 修改會員資訊 ＊＊
    a.updateUserInfo = function (nickname, country_code, phone, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/info', callback, target, {
            nickname:nickname,
            country_code:country_code,
            phone:phone,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 修改會員密碼 ＊＊
    a.updateUserPassword = function (old_pw, new_pw, new_pw2, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/pw', callback, target, {
            old_pw:old_pw,
            new_pw:new_pw,
            new_pw2:new_pw2,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 修改銀行資訊 ＊＊
    a.updateBankInfo = function (bank_code, bank_branch, bank_account, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/bank', callback, target, {
            bank_code:bank_code,
            bank_branch:bank_branch,
            bank_account:bank_account,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得銀行資訊 ＊＊
    a.getBankInfo = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/user/bank', callback, target, {}, errorCallback, errorCallbackTarget)
    };

   // ＊＊ 取得錢包點數 ＊＊
    a.getPoint = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/wallet/point', callback, target, {}, errorCallback, errorCallbackTarget)
    }; 

   // ＊＊ 取得彩點點數 ＊＊
    a.getColorPoint = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/wallet/colorpoint', callback, target, {}, errorCallback, errorCallbackTarget)
    }; 


    // ＊＊ 取得目前配套 ＊＊
    a.getSet = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/org/set', callback, target, {}, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 激活配套 ＊＊
    a.activeLevel = function (new_level, ball_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/level', callback, target, {
            new_level : new_level,
            ball_id : ball_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 轉出點數 ＊＊
    a.depositPoints = function (deposit_account, points, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/deposit/point', callback, target, {
            deposit_account : deposit_account,
            points : points,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 轉出彩點 ＊＊
    a.depositColorPoints = function (deposit_account, color_points, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/deposit/colorpoint', callback, target, {
            deposit_account : deposit_account,
            color_points : color_points,
        }, errorCallback, errorCallbackTarget)
    }; 


    // ＊＊ 切換機活 ＊＊
    a.switchActive = function (ball_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/org/active/auto', callback, target, {
            ball_id : ball_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 靜態激活 ＊＊
    a.activeStaticOut = function (ball_id, callback, target, errorCallback, errorCallbackTarget) {
        // alert('ball_id = '+ ball_id);
        a.send('post', '/api/v1/org/active/static', callback, target, {
            ball_id : ball_id,
        }, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 取得ball ID ＊＊
    a.getBallID = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/org/ball/id', callback, target, {}, errorCallback, errorCallbackTarget)
    }; 

    // ＊＊ 創 order單 ＊＊
    a.createOrder = function (amount, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/order/create', callback, target, {
            amount:amount,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 撤銷 order單 ＊＊
    a.cancelOrder = function (order_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/order/cancel', callback, target, {
            order_id : order_id,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 購買點數from order ＊＊
    a.buyPointsFromOrder = function (amount, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/order/buy', callback, target, {
            amount : amount,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取消交易 ＊＊
    a.cancelTrading = function (trading_id, type, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/trading/cancel', callback, target, {
            trading_id : trading_id,
            type : type,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 確認交易 ＊＊
    a.confirmTrading = function (trading_id, type, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/txn/trading/confirm', callback, target, {
            trading_id : trading_id,
            type : type,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 指定ID取得Account ＊＊
    a.getAccountFromID = function (user_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/user/account/'+user_id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 推薦人註冊 ＊＊
    a.registerFromRecommender = function (recommender_account, account, nickname, country_code, phone, pw, pw2, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/user/register/'+recommender_account.toString(), callback, target, {
            recommender_account : recommender_account,
            account : account,
            nickname : nickname,
            country_code : country_code,
            phone : phone,
            pw : pw,
            pw2 : pw2,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 下(Pay)訂單 ＊＊
    a.createPayOrder = function (pay_type, amount, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/create/pay/order', callback, target, {
            pay_type:pay_type,
            amount:amount,
        }, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 金流api ＊＊
    a.createTCOrder = function (pay_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/create/tc/order/'+pay_id.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 取得商品單價 ＊＊
    a.getPrice = function (product, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/price/'+product.toString(), callback, target, {}, errorCallback, errorCallbackTarget)
    };

    // ＊＊ 九宮格抽抽樂 (單抽) ＊＊
    a.play9BlockLottery = function (i_click ,callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/lottery/block9', callback, target, {
            i_click:i_click,
        }, errorCallback, errorCallbackTarget)
    };    

    // ＊＊ 九宮格抽抽樂 (10連抽) ＊＊
    a.play9BlockLottery10Times = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/lottery/block9/ten_times', callback, target, {}, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 驗證簡訊 ＊＊
    a.createSms = function (country_code, phone, callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/sms/create', callback, target, {
            country_code:country_code,
            phone:phone,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 綁定手機(驗證碼確認) ＊＊
    a.bindMobile = function (verify_code, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/sms/bind', callback, target, {
            verify_code:verify_code,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 檢查現在是否要用彩點 ＊＊
    a.checkIsColorpoint = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/wallet/colorpoint/is_positive', callback, target, {}, errorCallback, errorCallbackTarget)
    };   


    // ＊＊ 申請提領 ＊＊
    a.applyFowWithdraw = function (amount, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/withdraw/apply', callback, target, {
            amount:amount,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 使用者取消提領 ＊＊
    a.cancelWithdraw = function (withdraw_id, callback, target, errorCallback, errorCallbackTarget) {
        a.send('post', '/api/v1/withdraw/user/cancel', callback, target, {
            withdraw_id:withdraw_id,
        }, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 使用者取消提領 ＊＊
    a.checkIsWithdrawAvailable  = function (callback, target, errorCallback, errorCallbackTarget) {
        a.send('get', '/api/v1/withdraw/check', callback, target, {}, errorCallback, errorCallbackTarget)
    };   

    // ＊＊ 註冊球 ＊＊
    a.registerNewBall  = function (level, amount, callback, target, errorCallback, errorCallbackTarget) {
       // alert('level = '+level);
        a.send('post', '/api/v1/register/ball', callback, target, {
            level:level,
            amount:amount,
        }, errorCallback, errorCallbackTarget)
    };   


    // ＊＊ 問題提問 ＊＊
    a.askQuestions  = function (title, comment, callback, target, errorCallback, errorCallbackTarget) {
       // alert('level = '+level);
        a.send('post', '/api/v1/ask/apply', callback, target, {
            title:title,
            comment:comment,
        }, errorCallback, errorCallbackTarget)
    };   





    // // ＊＊ 驗證 ＊＊
    // a.verifyTag = function (tag, callback, target, errorCallback, errorCallbackTarget) {
    //     a.send('get', '/api/v1/verify', callback, target, {
    //         tag:tag,
    //     }, errorCallback, errorCallbackTarget)
    // }; 



})(p_ApiMgr);
