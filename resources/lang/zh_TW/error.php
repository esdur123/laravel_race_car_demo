<?php

return array(
	'UNDEFINED_ARGUMENT' => '未定義參數',
	'ILLEGAL_VALUES' => '數值不合規範',

	'USER_LOGIN_ERROR' => '帳號或密碼錯誤',

	'LANGUAGE_ILLEGAL' => '語系不合法',

	'TOKEN_EXPIRE' => 'Token到期',
	'TOKEN_BE_REVOKED' => 'Token被拒绝',
	'SESSION_BE_REVOKED' => 'Session被拒绝',
);