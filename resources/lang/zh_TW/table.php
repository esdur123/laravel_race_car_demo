<?php

return array(

	'next_page' => '下一頁',
	'pre_page' => '上一頁',
	'first_page' => '首頁',
	'last_page' => '末頁',
	'search' => '搜尋:',
	'show_n_result' => '顯示 _MENU_ 項結果',
	'info' => '第 _START_ - _END_ 筆，共 _TOTAL_ 筆',
	'keyword' => '關鍵字',	
	'processing' => '處理中...',
	'loadingRecords' => '載入中...',
	'zeroRecords' => '找不到相關資料',
	'infoEmpty' => '找不到相關資料',


);