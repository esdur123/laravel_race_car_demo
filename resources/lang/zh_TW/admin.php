<?php

return array(

	"admin_login" => "管理員登入",
	"manager_members" => "會員管理",
	"member_account" => "會員帳號",
	"member_branch" => "會員組織",
	"account_list" => "帳號列表",
	"member_info" => "用戶資訊",
	"account_status" => "帳號狀態",
	"save" => "保存",	
	"reset_pw" => "重設密碼",	
	"update_user_info" => "修改用戶資訊",	
	"trading_list" => "交易中",	
	"ordering_list" => "委託中",		
	"confirm_cancel_trading" => "取消交易單",	
	"confirm_cancel" => "確認取消",	
	"buyer_info" => "買家資訊",	
	"seller_info" => "賣家資訊",		
	"deal_record" => "交易紀錄",		
	"options" => "操作",		
	"nickname" => "暱稱",	
	"phone" => "電話",		
	"bank_code" => "銀行",		
	"back_account" => "帳號",			
	"buyer_confirmed" => "買方已付款",
	"seller_confirmed" => "賣方已收款",
	"member_id" => "會員ID",	
	"order_amount" => "委託量",
	"dealed" => "已交易",
	"on_trade" => "交易中",	
	"none_record" => "尚無紀錄",	
	"confirm_cancel_order" => "取消委託",	
	"controllers" => "帳號控制",
	"today_pay_max" => "允許用戶儲值",
	"switch_pay_limit" => "切換 儲值限制",
	"confirm_switch" => "確認切換",
	"create_new_user_account" => "建立新帳號",	
	"frozen_trade" => "帳號凍結",		
	"switch_frozen" => "切換 凍結交易",	
	"sys_control" => "控制台",
	"setting" => "用戶設定",
	"daily_phone_verify_max" => "每日驗證簡訊上限",
	"switch_daily_verify_message" => "更改每日驗證簡訊上限",
	"daily_verify_message_value" => "每日驗證簡訊上限",	
	"admin_setting" => "管理員資訊",
	"pw_setting" => "密碼設定",
	"frozen_trade_by_id" => "帳號ID凍結",	
	"frozen_trade_by_phone" => "相關電話凍結",	
	"frozen_trade_by_bank" => "相關銀行帳號凍結",	
	"frozen_control" => "凍結控制",	
	"type_phone" => "相關電話",
	"type_id" => "用戶ID", 
	"type_bank_account" => "相關銀行帳號",
	"frozen" => "[凍結中]",
	"thaw" => "[已解凍]", 
	"frozen_status" => "狀態",
	"frozen_created_at" => "凍結時間",
	"frozen_range" => "凍結範圍",
	"frozen_content" => "內容",
	"admin_pay_points" => "手動撥點",
	"send_point" => "撥點",
	"admin_pay_color_points" => "手動贈彩點",
	'site_name' => '北京賽車專業代操',	
	'allow_withdraw' => '允許申請提領',	
	'register_new_null_upline' => '提示: 推薦帳號為空 此帳號不會有上線',	
	'dividend_setting' => '分紅百分比設定',	
	'force_verified' => '強制通過驗證',	









);