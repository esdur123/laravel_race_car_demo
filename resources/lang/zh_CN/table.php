<?php

return array(

	'next_page' => '下一页',
	'pre_page' => '上一页',
	'first_page' => '首页',
	'last_page' => '末页',
	'search' => '搜寻:',
	'show_n_result' => '显示 _MENU_ 项结果',
	'info' => '第 _START_ - _END_ 笔，共 _TOTAL_ 笔',
	'keyword' => '关键字',	
	'processing' => '处理中...',
	'loadingRecords' => '载入中...',
	'zeroRecords' => '找不到相关资料',
	'infoEmpty' => '找不到相关资料',


);