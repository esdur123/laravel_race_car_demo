<?php

return array(

	"admin_login" => "管理员登入",
	"manager_members" => "会员管理",
	"member_account" => "会员帐号",
	"member_branch" => "会员组织",
	"account_list" => "帐号列表",
	"member_info" => "用户资讯",
	"account_status" => "帐号状态",
	"save" => "保存",	
	"reset_pw" => "重设密码",	
	"update_user_info" => "修改用户资讯",	
	"trading_list" => "交易中",	
	"ordering_list" => "委託中",		
	"confirm_cancel_trading" => "取消交易单",	
	"confirm_cancel" => "确认取消",	
	"buyer_info" => "买家资讯",	
	"seller_info" => "卖家资讯",		
	"deal_record" => "交易纪录",		
	"options" => "操作",		
	"nickname" => "暱称",	
	"phone" => "电话",		
	"bank_code" => "银行",		
	"back_account" => "帐号",			
	"buyer_confirmed" => "买方已付款",
	"seller_confirmed" => "卖方已收款",
	"member_id" => "会员ID",	
	"order_amount" => "委託量",
	"dealed" => "已交易",
	"on_trade" => "交易中",	
	"none_record" => "尚无纪录",	
	"confirm_cancel_order" => "取消委託",	
	"controllers" => "帐号控制",
	"today_pay_max" => "允许用户储值",
	"switch_pay_limit" => "切换 储值限制",
	"confirm_switch" => "确认切换",
	"create_new_user_account" => "建立新帐号",	
	"frozen_trade" => "帐号冻结",		
	"switch_frozen" => "切换 冻结交易",	
	"sys_control" => "控制台",
	"setting" => "用户设定",
	"daily_phone_verify_max" => "每日验证简讯上限",
	"switch_daily_verify_message" => "更改每日验证简讯上限",
	"daily_verify_message_value" => "每日验证简讯上限",	
	"admin_setting" => "管理员资讯",
	"pw_setting" => "密码设定",
	"frozen_trade_by_id" => "帐号ID冻结",	
	"frozen_trade_by_phone" => "相关电话冻结",	
	"frozen_trade_by_bank" => "相关银行帐号冻结",	
	"frozen_control" => "冻结控制",	
	"type_phone" => "相关电话",
	"type_id" => "用户ID", 
	"type_bank_account" => "相关银行帐号",
	"frozen" => "[冻结中]",
	"thaw" => "[已解冻]", 
	"frozen_status" => "状态",
	"frozen_created_at" => "冻结时间",
	"frozen_range" => "冻结范围",
	"frozen_content" => "内容",
	"admin_pay_points" => "手动拨点",
	"send_point" => "拨点",
	"admin_pay_color_points" => "手动赠彩点",
	'site_name' => '北京赛车专业代操',	
	'allow_withdraw' => '允许申请提领',	
	'register_new_null_upline' => '提示: 推荐帐号为空 此帐号不会有上线',	
	'dividend_setting' => '分红百分比设定',	
	'force_verified' => '强制通过验证',	










);