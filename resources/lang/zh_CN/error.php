<?php

return array(
	'UNDEFINED_ARGUMENT' => '未定义参数',
	'ILLEGAL_VALUES' => '数值不合规范',

	'USER_LOGIN_ERROR' => '帐号或密码错误',

	'LANGUAGE_ILLEGAL' => '语系不合法',

	'TOKEN_EXPIRE' => 'Token到期',
	'TOKEN_BE_REVOKED' => 'Token被拒绝',
	'SESSION_BE_REVOKED' => 'Session被拒绝',
);