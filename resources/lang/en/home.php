<?php

return array(
	'mem_login' => 'Member Sign In',
	'account' => 'Account',
	'pw' => 'Password',
	'login' => 'Sign In',
	'register' => 'Sign Up',
	'admin_login' => 'Admin Sign In',
	'disclaimer' => 'Disclaimer',	
	'disclaimer_info' => '
"The ultimate Beijing racing professional generation" is the electronic online agency website provided by this platform. All contents are only used as "entertainment games". The purpose is to study the comparative advantages of Beijing racing events, comparative disadvantages, interactive discussions, and jointly enhance game knowledge. Points registration website, membership access and all services on the website use points. This website declares that there is no business activity such as profit taking in the name of the company. In the event of a members actual monetary participation, it is a free will of both parties. The two parties have a direct and indirect relationship with this website and the company. It is a "sustainable development education promotion platform". It is hereby solemnly stated that this website is dedicated to education. The just position of academic research.',		
	'i_agree' => 'I Agree',		
	'please_agree_disclaimer' => 'Please agree to the statement first.',	
	'my_site_copyright' => 'Copyright © 2018 北京赛车',	

		
);