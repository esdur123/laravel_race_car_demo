<?php

return array(

	'next_page' => 'Next',
	'pre_page' => 'Previous',
	'first_page' => 'First',
	'last_page' => 'Last',
	'search' => 'Search:',
	'show_n_result' => 'Show _MENU_ entries',
	'info' => 'Showing _START_ to _END_ of _TOTAL_ entries',
	'keyword' => 'Keyword',	
	'processing' => 'Processing...',
	'loadingRecords' => 'Loading...',
	'zeroRecords' => 'Zero Records',
	'infoEmpty' => 'Info Empty',


);