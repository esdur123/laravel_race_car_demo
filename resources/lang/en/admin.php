<?php

return array(

	"admin_login" => "Admin Login",
	"manager_members" => "Members",
	"member_account" => "Account",
	"member_branch" => "Organization",
	"account_list" => "Account List",
	"member_info" => "Member Info",
	"account_status" => "Account Status",
	"save" => "Save",	
	"reset_pw" => "Reset Password",	
	"update_user_info" => "Update User Info",	
	"trading_list" => "Trades",	
	"ordering_list" => "Orders",	
	"confirm_cancel_trading" => "Cancel Trade",	
	"confirm_cancel" => "Confirm",	
	"buyer_info" => "Buyer",	
	"seller_info" => "Seller",		
	"deal_record" => "Records",		
	"options" => "Options",		
	"nickname" =>     "Nickname    ",	
	"phone" =>        "Phone       ",		
	"bank_code" =>    "Bank Code   ",		
	"back_account" => "Bank Account",			
	"buyer_confirmed" => "Buyer Paid",
	"seller_confirmed" => "Seller Received",
	"member_id" => "Member ID",	
	"order_amount" => "Total",
	"dealed" => "Deal",
	"on_trade" => "Trading",	
	"none_record" => "none",
	"confirm_cancel_order" => "Cancel Order",	
	"controllers" => "Controllers",
	"today_pay_max" => "Able To Pay",	
	"switch_pay_limit" => "Switch Pay Limit",
	"confirm_switch" => "Comfirm Switch",
	"create_new_user_account" => "Create New User Account",	
	"frozen_trade" => "Frozen AccountID",		
	"switch_frozen" => "Switch Frozen Trading",		
	"sys_control" => "Controller",
	"setting" => "Setting",
	"daily_phone_verify_max" => "Daily Verify Message Limit",
	"switch_daily_verify_message" => "Change Daily Verify Message Limit",
	"daily_verify_message_value" => "Daily Verify Message Limit ",		
	"admin_setting" => "Admin",
	"pw_setting" => "Admin Password Setting",
	"frozen_trade_by_id" => "Frozen Account ID",	
	"frozen_trade_by_phone" => "Frozen Phone",	
	"frozen_trade_by_bank" => "Frozen Bank Account",	
	"frozen_control" => "Frozen Control",	
	"type_phone" => "Phone",
	"type_id" => "User ID",
	"type_bank_account" => "Bank Account",	
	"frozen" => "[Frozen]",
	"thaw" => "[Thaw]", 
	"frozen_status" => "Status",
	"frozen_created_at" => "Start",
	"frozen_range" => "Range",
	"frozen_content" => "Content",
	"admin_pay_points" => "Send Points",
	"send_point" => "Send",
	"admin_pay_color_points" => "Send Color Points",
	'site_name' => '北京赛车专业代操',	
	'allow_withdraw' => 'Allow Withdraw',	
	'register_new_null_upline' => 'Confirm: This is a non-upline account.',	
	'dividend_setting' => 'Dividend Rate',
	'force_verified' => 'Force Verified',	





);