
@extends('layouts.user') 




@push('head')
<style type="text/css">
  .nori_square_01{
    width: 165px;
    height: 165px;
    background-color: #242527;
    display:block;
    margin: auto;
    border-width:3px;
    border-color:#E6B800; 
    border-style:solid;
    border-radius:10px;
    text-align: center;
    color: white;
  }

  .nori_square_01 span{   
    font-size: 12px;
  }

    #nori_main_content{
      position: relative;
      top: -20px;
    }
    
</style>      
@endpush




@push('script')
<script type="text/javascript">
    
    $(function(){
       //$("#add_white_area").css('display','block');
      
    }) 

  
</script>


@endpush





@section('content')
<!-- 大圖上方細線 --> 
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#13b5b1; height: 3px;"></div>
</div> 
<!-- 大圖 --> 
<div class="row" style="z-index:0;">
<!--  <img src="{{ asset('img/banne.jpg') }}" style="max-width: 100%;"> -->

          <!-- Banner -->
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
<!--             <li data-target="#myCarousel" data-slide-to="2"></li> -->
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="{{ asset('img/lottery_banner_01.png') }}" alt="">
            </div>

            <div class="item">
              <img src="{{ asset('img/lottery_banner_02.png') }}" alt="">
            </div>

<!--             <div class="item">
              <img src="{{ asset('img/lottery_banner_03.png') }}" alt="">
            </div> -->
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="banner_arrow" ><i class="fas fa-chevron-left"></i></span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="banner_arrow"><i class="fas fa-chevron-right"></i></span>
          </a>
        </div>
        
</div>

<!-- 跑馬燈 --> 
<div class="row"> 
    <!-- 大圖下方細線 --> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#13b5b1; height: 3px;"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#ffd653; height: 40px;">
   <!--    <marquee><span style="font-size: 20px;font-weight: normal;line-height: 35px;">2018.10.4 開獎內容</span></marquee> -->
    </div>
</div>

<!-- 下半部 -->
  <div class="row" style="height: 600px;background-size:cover;background-repeat:no-repeat;background-position: center center;background-image: url('{{ asset('img/color_white.png') }}'">
  
    <br /><br />

    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="height: 20px;">
      
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
      <div class="nori_square_01">
        <br />
        <img src="{{ asset('img/icon_computer.png') }}" style="display:block;margin: auto;">      
        <span style="color: white;">{{ Lang::get('user.down_title_0')}}</span>
        <br />
        <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_0')}}
        </span>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
      <div class="nori_square_01">
        <br />
        <img src="{{ asset('img/icon_lottery.png') }}" style="display:block;margin: auto;">      
        <span style="color: white;">{{ Lang::get('user.down_title_1')}}</span>
        <br />
        <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_1')}}
        </span>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
      <div class="nori_square_01">
        <br />
        <img src="{{ asset('img/icon_praise.png') }}" style="display:block;margin: auto;">      
        <span style="color: white;">{{ Lang::get('user.down_title_2')}}</span>
        <br />
        <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_2')}}
        </span>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
      <div class="nori_square_01">
        <br />
        <img src="{{ asset('img/icon_coin.png') }}" style="display:block;margin: auto;">      
        <span style="color: white;">{{ Lang::get('user.down_title_3')}}</span>
        <br />
        <span style="color: #f27a3d; word-break: break-all;">
          {{ Lang::get('user.down_square_3')}}
        </span>
      </div>
    </div>
    
<!--      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="height: 200px;">
     
    </div>   -->
  <!-- <img src="{{ asset('img/background.jpg') }}" style="max-width: 100%;"> -->
</div>


    

@endsection