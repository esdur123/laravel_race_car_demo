
@extends('layouts.user') 




@push('head')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">

</style>
	    
@endpush



@push('script')
<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/u_my_utility.js') }}"></script>
<script>

	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

	var m_lang_order_down = "{{ Lang::get('user.order_down')}}";
	var m_lang_cancel_order = "{{ Lang::get('user.cancel_order')}}";
	var m_lang_paid_already = "{{ Lang::get('user.paid_already')}}";
	var m_lang_contact = "{{ Lang::get('user.contact')}}";
	var m_lang_cancel = "{{ Lang::get('user.cancel')}}";
	var m_lang_payment_received = "{{ Lang::get('user.payment_received')}}";
	var m_lang_payment_paid = "{{ Lang::get('user.payment_paid')}}";
	var m_lang_seller_name = "{{ Lang::get('user.seller_name')}}";
	var m_lang_buyer_name = "{{ Lang::get('user.buyer_name')}}";		
	var m_lang_paid_complete = "{{ Lang::get('user.paid_complete')}}";
	var m_lang_receive_complete = "{{ Lang::get('user.receive_complete')}}";
	var m_lang_deal_done = "{{ Lang::get('user.deal_done')}}";
	var m_lang_buyer_cancel = "{{ Lang::get('user.buyer_cancel')}}";
	var m_lang_seller_cancel = "{{ Lang::get('user.seller_cancel')}}";
	var m_lang_buyer_paid_already = "{{ Lang::get('user.buyer_paid_already')}}";
	var m_lang_cancel_oredr = "{{ Lang::get('user.cancel_oredr')}}";



	// ＊＊ ready-jobs ＊＊
	$(function(){

	  getSet();
	  setTimeout(function(){
	      GetOrders();
	      getBuy();
	      getSell();
	  },500);

	})


	// ＊＊ 取得配套和點數 ＊＊
	var m_user_set = 0;
	function getSet(){

	  p_ApiMgr.getSet(

	        function (result) {
	            //console.log(result);
	            m_user_set = result.toString();
	            //alert(m_user_set);
	                        
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	var m_received_id = 0;
	var m_received_point = 0;

	// ＊＊ 已付款視窗＊＊
	function clickReceivedModal(obj){

	  m_received_id = $(obj).attr("attr_received_id");
	  m_received_point = $(obj).attr("attr_received_point");

	  $("#span_received_point").text(m_received_point);

	  $('#receivedModal').modal('toggle');

	}

	// ＊＊ 確認 收到錢 ＊＊
	function confirmReceived(){

	  $('#receivedModal').modal('hide');

	  //alert("received!! trading_id="+m_received_id);
	  p_ApiMgr.confirmTrading(
	        m_received_id,
	        'sell',
	        function (result) {
	            //console.log(result);
	            //alert('已提交');
	            //alert(result);
	            location.reload();
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	var m_paid_id = 0;
	var m_paid_point = 0;


	// ＊＊ 已付款視窗＊＊
	function clickPaidModal(obj){

	  m_paid_id = $(obj).attr("attr_paid_id");
	  m_paid_point = $(obj).attr("attr_paid_point");  

	  $("#span_paid_point").text(m_paid_point);

	  $('#paidModal').modal('toggle');

	}


	// ＊＊ 確認 已付款 ＊＊
	function confirmPaid(){

	  $('#paidModal').modal('hide');
	  //alert("id="+m_paid_id);
	  p_ApiMgr.confirmTrading(
	        m_paid_id,
	        'buy',
	        function (result) {
	            //console.log(result);
	            //alert('已提交');
	            //alert(result);
	            location.reload();
	        },this,
	        function (error) {},this
	  );

	  return false;
	}



	// ＊＊ 賣出視窗＊＊
	function openSellModal(){

	  if($("#input_sell").val() == ''){
	    return false;
	  }

	  $("#span_sell_points").text($("#input_sell").val());

	  $('#sellModal').modal('toggle');

	}


	// ＊＊ 賣出確認＊＊
	function confirmSell(){ 

	  $('#sellModal').modal('hide');

	  p_ApiMgr.createOrder(
	        $("#input_sell").val(),
	        function (result) {          
	            //alert('委託成功');
	            $("#input_sell").val('');
	            location.reload();
	        },this,
	        function (error) {
	          $("#input_sell").val('');
	        },this
	  );

	  
	  return false;
	}


	// ＊＊ 買進視窗＊＊
	var m_trading_id = 0;
	var m_cancel_type = 'none';
	function openCancelModal(obj){

	  m_trading_id = $(obj).attr("attr_trading_id");
	  m_cancel_type = $(obj).attr("attr_type");

	  var point = $(obj).attr("attr_point");  
	  

	  $("#span_cancel_point").text(point);

	  $('#cancelModal').modal('toggle');

	}


	// ＊＊ 取得 帳號資訊 ＊＊
	function cancelTrading(){


	  $('#cancelModal').modal('hide');
	  p_ApiMgr.cancelTrading(
	        m_trading_id,
	        m_cancel_type,
	        function (result) {
	            //alert('取消成功');
	            location.reload();
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 打開聯繫視窗 ＊＊
	function openContactModal(obj){

	  var attr_date = $(obj).attr("attr_date");
	  var attr_point = $(obj).attr("attr_point");
	  var attr_name = $(obj).attr("attr_name");  
	  var attr_type = $(obj).attr("attr_type");  
	  var attr_phone = $(obj).attr("attr_phone");    


	  if(attr_type=='sell'){
	    $("#span_name").text(m_lang_buyer_name); 
	  } else {
	    $("#span_name").text(m_lang_seller_name); 
	  }

	  //alert(attr_id);

	  $("#span_contact_datatime").text(attr_date);
	  $("#span_contact_point").text(attr_point);
	  $("#span_contact_name").text(attr_name);  
	  $("#span_contact_phone").text(attr_phone);    

	  $('#contactModal').modal('toggle');

	}


	// ＊＊ 取得賣出列表 ＊＊
	var m_sell_datatable = null;
	function getSell(){

	    var m_id = '';
	    var m_nickname = '';
	    var m_country_code = '';
	    var m_phone = '';
	    var m_bank_code = '';
	    var m_bank_branch = '';
	    var m_bank_account = '';
	    var m_datatime = '';
	    var m_point = '';
	    var m_trading_id = '';

	    var ajax_url = '/api/v1/txn/sell';  // TransactionController@getOrder
	    m_sell_datatable = $('#table_sell').DataTable({
	             "language": {
	                  "info":m_lang_info,
	                  "sLengthMenu": m_lang_show_n_result,
	                  "search": m_lang_keyword,  
	                  "processing": m_lang_processing,
	                  "loadingRecords": m_lang_loadingRecords,
	                  "zeroRecords": m_lang_show_zeroRecords,
	                  "infoEmpty": m_lang_infoEmpty,
	                  "infoPostFix": "",

	                 "oPaginate": {
	                   "sFirst": m_lang_first_page,
	                   "sPrevious": m_lang_pre_page,
	                   "sNext": m_lang_next_page,
	                   "sLast": m_lang_last_page,
	                 },
	             },
	            "processing": true,
	            "lengthChange": false,
	            "pagingType": "simple_numbers",
	            "searching": false,     
	            "destroy": true, 
	            "columns": [          
	                {
	                    'data': 'created_at',
	                    'render': function (data, type, row, meta) {
	                        m_datatime = data.date;
	                        m_datatime = m_datatime.toString();
	                        m_datatime = m_datatime.substring(0, m_datatime.length-7);
	                        return m_datatime;
	                    }
	                },
	                {
	                    'data': 'buyer_info',
	                    'render': function (data, type, row, meta) {
	                        var array_info = data.split(",");
	                        if(array_info.length == 7){
	                            m_id = array_info[0];
	                            m_nickname = array_info[1];
	                            m_country_code = array_info[2];
	                            m_phone = array_info[3];
	                            m_bank_code = array_info[4];
	                            m_bank_branch = array_info[5];
	                            m_bank_account = array_info[6];
	                        }

	                        var return_data = m_bank_code+'<br />'+m_bank_account;

	                        return return_data;
	                    }
	                },
	                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                        return m_nickname;
	                    }
	                },
	                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                        return m_phone;
	                    }
	                },                                
	                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                        m_point = data;
	                        return data;
	                    }
	                },
	                {      
	                    'data': 'confirm_value',
	                    'render': function (data, type, row, meta, aaa) {
	                        m_trading_id = row['id'];
	                       // var confirm_value = data;

	                        var _confirm_value = '';
	                        var _confirm_date = '';
	                        var confirm_array = data.split(",");

	                        if(confirm_array.length == 1) {
	                          _confirm_value = confirm_array[0];
	                          _confirm_date = confirm_array[0];
	                        } else if(confirm_array.length == 2) {
	                          _confirm_value = confirm_array[0];
	                          _confirm_date =  confirm_array[1];
	                        } else {
	                          return '-';
	                        }

	                        data  = '<div class="btn-group-vertical">';

	                        if(_confirm_value == 'none' ){
	                          data += '<button attr_received_id="'+m_trading_id+'" attr_received_point="'+m_point+'" type="button" class="btn btn-warning" onclick="clickReceivedModal(this)">'+m_lang_payment_received+'</button>';
	                        } else if (_confirm_value == 'seller-confirm'){
	                          data += '<button type="button" class="btn">'+m_lang_receive_complete+'<br />'+_confirm_date+'</button>';
	                          data += '</div>';   
	                          return data;                       
	                        } else if (_confirm_value == 'buyer-confirm'){
	                          data += '<button attr_received_id="'+m_trading_id+'" attr_received_point="'+m_point+'" type="button" class="btn btn-warning" onclick="clickReceivedModal(this)">'+m_lang_payment_received+'<br />'+m_lang_buyer_paid_already+_confirm_date+'</button>';                     
	                        } 

	                        else if (_confirm_value == 'seller-cancel'){

	                          data += m_lang_seller_cancel+'<br />'+_confirm_date;
	                          return data;     

	                        } else if (_confirm_value == 'buyer-cancel'){

	                          data += m_lang_buyer_cancel+'<br />'+_confirm_date;
	                          return data;     

	                        } else {

	                          data += m_lang_deal_done+'<br />'+_confirm_date;
	                          data += '</div>';
	                          return data;
	                        }

	                        // data += '<button type="button" class="btn btn-default" ';
	                        // data += ' attr_phone="'+m_phone+'" attr_type="sell" attr_name="'+m_nickname+'" attr_point="'+m_point+'" attr_date="'+m_datatime+'" onclick="openContactModal(this)">'+m_lang_contact+'</button>';
	                        data += '<button  attr_type="sell" attr_point="'+m_point+'" attr_trading_id="'+m_trading_id+'" type="button" class="btn btn-default" onclick="openCancelModal(this)">'+m_lang_cancel+'</button>';
	                        data += '</div>';
	                        
	                        return data;
	                    }
	                }
	            ],
	            "serverSide": true,
	            "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
	    })

	  return false;
	}



	// ＊＊ 取得買進列表 ＊＊
	var m_buy_datatable = null;
	function getBuy(){

	    var m_id = '';
	    var m_nickname = '';
	    var m_country_code = '';
	    var m_phone = '';
	    var m_bank_code = '';
	    var m_bank_branch = '';
	    var m_bank_account = '';
	    var m_datatime = '';
	    var m_point = '';
	    var m_trading_id = '';

	    var ajax_url = '/api/v1/txn/buy';  // TransactionController@getOrder
	    m_buy_datatable = $('#table_buy').DataTable({
	             "language": {
	                  "info":m_lang_info,
	                  "sLengthMenu": m_lang_show_n_result,
	                  "search": m_lang_keyword,  
	                  "processing": m_lang_processing,
	                  "loadingRecords": m_lang_loadingRecords,
	                  "zeroRecords": m_lang_show_zeroRecords,
	                  "infoEmpty": m_lang_infoEmpty,
	                  "infoPostFix": "",
	                 "oPaginate": {
	                   "sFirst": m_lang_first_page,
	                   "sPrevious": m_lang_pre_page,
	                   "sNext": m_lang_next_page,
	                   "sLast": m_lang_last_page,
	                 },
	             },
	            "processing": true,
	            "lengthChange": false,
	            "pagingType": "simple_numbers",
	            "searching": false,   
	            "destroy": true,                      
	            "columns": [
	                {
	                    'data': 'created_at',
	                    'render': function (data, type, row, meta) {
	                        m_datatime = data.date;
	                        m_datatime = m_datatime.toString();
	                        m_datatime = m_datatime.substring(0, m_datatime.length-7);
	                        return m_datatime;
	                    }
	                },
	                {
	                    'data': 'seller_info',
	                    'render': function (data, type, row, meta) {
	                        var array_info = data.split(",");
	                        if(array_info.length == 7){
	                            m_id = array_info[0];
	                            m_nickname = array_info[1];
	                            m_country_code = array_info[2];
	                            m_phone = array_info[3];
	                            m_bank_code = array_info[4];
	                            m_bank_branch = array_info[5];
	                            m_bank_account = array_info[6];

	                        }

	                        var return_data = m_bank_code+'<br />'+m_bank_account;

	                        return return_data;
	                    }
	                },
	                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                                  
	                        return m_nickname;
	                    }
	                },
	                                                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                                     
	                        return m_phone;
	                    }
	                },
	                {
	                    'data': 'amount',
	                    'render': function (data, type, row, meta) {
	                        m_point = data;                      
	                        return data;
	                    }
	                },
	                {
	                    'data': 'confirm_value',
	                    'render': function (data, type, row, meta, aaa) {
	                        m_trading_id = row['id'];
	                        //var confirm_value = data;

	                        var _confirm_value = '';
	                        var _confirm_date = '';
	                        var confirm_array = data.split(",");

	                        if(confirm_array.length == 1) {
	                          _confirm_value = confirm_array[0];
	                          _confirm_date = confirm_array[0];
	                        } else if(confirm_array.length == 2) {
	                          _confirm_value = confirm_array[0];
	                          _confirm_date =  confirm_array[1];
	                        } else {
	                          return '-';
	                        }

	                        data  = '<div class="btn-group-vertical">';                        
	                        if(_confirm_value == 'none' || _confirm_value == 'seller-confirm'){
	                          data += '<button attr_paid_id="'+m_trading_id+'" attr_type="buy" attr_paid_point="'+m_point+'" type="button" class="btn btn-info" onclick="clickPaidModal(this)">'+m_lang_payment_paid+'</button>';
	                        } else if (_confirm_value == 'buyer-confirm'){
	                          data += '<button type="button" class="btn">'+m_lang_paid_complete+'<br />'+_confirm_date+'</button>';
	                          data += '</div>';   
	                          return data;   // return                    
	                        } else if (_confirm_value == 'seller-cancel'){

	                          data += m_lang_seller_cancel+'<br />'+_confirm_date;
	                          return data;   // return               

	                        } else if (_confirm_value == 'buyer-cancel'){

	                          data += m_lang_buyer_cancel+'<br />'+_confirm_date;
	                          return data;   // return              

	                        } else {
	                          data += m_lang_deal_done+'<br />'+_confirm_date;
	                          data += '</div>';
	                          return data;   // return             
	                        }

	                        if(m_user_set != '0'){ 
	                          data += '<button  attr_type="buy" attr_point="'+m_point+'" attr_trading_id="'+m_trading_id+'" type="button" class="btn btn-default" onclick="openCancelModal(this)">'+m_lang_cancel+'</button>';
	                        } else {  // 未激活不給取消xd

	                        }
	                        
	                        data += '</div>';
	                        
	                        return data;
	                    }
	                }
	            ],
	            "serverSide": true,
	            "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
	    })

	  return false;
	}



	// ＊＊ 買進視窗＊＊
	function openBuyModal(){


	  if($("#input_buy").val() == ''){
	    return false;
	  }

	  $("#span_buy_points").text($("#input_buy").val());

	  $('#buyModal').modal('toggle');

	}


	// ＊＊ 買進確認＊＊
	function confirmBuy(){ 

	  $('#buyModal').modal('hide');

	  p_ApiMgr.buyPointsFromOrder(
	        $("#input_buy").val(),
	        function (result) {          
	            //console.log(result);
	            
	            //alert('已提交');
	            location.reload();

	        },this,
	        function (error) {
	        },this
	  );

	  $("#input_buy").val('');
	  return false;
	}


	// ＊＊ 取得委託列表 ＊＊
	var m_order_datatable = null;
	function GetOrders(){

	    var m_total = 0;
	    var m_deal = 0;
	    var ajax_url = '/api/v1/txn/order';  // TransactionController@getOrder
	    m_order_datatable = $('#table_orders').DataTable({
	             "language": {
	                  "info":m_lang_info,
	                  "sLengthMenu": m_lang_show_n_result,
	                  "search": m_lang_keyword,  
	                  "processing": m_lang_processing,
	                  "loadingRecords": m_lang_loadingRecords,
	                  "zeroRecords": m_lang_show_zeroRecords,
	                  "infoEmpty": m_lang_infoEmpty,
	                  "infoPostFix": "",
	                 "oPaginate": {
	                   "sFirst": m_lang_first_page,
	                   "sPrevious": m_lang_pre_page,
	                   "sNext": m_lang_next_page,
	                   "sLast": m_lang_last_page,
	                 },
	             },
	            "processing": true,
	            "lengthChange": false,
	            "pagingType": "simple_numbers",
	            "searching": false,     
	            "destroy": true,                    
	            "columns": [
	                {
	                    'data': 'created_at',
	                    'render': function (data, type, row, meta) {

	                        var datatime = data.date;
	                        datatime = datatime.toString();
	                        datatime = datatime.substring(0, datatime.length-7);
	                        return datatime;
	                    }
	                },
	                {
	                    'data': 'total',
	                    'render': function (data, type, row, meta) {
	                        m_total = data;
	                        return data;
	                    }
	                },
	                {
	                    'data': 'deal',
	                    'render': function (data, type, row, meta) {
	                        m_deal = data;
	                        return data;
	                    }
	                },
	                {
	                    'data': 'trading',
	                    'render': function (data, type, row, meta) {
	                  
	                        return data;
	                    }
	                },	                
	                {
	                    'data': 'deleted_at',
	                    'render': function (data, type, row, meta) {
	    				
	    				var datatime = '';
	    				var is_null = 0;
						try {
						   datatime = data.date;
						}
						catch(err) {
						    is_null = 1;
						}

						if(is_null==0){
	                        datatime = datatime.toString();
	                        datatime = datatime.substring(0, datatime.length-7);
	                        return m_lang_cancel_oredr+ datatime;
						} else {
	                        if(m_total == m_deal ) {
	                            data = m_lang_order_down;
	                        } else if(m_total > m_deal ) {
	                            data = '<button type="button" attr_deal="'+ m_deal +'" attr_id="'+ row['id'] +'" attr_total="'+ m_total +'" class="btn btn-danger" onclick="clickCancelOrder(this)">'+m_lang_cancel_order+'</button>';
	                        } else {
	                          data = '-';
	                        }

	                        return data;
						}	    					    				    						             

	                    }
	                }
	            ],
	            "serverSide": true,
	            "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
	    })



	  return false;
	}


	// ＊＊ 撤回委託 (跳視窗) ＊＊
	var m_cancel_order_id = 0;

	function clickCancelOrder(obj){

	  var total = $(obj).attr("attr_total");  
	  var deal = $(obj).attr("attr_deal");  
	  m_cancel_order_id = $(obj).attr("attr_id");
	  $("#cancel_order_total").text(total);
	  $("#cancel_order_deal").text(deal);
	  //alert('order_id: '+m_cancel_order_id);
	  $('#confirmCancelOrderModal').modal('toggle');
	  return false;
	}


	// ＊＊ 撤回委託 (跳視窗) ＊＊
	function cancelOrder(){

	  
	  $('#confirmCancelOrderModal').modal('hide');

	   //alert('bbb'+m_cancel_order_id);
	    p_ApiMgr.cancelOrder(
	        m_cancel_order_id,
	        function (result) {          
	            //console.log(JSON.parse(result));
	            //alert('撤銷成功');            
	            //m_cancel_order_id = 0;
	            location.reload();

	        },this,
	        function (error) {
	         // m_cancel_order_id = 0;
	        },this
	  );

	  return false;
	}




</script>
@endpush




@section('content')
	
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.system_trade')}}</h2>
		</div>
		<div class="web_map">			
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.org_mang')}}
			&nbsp;>&nbsp;{{ Lang::get('user.system_trade')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">      		
		<div style="background-color: #13b5b1;color: white;">
			<br />
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;{{ Lang::get('user.buy_point')}}</h3>
			<div class="form-group">
			  <input type="text" class="form-control" id="input_buy" placeholder="{{ Lang::get('user.buy_point_amount')}}" style="width: 90%;display: block;margin: auto;" maxlength="9">
			</div>
			<button type="button" class="btn btn-success" style="width: 90%;display: block;margin:auto;background-color:#f27a3d!important;" onclick="openBuyModal()">
				{{ Lang::get('user.trade_buy')}}
			</button>
			<br />			
		</div>

		<div>
			&nbsp;
		</div>		
	</div>


	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">      		
		<div style="background-color: #13b5b1;color: white;">
			<br />
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;{{ Lang::get('user.sell_point')}}</h3>
			<div class="form-group">
			  <input type="text" class="form-control" id="input_sell" placeholder="{{ Lang::get('user.sell_point_amount')}}" style="width: 90%;display: block;margin: auto;" maxlength="9">
			</div>
			<button type="button" class="btn btn-success" style="width: 90%;display: block;margin: auto;background-color:#f27a3d!important;" onclick="openSellModal()">
				{{ Lang::get('user.trade_sell')}}
			</button>
			<br />			
		</div>

		<div>
			&nbsp;
		</div>	
	</div>

</div>



	<!-- Buy DataTable -->
	<div class="container">
		<h2>{{ Lang::get('user.my_buy')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
			        <table id="table_buy" class="display" style="width:100%">
			            <thead>
			                <tr>
			  
			                    <th>{{ Lang::get('user.datetime')}}</th>	                	
			                    <th>{{ Lang::get('user.seller_bankinfo')}}</th>
								<th>{{ Lang::get('user.seller_nickname')}}</th> 
								<th>{{ Lang::get('user.seller_phone')}}</th> 			                    
			                    <th>{{ Lang::get('user.point')}}</th> 
			                    <th>{{ Lang::get('user.table_options')}}</th>  
			                    <!-- 已配對完 撤回 -->
			                </tr>
			            </thead>
			            <tbody>
			                <tr>			             
			                	<td></td>
			                    <td></td>
			                	<td></td>
			                    <td></td>			                    
			                    <td></td>
			                    <td></td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			  </div>

			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
					<br /><br /><br /><br /><br />
				</div>
			  </div>
		</div>
	</div>


	<!-- Sell DataTable -->
	<div class="container">
		<h2>{{ Lang::get('user.my_sell')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		        <table id="table_sell" class="display" style="width:100%">
		            <thead>
		                <tr>		               
		                    <th>{{ Lang::get('user.datetime')}}</th>	                	
		                    <th>{{ Lang::get('user.buyer_bankinfo')}}</th>
							<th>{{ Lang::get('user.buyer_nickname')}}</th> 
							<th>{{ Lang::get('user.buyer_phone')}}</th> 		                    
		                    <th>{{ Lang::get('user.point')}}</th> 
		                    <th>{{ Lang::get('user.table_options')}}</th>  
		                    <!-- 已配對完 撤回 -->
		                </tr>
		            </thead>
		            <tbody>
		                <tr>		             
		                	<td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>		                    
		                </tr>
		            </tbody>
		        </table>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br /><br /><br /><br />
			</div>
		  </div>
		</div>
	</div>


	<!-- Order DataTable -->
	<div class="container">
		<h2>{{ Lang::get('user.my_orders')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		        <table id="table_orders" class="display" style="width:100%">
		            <thead>
		                <tr>
		                    <th>{{ Lang::get('user.datetime')}}</th>	                	
		                    <th>{{ Lang::get('user.point')}}</th>
		                    <th>{{ Lang::get('user.deal_down')}}</th> 
		                    <th>{{ Lang::get('user.trading')}}</th> 
		                    <th>{{ Lang::get('user.table_options')}}</th>  
		                    <!-- 已配對完 撤回 -->
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                	<td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br /><br /><br /><br />
			</div>
		  </div>
		</div>
	</div>

<!--  end overflow -->


<!-- confirm cancel-Order Modal -->
<div id="cancelModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.cancel_deal')}}</h4>
      </div>

      <div class="modal-body">
        <span>{{ Lang::get('user.point')}}&nbsp;</span> <span id="span_cancel_point" style="color: blue;"></span>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="cancelTrading()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- confirm cancel-Order Modal -->
<div id="confirmCancelOrderModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.cancel_this_order')}}</h4>
      </div>

      <div class="modal-body">
        <p><span>{{ Lang::get('user.point')}}</span>&nbsp;<span id="cancel_order_total" style="color: blue;"></span></p>
        <p><span>{{ Lang::get('user.deal_down')}}</span>&nbsp;<span id="cancel_order_deal" style="color: blue;"></span></p>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="cancelOrder()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

	
<!-- Sell Modal -->
<div id="sellModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.sell_point')}}</h4>
      </div>

      <div class="modal-body">
        <span>{{ Lang::get('user.confirm_sell')}}&nbsp;</span><span id ="span_sell_points" style="color: blue;"></span>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="confirmSell()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Buy Modal -->
<div id="buyModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.buy_point')}}</h4>
      </div>

      <div class="modal-body">
        <p><span>{{ Lang::get('user.confirm_buy')}}&nbsp;</span><span id ="span_buy_points" style="color: blue;"></span></p>
      
        <p style="color: red;">{{ Lang::get('user.confirm_buy_warning')}}</p>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="confirmBuy()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Contact Modal -->
<div id="contactModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.contact')}}</h4>
      </div>

      <div class="modal-body">
        <p><span id="span_name"></span>&nbsp;<span id ="span_contact_name" style="color: blue;"></span></p>
        <p><span>{{ Lang::get('user.phone')}}&nbsp;</span><span id ="span_contact_phone" style="color: blue;"></span></p>
        <p><span>{{ Lang::get('user.point')}}&nbsp;</span><span id ="span_contact_point" style="color: blue;"></span></p>        
        <p><span>{{ Lang::get('user.datetime')}}&nbsp;</span><span id ="span_contact_datatime" style="color: blue;"></span></p>        
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Paid Modal -->
<div id="paidModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.payment_paid')}}</h4>
      </div>

      <div class="modal-body">
        <p>
        	<span>{{ Lang::get('user.point')}}&nbsp;</span>
        	<span id ="span_paid_point" style="color: red;"></span>
        </p>              
      </div>

      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="confirmPaid()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Paid Modal -->
<div id="receivedModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.payment_received')}}</h4>
      </div>

      <div class="modal-body">
        <p>
        	<span>{{ Lang::get('user.point')}}&nbsp;</span>
        	<span id ="span_received_point" style="color: red;"></span>
        </p>              
      </div>

      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="confirmReceived()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection