
@extends('layouts.front') 




@push('head')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<style type="text/css">
	/*Register*/

#playground-container {
    height: 500px;
    overflow: hidden !important;
    -webkit-overflow-scrolling: touch;
}
body, html{
     height: 100%;
      background-size: cover;
}

.main{
  margin:50px 15px;
}


hr{
  width: 10%;
  color: #fff;
}

.form-group{
  margin-bottom: 15px;
}

label{
  margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {

    padding-top: 3px;
}

.main-login{
  background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}
.form-control {
    height: auto!important;
padding: 8px 12px !important;
}
.input-group {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
}

.main-center{
  margin-top: 30px;
  margin: 0 auto;
  max-width: 400px;
    padding: 10px 40px;
  background:#009edf;
      color: #FFF;
    text-shadow: none;
  -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
-moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

}
span.input-group-addon i {
    color: #706760;

}

.login-button{
  margin-top: 5px;
}

.login-register{

  text-align: center;
}



</style>
@endpush




@push('script')

<!-- 	<script src="{{ asset('js/u_my_utility.js') }}"></script>
    <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/user/wallet/b_user_wallet.js') }}"></script> -->
<script type="text/javascript">
	

      // ＊＊ Ready ＊＊
      $(document).ready(function() {
//      	alert(m_recommender);
        $( "#btn_register" ).click(function() {
          registerFromRecommender();
        });

        $("#signup_recommender").val(m_recommender);
        // setTimeout(function(){
        //    getAccountFromID();        
        // },500);

      });


      // 取得id
      var m_recommender = '{{ $recommender_account }}';
      

      // 取得account
      var is_get_account = false;

      // ＊＊ 取得Account ＊＊
      // function getAccountFromID(){
  
      //     p_ApiMgr.getAccountFromID(
      //       m_recommender,
      //       function (result) {
      //         is_get_account = true;
      //         //console.log(result['account']); 
      //         $("#signup_recommender").val(result['account']);
      //       },this,
      //       function (error) {},this
      //     );

      //   return false;
      // }


      // ＊＊ 推薦註冊 ＊＊
      var is_waiting_register = 0;
      function registerFromRecommender(){
          if($("#signup_recommender").val() == '') return false;
          if($("#signup_account").val() == '') return false;
          if($("#signup_nickname").val() == '') return false;
          if($("#signup_phone").val() == '') return false;
          if($("#signup_pw").val() == '') return false;
          if($("#signup_pw2").val() == '') return false;


          // 防止連按
          if(is_waiting_register==1) return false;
          is_waiting_register = 1;

          p_ApiMgr.registerFromRecommender(
            m_recommender,
            $("#signup_account").val(),
            $("#signup_nickname").val(),
            $("#select_country_code").val(),
            $("#signup_phone").val(),
            $("#signup_pw").val(),
            $("#signup_pw2").val(),
            function (result) {
            
              alert('註冊成功');
              p_ApiMgr.login(
                    $("#signup_account").val(),
                    $("#signup_pw").val(),
                    function (result) {
                        var obj = JSON.parse(result);
                        var id = obj.id;
                      location.href = "/user/news";
                    
                    },this,
                    function (error) {
                      is_waiting_register = 0;
                    },this
              );

            },this,
            function (error) {},this
          );

        return false;
      }


</script>    

@endpush



@section('content')
	        <!-- 註冊 --> 
        <div class="container" style="position: relative;top:-40px;">
          <div class="row main">

            <h1>{{ Lang::get('user.register')}}</h1>
              <form class="" method="post" action="#">
                
                <div class="form-group">
                  <label for="name" class="cols-sm-2 control-label">{{ Lang::get('user.recommender')}}</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-book-open"></i></span>
                      <input type="text" class="form-control" name="name" id="signup_recommender" placeholder="{{ Lang::get('user.recommender')}}" disabled>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="email" class="cols-sm-2 control-label">{{ Lang::get('user.account')}}</label>
                  <div class="cols-sm-10">
                    <div class="input-group"> 
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>  
                      <input type="text" class="form-control" maxlength="16" id="signup_account"  placeholder="{{ Lang::get('user.account')}}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="username" class="cols-sm-2 control-label">{{ Lang::get('user.nickname')}}</label>
                  <div class="cols-sm-10">   
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                      <input type="text" class="form-control"  maxlength="16" id="signup_nickname"  placeholder="{{ Lang::get('user.nickname')}}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="password" class="cols-sm-2 control-label">{{ Lang::get('user.country_code')}}</label>
                  <div class="cols-sm-10">   
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-globe-asia"></i></span>
                      <select class="form-control" id="select_country_code">
                        @include('components.country_code')
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="confirm" class="cols-sm-2 control-label">{{ Lang::get('user.phone')}}</label>
                  <div class="cols-sm-10">  
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-phone"></i></span>
                      <input type="text" class="form-control" maxlength="10"  id="signup_phone"  placeholder="09xxxxxxxx"/>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="confirm" class="cols-sm-2 control-label">{{ Lang::get('user.password')}}</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                      <input type="password" class="form-control"  maxlength="16" id="signup_pw"  placeholder="{{ Lang::get('user.password')}}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="confirm" class="cols-sm-2 control-label">{{ Lang::get('user.confirm_password')}}</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                      <input type="password" class="form-control"  maxlength="16" id="signup_pw2"  placeholder="{{ Lang::get('user.confirm_password')}}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group " align="center">
                  <br />
                  <button id="btn_register" type="button" class="btn btn-success" style="width: 100%;">{{ Lang::get('user.register')}}</button>
                </div>
                
              </form>

          </div>
        </div>



@endsection