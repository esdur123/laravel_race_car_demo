<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    <title>極致</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/u_bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user/b_user.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/u_language.css') }}">
    <link rel="stylesheet" href="{{ asset('css/u_scrollbar.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">    
    <style>


      

      .nori_float_nav_01 {
        width: 10%; 
        float: left; 
        height: 40px;
        color: #706760;
      }

      .nori_float_nav_02 {
        width: 10%; 
        float: left;
        height: 120px;
        color: #706760;
      }

      .nori_pointer_1 {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_up {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_down_01 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_0.jpg') }}');
      }

      .nori_draw_down_02 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_1.jpg') }}');
      }

      #tag_point{
        position: relative;
        cursor: pointer;
        left: 10px; 
        top: 10px;
        color: white;
        background:url('{{ asset('img/button_3.png') }}') no-repeat;
        width: 125px;
        height: 40px
      }

      #tag_point_span{
        position: relative;
        top: 5px;
        left: 8px;
      }

      #tag_login{

        cursor: pointer;
        position:relative;
        left: 10px; 
        top: 10px;

        background:url('{{ asset('img/button_6.png') }}') no-repeat;
        width: 125px;
        height: 40px;
      }

      #tag_logout{
        cursor: pointer;
        float: left;
        position:relative;
        left: 10px;
        top: 10px;

        background:url('{{ asset('img/button_6.png') }}') no-repeat;
        width: 125px;
        height: 40px;
      }

      #tag_logout_span{
        position:relative;
        top: -15px;
        left: 42px;

      }

      #nav_show_account{
        color: #242527;
        position:relative;
        top: 4px;
        left: 12px;
      }

    .nori_square_01{
      width: 165px;
      height: 165px;
      background-color: #242527;
      display:block;
      margin: auto;
      border-width: 3px;
      border-color: #E6B800; 
      border-style:solid;
      border-radius: 10px;
      text-align: center;
      color: white;

    }

    .nori_square_01 span{   
      font-size: 12px;
    }      

    #nori_main_content{
      position: relative;
      top: -20px;
    }

    .navbar-nav > li > a{
        color: white !important;
    }

/*    .navbar-nav > li:hover{
        background-color: white !important;
    }*/

    .white_hover_bg:hover{
        background-color: white !important;
    }    

    .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus{
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:#f27a3d !important;
    } 

    .dropdown-toggle{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/

    }

    .dropdown-menu{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/
        background-color: #313131 !important;
    }

    .dropdown-menu > li > a{
        color: #706760 !important;
    }

    .dropdown-menu > li > a:hover {
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:white !important;
      background-color: #313131 !important;
    }

    #black_header{
      background-color: #313131;
      float: left;
    }

    .navbar{
      /*background-image: url('{{ asset('img/color_2.jpg') }}') !important;*/
      background-color: #313131 !important;
      border-style: none !important;

    }

    #inside_navbar_div{
      float:right;
      margin-top: 15px;
    }

    .banner_arrow{
      line-height: 32vw;
    }

/*    #add_white_area{
      background-color: white;
      width: 70%;
      height: 360px;
      display: none;
      margin: auto;
      position: relative;
      top: -620px;
      z-index: 1;
    }*/



    @media only screen and (max-width: 767px) {

/*      #add_white_area{
        background-color: white;
        width: 90%;
        height: 490px;
        display: none;
        margin: auto;
        position: relative;
        top: -620px;
        z-index: 1;
      }*/

        #black_header{
          background-color: #313131;
          float: none;
        }

        .navbar{
          background-color: #313131 !important;
          border-style: none !important;

        }

        #inside_navbar_div{
          float:none;
          display: block;
        }

        .dropdown-menu{
          background-color: #313131 !important;
        }

        .dropdown-toggle{
          background-color: #313131 !important;
        }
        
        .dropdown-menu > li > a:hover {
          background-color: #313131 !important;
        }        
    }



    </style>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112909986-4');
    </script>

    @stack('head')
</head>
<!-- <body style="font-family: Microsoft JhengHei;font-weight: bold; background-image: url('{{ asset('img/background.jpg') }}'"> -->
<body style="font-family: Microsoft JhengHei;font-weight: bold; background-image: url('{{ asset('img/color_4.jpg') }}'">  

<div id="app">


  <!-- 主選單 -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">

    <div class="row" style="height: 80px !important;" id="black_header">
      <div class="navbar-header" style="height: 80px !important;">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="position: relative;top: 15px;">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span> 
        </button>
        <div class="navbar-brand">
        <img src="{{ asset('img/logo_ex2.png') }}" style="position: relative; top: -10px;cursor: pointer;" onclick="pageHome()"> 
       </div>
      </div>
    </div>


      <div class="collapse navbar-collapse" id="myNavbar">
        <div id="inside_navbar_div">
        <!-- 登入/點數層 -->

          <!-- 主選單 -->
          <ul class="nav navbar-nav">
            <li id="li_point">
              <div id = "tag_point" >
                <span id="tag_point_span">  </span>
              </div>
            </li>
            <li>
              <div id="tag_login" data-toggle="modal" data-target="#login_modal">              
                <span id="nav_show_account"></span>
                <div id="nav_show_login" style="opacity: 0;">
                  <div style="float: left;">
                    <img id="nav_img_login" src="{{ asset('img/icon_avatar.png') }}" style="position:relative;top:3px;left:15px;">&nbsp;&nbsp;
                  </div>
                  <span style="color: white;position:relative;top:5px;left:20px;">{{ Lang::get('user.up_login')}}</span>
                  <div class="clear"></div>
                </div>
              </div>
            </li>           
            <li id="nori_nav_li_01" onclick="pageHome()" class="white_hover_bg">
              <a href="#">                      
               <img id="nav_img_home" src="{{ asset('img/icon_home2.png') }}">
               <span >&nbsp;{{ Lang::get('user.home')}}</span>
              </a>
            </li>
            <li id="nori_nav_li_12" onclick="pageOrgRecommend()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_org" src="{{ asset('img/icon_organization2.png') }}">
               <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>
              </a>
            </li>  

<!--             <li class="dropdown" id="nori_nav_li_02">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
               <img id="nav_img_org" src="{{ asset('img/icon_organization.png') }}">
               <span id = "nori_nav_li_span_02">&nbsp;{{ Lang::get('user.org')}}</span>
               <img id="nav_img_arrow" src="{{ asset('img/icon_arrow.png') }}">             
              </a>
              <ul class="dropdown-menu">
                <li id="nori_nav_li_02_branch" onclick="pageOrgBranch()">
                  <a href="#">
                   <img id="org_branch_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_branch')}}</span>              
                  </a>
                </li>
                <li id="nori_nav_li_02_recommend" onclick="pageOrgRecommend()">
                  <a href="#">
                   <img id="org_recommend_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>              
                  </a>                
                </li>
              </ul>
            </li> -->
            <li id="nori_nav_li_03" onclick="pageWallet()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_wallet" src="{{ asset('img/icon_wallet2.png') }}">
               <span >&nbsp;{{ Lang::get('user.wallet_info')}}</span>              
              </a>
            </li> 
            <li id="nori_nav_li_04" onclick="pageActive()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_active" src="{{ asset('img/icon_rotate2.png') }}">
               <span >&nbsp;{{ Lang::get('user.active_account')}}</span>              
              </a>                    
            <li id="nori_nav_li_05" onclick="pageTransform()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_transform" src="{{ asset('img/icon_exchange2.png') }}">
               <span >&nbsp;{{ Lang::get('user.tranform_system')}}</span>              
              </a>                
            </li>         
            <li id="nori_nav_li_06" onclick="pageTrade()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_trade" src="{{ asset('img/icon_money2.png') }}">
               <span >&nbsp;{{ Lang::get('user.system_trade')}}</span>              
              </a>                 
            </li>   
<!--             <li id="nori_nav_li_08"" onclick="pagePay()">
               <a href="#">
               <img id="nav_img_buy" src="{{ asset('img/icon_money.png') }}">
               <span >&nbsp;{{ Lang::get('user.pay_financial_flows')}}</span>              
              </a>
            </li>  -->
            <li id="nori_nav_li_09" onclick="pageGame()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_game" src="{{ asset('img/icon_game2.png') }}">
               <span >&nbsp;{{ Lang::get('user.game')}}</span>              
              </a>
            </li>              
            <li id="nori_nav_li_07" onclick="pageAccount()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_account" src="{{ asset('img/icon_gear2.png') }}">
               <span >&nbsp;{{ Lang::get('user.manager_account')}}</span>              
              </a>
            </li>     
            <li id="nori_nav_li_11" onclick="pageWithdraw()" class="white_hover_bg" style="display: none;">
               <a href="#">
               <img id="nav_img_withdraw" src="{{ asset('img/icon_withdrawal2.png') }}">
               <span >&nbsp;{{ Lang::get('user.page_withdraw')}}</span>
              </a>
            </li>      
            <li id="nori_nav_li_14" onclick="pageAsk()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_ask" src="{{ asset('img/icon_ask2.png') }}">
               <span >&nbsp;{{ Lang::get('user.ask_questions')}}</span>
              </a>
            </li>  
            <li id="nori_nav_li_13" onclick="pageReport()" class="white_hover_bg">
              <a href="#">
                  <img id="nav_img_report" src="{{ asset('img/icon_form2.png') }}">
                  <span >&nbsp;{{ Lang::get('user.page_report')}}</span>
              </a>
              </li>  
             <li id="nori_nav_li_10" onclick="pageIntro()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_intro" src="{{ asset('img/icon_help2.png') }}">
               <span >&nbsp;{{ Lang::get('user.page_intro')}}</span>
              </a>
            </li>
          
            <li id="li_logout" onclick="Logout()">
              <div id="tag_logout"> 
                <div>
                  <img id="nav_img_login" src="{{ asset('img/icon_exit2.png') }}" style="position:relative;top:4px;left:15px;">&nbsp;&nbsp;
                 </div>
               <span id="tag_logout_span"> {{ Lang::get('user.signout')}} </span>
              </div>                          
            </li>

            <li>
              <div style="height: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
              <div id="div_select_language">
                <select class="form-control" id="id_select_language" >
                  @include('components.language')

                </select>   
              </div>
            </li> 
   

          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- 大圖上方細線 --> 
  <div style="background-color:#13b5b1; height: 3px;width: 100%;position: relative;top: -20px;"></div>  
  <div class="container-fluid" id='nori_main_content'>

      <!-- 中間 -->

      <!-- 大圖 --> 
      <div class="row">
        <!-- <img src="{{ asset('img/banne.jpg') }}" style="max-width: 100%;"> -->

          <!-- Banner -->
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
<!--             <li data-target="#myCarousel" data-slide-to="2"></li> -->
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="{{ asset('img/lottery_banner_01.png') }}" alt="">
            </div>

            <div class="item">
              <img src="{{ asset('img/lottery_banner_02.png') }}" alt="">
            </div>

<!--             <div class="item">
              <img src="{{ asset('img/lottery_banner_03.png') }}" alt="">
            </div> -->
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="banner_arrow" ><i class="fas fa-chevron-left"></i></span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="banner_arrow"><i class="fas fa-chevron-right"></i></span>
          </a>
        </div>


         @yield('content') 
      </div>

      <!-- 跑馬燈 -->

      <div class="row" style="background-color:#ffd653; height: 40px; ">
            <!-- 大圖下方細線 --> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#13b5b1; height: 3px;"></div>        
<!--             <marquee style="font-size: 20px;font-weight: normal;line-height: 35px;">
              <span>2018.10.4</span>
              <span>&nbsp;{{ Lang::get('user.price_info')}}</span>
            </marquee> -->
      </div>

      <!-- 下半部 -->
      <!-- <div class="row" class="first_page_down_area" style="height: 600px;background-image: url('{{ asset('img/color_4.jpg') }}'"> -->
        <div class="row" style="height: 600px;background-size:cover;background-repeat:no-repeat;background-position: center center;background-image: url('{{ asset('img/color_white.png') }}'">
        
          <br /><br />

          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="height: 20px;">
            
          </div>

          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
            <div class="nori_square_01">
              <br />
              <img src="{{ asset('img/icon_computer.png') }}" style="display:block;margin: auto;">      
              <span style="color: white;">{{ Lang::get('user.down_title_0')}}</span>
              <br />
              <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_0')}}
              </span>
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
            <div class="nori_square_01">
              <br />
              <img src="{{ asset('img/icon_lottery.png') }}" style="display:block;margin: auto;">      
              <span style="color: white;">{{ Lang::get('user.down_title_1')}}</span>
              <br />
              <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_1')}}
              </span>
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
            <div class="nori_square_01">
              <br />
              <img src="{{ asset('img/icon_praise.png') }}" style="display:block;margin: auto;">      
              <span style="color: white;">{{ Lang::get('user.down_title_2')}}</span>
              <br />
              <span style="color: #f27a3d; word-break: break-all;">{{ Lang::get('user.down_square_2')}}
              </span>
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" style="height: 200px; z-index: 5;">
            <div class="nori_square_01">
              <br />
              <img src="{{ asset('img/icon_coin.png') }}" style="display:block;margin: auto;">      
              <span style="color: white;">{{ Lang::get('user.down_title_3')}}</span>
              <br />
              <span style="color: #f27a3d; word-break: break-all;">
                {{ Lang::get('user.down_square_3')}}
              </span>
            </div>
          </div>
      </div>

  </div> <!-- end container-fluid -->



  <!-- 底部 Copyright-->
  <div class = "nori_footer">
    {{ Lang::get('home.my_site_copyright')}}
  </div>


  <!-- Login Modal-->
  <div id="login_modal" class="modal fade" role="dialog">
    <div class="modal-dialog"  style="max-width: 98vw;">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ Lang::get('user.signin')}}</h4>
        </div>
        <div class="modal-body">
          
          <form id="login">
          
            <div class="form-group">
              <label for="account">{{ Lang::get('home.account')}}:</label>
              <input type="text" class="form-control" id="account" name="account" value="soltest">
            </div>

            <div class="form-group">
              <label for="password">{{ Lang::get('home.pw')}}:</label>
              <input type="password" class="form-control" id="password" name="password" value="123456">
            </div>
  
            <div> 
              <br />
              <p style="font-size: 20px;">{{ Lang::get('home.disclaimer')}}</p>
              <p>
                {{ Lang::get('home.disclaimer_info')}}
             </p>
              <div class="checkbox">
                <label><input id="checkbox_i_agree" type="checkbox" style="width: 30px;height: 30px;">
                  <span style="position: relative;top:5px;left:10px;font-weight:bold;font-size: 20px;">&nbsp;{{ Lang::get('home.i_agree')}}</span>
                </label>
              </div>
            </div>
           <div>
            <br /><br />
              <button type="submit" class="btn btn-success">{{ Lang::get('home.login')}}</button>     
           </div>
            
          </form>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>  



<!--  <div id="add_white_area" ></div> -->

</div> <!-- end app..-->

  <script src="{{ asset('js/u_jquery.min.js') }}"></script>
  <script src="{{ asset('js/u_bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/user/u_api_mgr.js') }}"></script>  
  <script src="{{ asset('js/u_language.js') }}"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-2"></script> <!-- Analytics -->
  <script type="text/javascript">

      var m_lang_please_agree_disclaimer = "{{ Lang::get('home.please_agree_disclaimer')}}";

      // Analytics
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-112909986-2');

      // 主控台
      $("#nori_nav_li_01").hover(function(){
          $("#nav_img_home").attr("src","{{ asset('img/icon_home3.png') }}");         
      },function(){
          $(this).css("background-image","");
          $("#nav_img_home").attr("src","{{ asset('img/icon_home2.png') }}"); 
      });

      $("#nori_nav_li_03").hover(function(){ 
          $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet3.png') }}");         
      },function(){
          $(this).css("background-image","");
          $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet2.png') }}"); 
      });

      $("#nori_nav_li_04").hover(function(){ 
          $("#nav_img_active").attr("src","{{ asset('img/icon_rotate3.png') }}");         
      },function(){
          $("#nav_img_active").attr("src","{{ asset('img/icon_rotate2.png') }}"); 
      });

      $("#nori_nav_li_05").hover(function(){
          $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange3.png') }}");         
      },function(){
          $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange2.png') }}"); 
      });

      $("#nori_nav_li_06").hover(function(){
          $("#nav_img_trade").attr("src","{{ asset('img/icon_money3.png') }}");         
      },function(){
          $("#nav_img_trade").attr("src","{{ asset('img/icon_money2.png') }}"); 
      });

      $("#nori_nav_li_07").hover(function(){
          $("#nav_img_account").attr("src","{{ asset('img/icon_gear3.png') }}");         
      },function(){
          $("#nav_img_account").attr("src","{{ asset('img/icon_gear2.png') }}"); 
      });

      $("#nori_nav_li_08").hover(function(){
          $("#nav_img_buy").attr("src","{{ asset('img/icon_money3.png') }}");         
      },function(){
          $("#nav_img_buy").attr("src","{{ asset('img/icon_money2.png') }}"); 
      });       

    $("#nori_nav_li_09").hover(function(){
        $("#nav_img_game").attr("src","{{ asset('img/icon_game3.png') }}");         
    },function(){
        $("#nav_img_game").attr("src","{{ asset('img/icon_game2.png') }}"); 
    });  

    $("#nori_nav_li_13").hover(function(){
        $("#nav_img_report").attr("src","{{ asset('img/icon_form3.png') }}");
    },function(){
        $("#nav_img_report").attr("src","{{ asset('img/icon_form2.png') }}");
    });    

      $("#nori_nav_li_02").hover(function(){
          $("#nav_img_org").attr("src","{{ asset('img/icon_organization3.png') }}");   
          $("#nori_nav_li_span_02").css("color","white");
          $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow2.png') }}");         
      },function(){
          $("#nav_img_org").attr("src","{{ asset('img/icon_organization2.png') }}"); 
          $("#nori_nav_li_span_02").css("color","#706760");
          $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow.png') }}");         
      });

      $("#nori_nav_li_02_branch").hover(function(){
          $("#org_branch_circle").attr("src","{{ asset('img/icon_circle3.png') }}");         
      },function(){
          $("#org_branch_circle").attr("src","{{ asset('img/icon_circle2.png') }}");              
      });

      $("#nori_nav_li_02_recommend").hover(function(){
          $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle3.png') }}");     
      },function(){
          $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle2.png') }}");        
      });

      $("#nori_nav_li_10").hover(function(){
          $("#nav_img_intro").attr("src","{{ asset('img/icon_help3.png') }}");     
      },function(){
          $("#nav_img_intro").attr("src","{{ asset('img/icon_help2.png') }}");        
      });

      $("#nori_nav_li_11").hover(function(){
          $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal3.png') }}");     
      },function(){
          $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal2.png') }}");        
      });

    $("#nori_nav_li_12").hover(function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization3.png') }}");         
    },function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization2.png') }}");              
    });

    $("#nori_nav_li_14").hover(function(){
        $("#nav_img_ask").attr("src","{{ asset('img/icon_ask3.png') }}");         
    },function(){
        $("#nav_img_ask").attr("src","{{ asset('img/icon_ask2.png') }}");              
    });




      // ready-jobs
      $(function(){
              
        $("#li_point").hide();  
        $("#li_logout").hide(); 
        $("#tag_point").hide();
        $("#nav_show_login").css("opacity","1");
        $("#tag_login").on("click", openLoginModal);
        $("#login").on("submit", Login);
        $("#org_hide").hide();

      })


      // ＊＊ login modal ＊＊

      function openLoginModal(){


              $("#login_modal").modal();
          
            
            return false;
      }


      function pageHome(){
            location.href = "/";
            return false;
      }

      function pageWithdraw(){
            openLoginModal();
            return false;
      }

      function pageIntro(){
            location.href = "/intro";        
            return false;
      }

      function pageReport(){
          openLoginModal();
      }   

      function pageOrgBranch(){
          openLoginModal();
      }

      function pageOrgRecommend(){
          openLoginModal();
      }

      function pageWallet(){
          openLoginModal();
      }

      function pageActive(){
          openLoginModal();
      }

      function pageTransform(){
          openLoginModal();
      }

      function pageTrade(){
          openLoginModal();
      }

      function pageAccount(){
          openLoginModal();
      }
      
      function pagePay(){
          openLoginModal();
      }

      function pageGame(){
          openLoginModal();
      }

    function pageAsk(){
           openLoginModal();
    }
    
      
      // login
      var m_is_waiting_login = false;
      function Login(){

        var account = $("#account").val();
        var pw = $("#password").val();
        var is_checked = $('#checkbox_i_agree').is(":checked"); 
        
        if(!is_checked){
          alert(m_lang_please_agree_disclaimer);
          return false;
        }
        //checkbox_i_agree

        if( !(account != '' && pw != '')){    
          return false;
        }

        // 防止連按
        if(m_is_waiting_login) {
          return false;
        }

        m_is_waiting_login = true;

        p_ApiMgr.login(
              account,
              pw,
              function (result) {
                  
                var obj = JSON.parse(result);
                var id = obj.id;
                location.href = "/user/news";
              
              },this,
              function (error) {
                m_is_waiting_login = false;
              },this
        );

        return false;
      }

  </script>
    
    @stack('script')
</body>
</html>













































