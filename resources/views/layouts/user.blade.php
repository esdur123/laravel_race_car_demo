<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    <title>極致</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/u_bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user/b_user.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/u_language.css') }}">
    <link rel="stylesheet" href="{{ asset('css/u_scrollbar.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <style>


      .hide_overflow::-webkit-scrollbar {display:none}    
      
      .nori_float_nav_01 {
        width: 10%; 
        float: left; 
        height: 40px;
        color: #706760;
      }

      .nori_float_nav_02 {
        width: 10%; 
        float: left; 
        height: 120px;
        color: #706760;
      }

      .nori_pointer_1 {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_up {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_down_01 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_0.jpg') }}');
      }

      .nori_draw_down_02 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_1.jpg') }}');
      }

      #tag_point{
        position: relative;
        cursor: pointer;

        left: 10px; 
        top: 10px;
        color: white;
        background:url('{{ asset('img/button_7.png') }}') no-repeat;
        width:125px;
        height:40px
      }

      #tag_colorpoint{
        position: relative;
        cursor: pointer;

        left: 10px; 
        top: 10px;
        color: white;
        background:url('{{ asset('img/button_5.png') }}') no-repeat;
        width:125px;
        height:40px
      }

      #tag_point_span{
        position: relative;
        top:5px;
        left: 8px;
      }

      #tag_colorpoint_span{
        position: relative;
        top:5px;
        left: 8px;
      }



      #tag_login{
        cursor: pointer;
        position:relative;  
        left: 10px; 
        top: 10px;
        background:url('{{ asset('img/button_6.png') }}') no-repeat;
        width:125px;
        height:40px;
      }

      #tag_logout{
        cursor: pointer;
        float: left;
        position:relative;
        left: 10px; 
        top: 10px;
        background:url('{{ asset('img/button_6.png') }}') no-repeat;
        width:125px;
        height:40px
      }

      #tag_logout_span{
        position:relative;
        top:-15px;
        left: 42px;
      }

      #nav_show_account{
        color: #242527;
        position:relative;
        top:4px;
        left:12px;
      }


    .navbar-nav > li > a{
        color: white !important;
    }

    .white_hover_bg:hover{
        background-color: white !important;
    }  

    .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus{
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:#f27a3d !important;
    } 

    .dropdown-toggle{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/

    }

    .dropdown-menu{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/
        background-color: #313131 !important;
    }

    .dropdown-menu > li > a{
        color: #706760 !important;
    }

    .dropdown-menu > li > a:hover {
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:white !important;
      background-color: #313131 !important;
    }

    #black_header{
      background-color: #313131;
      float: left;
    }

    .navbar{
      /*background-image: url('{{ asset('img/color_2.jpg') }}') !important;*/
      background-color: #313131 !important;
      border-style: none !important;

    }

    #inside_navbar_div{
      float:right;
      margin-top: 15px;
    }

/*    #add_white_area{
      background-color: white;
      width: 70%;
      height: 360px;
      display: none;
      margin: auto;
      position: relative;
      top: -620px;
      z-index: 1;
    }*/

    .banner_arrow{
      line-height: 32vw;
    }

    .top_line{
      background-color:#13b5b1; 
      height: 1px;
      position: relative;
      top: -20px;
    }

    @media only screen and (max-width: 767px) {

/*      #add_white_area{
        background-color: white;
        width: 90%;
        height: 490px;
        display: none;
        margin: auto;
        position: relative;
        top: -620px;
        z-index: 1;
      }*/

        #black_header{
          background-color: #313131;
          float: none;
        }

        .navbar{
          background-color: #313131 !important;
          border-style: none !important;

        }

        #inside_navbar_div{
          float:none;
          display: block;
        }

        .dropdown-menu{
          background-color: #313131 !important;
        }

        .dropdown-toggle{
          background-color: #313131 !important;
        }

        .dropdown-menu > li > a:hover {
          background-color: #313131 !important;
        }        
    }

    </style>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112909986-4');
    </script>

    @stack('head')
</head>
<body style="font-family: Microsoft JhengHei;font-weight: bold; background-image: url('{{ asset('img/color_4.jpg') }}'">

<div id="app">


  <!-- 主選單 -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="row" style="height: 80px !important;" id="black_header">
        <div class="navbar-header" >
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="position: relative;top: 15px;">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
          <div class="navbar-brand">
          <img src="{{ asset('img/logo_ex2.png') }}" style="position: relative; top: -10px;cursor: pointer;" onclick="pageHome()"> 
         </div>
        </div>
      </div>

      <div class="collapse navbar-collapse" id="myNavbar">
        <div id="inside_navbar_div">
          <ul class="nav navbar-nav">
            <li>
              <div id = "tag_point" onclick="pageWallet()">
                <span id="tag_point_span">  </span>
              </div>
            </li>
            <li id="li_color_point"  style="display:none">
              <div id = "tag_colorpoint" onclick="pageWallet()">
                <span id="tag_colorpoint_span"> </span>
              </div>
            </li>            
            <li>
              <div id="tag_login" onclick="pageAccount()" >
              <img id="nav_img_login" src="{{ asset('img/icon_avatar.png') }}" style="position:relative;top:3px;left:15px;">&nbsp;&nbsp;          
                <span id="nav_show_account"></span>
              </div>
            </li>          
            <li id="nori_nav_li_01" onclick="pageHome()" class="white_hover_bg">
              <a href="#">                      
               <img id="nav_img_home" src="{{ asset('img/icon_home2.png') }}">
               <span >&nbsp;{{ Lang::get('user.home')}}</span>
              </a>
            </li>
            <li id="nori_nav_li_12"" onclick="pageOrgRecommend()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_org" src="{{ asset('img/icon_organization2.png') }}">
               <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>
              </a>
            </li>             
<!--             <li class="dropdown" id="nori_nav_li_02">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
               <img id="nav_img_org" src="{{ asset('img/icon_organization.png') }}">
               <span id = "nori_nav_li_span_02">&nbsp;{{ Lang::get('user.org')}}</span>
               <img id="nav_img_arrow" src="{{ asset('img/icon_arrow.png') }}">             
              </a>
              <ul class="dropdown-menu">
                <li id="nori_nav_li_02_branch" onclick="pageOrgBranch()">
                  <a href="#">
                   <img id="org_branch_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_branch')}}</span>              
                  </a>
                </li>
                <li id="nori_nav_li_02_recommend" onclick="pageOrgRecommend()">
                  <a href="#">
                   <img id="org_recommend_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>              
                  </a>                
                </li>
              </ul>
            </li> -->
            <li id="nori_nav_li_03" onclick="pageWallet()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_wallet" src="{{ asset('img/icon_wallet2.png') }}">
               <span >&nbsp;{{ Lang::get('user.wallet_info')}}</span>              
              </a>
            </li> 
            <li id="nori_nav_li_04" onclick="pageActive()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_active" src="{{ asset('img/icon_rotate2.png') }}">
               <span >&nbsp;{{ Lang::get('user.active_account')}}</span>   <span id="active_prompt" style="display:none;color: rgb(164, 161, 25)"><i class="fas fa-exclamation-circle"></i></span>
              </a>                    
            <li id="nori_nav_li_05" onclick="pageTransform()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_transform" src="{{ asset('img/icon_exchange2.png') }}">
               <span >&nbsp;{{ Lang::get('user.tranform_system')}}</span>              
              </a>                
            </li>         
            <li id="nori_nav_li_06" onclick="pageTrade()" class="white_hover_bg">
              <a href="#">
               <img id="nav_img_trade" src="{{ asset('img/icon_shake_hands2.png') }}">
               <span >&nbsp;{{ Lang::get('user.system_trade')}}</span>              
              </a>                 
            </li>  
            <li id="nori_nav_li_15"" onclick="pagePay()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_buy" src="{{ asset('img/icon_money2.png') }}">
               <span >&nbsp;{{ Lang::get('user.pay_financial_flows')}}</span>              
              </a>
            </li> 
            <li id="nori_nav_li_09" onclick="pageGame()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_game" src="{{ asset('img/icon_game2.png') }}">
               <span >&nbsp;{{ Lang::get('user.game_9block')}}</span>              
              </a>
            </li>
            <li id="nori_nav_li_07" onclick="pageAccount()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_account" src="{{ asset('img/icon_gear2.png') }}">
               <span >&nbsp;{{ Lang::get('user.manager_account')}}</span>              
              </a>
            </li>   
            <li id="nori_nav_li_11" onclick="pageWithdraw()" class="white_hover_bg" style="display: none;">
               <a href="#">
               <img id="nav_img_withdraw" src="{{ asset('img/icon_withdrawal2.png') }}">
               <span >&nbsp;{{ Lang::get('user.page_withdraw')}}</span>
              </a>
            </li>     
            <li id="nori_nav_li_14" onclick="pageAsk()" class="white_hover_bg">
               <a href="#">
               <img id="nav_img_ask" src="{{ asset('img/icon_ask2.png') }}">
               <span >&nbsp;{{ Lang::get('user.ask_questions')}}</span>
              </a>
            </li>
              <li id="nori_nav_li_13"" onclick="pageReport()" class="white_hover_bg">
              <a href="#">
                  <img id="nav_img_report" src="{{ asset('img/icon_form2.png') }}">
                  <span >&nbsp;{{ Lang::get('user.page_report')}}</span>
              </a>
              </li>
                          
              <li id="nori_nav_li_10" onclick="pageIntro()" class="white_hover_bg">
              <a href="#">
                  <img id="nav_img_intro" src="{{ asset('img/icon_help2.png') }}">
                  <span >&nbsp;{{ Lang::get('user.page_intro')}}</span>
              </a>
              </li>



            <li id="li_logout" onclick="Logout()" style="z-index: 1;">
              <div id="tag_logout" > 
                <div>
                  <img src="{{ asset('img/icon_exit2.png') }}" style="position:relative;top:4px;left:15px;">&nbsp;&nbsp;
                 </div>
               <span id="tag_logout_span"> {{ Lang::get('user.signout')}} </span>
              </div>                          
            </li>

            <li>
              <div style="height: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
              <div id="div_select_language">
                <select class="form-control" id="id_select_language" >
                  @include('components.language')
                </select>   
              </div>
            </li> 
          </ul>
        </div> 
      </div>
    </div>
  </nav>


<!-- 頂端細線 -->
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="top_line"></div>
  </div>
</div>  

  <!-- 內容 -->
  <div class="container-fluid" id="nori_main_content">
    <div name="row-main" class="row" id="nori_user_main">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">            
                <div id="div_main" >
                    @yield('content')
                </div>
        </div>            
    </div>     
  </div> 

  <!-- 底部 Copyright-->
  <div class = "nori_footer">
    {{ Lang::get('home.my_site_copyright')}}
  </div>

<!--  <div id="add_white_area" ></div> -->

</div> <!-- end app..-->

  <script src="{{ asset('js/u_defined.js') }}"></script>
  <script src="{{ asset('js/u_jquery.min.js') }}"></script>
  <script src="{{ asset('js/u_bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/user/u_api_mgr.js') }}"></script>
  <script src="{{ asset('js/user/u_common.js') }}"></script>
  <script src="{{ asset('js/u_language.js') }}"></script>
  <script src="{{ asset('js/u_my_utility.js') }}"></script>

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-2"></script> <!-- Analytics -->
  <script type="text/javascript">
    
    // Analytics
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-112909986-2');

    // # ready-jobs
    $(function(){
      //$("#li_color_point").hide();
      
      //p_checkIsColorpoint();
      $("#active_prompt").hide();
      getAccount();
      getPoint_general();
      checkIsAvailableWithdraw();
      
      //getSet();
      
    }) // end function ready



    // ＊＊ 檢查現在給不給申請提領 ＊＊        
    function checkIsAvailableWithdraw(){
        
        p_ApiMgr.checkIsWithdrawAvailable(                         
            function (result) {                 

                if(result['is_tagged'] == '0'){
                  $("#nori_nav_li_11").show();
                    // console.log('withdraw');
                } else {
                    // location.href = "/user/news";
                    // console.log('non withdraw');
                    //$("#nori_nav_li_11").show();
                }

            },this,
            function (error) {},this
        );
    }


    // ＊＊ 檢查用點數還是彩點 ＊＊
    var p_is_colorpoint = 0;
    function p_checkIsColorpoint(){

      p_ApiMgr.checkIsColorpoint(

            function (result) {
                
                p_is_colorpoint = result['is_colorpoint'];
                //var point_type = 'point';
                if( p_is_colorpoint== 1){
                  getColorPoint_general();
                  //point_type = 'colorpoint';
                }
                console.log(p_is_colorpoint);
                // if(m_is_colorpoint == 1){
                //     $("#div_colorpoint").show();
                //     GetColorwalletInfo();   // 取得彩點錢包ColorPoint資訊
                // }

            },this,
            function (error) {},this
      );


      return false;
    }



    // ＊＊ 檢查是否已開通 ＊＊
    var p_cur_set = 0;
    function getSet(){
        
        p_ApiMgr.getSet(
          function (result) {
            p_cur_set = result;
            if(p_cur_set <= 0)
            {
              $("#active_prompt").show();
            }
            //alert(result)
            //console.log(result['account']);
            // p_user_account = result['account'];
            // $("#nav_show_account").text(p_user_account);
            // is_get_account = true;
            
          },this,
          function (error) {},this
        );

      return false;
    } 


    // ＊＊ 取得account id ＊＊
    var is_get_account = false; // 是否已取得account
    var p_user_account = '';
    function getAccount(){
        
        p_ApiMgr.getAccount(
          function (result) {
            //console.log(result['account']);
            p_user_account = result['account'];
            $("#nav_show_account").text(p_user_account);
            is_get_account = true;
            
          },this,
          function (error) {},this
        );

      return false;
    }    

    // ＊＊ 取得目前點數 ＊＊
    function getPoint_general(){
        
        p_ApiMgr.getPoint(
          function (result) {

            var my_point  = '$ '+toDecimal2(result);
            $("#tag_point_span").text(my_point);

            
          },this,
          function (error) {},this
        );

      return false;
    }

    function getColorPoint_general(){

      p_ApiMgr.getColorPoint(

            function (result) {
              var my_colorpoint  = '$ '+toDecimal2(result);
              $("#tag_colorpoint_span").text(my_colorpoint);
              $("#li_color_point").show();
            },this,
            function (error) {},this
      );


      return false;
    }

    // ＊＊ 登出 ＊＊
    function Logout(){
      
        p_ApiMgr.logout(
              function (result) {
                location.href = "/";
              },this,
              function (error) {
                location.href = "/";
              },this
        );          
        return false;
    }


    // ＊＊ 頁面-最新消息 ＊＊
    function pageHome(){
          location.href = "/user/news";
          return false;
    }


    // ＊＊ 頁面-安置組織 ＊＊
    function pageOrgBranch(){

        if(!is_get_account){
          return false;
        }

        location.href = "/user/org/branch/"+p_user_account;
        ////＊＊ if by id　＊＊
        // p_ApiMgr.getBallID(
        //     function (result) {
        //       //console.log(result);
        //       location.href = "/user/org/branch/"+result;
        //     },this,
        //     function (error) {},this
        // );
        return false;
    }


    // ＊＊ 頁面-推薦組織 ＊＊
    function pageOrgRecommend(){

      if(!is_get_account){
        return false;
      }

      location.href = "/user/org/recommend/"+p_user_account;
      ////＊＊ if by id　＊＊
      // p_ApiMgr.getBallID(
      //       function (result) {
      //         //console.log(result);
      //         location.href = "/user/org/recommend/"+result;
      //       },this,
      //       function (error) {},this
      // );
      return false;
    }



    // ＊＊ 頁面-帳號管理 ＊＊
    function pageAccount(){

          location.href = "/user/account";
          return false;
    }

    // ＊＊ 頁面-帳號管理 ＊＊
    function pageIntro(){

        location.href = "/intro";
        return false;
    }


    // ＊＊ 頁面-帳務 ＊＊
    function pageReport(){

        location.href = "/report";
        return false;
    }



    // ＊＊ 頁面-帳戶激活 ＊＊
    function pageActive(){

          location.href = "/user/active";
          return false;
    }


    // ＊＊ 頁面-轉帳系統 ＊＊
    function pageTransform(){

          location.href = "/user/transform";
          return false;
    }


    // ＊＊ 頁面-系統交易 ＊＊
    function pageTrade(){

          location.href = "/user/trade";
          return false;
    }


    // ＊＊ 頁面-錢包明細 ＊＊
    function pageWallet(){

          location.href = "/user/wallet";
          return false;
    }


    // ＊＊ 金流 ＊＊
    function pagePay(){

          location.href = "/user/pay";
          return false;
    }

    // ＊＊ 遊戲 ＊＊
    function pageGame(){

          location.href = "/user/game";
          return false;
    }

    function pageWithdraw(){
          location.href = "/user/withdraw";
          return false;
    }

    function pageAsk(){
          location.href = "/user/ask";
          return false;
    }

    // 上方控制台
    $("#nori_nav_li_01").hover(function(){
        $("#nav_img_home").attr("src","{{ asset('img/icon_home3.png') }}");         
    },function(){
        $(this).css("background-image","");
        $("#nav_img_home").attr("src","{{ asset('img/icon_home2.png') }}"); 
    });

    $("#nori_nav_li_03").hover(function(){ 
        $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet3.png') }}");         
    },function(){
        $(this).css("background-image","");
        $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet2.png') }}"); 
    });

    $("#nori_nav_li_04").hover(function(){ 
        $("#nav_img_active").attr("src","{{ asset('img/icon_rotate3.png') }}");         
    },function(){
        $("#nav_img_active").attr("src","{{ asset('img/icon_rotate2.png') }}"); 
    });

    $("#nori_nav_li_05").hover(function(){
        $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange3.png') }}");         
    },function(){
        $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange2.png') }}"); 
    });

    $("#nori_nav_li_06").hover(function(){
        $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands3.png') }}");         
    },function(){
        $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands2.png') }}"); 
    });

    $("#nori_nav_li_15").hover(function(){
        $("#nav_img_buy").attr("src","{{ asset('img/icon_money3.png') }}");         
    },function(){
        $("#nav_img_buy").attr("src","{{ asset('img/icon_money2.png') }}"); 
    });  

    $("#nori_nav_li_07").hover(function(){
        $("#nav_img_account").attr("src","{{ asset('img/icon_gear3.png') }}");         
    },function(){
        $("#nav_img_account").attr("src","{{ asset('img/icon_gear2.png') }}"); 
    });

    $("#nori_nav_li_09").hover(function(){
        $("#nav_img_game").attr("src","{{ asset('img/icon_game3.png') }}");         
    },function(){
        $("#nav_img_game").attr("src","{{ asset('img/icon_game2.png') }}"); 
    });          

    $("#nori_nav_li_10").hover(function(){
        $("#nav_img_intro").attr("src","{{ asset('img/icon_help3.png') }}");     
    },function(){
        $("#nav_img_intro").attr("src","{{ asset('img/icon_help2.png') }}");        
    });

    $("#nori_nav_li_11").hover(function(){
        $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal3.png') }}");     
    },function(){
        $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal2.png') }}");        
    });

    $("#nori_nav_li_12").hover(function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization3.png') }}");
    },function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization2.png') }}");
    });

    $("#nori_nav_li_13").hover(function(){
        $("#nav_img_report").attr("src","{{ asset('img/icon_form3.png') }}");
    },function(){
        $("#nav_img_report").attr("src","{{ asset('img/icon_form2.png') }}");
    });

    $("#nori_nav_li_02").hover(function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization3.png') }}");   
        $("#nori_nav_li_span_02").css("color","white");
        $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow2.png') }}");         
    },function(){
        $("#nav_img_org").attr("src","{{ asset('img/icon_organization2.png') }}"); 
        $("#nori_nav_li_span_02").css("color","#706760");
        $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow.png') }}");         
    });

    $("#nori_nav_li_14").hover(function(){
        $("#nav_img_ask").attr("src","{{ asset('img/icon_ask3.png') }}");         
    },function(){
        $("#nav_img_ask").attr("src","{{ asset('img/icon_ask2.png') }}");              
    });

    $("#nori_nav_li_02_branch").hover(function(){
        $("#org_branch_circle").attr("src","{{ asset('img/icon_circle2.png') }}");         
    },function(){
        $("#org_branch_circle").attr("src","{{ asset('img/icon_circle.png') }}");              
    });

    $("#nori_nav_li_02_recommend").hover(function(){
        $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle2.png') }}");     
    },function(){
        $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle.png') }}");        
    });


  </script>
  @stack('script')
</body>
</html>