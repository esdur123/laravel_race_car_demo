<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="auth-token" content="{{ session('token') }}">
        <title>極致</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/u_sidebar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/b_admin.css') }}">   
        <link rel="stylesheet" href="{{ asset('css/u_scrollbar.css') }}">   
        <style type="text/css">


            .hide_overflow::-webkit-scrollbar {display:none}  

            #nav_up_zone{
                width: calc(95vw);
            }      

            .sidebar-header{
                background-color: #313131!important;
                color: white;
            }
    
            #sidebar{
                background-color: #313131!important;
                color: white;
            }

            #homeSubmenu > li > a{
                background-color: #313131!important;  
                color: white!important;   
    
            } 
  
            .list-unstyled > li > a{
                color: white!important;   
            }

            .list-unstyled > li > a:hover{
                color: #6f6760!important;   
            }

            #nav_toggle_01{
                background-color: #313131!important;  
                color:white;
            }

            #homeSubmenu>li>a:hover,#nav_toggle_01:hover{
                background-color:white!important;   
                color:#6f6760!important;   
                /* color: white!important; */
                /* background-image: url('{{ asset('img/select_1.jpg') }}') !important; */
             
            } 

            @media only screen and (max-width: 767px) {

            }

        </style>
            @stack('head')
    </head>
    <body>





            <!-- 內容 -->
            <div class="container">
             
                    @yield('content') 
             
            </div>
            <!-- 補一條線 -->
            <div class="line"></div>

                
                

            </div>
    

    
      <!-- 底部 Copyright-->
      <div class = "admin_footer">
        {{ Lang::get('home.my_site_copyright')}}
      </div>

        


        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
         <script src="{{ asset('js/u_defined.js') }}"></script>

         <script type="text/javascript">
             $(document).ready(function () {

                  $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                  });

                  $("#nori_nav_li_user").hover(function(){
                      $("#nav_img_user").attr("src","{{ asset('img/icon_avatar4.png') }}");         
                  },function(){
                      $("#nav_img_user").attr("src","{{ asset('img/icon_avatar.png') }}");              
                  });

                  $("#nori_nav_li_home").hover(function(){
                      $("#nav_img_home").attr("src","{{ asset('img/icon_home.png') }}");         
                  },function(){
                      $("#nav_img_home").attr("src","{{ asset('img/icon_home2.png') }}");              
                  });

                  $("#nav_toggle_01").hover(function(){
                      $("#nav_img_tootle_01").attr("src","{{ asset('img/icon_organization.png') }}");         
                  },function(){
                      $("#nav_img_tootle_01").attr("src","{{ asset('img/icon_organization2.png') }}");              
                  });

                  $("#nori_nav_li_mgr_acc").hover(function(){
                      $("#nav_img_mgr_acc").attr("src","{{ asset('img/icon_circle.png') }}");         
                  },function(){
                      $("#nav_img_mgr_acc").attr("src","{{ asset('img/icon_circle2.png') }}");              
                  });

                  $("#nori_nav_li_mgr_org").hover(function(){
                      $("#nav_img_mgr_org").attr("src","{{ asset('img/icon_circle.png') }}");         
                  },function(){
                      $("#nav_img_mgr_org").attr("src","{{ asset('img/icon_circle2.png') }}");              
                  });

                  $("#nori_nav_li_trade").hover(function(){
                      $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands.png') }}");         
                  },function(){
                      $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands2.png') }}");              
                  });

                  $("#nori_nav_li_withdraw").hover(function(){
                      $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal.png') }}");         
                  },function(){
                      $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal2.png') }}");              
                  });

                  $("#nori_nav_li_admin").hover(function(){
                      $("#nav_img_admin").attr("src","{{ asset('img/icon_gear.png') }}");         
                  },function(){
                      $("#nav_img_admin").attr("src","{{ asset('img/icon_gear2.png') }}");              
                  });

                  $("#nori_nav_li_logout").hover(function(){
                      $("#nav_img_exit").attr("src","{{ asset('img/icon_exit.png') }}");         
                  },function(){
                      $("#nav_img_exit").attr("src","{{ asset('img/icon_exit2.png') }}");              
                  });
                                                                                                                           
                  $("#nori_nav_li_calender").hover(function(){
                      $("#nav_img_calender").attr("src","{{ asset('img/icon_rotate.png') }}");         
                  },function(){
                      $("#nav_img_calender").attr("src","{{ asset('img/icon_rotate2.png') }}");              
                  });


             });
         </script>
         @stack('script')
    </body>
</html>











































