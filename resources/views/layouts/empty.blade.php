<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    <title>極致</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/u_bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user/b_user.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/u_language.css') }}">
    <style>


    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112909986-4');
    </script>

    @stack('head')
</head>
<body style="font-family: Microsoft JhengHei;font-weight: bold;">

<div id="app">


  <!-- 主選單 -->


  <!-- 內容 -->
  <div class="container-fluid" id="nori_main_content">
    <div name="row-main" class="row" id="nori_user_main">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">            
                <div id="div_main" >
                    @yield('content') 
                </div>            
        </div>            
    </div>     
  </div> 

  <!-- 底部 Copyright-->
  <div class = "nori_footer">
    {{ Lang::get('home.my_site_copyright')}}
  </div>

</div> <!-- end app..-->

    <script src="{{ asset('js/u_jquery.min.js') }}"></script>
    <script src="{{ asset('js/u_bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/user/u_api_mgr.js') }}"></script>  
    <script src="{{ asset('js/u_language.js') }}"></script>
    <script src="{{ asset('js/u_my_utility.js') }}"></script>

    <script type="text/javascript">

  

    </script>
    @stack('script')
</body>
</html>