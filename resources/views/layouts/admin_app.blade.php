<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="auth-token" content="{{ session('token') }}">
        <title>極致</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/u_sidebar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/b_admin.css') }}">      
        <link rel="stylesheet" href="{{ asset('css/u_scrollbar.css') }}">
        <style type="text/css">

            .hide_overflow::-webkit-scrollbar {display:none}    
            
            #nav_up_zone{
                width: calc(95vw);
            }      

            .sidebar-header{
                background-color: #313131!important;
                color: white;
            }
    
            #sidebar{
                background-color: #313131!important;
                color: white;
            }

            #homeSubmenu > li > a{
                background-color: #313131!important;  
                color: white!important;   
    
            } 
  
            .list-unstyled > li > a{
                color: white!important;   
            }

            .list-unstyled > li > a:hover{
                color: #6f6760!important;   
            }

            #nav_toggle_01{
                background-color: #313131!important;  
                color:white;
            }

            #homeSubmenu>li>a:hover,#nav_toggle_01:hover{
                background-color:white!important;   
                color:#6f6760!important;   
                /* color: white!important; */
                /* background-image: url('{{ asset('img/select_1.jpg') }}') !important; */
             
            } 

            @media only screen and (max-width: 767px) {

            }

        </style>
            @stack('head')
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="{{ asset('img/logo_ex2.png') }}" style="cursor: pointer;" onclick="">
                </div>

                <ul class="list-unstyled components">
                    <!-- <p>Dummy Heading</p> -->
                    <li id="nori_nav_li_user" onclick="">
                      <a href="#">
                        &nbsp;&nbsp;
                       <img id="nav_img_user" src="{{ asset('img/icon_avatar.png') }}">
                       &nbsp;<span id="text_show_login">{{ Lang::get('admin.admin_login')}}</span>              
                      </a>                 
                    </li> 

                    <li id="nori_nav_li_home" onclick="pageControl()">
                      <a href="#">
                        &nbsp;
                       <img id="nav_img_home" src="{{ asset('img/icon_home2.png') }}">
                       <span >&nbsp;{{ Lang::get('user.home')}}</span>              
                      </a>                 
                    </li> 

<!--                     <li id="nori_nav_li_calender" onclick="pageCalender()">
                      <a href="#">
                        &nbsp;
                       <img id="nav_img_calender" src="{{ asset('img/icon_rotate2.png') }}">
                       <span >&nbsp;{{ Lang::get('admin.dividend_setting')}}</span>              
                      </a>                 
                    </li>  -->

                    <li>
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" id="nav_toggle_01">
                            &nbsp;
                           <img id="nav_img_tootle_01" src="{{ asset('img/icon_organization2.png') }}">
                           <span id = "nori_nav_li_span_02">&nbsp;{{ Lang::get('admin.manager_members')}}</span>
                        </a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li id="nori_nav_li_mgr_acc" onclick="pageMgrAccount()">
                              <a href="#">
                               <img id="nav_img_mgr_acc" src="{{ asset('img/icon_circle2.png') }}">
                               <span >&nbsp;{{ Lang::get('admin.member_account')}}</span>              
                              </a>
                            </li>
                            <li id="nori_nav_li_mgr_org" onclick="pageMgrRecommend()">
                              <a href="#">
                               <img id="nav_img_mgr_org" src="{{ asset('img/icon_circle2.png') }}">
                               <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>              
                              </a> 
                            </li>
                        </ul>
                    </li>

                    <li id="nori_nav_li_trade" onclick="pageTrade()">
                       <a href="#">
                           &nbsp;
                           <img id="nav_img_trade" src="{{ asset('img/icon_shake_hands2.png') }}">
                           <span >&nbsp;{{ Lang::get('user.system_trade')}}</span>              
                      </a> 
                    </li>
                    <li id="nori_nav_li_withdraw" onclick="pageWithdraw()">
                       <a href="#">
                           &nbsp;
                           <img id="nav_img_withdraw" src="{{ asset('img/icon_withdrawal2.png') }}">
                           <span >&nbsp;{{ Lang::get('user.page_withdraw')}}</span>              
                      </a> 
                    </li>    
                    <li id="nori_nav_li_ask" onclick="pageAsk()" class="white_hover_bg">
                       <a href="#">
                        &nbsp;
                       <img id="nav_img_ask" src="{{ asset('img/icon_ask2.png') }}">
                       <span >&nbsp;{{ Lang::get('user.ask_questions')}}</span>
                      </a>
                    </li>                                        
                    <li id="nori_nav_li_admin"  onclick="pageInfo()">
                        <a href="#" >
                            &nbsp;
                           <img id="nav_img_admin" src="{{ asset('img/icon_gear2.png') }}">
                           <span >&nbsp;{{ Lang::get('admin.admin_setting')}}</span>    
                        </a>
                    </li>
                    <li id="nori_nav_li_logout">
                        <a href="#" onclick="logout()">
                            &nbsp;
                           <img id="nav_img_exit" src="{{ asset('img/icon_exit2.png') }}">
                           <span >&nbsp;{{ Lang::get('user.signout')}}</span>    
                        </a>
                    </li>    
                    <li>
                      
                      <div id="div_select_language" style="max-width: 130px;position: relative;left: 15px;top:10px;">
                        <select class="form-control" id="id_select_language" >
                          @include('components.language')
                        </select>  
                        <br /> 
                      </div>
                    </li>                                     
                </ul>

<!--                 <ul class="list-unstyled CTAs">
                    <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
                    <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
                </ul> -->
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav id="nav_up_zone" class="navbar navbar-default" >
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <!-- <span>Toggle Sidebar</span> -->
                            </button>



                        </div>

<!--                         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li>
                            </ul>
                        </div> -->
                    </div>
                </nav>

            <!-- 內容 -->
            <div class="container">
             
                    @yield('content') 
             
            </div>
            <!-- 補一條線 -->
            <div class="line"></div>

                
                

            </div>
        </div>

    
      <!-- 底部 Copyright-->
      <div class = "admin_footer">
        {{ Lang::get('home.my_site_copyright')}}
      </div>

        


        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
         <script src="{{ asset('js/u_defined.js') }}"></script>
         <script src="{{ asset('js/admin/u_language.js') }}"></script>


         <script type="text/javascript">
             $(document).ready(function () {

                  $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                  });

                  $("#nori_nav_li_user").hover(function(){
                      $("#nav_img_user").attr("src","{{ asset('img/icon_avatar4.png') }}");         
                  },function(){
                      $("#nav_img_user").attr("src","{{ asset('img/icon_avatar.png') }}");              
                  });

                  $("#nori_nav_li_home").hover(function(){
                      $("#nav_img_home").attr("src","{{ asset('img/icon_home.png') }}");         
                  },function(){
                      $("#nav_img_home").attr("src","{{ asset('img/icon_home2.png') }}");              
                  });

                  $("#nav_toggle_01").hover(function(){
                      $("#nav_img_tootle_01").attr("src","{{ asset('img/icon_organization.png') }}");         
                  },function(){
                      $("#nav_img_tootle_01").attr("src","{{ asset('img/icon_organization2.png') }}");              
                  });

                  $("#nori_nav_li_mgr_acc").hover(function(){
                      $("#nav_img_mgr_acc").attr("src","{{ asset('img/icon_circle.png') }}");         
                  },function(){
                      $("#nav_img_mgr_acc").attr("src","{{ asset('img/icon_circle2.png') }}");              
                  });

                  $("#nori_nav_li_mgr_org").hover(function(){
                      $("#nav_img_mgr_org").attr("src","{{ asset('img/icon_circle.png') }}");         
                  },function(){
                      $("#nav_img_mgr_org").attr("src","{{ asset('img/icon_circle2.png') }}");              
                  });

                  $("#nori_nav_li_trade").hover(function(){
                      $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands.png') }}");         
                  },function(){
                      $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands2.png') }}");              
                  });

                  $("#nori_nav_li_withdraw").hover(function(){
                      $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal.png') }}");         
                  },function(){
                      $("#nav_img_withdraw").attr("src","{{ asset('img/icon_withdrawal2.png') }}");              
                  });

                  $("#nori_nav_li_admin").hover(function(){
                      $("#nav_img_admin").attr("src","{{ asset('img/icon_gear.png') }}");         
                  },function(){
                      $("#nav_img_admin").attr("src","{{ asset('img/icon_gear2.png') }}");              
                  });

                  $("#nori_nav_li_ask").hover(function(){
                      $("#nav_img_ask").attr("src","{{ asset('img/icon_ask.png') }}");         
                  },function(){
                      $("#nav_img_ask").attr("src","{{ asset('img/icon_ask2.png') }}");              
                  });



                  $("#nori_nav_li_logout").hover(function(){
                      $("#nav_img_exit").attr("src","{{ asset('img/icon_exit.png') }}");         
                  },function(){
                      $("#nav_img_exit").attr("src","{{ asset('img/icon_exit2.png') }}");              
                  });
                                                                                                                           
                  $("#nori_nav_li_calender").hover(function(){
                      $("#nav_img_calender").attr("src","{{ asset('img/icon_rotate.png') }}");         
                  },function(){
                      $("#nav_img_calender").attr("src","{{ asset('img/icon_rotate2.png') }}");              
                  });


             });
         </script>
         @stack('script')
    </body>
</html>











































