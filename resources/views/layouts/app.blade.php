<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    <title>極致</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/u_bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/u_language.css') }}">

    @stack('head')
</head>
<body>
    <div id="app">
      @yield('content') 
    </div>


    <script src="{{ asset('js/u_jquery.min.js') }}"></script>
    <script src="{{ asset('js/u_bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/user/u_api_mgr.js') }}"></script>
    <script src="{{ asset('js/u_language.js') }}"></script>

    @stack('script')
</body>
</html>
