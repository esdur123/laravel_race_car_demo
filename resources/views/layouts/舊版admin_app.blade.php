<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/u_bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user/b_user.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/u_language.css') }}">
    <style>
      .hide_overflow::-webkit-scrollbar {display:none}    
          
      .nori_float_nav_01 {
        width: 10%; 
        float: left; 
        height: 40px;
        color: #706760;
      }

      .nori_float_nav_02 {
        width: 10%; 
        float: left; 
        height: 120px;
        color: #706760;
      }

      .nori_pointer_1 {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_up {
        height: 40px;
        cursor: pointer;
      }

      .nori_draw_down_01 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_0.jpg') }}');
      }

      .nori_draw_down_02 {
        height: 40px;
        cursor: pointer;
        background-image: url('{{ asset('img/select_1.jpg') }}');
      }

      #tag_point{
        position: relative;
        cursor: pointer;

        left: 10px; 
        top: 10px;
        color: white;
        background:url('{{ asset('img/button_3.png') }}') no-repeat;
        width:125px;
        height:40px
      }

      #tag_point_span{
        position: relative;
        top:5px;
        left: 8px;
      }

      #tag_login{

        cursor: pointer;
        position:relative;  
        left: 10px; 
        top: 10px;

        background:url('{{ asset('img/button_2.png') }}') no-repeat;
        width:125px;
        height:40px;
      }

      #tag_logout{
        cursor: pointer;
        float: left;
        position:relative;
        left: 10px; 
        top: 10px;

        background:url('{{ asset('img/button_2.png') }}') no-repeat;
        width:125px;
        height:40px
      }

      #tag_logout_span{
        position:relative;
        top:-15px;
        left: 42px;

      }

      #nav_show_account{
        color: #242527;
        position:relative;
        top:4px;
        left:12px;
      }

    .nori_square_01{
      width: 165px;
      height: 165px;
      background-color: #242527;
      display:block;
      margin: auto;
      border-width:3px;
      border-color:#E6B800; 
      border-style:solid;
      border-radius:10px;
      text-align: center;
      color: white;
    }

    .nori_square_01 span{   
      font-size: 12px;
    }      

    #nori_main_content{
      position: relative;
      top: -20px;
    }


    .navbar-nav > li > a{
        color: #706760 !important;
    }

    .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus{
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:white !important;
    } 


    .dropdown-toggle{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/

    }

    .dropdown-menu{
/*      background-image: url('{{ asset('img/color_1.jpg') }}') !important;*/
        background-color: black !important;
    }

    .dropdown-menu > li > a{
        color: #706760 !important;
    }

    .dropdown-menu > li > a:hover {
/*      background-image: url('{{ asset('img/select_1.jpg') }}') !important;*/
      color:white !important;
      background-color: black !important;
    }


    #black_header{
      background-color: black;
      float: left;
    }

    .navbar{
      /*background-image: url('{{ asset('img/color_2.jpg') }}') !important;*/
      background-color: black !important;
      border-style: none !important;

    }

    #inside_navbar_div{
      float:right;
      margin-top: 15px;
    }

    @media only screen and (max-width: 767px) {
        #black_header{
          background-color: #c9bc9c;
          float: none;
        }

        .navbar{
          background-color: #c9bc9c !important;
          border-style: none !important;

        }

        #inside_navbar_div{
          float:none;
          display: block;
        }

        .dropdown-menu{
          background-color: #c9bc9c !important;
        }

        .dropdown-toggle{
          background-color: #c9bc9c !important;
        }
        
        .dropdown-menu > li > a:hover {
          background-color: #c9bc9c !important;
        }        
    }


    </style>

    @stack('head')
</head>
<body style="font-family: Microsoft JhengHei;font-weight: bold; background-image: url('{{ asset('img/background.jpg') }}'">

<div id="app">


  <!-- 主選單 -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">

    <div class="row" style="height: 80px !important;" id="black_header">
      <div class="navbar-header" style="height: 80px !important;">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="position: relative;top: 15px;">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span> 
        </button>
        <div class="navbar-brand">
        <img src="{{ asset('img/logo_ex2.png') }}" style="position: relative; top: -10px;"> 
       </div>
      </div>
    </div>

      <div class="collapse navbar-collapse" id="myNavbar">
        <div id="inside_navbar_div">
        <!-- 登入/點數層 -->

          <!-- 主選單 -->
          <ul class="nav navbar-nav">
<!--             <li id="li_point">
              <div id = "tag_point" >
                <span id="tag_point_span">  </span>
              </div>
            </li> -->
            <li>
              <div id="tag_login" data-toggle="modal" data-target="#admin_login_modal">              
                <span id="nav_show_account"></span>
                <div  >
                  <div style="float: left;">
                    <img id="nav_img_login" src="{{ asset('img/icon_avatar.png') }}" style="position:relative;top:3px;left:15px;">&nbsp;&nbsp;
                  </div>
                  <span id="text_show_login" style="color: white;position:relative;top:5px;left:20px;">{{ Lang::get('user.up_login')}}</span>
                  <div class="clear"></div>
                </div>
              </div>
            </li>           

             <li id="nori_nav_li_01" onclick="pageControl()">
              <a href="#">
               <img id="nav_img_home" src="{{ asset('img/icon_home.png') }}">
               <span >&nbsp;{{ Lang::get('user.home')}}</span>              
              </a>                 
            </li> 

            <li class="dropdown" id="nori_nav_li_02">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
               <img id="nav_img_org" src="{{ asset('img/icon_organization.png') }}">
               <span id = "nori_nav_li_span_02">&nbsp;{{ Lang::get('admin.manager_members')}}</span>
               <img id="nav_img_arrow" src="{{ asset('img/icon_arrow.png') }}">             
              </a>
              <ul class="dropdown-menu">
                <li id="nori_nav_li_02_account" onclick="pageMgrAccount()">
                  <a href="#">
                   <img id="org_account_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('admin.member_account')}}</span>              
                  </a>
                </li>
                <li id="nori_nav_li_02_branch" onclick="pageMgrBranch()">
                  <a href="#">
                   <img id="org_branch_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_branch')}}</span>              
                  </a>                
                </li>
                <li id="nori_nav_li_02_recommend" onclick="pageMgrRecommend()">
                  <a href="#">
                   <img id="org_recommend_circle" src="{{ asset('img/icon_circle.png') }}">
                   <span >&nbsp;{{ Lang::get('user.org_recommend')}}</span>              
                  </a>                
                </li>                
              </ul>
            </li>
 
             <li id="nori_nav_li_06" onclick="pageTrade()">
              <a href="#">
               <img id="nav_img_trade" src="{{ asset('img/icon_shake_hands.png') }}">
               <span >&nbsp;{{ Lang::get('user.system_trade')}}</span>              
              </a>                 
            </li>  

            <li id="nori_nav_li_07"" onclick="pageInfo()">
               <a href="#">
               <img id="nav_img_account" src="{{ asset('img/icon_gear.png') }}">
               <span >&nbsp;{{ Lang::get('admin.admin_setting')}}</span>              
              </a>
            </li>  

            <li >
              <div id="tag_logout" style="z-index: 20;"> 
                <div>
                  <img id="nav_img_login" src="{{ asset('img/icon_exit2.png') }}" style="position:relative;top:4px;left:15px;">&nbsp;&nbsp;
                 </div>
               <span id="tag_logout_span"> {{ Lang::get('user.signout')}} </span>
              </div>                          
            </li>

            <li>
              <div style="height: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
              <div id="div_select_language">
                <select class="form-control" id="id_select_language" >
                  <option selected hidden>{{ Lang::get('lang.choose_language')}}</option>
                  <option value ="en">English</option>
                  <option value ="zh_TW ">繁體中文</option>
                </select>   
              </div>
            </li> 

          </ul>
        </div>
      </div>
    </div>
  </nav>

  <div class="container-fluid" id='nori_main_content'>
      @yield('content') 


  </div> <!-- end container-fluid -->



  <!-- 底部 Copyright-->
  <div class = "nori_footer">
    {{ Lang::get('home.my_site_copyright')}}
  </div>



</div> <!-- end app..-->

  <script src="{{ asset('js/u_defined.js') }}"></script>
  <script src="{{ asset('js/u_jquery.min.js') }}"></script>
  <script src="{{ asset('js/u_bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/admin/u_language.js') }}"></script>
  <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112909986-2"></script> <!-- Analytics -->

  <script type="text/javascript">

      // Analytics
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-112909986-2');

      // 
      $("#nori_nav_li_01").hover(function(){
          $("#nav_img_home").attr("src","{{ asset('img/icon_home2.png') }}");         
      },function(){
          $(this).css("background-image","");
          $("#nav_img_home").attr("src","{{ asset('img/icon_home.png') }}"); 
      });

      $("#nori_nav_li_03").hover(function(){ 
          $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet2.png') }}");         
      },function(){
          $(this).css("background-image","");
          $("#nav_img_wallet").attr("src","{{ asset('img/icon_wallet.png') }}"); 
      });

      $("#nori_nav_li_04").hover(function(){ 
          $("#nav_img_active").attr("src","{{ asset('img/icon_rotate2.png') }}");         
      },function(){
          $("#nav_img_active").attr("src","{{ asset('img/icon_rotate.png') }}"); 
      });

      $("#nori_nav_li_05").hover(function(){
          $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange2.png') }}");         
      },function(){
          $("#nav_img_transform").attr("src","{{ asset('img/icon_exchange.png') }}"); 
      });

      $("#nori_nav_li_06").hover(function(){
          $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands2.png') }}");         
      },function(){
          $("#nav_img_trade").attr("src","{{ asset('img/icon_shake_hands.png') }}"); 
      });

      $("#nori_nav_li_07").hover(function(){
          $("#nav_img_account").attr("src","{{ asset('img/icon_gear2.png') }}");         
      },function(){
          $("#nav_img_account").attr("src","{{ asset('img/icon_gear.png') }}"); 
      });

      $("#nori_nav_li_02").hover(function(){
          $("#nav_img_org").attr("src","{{ asset('img/icon_organization2.png') }}");   
          $("#nori_nav_li_span_02").css("color","white");
          $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow2.png') }}");         
      },function(){
          $("#nav_img_org").attr("src","{{ asset('img/icon_organization.png') }}"); 
          $("#nori_nav_li_span_02").css("color","#706760");
          $("#nav_img_arrow").attr("src","{{ asset('img/icon_arrow.png') }}");         
      });

      $("#nori_nav_li_02_account").hover(function(){
          $("#org_account_circle").attr("src","{{ asset('img/icon_circle2.png') }}");         
      },function(){
          $("#org_account_circle").attr("src","{{ asset('img/icon_circle.png') }}");              
      });

      $("#nori_nav_li_02_branch").hover(function(){
          $("#org_branch_circle").attr("src","{{ asset('img/icon_circle2.png') }}");         
      },function(){
          $("#org_branch_circle").attr("src","{{ asset('img/icon_circle.png') }}");              
      });      

      $("#nori_nav_li_02_recommend").hover(function(){
          $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle2.png') }}");     
      },function(){
          $("#org_recommend_circle").attr("src","{{ asset('img/icon_circle.png') }}");        
      });


      // ready-jobs
      $(function(){
              
        //$("#li_point").hide();  

        $("#tag_point").hide();

        //$("#tag_login").on("click", openLoginModal);
        $("#login").on("submit", Login);
        $("#org_hide").hide();

      })


      // login
      function Login(){
        return false;
      }

  </script>
    
    @stack('script')
</body>
</html>













































