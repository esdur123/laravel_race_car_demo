
@extends('layouts.empty') 




@push('head')

<style type="text/css">
body.loading:before {
  background: #4882C8;
}

.top-bar nav a:hover,
nav.header-nav nav.page-nav ul li a:hover {
  color: #4882C8;
}

.button.primary {
  background: #4882C8;
  border-color: #4882C8;
}

.button.primary:hover,
.button.primary:focus {
  background: #3d6eaa;
  border-color: #3d6eaa;
}

.message.notice {
  background: #4882C8;
}

.account-navigation-item.active a,
.account-navigation-item a:hover {
  border-color: #4882C8;
}

.inline_li >ul>li{
	float: right;
	/*display:inline;*/
	cursor: pointer;
	color: black;

}

#descript_li ul li a{
	text-decoration:underline;
	cursor: pointer;
}

#descript_li ul li {
	line-height:1;
}

.inline_li{
	float: right;
	position: relative;
	top: -70px;
	height: 50px;
	z-index: 100;
	width: 650px;
	border:red 0px solid; 

}

.div_down_main{
	border:pink 0px solid;
	position: relative;
	top: -50px;
}

.div_back_top{
	position: fixed;
	bottom: 70px;
	right: 15px;

}

#img_back_top{
	z-index: 10;
	cursor: pointer;
}

    @media only screen and (max-width: 767px) {
		.inline_li{
			float: right;
			position: relative;
			top: -85px;
			height: 50px;
			z-index: 5;
			width: 400px;
			border:red 0px solid; 

		}

    }

</style>

@endpush




@push('script')

<link rel="stylesheet" href="{{ asset('css/u_getting_start.css') }}">
<script type="text/javascript">

	// ready-jobs
    $(function(){
    	
		$('#img_back_top').click(function(){
			//console.log('top');
			$('html,body').animate({scrollTop : $('#mark_0').offset().top},500);		    
		});

		$('#title_1').click(function(){
			$('html,body').animate({scrollTop : $('#mark_1').offset().top},500);		    
		});

		$('#title_2').click(function(){
			$('html,body').animate({scrollTop : $('#mark_2').offset().top},500);		    
		});

		$('#title_3').click(function(){
			$('html,body').animate({scrollTop : $('#mark_3').offset().top},500);		    
		});

		$('#title_4').click(function(){
			$('html,body').animate({scrollTop : $('#mark_4').offset().top},500);		    
		});				

		$('#title_5').click(function(){
			$('html,body').animate({scrollTop : $('#mark_5').offset().top},500);		    
		});

		$('#title_6').click(function(){
			$('html,body').animate({scrollTop : $('#mark_6').offset().top},500);		    
		});	

		$('#title_7').click(function(){
			$('html,body').animate({scrollTop : $('#mark_7').offset().top},500);		    
		});	

    })


</script>    

@endpush



@section('content')

<!-- 內容 -->
<div id="mark_0" class="container">
	<nav class="header-nav">
	<a class="site-link" href="/user/news" ><h2><img height="25" class="logo" src="https://assets.hostedwiki.co/assets/house-c17a1ff19755a7c07ba16a21ff269db16e04708b5ea73e63d5ada099212c3f81.svg" alt="House" /><span class="site-name">{{ Lang::get('intro.top_title')}}</span></h2></a>
<nav class="right-nav">





	<nav class="page-nav large-nav"></nav></nav></nav>



<div>
	
</div>



<div class="div_down_main" >

	<nav class="page-controls">
		<div class="page-control page-actions"></div>
		<div class="page-control page-meta">Last Updated Sep 03, 2018 at 2:46AM</div>
	</nav>
	
	<br />
	<h4 class="page-title">{{ Lang::get('intro.header_1')}}</h4><div class="page-controls" id="under-title-page-controls"></div>

<div class="content"><h4 id=""></h4>

<div id="descript_li" style="color: black;position: relative;top: -10px;font-size: 16px;">
	<ul>
	　<li><a><span id="title_1">{{ Lang::get('intro.title_1')}}</span><span>&nbsp;</span></a></li>	
	　<li><a><span id="title_2">{{ Lang::get('intro.title_2')}}</span><span>&nbsp;</span></a></li>
	　<li><a><span id="title_3">{{ Lang::get('intro.title_3')}}</span><span>&nbsp;</span></a></li>
	　<li><a><span id="title_4">{{ Lang::get('intro.title_4')}}</span><span>&nbsp;</span></a></li>
	　<li><a><span id="title_5">{{ Lang::get('intro.title_5')}}</span><span>&nbsp;</span></a></li>
	　<li><a><span id="title_6">{{ Lang::get('intro.title_6')}}</span><span>&nbsp;</span></a></li>
	  <br />
	  <li><a><span id="title_7">{{ Lang::get('intro.title_7')}}</span><span>&nbsp;</span></a></li>
	</ul>
	<br />
</div>




	
<hr>
<h1 id="mark_1">{{ Lang::get('intro.title_1')}}</h1>
<p>{{ Lang::get('intro.content_1_1')}}</p>
<p>{{ Lang::get('intro.content_1_2')}}</p>
<p>{{ Lang::get('intro.content_1_3')}}</p>
<p>{{ Lang::get('intro.content_1_4')}}</p>
<hr>
<h1 id="mark_2">{{ Lang::get('intro.title_2')}}</h1>
<p>{{ Lang::get('intro.content_2_1')}}</p>
<p>{{ Lang::get('intro.content_2_2')}}</p>
<p>{{ Lang::get('intro.content_2_3')}}</p>
<hr>
<h1 id="mark_3">{{ Lang::get('intro.title_3')}}</h1>
<p>{{ Lang::get('intro.content_3_1')}}</p>
<p>{{ Lang::get('intro.content_3_2')}}</p>
<p>{{ Lang::get('intro.content_3_3')}}</p>
<p>{{ Lang::get('intro.content_3_4')}}</p>
<p>{{ Lang::get('intro.content_3_5')}}</p>
<p>{{ Lang::get('intro.content_3_6')}}</p>
<hr>
<h1 id="mark_4">{{ Lang::get('intro.title_4')}}</h1>
<p>{{ Lang::get('intro.content_4_1')}}</p>
<p>{{ Lang::get('intro.content_4_2')}}</p>
<p>{{ Lang::get('intro.content_4_3')}}</p>
<p>{{ Lang::get('intro.content_4_4')}}</p>
<p>{{ Lang::get('intro.content_4_5')}}</p>
<p>{{ Lang::get('intro.content_4_6')}}</p>
<hr>
<h1 id="mark_5">{{ Lang::get('intro.title_5')}}</h1><p>{{ Lang::get('intro.content_5')}}</p>
<hr>
<h1 id="mark_6">{{ Lang::get('intro.title_6')}}</h1>
<p>{{ Lang::get('intro.content_6_1')}}</p>
<p>{{ Lang::get('intro.content_6_2')}}</p>
<p>{{ Lang::get('intro.content_6_3')}}</p>
<p>{{ Lang::get('intro.content_6_4')}}</p>
<p>{{ Lang::get('intro.content_6_5')}}</p>
<p>{{ Lang::get('intro.content_6_6')}}</p>
<hr>
<h1 id="mark_7">{{ Lang::get('intro.title_7')}}</h1>
<p>{{ Lang::get('intro.content_7_1')}}</p>
<p>{{ Lang::get('intro.content_7_2')}}</p>
<p>{{ Lang::get('intro.content_7_3')}}</p>
<p>{{ Lang::get('intro.content_7_4')}}</p>
<p>{{ Lang::get('intro.content_7_5')}}</p>



</div>


</div> <!-- end div_down_main -->
</div> <!-- end container -->

<div style="height: 300px;"></div> <!-- 底下留白 -->

<div class="div_back_top">
	<img id="img_back_top" src="{{ asset('img/top_01.png') }}" style="max-width: 80px;max-height: 80px;">
</div> <!-- 返回op -->

@endsection