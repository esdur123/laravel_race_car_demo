

@extends('layouts.user')


@push('head')

        <link rel="stylesheet" href="{{ asset('css/u_google_chart.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <style>
        table {
            border-collapse: inherit;
        }

        .google-visualization-orgchart-node {
            border: inherit !important;
            -webkit-box-shadow: inherit !important;
            -moz-box-shadow: inherit !important;
            background-color: inherit !important;
            background: inherit !important;
        }
    </style>     
@endpush


@push('script')
	<script src="{{ asset('js/u_loader.js') }}"></script>
    <script src="{{ asset('js/u_tree_google_org_chart.js') }}"></script>

    <script>
 	
        var m_branch_account = '{{ $account }}';
		var m_lang_set = "{{ Lang::get('user.set')}}";
		var m_lang_performance_total = "{{ Lang::get('user.performance_total')}}";
		var m_lang_performance_L = "{{ Lang::get('user.performance_L')}}";
		var m_lang_performance_R = "{{ Lang::get('user.performance_R')}}";
		var m_lang_select = "{{ Lang::get('user.select')}}";


		$(document).ready(function() {
		    //getAccount();
		    
		    setTimeout(function(){
		      getBranchSpecific(); // 取得branch列表資訊  
		    },500);

		    $("#div_show_register").hide();

		    $('#select_max').on('change', function() {
		        switchBranch();
		    })

		    showSmallManSet();
			showRegisterSetSelect();

		} );


		// ＊＊ 註冊選擇配套的select - html ＊＊
		function showRegisterSetSelect(){
			var _html = '<select class="form-control" id="select_user_level"><option disabled selected value> -- '+m_lang_select+' --</option><option value="1">'+C_SET_LEVEL_1+'</option><option value="2">'+C_SET_LEVEL_2+'</option><option value="3">'+C_SET_LEVEL_3+'</option></select>';
			$("#select_sets").html(_html); 
		}


		// ＊＊ 板面小人配套顯示 ＊＊
		function showSmallManSet(){
			$("#span_lvl_1").text(C_SET_LEVEL_1);
			$("#span_lvl_2").text(C_SET_LEVEL_2);
			$("#span_lvl_3").text(C_SET_LEVEL_3);
		}


		// ＊＊ 取得branch tree ＊＊
		var m_max_L_id = '';
		var m_max_R_id = '';
		function getBranchSpecific(){

			// alert(m_branch_account);
			// return;
		    p_ApiMgr.getBranchSpecific_by_account(
		      m_branch_account,
		      function (result) {

		      	//alert(result);
		        m_max_L_id = result['max_L_id'];
		        m_max_R_id = result['max_R_id'];

		        if(result['is_user_root'] == 'true'){ // 使用者主頁 && set不為0 才顯示註冊按鈕

		          p_ApiMgr.getSet(
		            function (result) {
		              if(result > 0){ // set不為0
		                $("#div_show_register").show();
		              }
		            },this,
		            function (error) {},this
		          );		        	
		        }
		             
		        m_tree_obj = JSON.parse(result['map']); // 先轉m_tree_obj    

		        // 用callback 呼叫 OrganizationTreeByObjAndDraw
		        google.charts.load('current', {packages:["orgchart"], 'callback': OrganizationTreeByObjAndDraw_Branch}); 
		        m_tree_mapdata = []; // 陣列清空  

		      },this,
		      function (error) {},this
		    );

			return false;
		}



		// ＊＊ 取得account id ＊＊
		function rigisterNew(){

		    // 如果未激活 就不給跳MODAL
		    $('#input_recommend_id').val(p_user_account); // 顯示註冊推薦帳號
		    switchBranch();
		    $('#modal_register').modal('toggle');
		    
		  return false;
		}


		// ＊＊ 切換極左極右的branch顯示 ＊＊
		function switchBranch(){

		    var branch = '';

		    if($('#select_max').val() == 'L'){
		      branch = m_max_L_id+$('#select_max').val();
		    }

		    if($('#select_max').val() == 'R'){
		      branch = m_max_R_id+$('#select_max').val();
		    }

		}


		// ＊＊ 確認註冊 ＊＊
		function confirmRegister(){
		    $("#modal_register .close").click()

		      p_ApiMgr.register(
		        $('#select_max').val(),
		        $('#input_account').val(),
		        $('#input_nickname').val(),
		        $('#select_country_code').val(),
		        $('#input_phone').val(),
		        $('#select_user_level').val(), 
		      function (result) {
		        console.log(result);
		        alert('註冊成功');
		        location.reload();
		      },this,
		      function (error) {},this
		    );

		  return false;
		}

    </script>

@endpush


@section('content')
		
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.org_branch')}}</h2>
		</div>
		<div class="web_map">
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.org_mang')}}
			&nbsp;>&nbsp;{{ Lang::get('user.org_branch')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

	<div class="hide_overflow" style="overflow:auto;">
		<!-- Main -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<!-- 顯示小人配套 -->
		        <div >
<!-- 		        	<i class="fas fa-user" style="font-size:20px;color:gray;">:</i><span style="font-size:18px;font-weight: normal;">{{ Lang::get('user.empty')}}</span>
		        	&nbsp;&nbsp; -->
		        	<i class="fas fa-user" style="font-size:20px;color:#FFAA33;"></i><span id="span_lvl_1" style="font-size:18px;font-weight: normal;"></span>
		        	&nbsp;&nbsp;
		        	<i class="fas fa-user" style="font-size:20px;color:#00FF99;"></i><span id="span_lvl_2" style="font-size:18px;font-weight: normal;"></span>
		        	&nbsp;&nbsp;
		        	<i class="fas fa-user" style="font-size:20px;color:#FFB7DD;"></i><span id="span_lvl_3" style="font-size:18px;font-weight: normal;"></span>
		        </div>
				<br />
		        
		        <div id='div_show_register'>
			        <button type="button" onclick="rigisterNew()" class="btn btn-success">{{ Lang::get('user.register_new')}}</button>
		        </div>

				<!-- 組織圖 -->
		        <div id="show_tree_div">	        	
		        </div>


			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br /><br /><br /><br />
			</div>
		</div>

	</div> <!-- end overflow -->


	<!-- Modal 註冊 -->
	<div class="modal fade" id="modal_register" role="dialog">
		<div class="modal-dialog" style="max-width: 98vw;">

		  <div class="modal-content">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
		      <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.add_account')}}</h5>		     
		    </div>
		    <div class="modal-body">
		      	
				<div class="form-group">					
					<label for="input_recommend_id">{{ Lang::get('user.recommend_acc')}}</label>
					<input type="text" class="form-control" id="input_recommend_id"  placeholder="{{ Lang::get('user.recommend_acc')}}" disabled>
					
					<br />
					<label for="select_max">{{ Lang::get('user.set_branch')}}</label>
						<select class="form-control" id="select_max">
						<option value="L">{{ Lang::get('user.max_l')}}</option>
						<option value="R">{{ Lang::get('user.max_r')}}</option>
					</select>

					<br />
					<label for="input_account">{{ Lang::get('user.account')}}</label>
					<input type="text" class="form-control" id="input_account"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

					<br />
					<label for="input_nickname">{{ Lang::get('user.nickname')}}</label>
					<input type="text" class="form-control" id="input_nickname"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

					<br />
					<label for="select_country_code">{{ Lang::get('user.country_code')}}</label>
					<select class="form-control" id="select_country_code">
						@include('components.country_code')
					</select>

					<br />
					<label for="input_phone">{{ Lang::get('user.phone')}}</label>
					<input type="text" class="form-control" id="input_phone"  placeholder="09xxxxxxxx" maxlength="10">

					<br />
					<label for="select_user_level">{{ Lang::get('user.set')}}</label>
					<div id = "select_sets"></div>
					<br />
				</div>

		    </div> 

		    <div class="modal-footer">
              <button type="button" class="btn btn-success" onclick="confirmRegister()">{{ Lang::get('user.confirm')}}</button>&nbsp;
		      <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
		    </div>
		  </div>
		  
		</div>
	</div>


@endsection