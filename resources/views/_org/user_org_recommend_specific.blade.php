

@extends('layouts.user')


@push('head')

        <link rel="stylesheet" href="{{ asset('css/u_google_chart.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <style>
        table {
            border-collapse: inherit;
        }

        .google-visualization-orgchart-node {
            border: inherit !important;
            -webkit-box-shadow: inherit !important;
            -moz-box-shadow: inherit !important;
            background-color: inherit !important;
            background: inherit !important;
        }
    </style>        
@endpush


@push('script')
	<script src="{{ asset('js/u_loader.js') }}"></script>
    <script src="{{ asset('js/u_tree_google_org_chart.js') }}"></script>

    <script>

		var m_lang_set = "{{ Lang::get('user.set')}}";
		var m_lang_performance_total = "{{ Lang::get('user.performance_total')}}";		
		var m_lang_performance_L = "{{ Lang::get('user.performance_L')}}";
		var m_lang_performance_R = "{{ Lang::get('user.performance_R')}}";
		var m_lang_select = "{{ Lang::get('user.select')}}";

        var m_branch_account = '{{ $account }}';
		$(document).ready(function() {

		    $("#div_show_register").hide();
			showRegisterSetSelect(); // modal的配套
			
			$("#small_man_lv1").text(C_SET_LEVEL_1);
			$("#small_man_lv2").text(C_SET_LEVEL_2);

		    setTimeout(function(){
		          getRecommenderSpecific();
		    },500);

		 });


		// ＊＊ 取得branch tree ＊＊
		function getRecommenderSpecific(){

		    p_ApiMgr.getRecommenderSpecific_by_account(
		      m_branch_account,
		      function (result) {
		        //console.log(result['tree']); 

		        m_tree_obj = JSON.parse(result['tree']); // 先轉m_tree_obj
		        if(p_user_account == m_tree_obj[0].account){
					$("#div_show_register").show();
		        }

		        // // 用callback 呼叫 OrganizationTreeByObjAndDraw_Recommend
		        google.charts.load('current', {packages:["orgchart"], 'callback': OrganizationTreeByObjAndDraw_Recommend}); 
		        m_tree_mapdata = []; // 陣列清空  

		      },this,
		      function (error) {},this
		    );

			return false;
		}




    </script>

@endpush


@section('content')
		
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.org_recommend')}}</h2>
		</div>
		<div class="web_map">
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.org_mang')}}
			&nbsp;>&nbsp;{{ Lang::get('user.org_recommend')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


<div class="hide_overflow" style="overflow:auto;"> 
	<!-- Main -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!-- 顯示小人配套 -->
	        <div >        	
<!-- 	        	<i class="fas fa-user" style="font-size:20px;color:#FFAA33;"></i><span style="font-size:18px;font-weight: normal;">C_SET_LEVEL_1</span>
	        	&nbsp;&nbsp; -->
	        	<i class="fas fa-user" style="font-size:20px;color:#77DDFF;"></i><span id="small_man_lv1" style="font-size:18px;font-weight: normal;"></span>
	        	&nbsp;&nbsp;
	        	<i class="fas fa-user" style="font-size:20px;color:#FFB7DD;"></i><span id="small_man_lv2" style="font-size:18px;font-weight: normal;"></span>
	        </div>
			<br />

			<!-- 註冊 -->
		    <div id='div_show_register'>
		        <button type="button" onclick="rigisterNew()" class="btn btn-success">{{ Lang::get('user.register_new')}}</button>
		        <button type="button" onclick="rigisterBall()" class="btn btn-warning">{{ Lang::get('user.register_new_ball')}}</button>

		    </div>

			<!-- 組織圖 -->
			<div class="nori_overflow">
	        	<div id="show_tree_div"></div>
	        </div>


		</div>

	</div>


</div>

<!-- 底部留高度150 -->
<div style="height: 150px;">
	
</div>


	<!-- Modal 註冊 -->
	<div class="modal fade" id="modal_register" role="dialog">
		<div class="modal-dialog" style="max-width: 98vw;">

		  <div class="modal-content">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
		      <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.add_account')}}</h5>		     
		    </div>
		    <div class="modal-body">
		      	
				<div class="form-group">					
					<label for="input_recommend_id">{{ Lang::get('user.recommend_acc')}}</label>
					<input type="text" class="form-control" id="input_recommend_id"  placeholder="{{ Lang::get('user.recommend_acc')}}" disabled>

					<br />
					<label for="input_account">{{ Lang::get('user.account')}}</label>
					<input type="text" class="form-control" id="input_account"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

					<br />
					<label for="input_nickname">{{ Lang::get('user.nickname')}}</label>
					<input type="text" class="form-control" id="input_nickname"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

					<br />
					<label for="select_country_code">{{ Lang::get('user.country_code')}}</label>
					<select class="form-control" id="select_country_code">
						@include('components.country_code')
					</select>

					<br />
					<label for="input_phone">{{ Lang::get('user.phone')}}</label>
					<input type="text" class="form-control" id="input_phone"  placeholder="09xxxxxxxx" maxlength="10">

					<br />
					<label for="select_user_level">{{ Lang::get('user.set')}}</label>
					<div id = "select_sets"></div>
					<br />
				</div>

		    </div> 

		    <div class="modal-footer">
              <button type="button" class="btn btn-success" onclick="confirmRegister()">{{ Lang::get('user.confirm')}}</button>&nbsp;
		      <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
		    </div>
		  </div>
		  
		</div>
	</div>




	<!-- Modal 球 -->
	<div class="modal fade" id="modal_ball" role="dialog">
		<div class="modal-dialog" style="max-width: 98vw;">

		  <div class="modal-content">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
		      <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.register_new_ball')}}</h5>		     
		    </div>
		    <div class="modal-body">
		      	
				<div class="form-group">					

					<label for="input_ball_account">{{ Lang::get('user.account')}}</label>
					<input type="text" class="form-control" id="input_ball_account"  placeholder="{{ Lang::get('user.account')}}" maxlength="16" disabled>
				

					<br />
					<label for="select_ball_sets">{{ Lang::get('user.set')}}</label>
					<div id = "select_ball_sets"></div>

					<br />
					<label for="input_ball_amount">{{ Lang::get('user.amount')}}</label>
					<input type="text" class="form-control" id="input_ball_amount" value="1"  maxlength="2" >					
				</div>

		    </div>

		    <div id="div_btns" class="modal-footer">
              <button type="button" class="btn btn-success" onclick="confirmNewBall()">{{ Lang::get('user.confirm')}}</button>&nbsp;
		      <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
		    </div>
		  </div>
		  
		</div>
	</div>




@endsection