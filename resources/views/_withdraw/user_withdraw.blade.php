
@extends('layouts.user') 




@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush




@push('script')
<script src="{{ asset('js/u_my_utility.js') }}"></script>
<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
<script>
	var m_lang_txn_out_transform = "{{ Lang::get('user.txn_out_transform')}}";
	var m_lang_txn_in_transform = "{{ Lang::get('user.txn_in_transform')}}";
	var m_lang_txn_out_active = "{{ Lang::get('user.txn_out_active')}}";	
	var m_lang_txn_in_pv = "{{ Lang::get('user.txn_in_pv')}}";
	var m_lang_txn_in_static = "{{ Lang::get('user.txn_in_static')}}";
	var m_lang_txn_in_direct_push_bonus = "{{ Lang::get('user.txn_in_direct_push_bonus')}}";
	var m_lang_txn_failed_pv_full = "{{ Lang::get('user.txn_failed_pv_full')}}";
	var m_lang_txn_failed_static_out = "{{ Lang::get('user.txn_failed_static_out')}}";
	var m_lang_txn_failed_static_auto_active_out = "{{ Lang::get('user.txn_failed_static_auto_active_out')}}";
	var m_lang_txn_out_static_reactive = "{{ Lang::get('user.txn_out_static_reactive')}}";
	var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
	var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
	var m_lang_txn_out_admin_recycle_point = "{{ Lang::get('user.txn_out_admin_recycle_point')}}";
	var m_lang_txn_out_market_sell = "{{ Lang::get('user.txn_out_market_sell')}}";
	var m_lang_txn_in_market_buy = "{{ Lang::get('user.txn_in_market_buy')}}";
	var m_lang_txn_in_tx_atm_buy = "{{ Lang::get('user.txn_in_tx_atm_buy')}}";
    var m_lang_txn_out_static_fee = "{{ Lang::get('user.txn_out_static_fee')}}";
    var m_lang_txn_out_static_inactive = "{{ Lang::get('user.txn_out_static_inactive')}}";
    var m_lang_txn_out_lottery_9blocks_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_bet')}}";
    var m_lang_txn_in_lottery_9blocks_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_bet')}}";
    var m_lang_txn_out_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_10times_bet')}}";
    var m_lang_txn_in_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_10times_bet')}}";
    var m_lang_txn_in_color_tx_atm_buy = "{{ Lang::get('user.txn_in_color_tx_atm_buy')}}";    
    var m_lang_txn_in_admin_deposit_point = "{{ Lang::get('user.txn_in_admin_deposit_point')}}"; 
    var m_lang_txn_in_admin_deposit_colorpoint = "{{ Lang::get('user.txn_in_admin_deposit_colorpoint')}}";
    var m_lang_txn_in_static_10k_bonus = "{{ Lang::get('user.txn_in_static_10k_bonus')}}";
    // generation
    var m_lang_txn_in_direct_push_bonus_generation_1 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_1')}}"; 
    var m_lang_txn_in_direct_push_bonus_generation_2 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_2')}}";
    var m_lang_txn_in_direct_push_bonus_generation_3 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_3')}}";

	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

    var m_lang_withdraw_result_waiting      = "{{ Lang::get('user.withdraw_result_waiting')}}";
    var m_lang_withdraw_result_deal         = "{{ Lang::get('user.withdraw_result_deal')}}";
    var m_lang_withdraw_result_user_cancel  = "{{ Lang::get('user.withdraw_result_user_cancel')}}";
    var m_lang_withdraw_result_admin_cancel = "{{ Lang::get('user.withdraw_result_admin_cancel')}}";
    var m_lang_withdraw_result_accepting = "{{ Lang::get('user.withdraw_result_accepting')}}";    
    var m_lang_cancel_withdraw = "{{ Lang::get('user.cancel_withdraw')}}";




// Fee-Daily-Static

	$(document).ready(function() {
        

        setTimeout(function(){     
            get_withdraw_orders();       
            //checkIsAvailableWithdraw();
        },200);

	});


    // // ＊＊ 檢查現在給不給申請提領 ＊＊        
    // function checkIsAvailableWithdraw(){
        
    //     p_ApiMgr.checkIsWithdrawAvailable(                         
    //         function (result) {                 

    //             if(result['is_tagged'] == '0'){
    //                 get_withdraw_orders();
    //             } else {
    //                 location.href = "/user/news";
    //             }

    //         },this,
    //         function (error) {},this
    //     );
    // }



//cancelWithdraw   //cancel_withdraw
    var m_cancel_id = 0;
    function cancel_withdraw(obj){
        m_cancel_id = $(obj).attr("uid");
        $('#cancelModal').modal('toggle');        
        // alert(m_cancel_id);  cancelModal
    }



    // ＊＊ 撤銷確認＊＊
    function confirm_cancel(){ 

      $('#cancelModal').modal('hide');

      p_ApiMgr.cancelWithdraw(
            m_cancel_id,
            function (result) {         

                //console.log(result);
                $('#table_orders').DataTable().destroy();                
                get_withdraw_orders();                
                alert('已撤銷');

            },this,
            function (error) {

            },this
      );

      
      return false;
    }  




    // ＊＊ 買進視窗＊＊
    function openwithdrawModal(){

      if($("#input_withdraw").val() == ''){
        return false;
      }

      $("#span_withdrow_amount").text($("#input_withdraw").val());
      $('#withdrawModal').modal('toggle');

    }


    // ＊＊ 買進確認＊＊
    function confirm_withdraw(){ 

      $('#withdrawModal').modal('hide');

      p_ApiMgr.applyFowWithdraw(
            $("#input_withdraw").val(),
            function (result) {         

                console.log(result);
                // 看能不能刷新表格

                alert('已提交');
                $('#table_orders').DataTable().destroy();                
                get_withdraw_orders();

            },this,
            function (error) {

            },this
      );

      $("#input_withdraw").val(''); // input數值歸0
      return false;
    }    



    // ＊＊ 取得委託列表 ＊＊
    var m_order_datatable = null;
    function get_withdraw_orders(){



        var ajax_url = '/api/v1/withdraw/info';  // TransactionController@getOrder
        m_order_datatable = $('#table_orders').DataTable({
                 "language": {
                      "info":m_lang_info,
                      "sLengthMenu": m_lang_show_n_result,
                      "search": m_lang_keyword,  
                      "processing": m_lang_processing,
                      "loadingRecords": m_lang_loadingRecords,
                      "zeroRecords": m_lang_show_zeroRecords,
                      "infoEmpty": m_lang_infoEmpty,
                      "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },
                "processing": true,
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,     
                "destroy": true,                    
                "columns": [
               
                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {

                            var datatime = data.date;
                            datatime = datatime.toString();
                            datatime = datatime.substring(0, datatime.length-7);
                            return datatime;
                        }
                    },
                    {
                        'data': 'amount',
                        'render': function (data, type, row, meta) {
                            return data;
                        }
                    },
                    {
                        'data': 'amount',
                        'render': function (data, type, row, meta) {
                            return '10';
                        }
                    },
                    {
                        'data': 'amount',
                        'render': function (data, type, row, meta) {
                             data -= 10;
                            return data;
                        }
                    },                                     
                    {
                        'data': 'state',
                        'render': function (data, type, row, meta) {

                            var datatime = row['updated_at'].date;
                            datatime = datatime.toString();
                            datatime = datatime.substring(0, datatime.length-7);

                            if(data == 'Waiting') {
                                data = m_lang_withdraw_result_waiting;
                            }

                            if(data == 'Deal') {
                                data = '<span style="color:blue;">'+m_lang_withdraw_result_deal+'</span><br />'+datatime;
                            }

                            if(data == 'User-Canceled') {
                                data = m_lang_withdraw_result_user_cancel+'<br />'+datatime;
                            }

                            if(data == 'Admin-Canceled') {
                                data = m_lang_withdraw_result_admin_cancel;
                            }

                            if(data == 'Accepting') {
                                data = m_lang_withdraw_result_accepting;
                            }


                            return data;
                        }
                    },
                    {
                        'data': 'state',
                        'render': function (data, type, row, meta) {


                            if(data == 'Waiting') {
                                data = '<button type="button" class="btn btn-danger" uid="'+row['id']+'" onclick="cancel_withdraw(this)">'+m_lang_cancel_withdraw+'</button>'
                            }

                            if(data == 'Deal') {
                                data = '';//m_lang_withdraw_result_deal;
                            }

                            if(data == 'User-Canceled') {
                                data = '';//m_lang_withdraw_result_user_cancel;
                            }

                            if(data == 'Admin-Canceled') {
                                data = '';//m_lang_withdraw_result_admin_cancel;
                            }

                            if(data == 'Accepting') {
                                data = '';
                            }                            

                            return data;
                        }
                    }                                     

                ],
                "serverSide": true,
                "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
        })



      return false;
    }






</script>



@endpush



@section('content')
		
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.page_withdraw')}}</h2>
		</div>
		<div class="web_map">
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.page_withdraw')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<div class="container"> 
    <div class="hide_overflow" style="overflow:auto;"> 
    	<div class="row" id="main_row">



    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">             
        <div style="background-color: #13b5b1;color: white;">
            <br />
            <h3>&nbsp;&nbsp;&nbsp;&nbsp;{{ Lang::get('user.page_withdraw')}}</h3>
            <div class="form-group">
              <input type="text" class="form-control" id="input_withdraw" placeholder="{{ Lang::get('user.apply_amount')}}" style="width: 90%;display: block;margin: auto;" maxlength="9">
            </div>
            <button type="button" class="btn btn-warning" style="width: 90%;display: block;margin:auto;background-color:#f27a3d!important; " onclick="openwithdrawModal()">
                {{ Lang::get('user.apply_for_withdraw')}}
            </button>
            <br />          
        </div>

        <div>
            &nbsp;
        </div>      
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>

		</div>
	</div>
</div>


    <!-- Order DataTable -->
    <br />
    <div class="container">
        <h2>{{ Lang::get('user.my_apply')}}</h2>
        <div class="hide_overflow" style="overflow:auto;"> 
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">           
                <table id="table_orders" class="display" style="width:100%">
                    <thead>
                        <tr>
                           
                            <th>{{ Lang::get('user.datetime')}}</th>                        
                            <th>{{ Lang::get('user.point')}}</th>
                            <th>{{ Lang::get('user.fee')}}</th>
                            <th>{{ Lang::get('user.true_get')}}</th>                                                        
                            <th>{{ Lang::get('user.state')}}</th> 
                            <th>{{ Lang::get('user.table_options')}}</th>                               
                            <!-- 已配對完 撤回 -->
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>                                                        
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">           
                <br /><br /><br /><br /><br />
            </div>
          </div>
        </div>
    </div>









<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<br /><br /><br /><br /><br />
	</div>
</div>




<!-- withdraw Modal -->
<div id="withdrawModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.page_withdraw')}}</h4>
      </div>

      <div class="modal-body">
        <p><span>{{ Lang::get('user.apply_amount')}}&nbsp;</span><span id ="span_withdrow_amount" style="color: blue;"></span></p>
      
        <p style="color: red;">{{ Lang::get('user.confirm_withdraw_warning')}}</p>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-success" onclick="confirm_withdraw()">{{ Lang::get('user.confirm')}}</button>    
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- Buy Modal -->
<div id="cancelModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.withdraw_result_user_cancel')}}</h4>
      </div>

      <div class="modal-body">
        <p><span>{{ Lang::get('user.confirm_cancel_withdraw')}}&nbsp;</span><span style="color: blue;"></span></p>
            
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-success" onclick="confirm_cancel()">{{ Lang::get('user.confirm')}}</button>    
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection