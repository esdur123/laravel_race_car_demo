
@extends('layouts.user') 




@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush




@push('script')
<script src="{{ asset('js/u_my_utility.js') }}"></script>
<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
<script>
	var m_lang_txn_out_transform = "{{ Lang::get('user.txn_out_transform')}}";
	var m_lang_txn_in_transform = "{{ Lang::get('user.txn_in_transform')}}";
	var m_lang_txn_out_active = "{{ Lang::get('user.txn_out_active')}}";	
	var m_lang_txn_in_pv = "{{ Lang::get('user.txn_in_pv')}}";
	var m_lang_txn_in_static = "{{ Lang::get('user.txn_in_static')}}";
	var m_lang_txn_in_direct_push_bonus = "{{ Lang::get('user.txn_in_direct_push_bonus')}}";
	var m_lang_txn_failed_pv_full = "{{ Lang::get('user.txn_failed_pv_full')}}";
	var m_lang_txn_failed_static_out = "{{ Lang::get('user.txn_failed_static_out')}}";
	var m_lang_txn_failed_static_auto_active_out = "{{ Lang::get('user.txn_failed_static_auto_active_out')}}";
	var m_lang_txn_out_static_reactive = "{{ Lang::get('user.txn_out_static_reactive')}}";
	var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
	var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
	var m_lang_txn_out_admin_recycle_point = "{{ Lang::get('user.txn_out_admin_recycle_point')}}";
	var m_lang_txn_out_market_sell = "{{ Lang::get('user.txn_out_market_sell')}}";
	var m_lang_txn_in_market_buy = "{{ Lang::get('user.txn_in_market_buy')}}";
	var m_lang_txn_in_tx_atm_buy = "{{ Lang::get('user.txn_in_tx_atm_buy')}}";
    var m_lang_txn_out_static_fee = "{{ Lang::get('user.txn_out_static_fee')}}";
    var m_lang_txn_out_static_inactive = "{{ Lang::get('user.txn_out_static_inactive')}}";
    var m_lang_txn_out_lottery_9blocks_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_bet')}}";
    var m_lang_txn_in_lottery_9blocks_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_bet')}}";
    var m_lang_txn_out_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_10times_bet')}}";
    var m_lang_txn_in_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_10times_bet')}}";
    var m_lang_txn_in_color_tx_atm_buy = "{{ Lang::get('user.txn_in_color_tx_atm_buy')}}";    
    var m_lang_txn_in_admin_deposit_point = "{{ Lang::get('user.txn_in_admin_deposit_point')}}"; 
    var m_lang_txn_in_admin_deposit_colorpoint = "{{ Lang::get('user.txn_in_admin_deposit_colorpoint')}}";
    var m_lang_txn_in_static_10k_bonus = "{{ Lang::get('user.txn_in_static_10k_bonus')}}";
    // generation
    var m_lang_txn_in_direct_push_bonus_generation_1 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_1')}}"; 
    var m_lang_txn_in_direct_push_bonus_generation_2 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_2')}}";
    var m_lang_txn_in_direct_push_bonus_generation_3 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_3')}}";
    var m_lang_txn_in_direct_push_bonus_generation_4 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_4')}}";    
    var m_lang_txn_out_user_withdraw = "{{ Lang::get('user.txn_out_user_withdraw')}}"; 

    var m_lang_txn_failed_direct_push_bonus_generation_1_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_1_out')}}";
    var m_lang_txn_failed_direct_push_bonus_generation_2_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_2_out')}}";
    var m_lang_txn_failed_direct_push_bonus_generation_3_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_3_out')}}";
    var m_lang_txn_failed_direct_push_bonus_generation_4_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_4_out')}}";

    var m_lang_txn_in_daily_reward_100k = "{{ Lang::get('user.txn_in_daily_reward_100k')}}"; 
    var m_lang_txn_in_daily_reward_200k = "{{ Lang::get('user.txn_in_daily_reward_200k')}}";
    var m_lang_txn_in_daily_reward_300k = "{{ Lang::get('user.txn_in_daily_reward_300k')}}";
    var m_lang_txn_in_daily_reward_400k = "{{ Lang::get('user.txn_in_daily_reward_400k')}}"; 

    var m_lang_txn_out_daily_reward_out = "{{ Lang::get('user.txn_out_daily_reward_out')}}"; 



	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

    var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
    var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
    var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
    var m_lang_info = "{{ Lang::get('table.info')}}";



// Fee-Daily-Static

	$(document).ready(function() {

        $("#div_colorpoint").hide();

        setTimeout(function(){
            GetWalletInfo();        // 取得錢包Point資訊
            if(p_is_colorpoint == 1){
                $("#div_colorpoint").show();
                // 因為首頁會在檢查彩點點數的時候, (null時)強迫創建colorpoint的錢包, 所以這頁是安全的
                GetColorwalletInfo();   // 取得彩點錢包ColorPoint資訊
            }

        },500);


	} );


    // // ＊＊ 取得 彩點錢包資訊 ＊＊
    // function GetColorwalletInfo(){
 
    //     var ajax_url = '/api/v1/wallet/colorpoint/info';

    //     $('#table_color_point').DataTable({
    //              "language": {
    //                 "info":m_lang_info,
    //                 "sLengthMenu": m_lang_show_n_result,
    //                 "search": m_lang_keyword,    
    //                 "processing": m_lang_processing,
    //                 "loadingRecords": m_lang_loadingRecords,
    //                 "zeroRecords": m_lang_show_zeroRecords,
    //                 "infoEmpty": m_lang_infoEmpty,
    //                 "infoPostFix": "",
    //                  "oPaginate": {
    //                    "sFirst": m_lang_first_page,
    //                    "sPrevious": m_lang_pre_page,
    //                    "sNext": m_lang_next_page,
    //                    "sLast": m_lang_last_page,
    //                  },
    //              },            
    //             "lengthChange": false,
    //             "pagingType": "simple_numbers",
    //             "searching": false,
    //             "columns": [
    //                 {

    //                     'data': 'type',
    //                     'render': function (data, type, row, meta) {
    //                         var return_type = data;
    //                         if(return_type == 'TX-ATM-Buy-Color-Point'){
    //                             return_type = m_lang_txn_in_color_tx_atm_buy;
    //                         }

    //                         if(return_type == 'Gift-From'){
    //                             return_type = m_lang_txn_out_transform;
    //                         }

    //                         if(return_type == 'Gift-To'){
    //                             return_type = m_lang_txn_in_transform;
    //                         }           
                            
    //                         if(return_type == 'Active-Set'){
    //                             return_type = m_lang_txn_out_active;
    //                         }                                             

    //                         if(return_type == 'Static-reActive'){
    //                             return_type = m_lang_txn_out_static_reactive;
    //                         } 

    //                         if(return_type == 'Auto-reActive'){
    //                             return_type = m_lang_txn_out_static_auto_reactive;
    //                         } 
                          
    //                         if(return_type == 'Admin-Deposit-Color-Point'){
    //                             return_type = m_lang_txn_in_admin_deposit_colorpoint;
    //                         }

    //                         if(return_type == 'Admin-Deposit-Point'){
    //                             return_type = m_lang_txn_in_admin_deposit_point;
    //                         }                            

    //                         return return_type;
    //                     }
    //                 },
    //                 {
    //                     'data': 'value',
    //                     'render': function (data, type, row, meta) {

    //                         data = toDecimal2(data);
    //                         return data;
    //                     }
    //                 },
    //                 {
    //                     'data': 'after',
    //                     'render': function (data, type, row, meta) {

    //                         data = toDecimal2(data);
    //                         return data;
    //                     }
    //                 },
    //                 {
    //                     'data': 'link',
    //                     'render': function (data, type, row, meta) {

    //                         if(row['type'] == 'Gift-To'){
    //                             data = ''
    //                             return data;
    //                         }

    //                         var return_val = '';

    //                         if(data){
    //                             var  m_tree_obj = JSON.parse(data)
    //                             if(m_tree_obj.comment != null){
    //                                 return_val = m_tree_obj.comment;
    //                             }
    //                         }

    //                         var myarr = return_val.split(",");
    //                         new_url = '';
    //                         if(myarr.length > 1){
    //                             var new_url = '<a href="/user/org/branch/'+myarr[0].toString()+'" >'+myarr[0].toString()+'</a>';
    //                         }

    //                         return new_url;

    //                     }
    //                 }

    //             ],
    //             "serverSide": true,
    //             "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
    //     })


    //     return false;
    // }


    // ＊＊ 取得錢包資訊 ＊＊
	function GetWalletInfo(){
 
		var ajax_url = '/api/v1/wallet/point/info';

        $('#table_points').DataTable({
                 "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,    
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },            
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "columns": [

                    // {
                    //     'data': 'created_at',
                    //     'render': function (data, type, row, meta) {
                    //         var datatime = data.date;
                    //         datatime = datatime.toString();
                    //         datatime = datatime.substring(0, datatime.length-7);
                    //         return datatime;
                    //     }
                    // },
                    // {
                    //     'data': 'id',
                    //     'render': function (data, type, row, meta) {
                    //         data = '#'+data;
                    //         return data;
                    //     }
                    // },
                    {

                        'data': 'type',
                        'render': function (data, type, row, meta) {
                            var return_type = data;
                            if(return_type == 'Gift-From'){
                                return_type = m_lang_txn_out_transform;
                            }

                            if(return_type == 'Gift-To'){
                                return_type = m_lang_txn_in_transform;
                            }

                            if(return_type == 'Active-Set'){
                                return_type = m_lang_txn_out_active;
                            }
                           
                            if(return_type == 'pvBonus'){
                                return_type = m_lang_txn_in_pv;
                            }

                            if(return_type == 'pvBonus-Full'){
                                return_type = m_lang_txn_failed_pv_full;
                            }   

                            if(return_type == 'Daily-Static'){
                                return_type = m_lang_txn_in_static;
                            }

                            if(return_type == 'Daily-Static-Out'){
                                return_type = m_lang_txn_failed_static_out;
                            }

                            if(return_type == 'Reactive-Failed'){
                                return_type = m_lang_txn_failed_static_auto_active_out;
                            }

                            if(return_type == 'Direct-Push-Bonus'){
                                return_type = m_lang_txn_in_direct_push_bonus;
                            }                            
                            
                            if(return_type == 'Static-reActive'){
                                return_type = m_lang_txn_out_static_reactive;
                            }  

                            if(return_type == 'Auto-reActive'){
                                return_type = m_lang_txn_out_static_auto_reactive;
                            }  

                            if(return_type == 'Admin-Recycle-Point'){
                                return_type = m_lang_txn_out_admin_recycle_point;
                            }  

                            if(return_type == 'Market-Sell'){
                                return_type = m_lang_txn_out_market_sell;
                            }  
                            
                            if(return_type == 'Market-Buy'){
                                return_type = m_lang_txn_in_market_buy;
                            }  

                            if(return_type == 'TX-ATM-Buy'){
                                return_type = m_lang_txn_in_tx_atm_buy;
                            } 

                            if(return_type == 'Fee-Daily-Static'){
                                return_type = m_lang_txn_out_static_fee;
                            }               

                            if(return_type == 'Daily-Static-Inactived'){
                                return_type = m_lang_txn_out_static_inactive;
                            }  

                            if(return_type == 'Lottery-9Blocks-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_bet;
                            }  

                            if(return_type == 'Lottery-9Blocks-10times-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_10times_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-10times-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_10times_bet;
                            } 

                            if(return_type == 'Admin-Deposit-Point'){
                                return_type = m_lang_txn_in_admin_deposit_point;
                            }

                            if(return_type == 'Daily-Static-10K-Bonus'){
                                return_type = m_lang_txn_in_static_10k_bonus;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-1'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_1;
                            } 

                            if(return_type == 'Direct-Push-Bonus-Generation-2'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_2;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-3'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_3;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-4'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_4;
                            }                            

                            if(return_type == 'User-Withdraw'){
                                return_type = m_lang_txn_out_user_withdraw;
                            } 

                            if(return_type == 'Direct-Push-Bonus-Generation-1-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_1_out;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-2-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_2_out;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-3-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_3_out;
                            }                            

                            if(return_type == 'Direct-Push-Bonus-Generation-4-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_4_out;
                            } 

                            if(return_type == 'Performance-100K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_100k;
                            }

                            if(return_type == 'Performance-200K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_200k;
                            }

                            if(return_type == 'Performance-300K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_300k;
                            }                            

                            if(return_type == 'Performance-400K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_400k;
                            } 

                            if(return_type == 'Daily-Reward-Out'){
                                return_type = m_lang_txn_out_daily_reward_out;
                            }                         



                            return return_type;
                        }
                    },
                    {
                        'data': 'value',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'after',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'link',
                        'render': function (data, type, row, meta) {

                            if(row['type'] == 'Gift-To'){
                                data = ''
                                return data;
                            }
                            
                            var return_val = '';

                            if(data){
                                var  m_tree_obj = JSON.parse(data)
                                if(m_tree_obj.comment != null){
                                    return_val = m_tree_obj.comment;
                                }
                            }

                            var myarr = return_val.split(",");
                            new_url = '';
                            if(myarr.length > 1){
                                var new_url = '<a href="/user/org/recommend/'+myarr[0].toString()+'" >'+myarr[0].toString()+'</a>';
                            }

                            return new_url;

                        }
                    }

                ],
                "serverSide": true,
                "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
        })


		return false;
	}


</script>



@endpush



@section('content')
		
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.wallet_info')}}</h2>
		</div>
		<div class="web_map">
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.wallet_info')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<div class="container"> 
    <div class="hide_overflow" style="overflow:auto;"> 
    	<div class="row">
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		

			    <h2>{{ Lang::get('user.point')}}</h2>
		        <table id="table_points" class="display" style="width:100%">
		            <thead>
		                <tr>
<!-- 		                    <th>{{ Lang::get('user.datetime')}}</th>	                	
		                    <th>{{ Lang::get('user.single_num')}}</th> -->
		                    <th>{{ Lang::get('user.content')}}</th>
		                    <th>{{ Lang::get('user.value_point')}}</th>
		                    <th>{{ Lang::get('user.balance')}}</th>
		                    <th>{{ Lang::get('user.comment')}}</th>
		                </tr>
		            </thead>
		                    <tbody>
		                        <tr>
<!-- 		                        	<td></td>
		                            <td></td> -->
		                            <td></td>
		                            <td></td>
		                            <td></td>
		                            <td></td>
		                        </tr>
		                    </tbody>
		        </table>


			</div>

            


            
            <div id="div_colorpoint" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                  
            <br /><br />

                <h2>{{ Lang::get('user.color_point')}}</h2>
                <table id="table_color_point" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>{{ Lang::get('user.content')}}</th>
                            <th>{{ Lang::get('user.value_point')}}</th>
                            <th>{{ Lang::get('user.balance')}}</th>
                            <th>{{ Lang::get('user.comment')}}</th>
<!--                             <th>{{ Lang::get('user.balance')}}</th>
                            <th>{{ Lang::get('user.comment')}}</th> -->
                        </tr>
                    </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
<!--                                     <td></td>
                                    <td></td> -->
                                </tr>
                            </tbody>
                </table>


            </div>







		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<br /><br /><br /><br /><br />
	</div>
</div>


@endsection