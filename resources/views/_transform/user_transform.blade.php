
@extends('layouts.user') 

@push('head')
	    
@endpush

@push('script')
	<script src="{{ asset('js/u_my_utility.js') }}"></script>
	<script type="text/javascript">
		
		
		// ＊＊ ready-jobs ＊＊
		$(function(){
			
        	$("#div_colorpoint").hide();
		   	getPoint();

   	        setTimeout(function(){
				if(p_is_colorpoint == 1){	
				$("#div_colorpoint").show();			
					getColorPoint();
				}	
        	},500);
	  		//checkIsColorpoint();

		})


		// ＊＊ 取得配套和點數 ＊＊
		function getPoint(){

		  p_ApiMgr.getPoint(

		        function (result) {

		            $("#show_point").val(toDecimal2(result));

		        },this,
		        function (error) {},this
		  );


		  return false;
		}


	    // ＊＊ 檢查用點數還是彩點 ＊＊
	    // var m_is_colorpoint = false;
	    // function checkIsColorpoint(){

	    //   p_ApiMgr.checkIsColorpoint(

	    //         function (result) {
	    //             console.log(result['is_colorpoint']);
	    //             m_is_colorpoint = result['is_colorpoint'];
					// if(m_is_colorpoint == 1){	
					// $("#div_colorpoint").show();			
					// 	getColorPoint();
					// }	                

	    //         },this,
	    //         function (error) {},this
	    //   );


	    //   return false;
	    // }


		// ＊＊ 取得彩點 ＊＊
		function getColorPoint(){

		  p_ApiMgr.getColorPoint(

		        function (result) {

		            $("#show_colorpoint").val(toDecimal2(result));

		        },this,
		        function (error) {},this
		  );


		  return false;
		}

		function openModal(){


		  if($("#deposit_pt_acc").val() == '' || $("#outward_pt_amount").val() == ''){
		    return false;
		  }

		  $("#span_tf_account").text( $("#deposit_pt_acc").val() );
		  $("#span_points").text( $("#outward_pt_amount").val() );  
		        //   <p>{{ Lang::get('user.account')}}&nbsp;<span id="span_tf_account"></span></p>
		        // <p>{{ Lang::get('user.point')}}&nbsp;<span id="span_points"></span></p>

		  $('#myModal').modal('toggle');

		}


		function openColorModal(){

		  if($("#deposit_cpt_acc").val() == '' || $("#outward_cpt_amount").val() == ''){
		    return false;
		  }

		  $("#span_color_tf_account").text( $("#deposit_cpt_acc").val() );
		  $("#span_color_points").text( $("#outward_cpt_amount").val() );  
		        //   <p>{{ Lang::get('user.account')}}&nbsp;<span id="span_tf_account"></span></p>
		        // <p>{{ Lang::get('user.point')}}&nbsp;<span id="span_points"></span></p>

		  $('#colorModal').modal('toggle');

		}


		// ＊＊ 轉出點數 ＊＊
		function depositPoints(){


		$('#myModal').modal('hide');
		  p_ApiMgr.depositPoints(
		          $("#deposit_pt_acc").val(),
		          $("#outward_pt_amount").val(),             
		        function (result) {
		            //console.log(result);
		            alert('贈與點數成功');
		            
		            location.reload();
		            //alert(result);
		        },this,
		        function (error) {
		          $('#myModal').modal('hide');
		        },this
		  );

		  return false;
		}


		// ＊＊ 轉出彩點 ＊＊
		function depositColorPoints(){

		  $('#colorModal').modal('hide');

		  p_ApiMgr.depositColorPoints(
		          $("#deposit_cpt_acc").val(),
		          $("#outward_cpt_amount").val(),             
		        function (result) {
		            //console.log(result);
		            alert('贈與彩點成功');
		            
		            location.reload();
		            //alert(result);
		        },this,
		        function (error) {
		          $('#myModal').modal('hide');
		        },this
		  );

		  return false;
		}



		

	</script>
@endpush

@section('content')
	
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.tranform_system')}}</h2>
		</div>
		<div class="web_map">		
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.tranform_system')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      			
		
	   		<h2>{{ Lang::get('user.point')}}</h2>
	   	
		  	{{ Lang::get('user.cur_point')}}		  	
		  	<input type="text" class="form-control" id="show_point"  disabled>
		  	<br />	  

			<div class="form-group">
			  <label for="deposit_pt_acc">{{ Lang::get('user.tf_in_acc')}}</label>
			  <input type="text" class="form-control" id="deposit_pt_acc" maxlength="16">
			</div>
			<div class="form-group">
			  <label for="outward_pt_amount">{{ Lang::get('user.tf_out_pt_amount')}}</label>
			  <input type="text" class="form-control" id="outward_pt_amount" maxlength="10">
			</div>

			<button type="button" class="btn btn-success" onclick="openModal()">{{ Lang::get('user.tf_out')}}</button>	
			<br />		

		</div>



		<div id="div_colorpoint" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      			
			<br /><br />
	   		<h2>{{ Lang::get('user.color_point')}}</h2>
	
		  	{{ Lang::get('user.cur_color_point')}}		  	
		  	<input type="text" class="form-control" id="show_colorpoint"  disabled>
		  	<br />	  

			<div class="form-group">
			  <label for="deposit_pt_acc">{{ Lang::get('user.tf_in_acc')}}</label>
			  <input type="text" class="form-control" id="deposit_cpt_acc" maxlength="16">
			</div>
			<div class="form-group">
			  <label for="outward_pt_amount">{{ Lang::get('user.tf_out_pt_amount')}}</label>
			  <input type="text" class="form-control" id="outward_cpt_amount" maxlength="10">
			</div>

			<button type="button" class="btn btn-success" onclick="openColorModal()">{{ Lang::get('user.tf_out')}}</button>	
			<br />		

		</div>

	</div>
</div>



<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<br /><br /><br /><br /><br />
	</div>
</div>




<!-- point Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
	
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_txn_out')}}</p>
        <p>{{ Lang::get('user.account')}}&nbsp;<span id="span_tf_account" style="color: blue;"></span></p>
        <p>{{ Lang::get('user.point')}}&nbsp;<span id="span_points" style="color: blue;"></span></p>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="depositPoints()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- colorpoint Modal -->
<div id="colorModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
	
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_txn_out')}}</p>
        <p>{{ Lang::get('user.account')}}&nbsp;<span id="span_color_tf_account" style="color: blue;"></span></p>
        <p><span style="color: red;">{{ Lang::get('user.color_point')}}</span>&nbsp;<span id="span_color_points" style="color: blue;"></span></p>
      </div>

      <div class="modal-footer">

      	<button type="button" class="btn btn-success" onclick="depositColorPoints()">{{ Lang::get('user.confirm')}}</button>	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection