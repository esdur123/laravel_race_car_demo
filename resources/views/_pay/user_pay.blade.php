
@extends('layouts.user') 




@push('head')
@endpush




@push('script')
<script type="text/javascript">
    var m_lang_point = "{{ Lang::get('user.point')}}";
    var m_lang_atm_transform = "{{ Lang::get('user.atm_transform')}}";
    var m_lang_point = "{{ Lang::get('user.point')}}";
    var m_lang_color_point = "{{ Lang::get('user.color_point')}}";    
    var m_product01_price = 5;

    // ready-jobs
    $(function(){

      //getProduct01Price();
      //checkIsColorpoint();
      setTimeout(function(){
        // if(p_is_colorpoint == 1){
        //     $("#div_colorpoint").show();      
        //     $("#buy_currency").text(m_lang_color_point); 
        // } else {
        //     $("#buy_currency").text(m_lang_point); 
        // }
      },500);

    })

    // ＊＊ 取得物品單價 ＊＊
    // function getProduct01Price(){

    //   p_ApiMgr.getPrice(
    //         '點數',
    //         function (result) {
    //             m_product01_price = result['price'];
    //             //$("#input_price").val(m_product01_price);
            
    //         },this,
    //         function (error) {},this
    //   );

    //   return false;
    // }


    // // ＊＊ 檢查用點數還是彩點 ＊＊
    // var m_is_colorpoint = false;
    // function checkIsColorpoint(){

    //   p_ApiMgr.checkIsColorpoint(

    //     function (result) {
    //       console.log(result['is_colorpoint']);
    //       m_is_colorpoint = result['is_colorpoint'];
    //       if(m_is_colorpoint == 1){ 
    //         $("#div_colorpoint").show();      
    //         $("#buy_currency").text(m_lang_color_point); 
    //       } else {
    //         $("#buy_currency").text(m_lang_point); 
    //       }             
    //         },this,
    //         function (error) {},this
    //   );


    //   return false;
    // }


  function createPayOrder(){
    $('#payModal').modal('hide');
    //var newTab = window.open('about:blank');
    p_ApiMgr.createPayOrder(
          $("#select_paytype").val(),
          $("#input_amount").val(),
          function (result) {
              //newTab.location.href = result.url;
              location.href = result.url;
          },this,
          function (error) {},this
    );

    return false;
  }


    // ＊＊ 提交購買 ＊＊
    function clickBuy(obj){

        if($("#input_price").val() == '-')        {
          return false;
        }

        var show_pay_type = "";
        if($("#select_paytype").val() == "TXPAY_VBANK")
        {
          show_pay_type = m_lang_atm_transform;
        }

        $("#show_pay_type").text(show_pay_type);
        $("#show_buy_amount").text($("#input_amount").val());

        var total = $("#input_amount").val() * m_product01_price;
        $("#total_price").text(total);

        $('#payModal').modal('toggle');

        return false;
    }


    // ＊＊ 確認購買 ＊＊
    function confirmBuy(){
        createPayOrder();
        return false;
    }



</script>


@endpush



@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
    <div style="display:inline-block">
      <h2>{{ Lang::get('user.pay_financial_flows')}}</h2>
    </div>
    <div class="web_map">
      &nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
      &nbsp;>&nbsp;{{ Lang::get('user.pay_financial_flows')}}
    </div>
  </div>
</div>

<hr align="left" width="20%" class="site_map_hr">


    <div>
      <form id='submit_buy'>

<!--           {{ Lang::get('user.product_price01')}}        
          <input type="text" class="form-control" id="input_price" value="-" disabled>
          <br />  -->

        {{ Lang::get('user.pay_type')}}
        <select class="form-control" id="select_paytype"></div>      
          @include('components.pay_type')
        </select>
          <br />
          
          <span id="buy_currency"> </span>
          <input type="text" class="form-control" id="input_amount" maxlength="4" value="1">
          <br />          
        <button type="button" class="btn btn-success" onclick="clickBuy()">{{ Lang::get('user.apply_buy')}}</button>   
      </form> 
    </div>    
    <br /><br /><br />


<!-- Modal -->
<div id="payModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.confirm_buy_points')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.pay_max_amount')}}:&nbsp;<span id="" style="color: red;">6000</span></p>           
        <p>{{ Lang::get('user.pay_type')}}:&nbsp;<span id="show_pay_type" style="color: blue;"></span></p>      
        <p>{{ Lang::get('user.buy_amount')}}:&nbsp;<span id="show_buy_amount" style="color: blue;"></span></p>
        <p>{{ Lang::get('user.total_price')}}:&nbsp;<span id="total_price" style="color: blue;"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="createPayOrder()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection