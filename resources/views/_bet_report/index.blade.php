@extends('layouts.user')




@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush




@push('script')
    <script src="{{ asset('js/u_my_utility.js') }}"></script>
    <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
    <script>
        var m_lang_txn_out_transform = "{{ Lang::get('user.txn_out_transform')}}";
        var m_lang_txn_in_transform = "{{ Lang::get('user.txn_in_transform')}}";
        var m_lang_txn_out_active = "{{ Lang::get('user.txn_out_active')}}";
        var m_lang_txn_in_pv = "{{ Lang::get('user.txn_in_pv')}}";
        var m_lang_txn_in_static = "{{ Lang::get('user.txn_in_static')}}";
        var m_lang_txn_in_direct_push_bonus = "{{ Lang::get('user.txn_in_direct_push_bonus')}}";
        var m_lang_txn_failed_pv_full = "{{ Lang::get('user.txn_failed_pv_full')}}";
        var m_lang_txn_failed_static_out = "{{ Lang::get('user.txn_failed_static_out')}}";
        var m_lang_txn_failed_static_auto_active_out = "{{ Lang::get('user.txn_failed_static_auto_active_out')}}";
        var m_lang_txn_out_static_reactive = "{{ Lang::get('user.txn_out_static_reactive')}}";
        var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
        var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
        var m_lang_txn_out_admin_recycle_point = "{{ Lang::get('user.txn_out_admin_recycle_point')}}";
        var m_lang_txn_out_market_sell = "{{ Lang::get('user.txn_out_market_sell')}}";
        var m_lang_txn_in_market_buy = "{{ Lang::get('user.txn_in_market_buy')}}";
        var m_lang_txn_in_tx_atm_buy = "{{ Lang::get('user.txn_in_tx_atm_buy')}}";
        var m_lang_txn_out_static_fee = "{{ Lang::get('user.txn_out_static_fee')}}";
        var m_lang_txn_out_static_inactive = "{{ Lang::get('user.txn_out_static_inactive')}}";
        var m_lang_txn_out_lottery_9blocks_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_bet')}}";
        var m_lang_txn_in_lottery_9blocks_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_bet')}}";
        var m_lang_txn_out_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_10times_bet')}}";
        var m_lang_txn_in_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_10times_bet')}}";
        var m_lang_txn_in_color_tx_atm_buy = "{{ Lang::get('user.txn_in_color_tx_atm_buy')}}";
        var m_lang_txn_in_admin_deposit_point = "{{ Lang::get('user.txn_in_admin_deposit_point')}}";
        var m_lang_txn_in_admin_deposit_colorpoint = "{{ Lang::get('user.txn_in_admin_deposit_colorpoint')}}";
        var m_lang_txn_in_static_10k_bonus = "{{ Lang::get('user.txn_in_static_10k_bonus')}}";
        // generation
        var m_lang_txn_in_direct_push_bonus_generation_1 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_1')}}";
        var m_lang_txn_in_direct_push_bonus_generation_2 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_2')}}";
        var m_lang_txn_in_direct_push_bonus_generation_3 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_3')}}";
        var m_lang_txn_in_direct_push_bonus_generation_4 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_4')}}";

        var m_lang_txn_out_user_withdraw = "{{ Lang::get('user.txn_out_user_withdraw')}}";

        var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
        var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
        var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
        var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
        var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
        var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
        var m_lang_processing = "{{ Lang::get('table.processing')}}";
        var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
        var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
        var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
        var m_lang_info = "{{ Lang::get('table.info')}}";


        // Fee-Daily-Static

        $(document).ready(function () {
            setTimeout(function () {
                GetWalletInfo();        // 取得錢包Point資訊
            }, 500);
        });

        // ＊＊ 取得錢包資訊 ＊＊
        function GetWalletInfo() {
            function dateToString(date) {
                var mm = date.getMonth() + 1; // getMonth() is zero-based
                var dd = date.getDate();

                return [date.getFullYear(),
                    (mm>9 ? '' : '0') + mm,
                    (dd>9 ? '' : '0') + dd
                ].join('-');
            }
            function formatNumber(str, glue) {
                var is_negative = str.toString().indexOf("-") == 0;
                // 如果傳入必需為數字型參數，不然就噴 isNaN 回去
                if (isNaN(str)) {
                    return NaN;
                } else if (is_negative) {
                    str = str.toString().slice(1)
                }
                // 決定三個位數的分隔符號
                var glue = (typeof glue == 'string') ? glue : ',';
                var digits = str.toString().split('.'); // 先分左邊跟小數點

                var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
                var threeDigits = []; // 用來存放3個位數的陣列

                // 當數字足夠，從後面取出三個位數，轉成字串塞回 threeDigits
                while (integerDigits.length > 3) {
                    threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
                }
                threeDigits.unshift(integerDigits.join(""));
                digits[0] = threeDigits.join(glue);
                return (is_negative ? "-" : "") + digits.join(".");
            }

            function formatNumberRed(str, glue) {
                var result = "";
                if (str <= 0) {
                    result = '<label style="color:#f00">' + formatNumber(str, glue) + '</label>';
                } else {
                    result = formatNumber(str, glue);
                }
                return result;
            }

            $('#table_points').DataTable({
                "language": {
                    "info": m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                    "oPaginate": {
                        "sFirst": m_lang_first_page,
                        "sPrevious": m_lang_pre_page,
                        "sNext": m_lang_next_page,
                        "sLast": m_lang_last_page
                    }
                },
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "order": [[0, "desc"]],
                "columns": [
                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {
                            var date = new Date(data * 1000);
                            return dateToString(date);
                        }
                    },
                    {
                        'data': 'total_amount',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'a_bet_amount',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'a_payout_amount',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'a_balance',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'a_profit',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'b_bet_amount',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'b_payout_amount',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'b_balance',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'b_commission',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'total_balance',
                        'render': function (data, type, row, meta) {
                            return formatNumberRed(data);
                        }
                    },
                    {
                        'data': 'income_ratio',
                        'render': function (data, type, row, meta) {
                            if (data <= 0) {
                                data = '<label style="color:#f00">' + (data * 100).toFixed(2) + '%' + '</label>';
                            } else {
                                data = (data * 100).toFixed(2) + "%";
                            }
                            return data;
                        }
                    }
                ],
                "serverSide": true,
                "ajax": p_ApiMgr.getDataTableAjax('GET', '/api/v1/report/bet')
            })
        }
    </script>
@endpush



@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div style="display:inline-block">
                <h2>{{ Lang::get('user.report_info')}}</h2>
            </div>
            <div class="web_map">
                &nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
                &nbsp;>&nbsp;{{ Lang::get('user.report_info')}}
            </div>
        </div>
    </div>

    <hr align="left" width="20%" class="site_map_hr">

    <div class="container">
        <div class="hide_overflow" style="overflow:auto;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- <div class="nori_overflow"> -->
                        <h2>{{ Lang::get('user.report_info')}}</h2>
                        <table id="table_points" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{ Lang::get('user.report_created_at')}}</th>
                                <th>{{ Lang::get('user.report_total_amount')}}</th>
                                <th>{{ Lang::get('user.report_a_bet_amount')}}</th>
                                <th>{{ Lang::get('user.report_a_payout_amount')}}</th>
                                <th>{{ Lang::get('user.report_a_balance')}}</th>
                                <th>{{ Lang::get('user.report_a_profit')}}</th>
                                <th>{{ Lang::get('user.report_b_bet_amount')}}</th>
                                <th>{{ Lang::get('user.report_b_payout_amount')}}</th>
                                <th>{{ Lang::get('user.report_b_balance')}}</th>
                                <th>{{ Lang::get('user.report_b_commission')}}</th>
                                <th>{{ Lang::get('user.report_total_balance')}}</th>
                                <th>{{ Lang::get('user.report_income_ratio')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                   <!--  </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br/><br/><br/><br/><br/>
        </div>
    </div>

@endsection