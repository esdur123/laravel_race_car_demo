
@extends('layouts.user') 




@push('head')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">

  .banner_arrow{
    line-height: 32vw;
  }

  input[type=radio] {
    width:0.6em;
    height:0.6em;
     
  }

  @media only screen and (max-width: 767px) {


  }

</style>
	    
@endpush



@push('script')
<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/u_my_utility.js') }}"></script>
<script>
  var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
  var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
  var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
  var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
  var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
  var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
  var m_lang_processing = "{{ Lang::get('table.processing')}}";
  var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
  var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
  var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
  var m_lang_info = "{{ Lang::get('table.info')}}";

	// ＊＊ ready-jobs ＊＊
	$(function(){

      $('input[type=radio][name=optradio]').change(function() {
        reload_datatable();
      });     

      setTimeout(function(){
          get_ask_info();    
      },500);


	})


    // ＊＊ 重新讀取datatable ＊＊
    function reload_datatable(){
           $('#table_ask').DataTable().destroy();
           get_ask_info();  
    }


    // ＊＊ 取得 提問資訊訊 ＊＊
    function get_ask_info(){
 
        var ajax_url = '/api/v1/ask/info/'+$('input[name=optradio]:checked').val();;

        $('#table_ask').DataTable({
                 "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,    
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },            
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "columns": [
                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {
                            m_datatime = data.date;
                            m_datatime = m_datatime.toString();
                            m_datatime = m_datatime.substring(0, m_datatime.length-7);
                            return m_datatime;
                        }
                    },
                    {
                        'data': 'title',
                        'render': function (data, type, row, meta) {

                            return data;
                        }
                    },
                    {
                        'data': 'comment',
                        'render': function (data, type, row, meta) {

                            return data;
                        }
                    },                    
                    {
                        'data': 'reply',
                        'render': function (data, type, row, meta) {
                            data = '<span style="color:blue;">'+data+'</span>'
                            return data;
                        }
                    },

                ],
                "serverSide": true,
                "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
        })


        return false;
    }


  function clickAsk(){
    $("#askModal").modal('toggle');
  }

  function confirmAsk(){

    $('#askModal').modal('hide');

    p_ApiMgr.askQuestions(
          $("#ask_title").val(),
          $("#ask_comment").val(),
          function (result) {         

              //console.log(result);
              alert('已提交');
              location.reload();
              // $('#table_orders').DataTable().destroy();                
              // get_withdraw_orders();                
              // alert('已撤銷');

          },this,
          function (error) {

          },this
    );

    return false;
  }


</script>
@endpush




@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
    <div style="display:inline-block">
      <h2>{{ Lang::get('user.ask_questions')}}</h2>
    </div>
    <div class="web_map">
      &nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
      &nbsp;>&nbsp;{{ Lang::get('user.ask_questions')}}
    </div>
  </div>
</div>

<hr align="left" width="20%" class="site_map_hr">



<div class="container"> 
    <div class="hide_overflow" style="overflow:auto;"> 
      <div class="row" id="main_row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  

          <form class="" method="post" action="#">
            <div class="form-group">
              <label for="ask_title">{{ Lang::get('user.ask_title')}}</label>
              <input type="text" class="form-control" id="ask_title">
            </div>

            <div class="form-group">
              <label for="ask_comment">{{ Lang::get('user.ask_comment')}}</label>
              <textarea class="form-control" rows="5" id="ask_comment"></textarea>
            </div>


            <div class="form-group ">
             
              <button id="btn_register" type="button" class="btn btn-success" onclick="clickAsk()">{{ Lang::get('user.ask')}}</button>
            </div>

          </form>

        </div>

      </div>
    </div>




<br /><br /><br />
    <div class="hide_overflow" style="overflow:auto;"> 
      <div class="row" id="main_row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
            <h2>{{ Lang::get('user.ask_history')}}</h2>
<br />
  <div style="font-size: 20px;">
    <label class="radio-inline"><input type="radio" value ="all" name="optradio" checked>{{ Lang::get('user.show_all')}}</label>
    <label class="radio-inline"><input type="radio" value ="waiting" name="optradio">{{ Lang::get('user.waiting_reply')}}</label>   
    <label class="radio-inline"><input type="radio" value ="reply" name="optradio">{{ Lang::get('user.replied')}}</label>
  </div>
<br />
            <table id="table_ask" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ Lang::get('user.datetime')}}</th>
                        <th>{{ Lang::get('user.ask_title')}}</th>
                        <th>{{ Lang::get('user.ask_comment')}}</th>
                        <th>{{ Lang::get('user.ask_reply')}}</th>
                    </tr>
                </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
            </table>          
        </div>
      </div>
    </div>









</div> <!-- end container -->




<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
    <br /><br /><br /><br /><br />
  </div>
</div>



<!-- Paid Modal -->
<div id="askModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.ask_questions')}}</h4>
      </div>

      <div class="modal-body">
        <p>
          <span>{{ Lang::get('user.confirm_ask')}}&nbsp;</span>
          <span id ="span_received_point" style="color: red;"></span>
        </p>              
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="confirmAsk()">{{ Lang::get('user.confirm')}}</button> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection