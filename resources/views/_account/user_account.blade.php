
@extends('layouts.user') 




@push('head')
	    
@endpush




@push('script')
<script>
	var m_cur_url = "{{ url('/')}}";

	var m_lang_country_tw = "{{ Lang::get('country.tw')}}";
	var m_lang_country_hk = "{{ Lang::get('country.hk')}}";
	var m_lang_country_cn = "{{ Lang::get('country.cn')}}";
	var m_lang_country_malaysia = "{{ Lang::get('country.malaysia')}}";
	var m_lang_country_singapore = "{{ Lang::get('country.singapore')}}";
	var m_lang_update_succeed = "{{ Lang::get('user.update_succeed')}}";
	var m_lang_bind_succeed = "{{ Lang::get('user.bind_succeed')}}";	
	var m_lang_inactivated = "{{ Lang::get('user.inactivated')}}";
	var m_lang_bind = "{{ Lang::get('user.bind')}}";
	var m_lang_binded = "{{ Lang::get('user.binded')}}";	



	// ＊＊ ready-jobs ＊＊
	$(function(){
	  //getAccount();
		getUserInfo();

	})


	// ＊＊ 複製 (舊版本)＊＊
	// function copyUrl(s){
	  	
	//   var clip_area = $('#show_my_register_url');
	//     //ee
	//   clip_area.text(s);
	//   clip_area.select();

	//   document.execCommand('copy');
	// }


	// ＊＊ 複製 (新版本) ＊＊
	function copyUrl(){
	  
	  // copy #show_my_register_url

	  // 開一個div在外太空
	  var tmpElem = $('<div></div>');
	    tmpElem.css({
	      position: "absolute",
	      left:     "-1000px",
	      top:      "-1000px",
	    });
	    tmpElem.text( $("#show_my_register_url").val() ); // 要複製的值值
	    $("body").append(tmpElem);

	    // 選取
	    range = document.createRange(),
	    range.selectNodeContents(tmpElem.get(0));
	    selection = window.getSelection ();
	    selection.removeAllRanges ();
	    selection.addRange (range);

	  try {  
	    var successful = document.execCommand ("copy", false, null);  // 複製
	    tmpElem.remove(); // 刪掉div
	    //alert("复制成功");
	  } catch(err) {  
	    console.log("Oops, unable to copy");  
	  }

	}

	// ＊＊ 立即激活＊＊
	function updateUserInfo(){

	  $('#accountModal').modal('toggle');
	  return false;
	}


	function updateBankInfo(){

	  $('#bankModal').modal('toggle');
	  return false;
	}


	function updateUserPassword(){

	  $('#passwordModal').modal('toggle');
	  return false;
	}


	// ＊＊ 取得 帳號資訊 ＊＊
	function getUserInfo(){

	  p_ApiMgr.getUserInfo(

	        function (result) {
	            var obj = JSON.parse(result);
	            //console.log(result);
	            var recommender_id = obj.recommender_id;
	           	if(recommender_id == 0){
	           		recommender_id = '-';
	           	}

	            if(obj.set > 0){
	              $("#show_set").val(obj.set+' RMB');
	            } else {
	              $("#show_set").val(m_lang_inactivated);
	            }
	            
	            //$("#show_recommender").val(recommender_id);
	            $("#show_nickname").val(obj.nickname);     

	            $("#select_country_code").val(obj.country_code);

	            if(obj.set > 0){
	              $("#show_my_register_url").val(m_cur_url+'/register/'+obj.account);
	            } else {
	          
	              $('#div_rigister_url').hide();
	              $("#show_my_register_url").val("");
	            }	           

	            $("#show_phone").val(obj.phone);

	            if(obj.binded_phone == '1'){
	            	// 改兩個btn 改兩個input
	            	$("#verify_code").html('');
	            	$("#btn_verify_sms").html('<button type="button" class="btn">'+m_lang_binded+'</button>	');
	            	$("#input_phone").html('<input type="text" class="form-control" id="show_phone" value="'+obj.phone+'" style="width: 99%;"  disabled>');
	            	$("#input_country_code").html('<select class="form-control" id="select_country_code" disabled><option value ="'+obj.country_code+'">'+obj.country_code+'</option></select>');
           	
	            }

	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 更新 帳號資訊 ＊＊
	function confirmUpdateUserInfo(){

	  p_ApiMgr.updateUserInfo(
	          $("#show_nickname").val(),
	          $("#select_country_code").val(),
	          $("#show_phone").val(),          
	        function (result) {
	            //console.log(result);
	            alert(m_lang_update_succeed);
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 修改密碼 ＊＊
	function confirmUpdateUserPassword(){

	  p_ApiMgr.updateUserPassword(
	          $("#old_pw").val(),
	          $("#new_pw").val(),
	          $("#new_pw2").val(),          
	        function (result) {
	            //console.log(result);
	            alert(m_lang_update_succeed);
	            $("#old_pw").val('');
	            $("#new_pw").val('');
	            $("#new_pw2").val('');
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 清除 密碼欄位資訊 ＊＊
	function clearPasswordInfo(){

	  $("#old_pw").val('');
	  $("#new_pw").val('');
	  $("#new_pw2").val('');
	  return false;
	}


	// ＊＊ 按下 驗證綁定 ＊＊
	function click_sms_bind(){

	  $("#smsBindModal").modal('toggle');

	  return false;
	}


	// ＊＊ 確認 綁定手機 ＊＊
	function confirmBindSMS(){

	  $("#smsBindModal").modal('hide');

	  p_ApiMgr.bindMobile(
         	$("#bind_code").val(),
	        function (result) {
	            //console.log(result);
	            alert('驗證成功');
	            location.reload();
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 按下 簡訊驗證 ＊＊
	function click_sms_verify(){

	  $("#smsModal").modal('toggle');

	  return false;
	}


	// ＊＊ 確認 簡訊驗證 ＊＊
	function confirmVerifySMS(){

	  $("#smsModal").modal('hide');
	  // var country_code = $("#select_country_code").val();
	  // var phone = $("#show_phone").val();

	  // alert(country_code+','+phone);

	  p_ApiMgr.createSms(
         	$("#select_country_code").val(),
         	$("#show_phone").val(),
	        function (result) {
	            console.log(result['debug']);
	            alert('成功! 驗證碼於十分鐘內傳出, 請留意手機簡訊');
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 更新 銀行資訊 ＊＊
	function confirmUpdateBankInfo(){

	  p_ApiMgr.updateBankInfo(
	          $("#select_bank_code").val(),
	          $("#show_bank_branch").val(),
	          $("#show_bank_account").val(),          
	        function (result) {
	            //console.log(result);
      
	            alert(m_lang_bind_succeed);
	            getBankInfo();

	            clearPasswordInfo();
	        },this,
	        function (error) {},this
	  );

	  return false;
	}


	// ＊＊ 取得 銀行資訊 ＊＊
	function getBankInfo(){

	  p_ApiMgr.getBankInfo(

	        function (result) {
	            var obj = JSON.parse(result);

	            $("#select_bank_code").val(obj.bank_code);
	            $("#show_bank_branch").val(obj.bank_branch);
	            $("#show_bank_account").val(obj.bank_account);
	            var binded = obj.binded_bank;

	            if(binded == 1){
	            	$("#select_code").html('<select class="form-control" id="select_bank_code" disabled><option>'+obj.bank_code+'</option></select>');
	            	$('#input_bank_branch').html('<input type="text" class="form-control" id="show_bank_branch" value="'+obj.bank_branch+'" disabled>');
					$('#input_bank_account').html('<input type="text" class="form-control" id="show_bank_account" value="'+obj.bank_account+'" disabled>');
					//<button type="button" class="btn">{{ Lang::get('user.binded')}}</button>
					$("#show_div_bank_bind").html('<button type="button" class="btn">'+m_lang_binded+'</button>');					
	            } else {
	            	$("#show_div_bank_bind").html('<button type="button" class="btn btn-danger"  onclick="updateBankInfo()">'+m_lang_bind+'</button>');
	            }

	            $("#show_phone").val(obj.phone);

	        },this,
	        function (error) {},this
	  );

	  return false;
	}
	
</script>



@endpush



@section('content')



<div class="rows">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.manager_account')}}</h2>
		</div>
		<div class="web_map">
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.manager_account')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		

		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#nav_acc_info" onclick="getUserInfo()">{{ Lang::get('user.acc_info')}}</a></li>
		  <li><a data-toggle="tab" href="#nav_bank_info" onclick="getBankInfo()">{{ Lang::get('user.bank_info')}}</a></li>
		  <li><a data-toggle="tab" href="#nav_update_pw" onclick="clearPasswordInfo()">{{ Lang::get('user.update_pw')}}</a></li>
		</ul>

		<div class="tab-content">
		  <br /><br />

		  <div id="nav_acc_info" class="tab-pane fade in active">	  	
			<div id="div_rigister_url">
				{{ Lang::get('user.recommender_url')}}	
				<div style="display:table-cell;width: 100%;">
			    <input type="text" class="form-control" id="show_my_register_url" placeholder="" style="color: #009FCC;width: 99%;" >
			    </div>	
			    <div style="display:table-cell;">
			    <button type="button" class="btn btn-info" onclick="copyUrl()">{{ Lang::get('user.copy')}}	</button>	
				</div>
			</div>
			<br />	
		  	{{ Lang::get('user.set')}}		  	
		  	<input type="text" class="form-control" id="show_set"  disabled>
		  	<br />			  	
		  	{{ Lang::get('user.nickname')}}		  	
		  	<input type="text" class="form-control" id="show_nickname" maxlength="16">
		  	<br />
		  	{{ Lang::get('user.country_code')}}
		  	<div id="input_country_code">
				<select class="form-control" id="select_country_code">			
					@include('components.country_code')
				</select>
		  	</div>

		  	<br />
		  	{{ Lang::get('user.phone')}}		  	
			<div  style="display:table-cell;width: 100%;">
		      <div id="input_phone"><input type="text" class="form-control" id="show_phone" maxlength="16" style="width: 99%;" ></div>
		    </div>	
		    <div  id="btn_verify_sms" style="display:table-cell;">
		    	<button type="button" class="btn btn-warning" onclick="click_sms_verify()">{{ Lang::get('user.verify_sms')}}</button>	
			</div>
			<br />	
			<div id="verify_code">
			  	<span style="color: black;"> {{ Lang::get('user.fill_in_verify_code')}} </span>
				<div style="display:table-cell;width: 100%;">
			    <input type="text" class="form-control" id="bind_code" maxlength="4" style="width: 99%;" >
			    </div>	
			    <div  id="btn_bind_sms" style="display:table-cell;">
			     <button type="button" class="btn btn-danger" onclick="click_sms_bind()">{{ Lang::get('user.verify_sms_and_bind')}}	</button>	
				</div>
				<br />
			</div>
			<button type="button" class="btn btn-success" onclick="updateUserInfo()">{{ Lang::get('user.confirm_update')}}</button>
		  </div>


		  <div id="nav_bank_info" class="tab-pane fade">

		  	{{ Lang::get('user.bank_code')}}
		  	<div id="select_code">
				<select class="form-control" id="select_bank_code">
					@include('components.bank_option')
				</select>
			</div>			
		  	<br />
		  	{{ Lang::get('user.bank_branch')}}
		  	<div id="input_bank_branch"><input type="text" class="form-control" id="show_bank_branch" maxlength="16"></div>
		  	<br />		  	
		  	{{ Lang::get('user.bank_account')}}
		  	<div id="input_bank_account"><input type="text" class="form-control" id="show_bank_account" maxlength="16"></div>
		  	<br />	
		  	<div id="show_div_bank_bind">
			    <button type="button" class="btn">{{ Lang::get('user.binded')}}</button>
		  	</div>

		  </div>


		  <div id="nav_update_pw" class="tab-pane fade">
		  	{{ Lang::get('user.old_pw')}} &nbsp; <span style="color: #BBB;">{{ Lang::get('user.first_login_pw')}}</span>
		  	<input type="password" class="form-control" id="old_pw" maxlength="16">
		  	<br />
		  	{{ Lang::get('user.new_pw')}}		  	
		  	<input type="password" class="form-control" id="new_pw" maxlength="16">
		  	<br />
		  	{{ Lang::get('user.new_pw2')}}		  	
		  	<input type="password" class="form-control" id="new_pw2" maxlength="16">
		  	<br />		  		
			<button type="button" class="btn btn-success" onclick="updateUserPassword()">{{ Lang::get('user.confirm_update')}}</button>			  			  	
		  </div>

		</div>

	</div>
</div>



<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<br /><br /><br /><br /><br />
	</div>
</div>


<div id="accountModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <p>{{ Lang::get('user.update_user_info')}}</p>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_update')}}</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmUpdateUserInfo()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="bankModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        {{ Lang::get('user.bind_bank_info')}}
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.warning_bind_bank_info')}}</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmUpdateBankInfo()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="passwordModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <p>{{ Lang::get('user.update_user_info')}}</p>

      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_update')}}</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmUpdateUserPassword()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- SMS Modal-->
<div id="smsModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        {{ Lang::get('user.verify_sms')}}
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.verify_sms_txt_1')}}</p>    
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmVerifySMS()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- SMS Modal-->
<div id="smsBindModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        {{ Lang::get('user.bind')}}
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.bind_sms_txt_1')}}</p>     
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmBindSMS()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection