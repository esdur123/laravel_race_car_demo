
@extends('layouts.user') 




@push('head')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  .btn_switch_on{
    background-color: rgb(0,0,0,0);
    border:red 0px solid; 
    background-image: url("{{ asset('img/switch_on.png') }}");
    width: 64px;
    height: 28px;
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;     
  }

  .btn_switch_off{
    background-color: rgb(0,0,0,0);
    border:red 0px solid; 
    background-image: url("{{ asset('img/switch_off.png') }}");
    width: 64px;
    height: 28px;
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;     
  }  
</style>
@endpush




@push('script')
<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/u_my_utility.js') }}"></script>
<script>
	var m_lang_stop_auto = "{{ Lang::get('user.stop_auto')}}";
	var m_lang_start_auto = "{{ Lang::get('user.start_auto')}}";
	var m_lang_auto_active_now = "{{ Lang::get('user.auto_active_now')}}";
  var m_lang_active_set = "{{ Lang::get('user.active_set')}}";

  var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
  var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
  var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
  var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
  var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
  var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
  var m_lang_processing = "{{ Lang::get('table.processing')}}";
  var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
  var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
  var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
  var m_lang_info = "{{ Lang::get('table.info')}}";


  var m_lang_set = "{{ Lang::get('user.set')}}";
  var m_lang_performance_total = "{{ Lang::get('user.performance_total')}}";    
  var m_lang_performance_L = "{{ Lang::get('user.performance_L')}}";
  var m_lang_performance_R = "{{ Lang::get('user.performance_R')}}";
  var m_lang_select = "{{ Lang::get('user.select')}}";


  $(function(){

    
    showRegisterSetSelect(); // modal的配套
    
    setTimeout(function(){
      GetOrgBranchs();  

    },500);



  })

  function reload_table(){
       $('#table_downlines').DataTable().destroy();
       GetOrgBranchs();  
  }


  function GetOrgBranchs(){

      var out_value = 'safe';
      var account_id = '';
      var is_active = 0;
      var switch_id = '';
      var index = 0;
      var is_active_array = [];
      var img_id_array = [];
      
      var ajax_url = '/api/v1/org/active/info';
      var datatable = $('#table_downlines').DataTable({
               "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,  
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                   "oPaginate": {
                     "sFirst": m_lang_first_page,
                     "sPrevious": m_lang_pre_page,
                     "sNext": m_lang_next_page,
                     "sLast": m_lang_last_page,
                   },
               },

            "rowCallback": function( row, data ) {
              index = 0;
            },
              "lengthChange": false,
              "pagingType": "simple_numbers",
              "searching": false,            
              "columns": [
          
                  {
                      'data': 'id',
                      'render': function (data, type, row, meta) {
                          if(row['ball_index'] == '0'){
                            data = p_user_account.toString();
                          } else {
                            data = p_user_account.toString()+'('+row['ball_index']+')';
                          }

                          return data;
                      }
                  },                              
                  {
                      'data': 'static_current',
                      'render': function (data, type, row, meta) {

                          if(row['level']<=0){
                            return '-';
                          }

                          var show_static = row['static_current']+'/'+row['static_max'];
                          data = show_static;
                          var i_cur = parseInt((row['static_current']).toString());
                          var i_max = parseInt((row['static_max']).toString());
                          if(i_cur >= i_max){
                            data = '<span style="color:red;">'+show_static+'</span>';
                          } 

                          return data;
                      }
                  }, 
                  {
                      'data': 'auto_active',
                      'render': function (data, type, row, meta) {

                          if(row['level']<=0){
                            return '-';
                          }

                          is_active = data;
                          var btn_style = 'default';
                          if(is_active == '0'){
                            btn_style = 'info';
                          }

                          if(is_active == '1'){
                            btn_style = 'danger';
                          }
                          
                          var btn_class = 'btn_switch_off';
                          if(is_active == '1'){
                            btn_class = 'btn_switch_on';
                          }
                          data = '<button type="submit" class="'+btn_class+'" attr_id="'+ row['id'] +'" attr_is_active="'+ is_active +'"  onclick="activeAuto(this)">';

                           data += '</button>';
                          return data;
                      }
                },
                {
                      'data': 'auto_active',
                      'render': function (data, type, row, meta) {

                          if(row['level']<=0){
                            return '-';
                          }
                          var btn_style = 'btn btn-warning';
                          data = '<button type="submit" class="'+btn_style+'" ball_id="'+row['id']+'" onclick="acticeNow(this)">';

                          data += m_lang_auto_active_now;
                          data += '</button>';
                          return data;
                      }
                },   
                {
                      'data': 'level',
                      'render': function (data, type, row, meta) {
                                                
                          if(row['level'] == -1){
                            data = '<select id="select_dt_table_set" class="form-control" id="set_select" style="width: 120px;">';
                            data += '<option value="'+getSetFromLevel(1)+'">'+getSetFromLevel(1)+'</option>';
                            data += '<option value="'+getSetFromLevel(2)+'">'+getSetFromLevel(2)+'</option>';
                            // data += '<option value="'+getSetFromLevel(3)+'">'+getSetFromLevel(3)+'</option>';
                            data += '</select>';
                          }

                          if(row['level'] == 1){
                            data = '<select id="select_dt_table_set" class="form-control" id="set_select" style="width: 120px;">';
                            data += '<option value="'+getSetFromLevel(2)+'">'+getSetFromLevel(2)+'</option>';
                            // data += '<option value="'+getSetFromLevel(3)+'">'+getSetFromLevel(3)+'</option>';
                            data += '</select>';
                          }

                          // if(row['level'] == 2){
                          //   data = '<select id="select_dt_table_set" class="form-control" id="set_select" style="width: 80px;">';
                          //   data += '<option value="'+getSetFromLevel(3)+'">'+getSetFromLevel(3)+'</option>';
                          //   data += '</select>';
                          // }

                          if(row['level'] == 2){
                            data = '-';
                          }

                          return data;
                      }


                },
                {
                      'data': 'set',
                      'render': function (data, type, row, meta) {
                           
                          var btn_style = 'btn btn-success';

                          data = '<button type="submit" class="'+btn_style+'" ball_id="'+row['id']+'" onclick="activeSet(this)">';
                          data += m_lang_active_set;
                          data += '</button>';
                          if(row['level']=='2'){
                            data = '-';
                          }

                          return data;
                      }


                }              
              ],
              "serverSide": true,
              "ajax": p_ApiMgr.getDataTableAjax('GET', ajax_url)
      })



    return false;
  }





  // ＊＊ 立即激活＊＊
  var m_active_now_id = 0;
  function acticeNow(obj){
    if(is_allow_apply==0) return false;
    m_active_now_id = $(obj).attr("ball_id");
    //alert('立即激活 ball_id = '+m_active_now_id);
    $('#acticeNowModal').modal('toggle');
    return false;
  }



  // ＊＊ 立即激活＊＊
  function confirmActiceNow(){
    if(is_allow_apply==0) return false;
    $('#acticeNowModal').modal('hide');
    p_ApiMgr.activeStaticOut(
          m_active_now_id,
          function (result) {
              //console.log(result);
              alert('激活成功');
              location.reload();
              //reload_table();

          },this,
          function (error) {},this
    );

    return false;
  }


  // ＊＊ 自動激活＊＊
  var m_active_auto_id = 0;
  function activeAuto(obj){
    if(is_allow_apply==0) return false;
    m_active_auto_id = $(obj).attr("attr_id");
    $('#activeAutoModal').modal('toggle');
    return false;
  }


  // ＊＊ 取得branch ID ＊＊
  function confirmActiveAuto(){
    if(is_allow_apply==0) return false;
    // var attr_id = $(obj).attr("attr_id");
    // var attr_is_active = $(obj).attr("attr_is_active");

     p_ApiMgr.switchActive(
          m_active_auto_id,
          function (result) {

              // if(result == '0'){
              //   alert('已關閉');
              // }

              // if(result == '1'){
              //   alert('已開啟');
              // }

               //location.reload();
               reload_table();

          },this,
          function (error) {},this
    );

    return false;
  }


  // ＊＊ 配套 ＊＊
  var m_active_level_ball_id = 0;
  function activeSet(obj){
    if(is_allow_apply==0) return false;
    m_active_level_ball_id = $(obj).attr("ball_id");
    $("#span_active_set").text( $("#select_dt_table_set").val() );
    $('#activeSetModal').modal('toggle');
    return false;
  }


  // ＊＊ 開通配套 ＊＊
  function confirmActiveSet(){
    if(is_allow_apply==0) return false;
    $('#activeSetModal').modal('hide');
    var new_level = getLevelFromSet($("#select_dt_table_set").val());

    p_ApiMgr.activeLevel(
            new_level,       
            m_active_level_ball_id,
          function (result) {
              console.log(result);
              alert('配套開通');
              location.reload();
              //reload_table();
              //alert(result);
          },this,
          function (error) {},this
    );

    return false;
  }


</script>


@endpush


@section('content')
	
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.active_account')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.active_account')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


<!-- 註冊 -->
  <div id='div_show_register'>
      <button type="button" onclick="rigisterNew()" class="btn btn-success">{{ Lang::get('user.register_new')}}</button>
      <button type="button" onclick="rigisterBall()" class="btn btn-warning">{{ Lang::get('user.register_new_ball')}}</button>

  </div>
  <br />

<!-- DataTable -->
<div class="hide_overflow" style="overflow:auto;"> 
  <div class="row">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
        <table id="table_downlines" class="display" style="width:100%">
            <thead>
                <tr>
<!--                     <th>#</th>	 -->                	
<!--                     <th>{{ Lang::get('user.collision')}}</th>
                    <th>{{ Lang::get('user.max_pv')}}</th> -->
                    <th>ID</th>
                    <th>{{ Lang::get('user.out_static')}}</th>
                    <th>{{ Lang::get('user.auto_active')}}</th>
                    <th>{{ Lang::get('user.auto_active_now')}}</th>
                    <th>{{ Lang::get('user.active_set')}}</th>
  				         <th>{{ Lang::get('user.set')}}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                	<td></td>
<!--                     <td></td> -->
<!--                     <td></td>
                    <td></td> -->
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  </div>

  <div class="row">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
  		<br /><br /><br /><br /><br />
  	</div>
  </div>
</div>

<!-- Modal -->
<div id="acticeNowModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.auto_active_now')}}?</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmActiceNow()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="activeAutoModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_switch_auto_active')}}</p>  
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmActiveAuto()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="activeSetModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
        <p>
            <span>{{ Lang::get('user.confirm_active_set')}}</span>
            <span id="span_active_set" style="color: blue;"></span>
        </p>  
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmActiveSet()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


  <!-- Modal 註冊 -->
  <div class="modal fade" id="modal_register" role="dialog">
    <div class="modal-dialog" style="max-width: 98vw;">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.add_account')}}</h5>         
        </div>
        <div class="modal-body">
            
        <div class="form-group">          
          <label for="input_recommend_id">{{ Lang::get('user.recommend_acc')}}</label>
          <input type="text" class="form-control" id="input_recommend_id"  placeholder="{{ Lang::get('user.recommend_acc')}}" disabled>

          <br />
          <label for="input_account">{{ Lang::get('user.account')}}</label>
          <input type="text" class="form-control" id="input_account"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

          <br />
          <label for="input_nickname">{{ Lang::get('user.nickname')}}</label>
          <input type="text" class="form-control" id="input_nickname"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

          <br />
          <label for="select_country_code">{{ Lang::get('user.country_code')}}</label>
          <select class="form-control" id="select_country_code">
            @include('components.country_code')
          </select>

          <br />
          <label for="input_phone">{{ Lang::get('user.phone')}}</label>
          <input type="text" class="form-control" id="input_phone"  placeholder="09xxxxxxxx" maxlength="10">

          <br />
          <label for="select_user_level">{{ Lang::get('user.set')}}</label>
          <div id = "select_sets"></div>
          <br />
        </div>

        </div> 

        <div class="modal-footer">
              <button type="button" class="btn btn-success" onclick="confirmRegister()">{{ Lang::get('user.confirm')}}</button>&nbsp;
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
        </div>
      </div>
      
    </div>
  </div>




  <!-- Modal 球 -->
  <div class="modal fade" id="modal_ball" role="dialog">
    <div class="modal-dialog" style="max-width: 98vw;">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.register_new_ball')}}</h5>         
        </div>
        <div class="modal-body">
            
        <div class="form-group">          

          <label for="input_ball_account">{{ Lang::get('user.account')}}</label>
          <input type="text" class="form-control" id="input_ball_account"  placeholder="{{ Lang::get('user.account')}}" maxlength="16" disabled>
        

          <br />
          <label for="select_ball_sets">{{ Lang::get('user.set')}}</label>
          <div id = "select_ball_sets"></div>

          <br />
          <label for="input_ball_amount">{{ Lang::get('user.amount')}}</label>
          <input type="text" class="form-control" id="input_ball_amount" value="1"  maxlength="2" >         
        </div>

        </div>

        <div id="div_btns" class="modal-footer">
              <button type="button" class="btn btn-success" onclick="confirmNewBall()">{{ Lang::get('user.confirm')}}</button>&nbsp;
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
        </div>
      </div>
      
    </div>
  </div>


@endsection