
@extends('layouts.admin_app') 


@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style type="text/css">
      
    .switch_group {
            width: 64px;
            height: 28px;
            -webkit-background-size:cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            cursor: pointer;
        }

    </style>

@endpush


@push('script')
<script src="{{ asset('js/u_my_utility.js') }}"></script>
  <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
  <script src="{{ asset('js/admin/u_common.js') }}"></script>

<script type="text/javascript">
  var p_user_id = '{{ $id }}';

	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

	var m_lang_stop_auto = "{{ Lang::get('user.stop_auto')}}";
	var m_lang_start_auto = "{{ Lang::get('user.start_auto')}}";
	var m_lang_auto_active_now = "{{ Lang::get('user.auto_active_now')}}";
  var m_lang_active_set = "{{ Lang::get('user.active_set')}}";

  var m_lang_country_tw = "{{ Lang::get('country.tw')}}";
  var m_lang_country_hk = "{{ Lang::get('country.hk')}}";
  var m_lang_country_cn = "{{ Lang::get('country.cn')}}";
  var m_lang_country_malaysia = "{{ Lang::get('country.malaysia')}}";
  var m_lang_country_singapore = "{{ Lang::get('country.singapore')}}";
  var m_lang_update_succeed = "{{ Lang::get('user.update_succeed')}}";
  var m_lang_inactivated = "{{ Lang::get('user.inactivated')}}";  

  var m_lang_txn_out_transform = "{{ Lang::get('user.txn_out_transform')}}";
  var m_lang_txn_in_transform = "{{ Lang::get('user.txn_in_transform')}}";
  var m_lang_txn_out_active = "{{ Lang::get('user.txn_out_active')}}";  
  var m_lang_txn_in_pv = "{{ Lang::get('user.txn_in_pv')}}";
  var m_lang_txn_in_static = "{{ Lang::get('user.txn_in_static')}}";
  var m_lang_txn_in_direct_push_bonus = "{{ Lang::get('user.txn_in_direct_push_bonus')}}";
  var m_lang_txn_failed_pv_full = "{{ Lang::get('user.txn_failed_pv_full')}}";
  var m_lang_txn_failed_static_out = "{{ Lang::get('user.txn_failed_static_out')}}";
  var m_lang_txn_failed_static_auto_active_out = "{{ Lang::get('user.txn_failed_static_auto_active_out')}}";
  var m_lang_txn_out_static_reactive = "{{ Lang::get('user.txn_out_static_reactive')}}";
  var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
  var m_lang_txn_out_static_auto_reactive = "{{ Lang::get('user.txn_out_static_auto_reactive')}}";
  var m_lang_txn_out_admin_recycle_point = "{{ Lang::get('user.txn_out_admin_recycle_point')}}";
  var m_lang_txn_out_market_sell = "{{ Lang::get('user.txn_out_market_sell')}}";
  var m_lang_txn_in_market_buy = "{{ Lang::get('user.txn_in_market_buy')}}";
  var m_lang_txn_in_tx_atm_buy = "{{ Lang::get('user.txn_in_tx_atm_buy')}}";
  var m_lang_txn_out_static_fee = "{{ Lang::get('user.txn_out_static_fee')}}";
  var m_lang_txn_out_static_inactive = "{{ Lang::get('user.txn_out_static_inactive')}}";
  var m_lang_txn_out_lottery_9blocks_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_bet')}}";
  var m_lang_txn_in_lottery_9blocks_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_bet')}}";
  var m_lang_txn_out_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_out_lottery_9blocks_10times_bet')}}";
  var m_lang_txn_in_lottery_9blocks_10times_bet = "{{ Lang::get('user.txn_in_lottery_9blocks_10times_bet')}}";
  var m_lang_txn_in_color_tx_atm_buy = "{{ Lang::get('user.txn_in_color_tx_atm_buy')}}";
  var m_lang_txn_in_admin_deposit_point = "{{ Lang::get('user.txn_in_admin_deposit_point')}}"; 
  var m_lang_txn_in_admin_deposit_colorpoint = "{{ Lang::get('user.txn_in_admin_deposit_colorpoint')}}";
  var m_lang_txn_in_static_10k_bonus = "{{ Lang::get('user.txn_in_static_10k_bonus')}}";
  var m_lang_txn_out_user_withdraw = "{{ Lang::get('user.txn_out_user_withdraw')}}"; 
  var m_lang_txn_failed_direct_push_bonus_generation_1_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_1_out')}}";
  var m_lang_txn_failed_direct_push_bonus_generation_2_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_2_out')}}";
  var m_lang_txn_failed_direct_push_bonus_generation_3_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_3_out')}}";
  var m_lang_txn_failed_direct_push_bonus_generation_4_out = "{{ Lang::get('user.txn_failed_direct_push_bonus_generation_4_out')}}";

  var m_lang_txn_in_daily_reward_100k = "{{ Lang::get('user.txn_in_daily_reward_100k')}}"; 
  var m_lang_txn_in_daily_reward_200k = "{{ Lang::get('user.txn_in_daily_reward_200k')}}";
  var m_lang_txn_in_daily_reward_300k = "{{ Lang::get('user.txn_in_daily_reward_300k')}}";
  var m_lang_txn_in_daily_reward_400k = "{{ Lang::get('user.txn_in_daily_reward_400k')}}"; 
  var m_lang_txn_out_daily_reward_out = "{{ Lang::get('user.txn_out_daily_reward_out')}}";     

  var m_lang_frozen_trade_by_id = "{{ Lang::get('admin.frozen_trade_by_id')}}";
  var m_lang_frozen_trade_by_phone = "{{ Lang::get('admin.frozen_trade_by_phone')}}";
  var m_lang_frozen_trade_by_bank = "{{ Lang::get('admin.frozen_trade_by_bank')}}";
  var m_lang_type_phone = "{{ Lang::get('admin.type_phone')}}";
  var m_lang_type_id = "{{ Lang::get('admin.type_id')}}";
  var m_lang_type_bank_account = "{{ Lang::get('admin.type_bank_account')}}";

  var m_lang_frozen = "{{ Lang::get('admin.frozen')}}";
  var m_lang_thaw = "{{ Lang::get('admin.thaw')}}";
  var m_lang_frozen_status = "{{ Lang::get('admin.frozen_status')}}";
  var m_lang_frozen_created_at = "{{ Lang::get('admin.frozen_created_at')}}";
  var m_lang_frozen_range = "{{ Lang::get('admin.frozen_range')}}";
  var m_lang_frozen_content = "{{ Lang::get('admin.frozen_content')}}";

  var m_lang_open = "{{ Lang::get('user.open')}}";  

	// generation
	var m_lang_txn_in_direct_push_bonus_generation_1 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_1')}}"; 
	var m_lang_txn_in_direct_push_bonus_generation_2 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_2')}}";
	var m_lang_txn_in_direct_push_bonus_generation_3 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_3')}}";
	var m_lang_txn_in_direct_push_bonus_generation_4 = "{{ Lang::get('user.txn_in_direct_push_bonus_generation_4')}}"; 


// ready-jobs
$(function(){
    $("#div_colorpoint").hide();



   	setTimeout(function(){
        checkIsColorpoint();
        verifyFrozen_byID();
        verifyFrozen_byPhone();
        verifyFrozen_byBank();
        GetFrozenInfo();

        getUserAccountInfo();
	      GetBallInfo();    
        GetWalletInfo();

	  },500);

})


// ＊＊ 取得 Ball資訊 ＊＊
var m_account = '';
function getUserAccountInfo(){

    p_ApiMgr_admin.getUserAccountInfo(
          p_user_id,
          function (result) {
            var obj = JSON.parse(result);
            //console.log(obj.account);
            $("#show_account").val(obj.account);            
            $("#show_nickname").val(obj.nickname);
            $("#select_country_code").val(obj.country_code);
            $("#show_phone").val(obj.phone);
            $("#select_bank_code").val(obj.bank_code);
            $("#show_bank_branch").val(obj.bank_branch);
            $("#show_bank_account").val(obj.bank_account);  
            $("#send_point_user_account").val(obj.account); 
            m_account = obj.account;

          },this,
          function (error) {},this
    );

    return false;
}


// ＊＊ 檢查用點數還是彩點 ＊＊
var m_is_colorpoint = 0;
function checkIsColorpoint(){


  p_ApiMgr_admin.checkIsColorpoint(

        function (result) {
            //console.log('result = '+result);
            //alert('result='+result);
            m_is_colorpoint = result['is_colorpoint'];
            //console.log('result='+result['data']);
            //var console_point = 'point';
            if(m_is_colorpoint == 1){
                $("#div_colorpoint").show();
                $("#div_send_color_point").show();
                //console_point = 'colorpoint';            
                GetColorwalletInfo();   // 取得彩點錢包ColorPoint資訊
            }

            console.log(m_is_colorpoint);

        },this,
        function (error) {},this
  );


  return false;
}


// ＊＊ 手動贈點 ＊＊
function confirmSendPointModal(){

  p_ApiMgr_admin.send_points(
        //$("#span_send_point_account").val(),
        p_user_id,
        $("#send_point_points").val(),
        function (result) {
            //console.log('重讀point表格');
            $('#table_wallet_info').DataTable().destroy();
             GetWalletInfo();           
             alert('撥點成功');

        },this,
        function (error) {},this
  );


  return false;
}


// ＊＊ 手動贈彩點 ＊＊
function confirmSendColorPointModal(){

  //alert('贈送彩點');
  p_ApiMgr_admin.send_color_points(
        //$("#span_send_point_account").val(),
        p_user_id,
        $("#send_point_color_points").val(),
        function (result) {
            console.log(result);
            $('#table_colorwallet_info').DataTable().destroy();
             GetColorwalletInfo();           
             alert('撥點成功');

        },this,
        function (error) {},this
  );


  return false;
}


// 強制通過驗證
function forceVerified(){

    $("#span_force_country_code").text($("#select_country_code").val());
    $("#span_force_phone").text($("#show_phone").val());
    $("#forceverifyModal").modal('toggle');
}

function confirmForceVerifyPhone(){
  $("#forceverifyModal").modal('hide');

    p_ApiMgr_admin.forceVerifyMobile(
        p_user_id,
        $("#select_country_code").val(),
        $("#show_phone").val(),
        function (result) {
            //console.log('強制驗證成功'+result);
            alert('強制驗證成功');
            location.reload();
        },this,
        function (error) {},this
  );
}

function sendPointModal(){

  $("#send_point_points").val('');
  $("#send_point_color_points").val('');

  $("#sendPointModal").modal('toggle');
}

  
// ＊＊ 按下 贈point ＊＊
function click_send_point(){

    if( $("#send_point_points").val() == ''){
      return false;
    }

    $("#span_send_point_account").text( m_account );
    $("#span_send_point").text( $("#send_point_points").val() );
    
    $("#sendPointModal").modal('hide');
    setTimeout(function(){
        $("#confirmSendPointModal").modal('toggle');
    },500);

    

    return false;
}


// ＊＊ 按下 贈color point ＊＊
function click_send_colot_point(){

    if( $("#send_point_color_points").val() == ''){
      return false;
    }

    $("#span_send_color_point_account").text( m_account );
    $("#span_send_color_point").text( $("#send_point_color_points").val() );
    
    $("#sendPointModal").modal('hide');
    setTimeout(function(){
        $("#confirmSendColorPointModal").modal('toggle');
    },500);

    

    return false;
}


// ＊＊ 按下-切換凍結by ID ＊＊
function clickSwitchFrozenByID(){

    $("#confirmSwitchFrozenbyIDModal").modal('toggle');

    return false;
}


// ＊＊ 按下-切換凍結by 電話 ＊＊
function clickSwitchFrozenByPhone(){

    $("#confirmSwitchFrozenbyPhoneModal").modal('toggle');

    return false;
}


// ＊＊ 按下-切換凍結by 銀行帳號 ＊＊
function clickSwitchFrozenByBank(){

    $("#confirmSwitchFrozenbyBankModal").modal('toggle');

    return false;
}


// ＊＊ 確認切換凍結 電話相關＊＊
var m_is_frozen_trade_by_bank = 1;
function confirmSwitchFrozenbyBankModal(){

    if(m_is_frozen_trade_by_bank==0) {
      m_is_frozen_trade_by_bank = 1;
    } else {
      m_is_frozen_trade_by_bank = 0;
    }

    // 等等做這個api
    p_ApiMgr_admin.switchFrozenByBank(
          p_user_id,
          m_is_frozen_trade_by_bank,                              
          function (result) {
            //alert('修改showFrozenIcon_Bank成功');
            showFrozenIcon_Bank(result['is_frozen_01']);
            $('#table_frozen_info').DataTable().destroy();
            GetFrozenInfo();            
          },this,
          function (error) {},this
    );

    return false;
}


function showFrozenIcon_Bank(value){

    m_is_frozen_trade_by_bank = value;
    if(m_is_frozen_trade_by_bank == 1){
      $("#switch_frozen_bank").attr("src","{{ asset('img/switch_on.png') }}");
    }

    if(m_is_frozen_trade_by_bank == 0){
      $("#switch_frozen_bank").attr("src","{{ asset('img/switch_off.png') }}");
    }

    return false;
}


// ＊＊ 確認切換凍結 電話相關＊＊
var m_is_frozen_trade_by_phone = 1;
function confirmSwitchFrozenbyPhoneModal(){

    //console.log('before:'+m_is_frozen_trade_by_id);
    if(m_is_frozen_trade_by_phone==0) {
      m_is_frozen_trade_by_phone = 1;
    } else {
      m_is_frozen_trade_by_phone = 0;
    }


    // 等等做這個api
    p_ApiMgr_admin.switchFrozenByPhone(
          p_user_id,
          m_is_frozen_trade_by_phone,                              
          function (result) {
            //alert('修改成功');
            showFrozenIcon_Phone(result['is_frozen_01']);
            $('#table_frozen_info').DataTable().destroy();
            GetFrozenInfo();
          },this,
          function (error) {},this
    );

    return false;
}


function showFrozenIcon_Phone(value){

    m_is_frozen_trade_by_phone = value;
    if(m_is_frozen_trade_by_phone == 1){
      $("#switch_frozen_phone").attr("src","{{ asset('img/switch_on.png') }}");
    }

    if(m_is_frozen_trade_by_phone == 0){
      $("#switch_frozen_phone").attr("src","{{ asset('img/switch_off.png') }}");
    }

    return false;
}


// ＊＊ 凍結切換 ＊＊
var m_is_frozen_trade_by_id = 1;
function showFrozenIcon_ID(value){

    m_is_frozen_trade_by_id = value;
    if(m_is_frozen_trade_by_id == 1){
      $("#switch_frozen_id").attr("src","{{ asset('img/switch_on.png') }}");
    }

    if(m_is_frozen_trade_by_id == 0){
      $("#switch_frozen_id").attr("src","{{ asset('img/switch_off.png') }}");
    }

    return false;
}


// ＊＊ 切換凍結 ＊＊
function confirmSwitchFrozenbyIDModal(){

    //console.log('before:'+m_is_frozen_trade_by_id);
    if(m_is_frozen_trade_by_id==0) {
      m_is_frozen_trade_by_id = 1;
    } else {
      m_is_frozen_trade_by_id = 0;
    }


    // 等等做這個api
    p_ApiMgr_admin.switchFrozenByID(
          p_user_id,
          m_is_frozen_trade_by_id,                              
          function (result) {
            //alert('修改成功');
            //console.log('showFrozenIcon_ID:'+result['is_frozen_01']);
            showFrozenIcon_ID(result['is_frozen_01']);
            $('#table_frozen_info').DataTable().destroy();
            GetFrozenInfo();            
          },this,
          function (error) {},this
    );

    return false;
}


// ＊＊ 檢查ID是否被凍結 ＊＊
function verifyFrozen_byID(){

    p_ApiMgr_admin.verifyFrozen_byID(
          p_user_id,                           
          function (result) {
            m_is_frozen_trade_by_id = result['tag_userid'];
            if(result['tag_userid'] == 1){
              showFrozenIcon_ID(result['tag_userid']);
            }
            
          },this,
          function (error) {},this
    );

    return false;
}


// ＊＊ 檢查Phone是否被凍結 ＊＊
function verifyFrozen_byPhone(){

    p_ApiMgr_admin.verifyFrozen_byPhone(
          p_user_id,                           
          function (result) {
            
            m_is_frozen_trade_by_phone = result['tag_phone'];
           
            showFrozenIcon_Phone(result['tag_phone']);
                     
          },this,
          function (error) {},this
    );

    return false;
}



// ＊＊ 檢查Bank是否被凍結 ＊＊
function verifyFrozen_byBank(){

    p_ApiMgr_admin.verifyFrozen_byBank(
          p_user_id,                           
          function (result) {
            //console.log('[verifyFrozen_byBank] tag_bankaccount = '+result['tag_bankaccount']);
            m_is_frozen_trade_by_bank = result['tag_bankaccount'];
           
            showFrozenIcon_Bank(result['tag_bankaccount']);
           
            
          },this,
          function (error) {},this
    );

    return false;
}


// ＊＊ 重設密碼 ＊＊
function resetPassword(){

    var rd6 = Math.floor(Math.random()*900000) + 100000;
    $("#show_new_pw").val(rd6);
    return false;
}


// ＊＊ 按下 保存新內容 ＊＊
function ClickSaveUserInfo(){

    $('#confirmSaveModal').modal('toggle');
    return false;
}


// ＊＊ 確認 保存新內容 ＊＊
function ConfirmSaveUserInfo(){

 
  $('#confirmSaveModal').modal('hide');

  var show_new_pw = $("#show_new_pw").val();
  if(show_new_pw == ''){
    show_new_pw = 0;
  }
  
  p_ApiMgr_admin.updateUserInfo(
        p_user_id,
        $("#show_nickname").val(),     
        $("#select_country_code").val(),
        $("#show_phone").val(),
        show_new_pw,                              
        $("#select_bank_code").val(),
        $("#show_bank_branch").val(),
        $("#show_bank_account").val(),
        function (result) {

          alert('修改成功');
          location.reload();
        },this,
        function (error) {},this
  );

    return false;
}


// ＊＊ 取得 Ball資訊 ＊＊
function GetBallInfo(){
    //m_account
    var out_value = 'safe';
    var account_id = '';
    var m_show = '0';
    
    var ajax_url = '/api/v1/user/ball/'+p_user_id.toString()+'/by/admin';
    $('#table_ball_info').DataTable({
             "language": {
                  "info":m_lang_info,
                  "sLengthMenu": m_lang_show_n_result,
                  "search": m_lang_keyword,  
                  "processing": m_lang_processing,
                  "loadingRecords": m_lang_loadingRecords,
                  "zeroRecords": m_lang_show_zeroRecords,
                  "infoEmpty": m_lang_infoEmpty,
                  "infoPostFix": "",
                 "oPaginate": {
                   "sFirst": m_lang_first_page,
                   "sPrevious": m_lang_pre_page,
                   "sNext": m_lang_next_page,
                   "sLast": m_lang_last_page,
                 },
             },

            "lengthChange": false,
            "pagingType": "simple_numbers",
            "searching": false,            
            "columns": [
                    
                {
                    'data': 'id',
                    'render': function (data, type, row, meta) {
                        if(row['ball_index'] == '0'){
                          data = m_account.toString();
                        } else {
                          data = m_account.toString()+'('+row['ball_index']+')';
                        }

                        return data;
                    }
                },     
                {
                    'data': 'created_at',
                    'render': function (data, type, row, meta) {
                        if(row['level']<=0){
                          return '<span style="color:blue;">'+m_lang_inactivated+'</span>';
                        }                      
                        m_datatime = data.date;
                        m_datatime = m_datatime.toString();
                        m_datatime = m_datatime.substring(0, m_datatime.length-7);
                        return m_datatime;
                    }
                },                                            
                {
                    'data': 'static_current',
                    'render': function (data, type, row, meta) {

                        if(row['level']<=0){
                          return '-';
                        }
                        
                        var row_static_current = parseInt(row['static_current'].toString()) ;
                        var row_static_max = parseInt(row['static_max'].toString()) ;

                        var show_static = row['static_current']+'/'+row['static_max'];
                        if(row_static_current >= row_static_max){
                          data = '<span style="color:red;">'+show_static+'</span>';
                        } else {
                          data = show_static;
                        }

                        return data;
                    }
                }, 
                {
                    'data': 'auto_active',
                    'render': function (data, type, row, meta) {
                      if(row['level']<=0){
                        return '-';
                      }  
                    	data = '<button attr_id="'+ row['id'] +'" type="button" class="btn btn-primary" onclick="openRecommenderTree(this)">'+m_lang_open+'</button>'

                        return data;
                    }
              }
            
            ],
            "serverSide": true,
            "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
    })


  return false;
}


  // ＊＊ 跳頁至branch ＊＊
  function openRecommenderTree(obj){

      var attr_id = $(obj).attr("attr_id");
      //console.log(attr_id);
      //alert('attr_id='+attr_id);
      if(!attr_id || attr_id==''){
  
        return false;
      } 

      location.href = "/admin/manage/recommend/"+attr_id.toString();
  }



  // ＊＊ 取得Wallet Info ＊＊
  function GetWalletInfo(){

    var ajax_url = '/api/v1/wallet/info/'+p_user_id.toString()+'/by/admin';

        $('#table_wallet_info').DataTable({
                 "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,    
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },    
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "columns": [

                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {
                            var datatime = data.date;
                            datatime = datatime.toString();
                            datatime = datatime.substring(0, datatime.length-7);
                            return datatime;
                        }
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, row, meta) {
                            data = '#'+data;
                            return data;
                        }
                    },
                    {

                        'data': 'type',
                        'render': function (data, type, row, meta) {
                            var return_type = data;
                            if(return_type == 'Gift-From'){
                                return_type = m_lang_txn_out_transform;
                            }

                            if(return_type == 'Gift-To'){
                                return_type = m_lang_txn_in_transform;
                            }

                            if(return_type == 'Active-Set'){
                                return_type = m_lang_txn_out_active;
                            }
                           
                            if(return_type == 'pvBonus'){
                                return_type = m_lang_txn_in_pv;
                            }

                            if(return_type == 'pvBonus-Full'){
                                return_type = m_lang_txn_failed_pv_full;
                            }   

                            if(return_type == 'Daily-Static'){
                                return_type = m_lang_txn_in_static;
                            }

                            if(return_type == 'Daily-Static-Out'){
                                return_type = m_lang_txn_failed_static_out;
                            }

                            if(return_type == 'Reactive-Failed'){
                                return_type = m_lang_txn_failed_static_auto_active_out;
                            }

                            if(return_type == 'Direct-Push-Bonus'){
                                return_type = m_lang_txn_in_direct_push_bonus;
                            }                            
                            

                            if(return_type == 'Static-reActive'){
                                return_type = m_lang_txn_out_static_reactive;
                            }  

                            if(return_type == 'Auto-reActive'){
                                return_type = m_lang_txn_out_static_auto_reactive;
                            }  

                            if(return_type == 'Admin-Recycle-Point'){
                                return_type = m_lang_txn_out_admin_recycle_point;
                            }  

                            if(return_type == 'Market-Sell'){
                                return_type = m_lang_txn_out_market_sell;
                            }  
                            
                            if(return_type == 'Market-Buy'){
                                return_type = m_lang_txn_in_market_buy;
                            }  

                            if(return_type == 'TX-ATM-Buy'){
                                return_type = m_lang_txn_in_tx_atm_buy;
                            } 

                            if(return_type == 'Fee-Daily-Static'){
                                return_type = m_lang_txn_out_static_fee;
                            }               

                            if(return_type == 'Daily-Static-Inactived'){
                                return_type = m_lang_txn_out_static_inactive;
                            }  

                            if(return_type == 'Lottery-9Blocks-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_bet;
                            }  

                            if(return_type == 'Lottery-9Blocks-10times-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_10times_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-10times-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_10times_bet;
                            } 
                      
                            if(return_type == 'Admin-Deposit-Point'){
                                return_type = m_lang_txn_in_admin_deposit_point;
                            }

                            if(return_type == 'Daily-Static-10K-Bonus'){
                                return_type = m_lang_txn_in_static_10k_bonus;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-1'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_1;
                            } 

                            if(return_type == 'Direct-Push-Bonus-Generation-2'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_2;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-3'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_3;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-4'){
                                return_type = m_lang_txn_in_direct_push_bonus_generation_4;
                            }  

                            if(return_type == 'User-Withdraw'){
                                return_type = m_lang_txn_out_user_withdraw;
                            } 

                            if(return_type == 'Direct-Push-Bonus-Generation-1-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_1_out;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-2-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_2_out;
                            }

                            if(return_type == 'Direct-Push-Bonus-Generation-3-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_3_out;
                            }                            

                            if(return_type == 'Direct-Push-Bonus-Generation-4-Out'){
                                return_type = m_lang_txn_failed_direct_push_bonus_generation_4_out;
                            }     

                            if(return_type == 'Performance-100K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_100k;
                            }

                            if(return_type == 'Performance-200K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_200k;
                            }

                            if(return_type == 'Performance-300K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_300k;
                            }                            

                            if(return_type == 'Performance-400K-Reward'){
                                return_type = m_lang_txn_in_daily_reward_400k;
                            }                             

                            if(return_type == 'Daily-Reward-Out'){
                                return_type = m_lang_txn_out_daily_reward_out;
                            }    

                            return return_type;
                        }
                    },
                    {
                        'data': 'value',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'after',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'link',
                        'render': function (data, type, row, meta) {
                            
                            var return_val = '';

                            if(data){
                                var  m_tree_obj = JSON.parse(data)
                                if(m_tree_obj.comment != null){
                                    return_val = m_tree_obj.comment;
                                }
                            }

                            var myarr = return_val.split(",");
                            new_url = '';
                            if(myarr.length > 1){
                                var new_url = '<a href="/admin/manage/recommend/'+myarr[1].toString()+'" >'+myarr[0].toString()+'</a>';
                            }
                            return new_url;
                        }
                    }

                ],
                "serverSide": true,
                "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
        })


    return false;
  }


  // ＊＊ 取得Wallet Info ＊＊
  function GetColorwalletInfo(){

    var ajax_url = '/api/v1/admin/wallet/colorpoint/info/'+p_user_id.toString();

        $('#table_colorwallet_info').DataTable({
                 "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,    
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },            
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "columns": [

                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {
                            var datatime = data.date;
                            datatime = datatime.toString();
                            datatime = datatime.substring(0, datatime.length-7);
                            return datatime;
                        }
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, row, meta) {
                            data = '#'+data;
                            return data;
                        }
                    },
                    {

                        'data': 'type',
                        'render': function (data, type, row, meta) {
                            var return_type = data;
                            if(return_type == 'Gift-From'){
                                return_type = m_lang_txn_out_transform;
                            }

                            if(return_type == 'Gift-To'){
                                return_type = m_lang_txn_in_transform;
                            }

                            if(return_type == 'Active-Set'){
                                return_type = m_lang_txn_out_active;
                            }
                           
                            if(return_type == 'pvBonus'){
                                return_type = m_lang_txn_in_pv;
                            }

                            if(return_type == 'pvBonus-Full'){
                                return_type = m_lang_txn_failed_pv_full;
                            }   

                            if(return_type == 'Daily-Static'){
                                return_type = m_lang_txn_in_static;
                            }

                            if(return_type == 'Daily-Static-Out'){
                                return_type = m_lang_txn_failed_static_out;
                            }

                            if(return_type == 'Reactive-Failed'){
                                return_type = m_lang_txn_failed_static_auto_active_out;
                            }

                            if(return_type == 'Direct-Push-Bonus'){
                                return_type = m_lang_txn_in_direct_push_bonus;
                            }                            
                            

                            if(return_type == 'Static-reActive'){
                                return_type = m_lang_txn_out_static_reactive;
                            }  

                            if(return_type == 'Auto-reActive'){
                                return_type = m_lang_txn_out_static_auto_reactive;
                            }  

                            if(return_type == 'Admin-Recycle-Point'){
                                return_type = m_lang_txn_out_admin_recycle_point;
                            }  

                            if(return_type == 'Market-Sell'){
                                return_type = m_lang_txn_out_market_sell;
                            }  
                            
                            if(return_type == 'Market-Buy'){
                                return_type = m_lang_txn_in_market_buy;
                            }  

                            if(return_type == 'TX-ATM-Buy'){
                                return_type = m_lang_txn_in_tx_atm_buy;
                            } 

                            if(return_type == 'Fee-Daily-Static'){
                                return_type = m_lang_txn_out_static_fee;
                            }               

                            if(return_type == 'Daily-Static-Inactived'){
                                return_type = m_lang_txn_out_static_inactive;
                            }  

                            if(return_type == 'Lottery-9Blocks-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_bet;
                            }  

                            if(return_type == 'Lottery-9Blocks-10times-Bet'){
                                return_type = m_lang_txn_out_lottery_9blocks_10times_bet;
                            }               

                            if(return_type == 'Lottery-9Blocks-10times-WinPoints'){
                                return_type = m_lang_txn_in_lottery_9blocks_10times_bet;
                            } 

                            if(return_type == 'TX-ATM-Buy-Color-Point'){
                                return_type = m_lang_txn_in_color_tx_atm_buy;
                            }
                     
                            if(return_type == 'Admin-Deposit-Color-Point'){
                                return_type = m_lang_txn_in_admin_deposit_colorpoint;
                            }

                            if(return_type == 'Admin-Deposit-Point'){
                                return_type = m_lang_txn_in_admin_deposit_point;
                            }                            
    

                            return return_type;
                        }
                    },
                    {
                        'data': 'value',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'after',
                        'render': function (data, type, row, meta) {

                            data = toDecimal2(data);
                            return data;
                        }
                    },
                    {
                        'data': 'link',
                        'render': function (data, type, row, meta) {
                            
                            var return_val = '';

                            if(data){
                                var  m_tree_obj = JSON.parse(data)
                                if(m_tree_obj.comment != null){
                                    return_val = m_tree_obj.comment;
                                }
                            }

                            var myarr = return_val.split(",");
                            new_url = '';
                            if(myarr.length > 1){
                                var new_url = '<a href="/admin/manage/recommend/'+myarr[1].toString()+'" >'+myarr[0].toString()+'</a>';
                            }
                            return new_url;
                        }
                    }

                ],
                "serverSide": true,
                "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
        })


    return false;
  }





  // ＊＊ 取得凍結記錄表格 ＊＊
  function GetFrozenInfo(){

    var ajax_url = '/api/v1/admin/frozen/info/'+p_user_id.toString();

        $('#table_frozen_info').DataTable({
                 "language": {
                    "info":m_lang_info,
                    "sLengthMenu": m_lang_show_n_result,
                    "search": m_lang_keyword,    
                    "processing": m_lang_processing,
                    "loadingRecords": m_lang_loadingRecords,
                    "zeroRecords": m_lang_show_zeroRecords,
                    "infoEmpty": m_lang_infoEmpty,
                    "infoPostFix": "",
                     "oPaginate": {
                       "sFirst": m_lang_first_page,
                       "sPrevious": m_lang_pre_page,
                       "sNext": m_lang_next_page,
                       "sLast": m_lang_last_page,
                     },
                 },            
                "lengthChange": false,
                "pagingType": "simple_numbers",
                "searching": false,
                "columns": [
                    {
                        'data': 'deleted_at',
                        'render': function (data, type, row, meta) {
                            var datatime = '';
                            try {
                                datatime = data.date;
                            }
                            catch(err) {
                                datatime = 'null';
                            }

                            if(datatime!='null'){
                              datatime = data.date;
                              datatime = datatime.toString();
                              datatime = datatime.substring(0, datatime.length-7);
                              return m_lang_thaw+datatime;
                            } else {
                              // datatime = row['created_at'].date;  var m_lang_frozen = "{{ Lang::get('admin.frozen')}}";

                              // datatime = datatime.toString();
                              // datatime = datatime.substring(0, datatime.length-7);                              
                              return '<span style="color:blue;">'+m_lang_frozen+'</span>';
                            }

                        }
                    },                
                    {
                        'data': 'created_at',
                        'render': function (data, type, row, meta) {
                              datatime = data.date;
                              datatime = datatime.toString();
                              datatime = datatime.substring(0, datatime.length-7);
                              return datatime;
                        }
                    },
                    {
                        'data': 'tag',
                        'render': function (data, type, row, meta) {
                            if(data=='PHONE'){
                               data = m_lang_type_phone;
                             }

                            if(data=='USER_ID'){
                               data = m_lang_type_id;
                             }

                            if(data=='BANK_ACCOUNT'){
                               data = m_lang_type_bank_account;
                             }
                            
                            return data;
                        }
                    },
                    {
                        'data': 'info',
                        'render': function (data, type, row, meta) {
                            data = data;
                            return data;
                        }
                    }


                ],
                "serverSide": true,
                "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
        })


    return false;
  }

</script>

@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.account')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
      &nbsp;>&nbsp;{{ Lang::get('admin.manager_members')}}      
			&nbsp;>&nbsp;{{ Lang::get('admin.member_account')}}
      &nbsp;>&nbsp;{{ Lang::get('user.account')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

  <!-- User Info -->
    <div class="container">

        <h2>{{ Lang::get('admin.admin_pay_points')}}</h2>
        <br />
        <button type="button" class="btn btn-success" onclick="sendPointModal()">{{ Lang::get('admin.admin_pay_points')}}</button>
        <br /><br />

      <h2>{{ Lang::get('admin.member_info')}}</h2>
      <div id="nav_acc_info" class="tab-pane fade in active">     

        {{ Lang::get('user.account')}}        
        <input type="text" class="form-control" id="show_account"  disabled>
        <br />          
        {{ Lang::get('user.nickname')}}        
        <input type="text" class="form-control" id="show_nickname" maxlength="16">  
        <br />
        {{ Lang::get('user.country_code')}}
      <select class="form-control" id="select_country_code"></div>      
        @include('components.country_code')
      </select>
        <br />
        {{ Lang::get('user.phone')}}        
        <!-- <input type="text" class="form-control" id="show_phone" maxlength="10"> -->
      <div style="display:table-cell;">
        <button type="button" class="btn btn-primary" onclick="forceVerified()">{{ Lang::get('admin.force_verified')}}  </button>         
      </div>      

      <div style="display:table-cell;width: 100%;">
        <input type="text" class="form-control" id="show_phone" placeholder="" style="width: 99%;" >
      </div>         
        <br />          


      {{ Lang::get('user.password')}}  
      <div style="display:table-cell;">
        <button type="button" class="btn btn-primary" onclick="resetPassword()">{{ Lang::get('admin.reset_pw')}}  </button>         
      </div>      

      <div style="display:table-cell;width: 100%;">
        <input type="text" class="form-control" id="show_new_pw" placeholder="" style="color: red;width: 99%;"  disabled>
      </div>      
      <br />

        {{ Lang::get('user.bank_code')}}
      <select class="form-control" id="select_bank_code"></div>     
        @include('components.bank_option')
      </select>    

        <br />
        {{ Lang::get('user.bank_branch')}}
        <input type="text" class="form-control" id="show_bank_branch" maxlength="16">
        <br />        
        {{ Lang::get('user.bank_account')}}
        <input type="text" class="form-control" id="show_bank_account" maxlength="16">
        <br /> 


      <button type="button" class="btn btn-warning" onclick="ClickSaveUserInfo()" >{{ Lang::get('admin.save')}}</button>   
      </div>
    </div>
  
  <br />
  <hr align="center" width="85%" class="site_map_hr">

	<!-- Ball DataTable -->
	<div class="container">
		<h2>{{ Lang::get('admin.account_status')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
			        <table id="table_ball_info" class="display" style="width:100%">
			            <thead>
			                <tr>			  			                     				                    
                          <th>{{ Lang::get('user.account')}}</th>
                          <th>{{ Lang::get('user.active_date')}}</th>
			                    <th>{{ Lang::get('user.out_static')}}</th>	                	
			                    <th>{{ Lang::get('user.org_recommend')}}</th>										                   
			                    <!-- 已配對完 撤回 -->
			                </tr>
			            </thead>
			            <tbody>
			                <tr>			             			             
			                    <td></td>
			                    <td></td>
			                    <td></td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			  </div>

			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
					<br />
				</div>
			  </div>
		</div>
	</div>

  <br />
  <hr align="center" width="85%" class="site_map_hr">


  <div class="container">
    <h2>{{ Lang::get('admin.frozen_control')}}</h2>
    <div class="hide_overflow" style="overflow:auto;"> 
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">     
            <div style="display:table-cell;">
    
              <table border="0">
              　<tr>
              　  <td>
                    <p>{{ Lang::get('admin.frozen_trade_by_id')}}</p>  
                    <div>
                      <img id="switch_frozen_id" class="switch_group" onclick="clickSwitchFrozenByID()" src="{{ asset('img/switch_off.png')}}">            
                    </div>   
                  </td>  

              　  <td style="width: 80px;"></td>

              　  <td>
                    <p>{{ Lang::get('admin.frozen_trade_by_phone')}}</p> 
                    <div>
                      <img id="switch_frozen_phone" class="switch_group" onclick="clickSwitchFrozenByPhone()" src="{{ asset('img/switch_off.png')}}">            
                    </div>   
                  </td> 

              　  <td style="width: 80px;"></td>

              　  <td>
                    <p>{{ Lang::get('admin.frozen_trade_by_bank')}}</p>  
                    <div>
                      <img id="switch_frozen_bank" class="switch_group" onclick="clickSwitchFrozenByBank()" src="{{ asset('img/switch_off.png')}}">            
                    </div>   
                  </td> 

              　</tr>
              </table>
     

            </div>  
          </div>
        </div>
      </div>
    </div>



<br />
 <!-- Frozen DataTable -->
  <div class="container">
    <h2></h2>
    <div class="hide_overflow" style="overflow:auto;"> 
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
              <table id="table_frozen_info" class="display" style="width:100%">
                  <thead>
                      <tr>       
                        <th>{{ Lang::get('admin.frozen_status')}}</th>                        
                        <th>{{ Lang::get('admin.frozen_created_at')}}</th>  
                        <th>{{ Lang::get('admin.frozen_range')}}</th>                    
                        <th>{{ Lang::get('admin.frozen_content')}}</th>          


                          <!-- 已配對完 撤回 -->
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                  </tbody>
              </table>
          </div>
        </div>

    </div>
  </div>



  <br />
  <hr align="center" width="85%" class="site_map_hr">

 <!-- Wallet DataTable -->
  <div class="container">
    <h2>{{ Lang::get('user.wallet_info')}}</h2>
    <div class="hide_overflow" style="overflow:auto;"> 
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
              <table id="table_wallet_info" class="display" style="width:100%">
                  <thead>
                      <tr>        
                        <th>{{ Lang::get('user.datetime')}}</th>                    
                        <th>{{ Lang::get('user.single_num')}}</th>
                        <th>{{ Lang::get('user.content')}}</th>
                        <th>{{ Lang::get('user.value_point')}}</th>
                        <th>{{ Lang::get('user.balance')}}</th>
                        <th>{{ Lang::get('user.comment')}}</th>                                    
                          <!-- 已配對完 撤回 -->
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                  </tbody>
              </table>
          </div>
        </div>

    </div>
  </div>

  <br />
  <hr align="center" width="85%" class="site_map_hr">

 <!-- Wallet DataTable -->
  <div id="div_colorpoint" class="container">
    <h2>{{ Lang::get('user.wallet_info')}}</h2>
    <div class="hide_overflow" style="overflow:auto;"> 
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
              <table id="table_colorwallet_info" class="display" style="width:100%">
                  <thead>
                      <tr>        
                        <th>{{ Lang::get('user.datetime')}}</th>                    
                        <th>{{ Lang::get('user.single_num')}}</th>
                        <th>{{ Lang::get('user.content')}}</th>
                        <th>{{ Lang::get('user.color_point')}}</th>
                        <th>{{ Lang::get('user.balance')}}</th>
                        <th>{{ Lang::get('user.comment')}}</th>                                    
                          <!-- 已配對完 撤回 -->
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                  </tbody>
              </table>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
          <br /><br /><br /><br /><br />
        </div>
        </div>
    </div>
  </div>





  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">         
    <br /><br /><br /><br /><br />
  </div>
  </div>

<!-- Save User Info Modal -->
<div id="confirmSaveModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.update_user_info')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_update')}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="ConfirmSaveUserInfo()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<!-- Switch Frozen by ID Modal -->
<div id="confirmSwitchFrozenbyIDModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.frozen_trade_by_id')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_switch')}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSwitchFrozenbyIDModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- Switch Frozen by Phone Modal -->
<div id="confirmSwitchFrozenbyPhoneModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.frozen_trade_by_phone')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_switch')}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSwitchFrozenbyPhoneModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Switch Frozen by Phone Bank -->
<div id="confirmSwitchFrozenbyBankModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.frozen_trade_by_bank')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_switch')}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSwitchFrozenbyBankModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="sendPointModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.admin_pay_points')}}</h4>
      </div>
      <div class="modal-body">
    
    <div class="form-group">
      <label for="">{{ Lang::get('user.account')}}</label>
      <input type="text" class="form-control" id="send_point_user_account"  disabled="">
    </div>
      <label for="">{{ Lang::get('user.point')}}</label>
        <div style="display:table-cell;width: 99%;">
          <input type="text" class="form-control" id="send_point_points" placeholder="" maxlength="8">
        </div> 

        <div style="display:table-cell;">  
          <button type="button" class="btn btn-success" onclick="click_send_point()">{{ Lang::get('admin.send_point')}}  </button>
        </div>      

        <br />
       <div id="div_send_color_point" style="display: none;"> 
        <label for="">{{ Lang::get('user.color_point')}}</label>
          <div style="display:table-cell;width: 99%;">
            <input type="text" class="form-control" id="send_point_color_points" placeholder="" maxlength="8">
          </div> 

          <div style="display:table-cell;">
            <button type="button" class="btn btn-success" onclick="click_send_colot_point()">{{ Lang::get('admin.send_point')}}  </button>
          </div>   
        </div>

      <br /><br />

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- confirmSendPointModal -->
<div id="confirmSendPointModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.admin_pay_points')}}</h4>
      </div>
      <div class="modal-body">
        
        <p>{{ Lang::get('user.account')}} &nbsp; <span id="span_send_point_account" style="color: blue;"></span></p>
        <p><span style="color: blue;">{{ Lang::get('user.point')}}</span>&nbsp;<span id="span_send_point" style="color: red;"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSendPointModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- confirmSendColorPointModal -->
<div id="confirmSendColorPointModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.admin_pay_color_points')}}</h4>
      </div>
      <div class="modal-body">
        
        <p>{{ Lang::get('user.account')}} &nbsp; <span id="span_send_color_point_account" style="color: blue;"></span></p>
        <p><span style="color: blue;">{{ Lang::get('user.color_point')}}</span>&nbsp;<span id="span_send_color_point" style="color: red;"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSendColorPointModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- confirmSendColorPointModal -->
<div id="forceverifyModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.force_verified')}}</h4>
      </div>
      <div class="modal-body">
        
        <p>(+<span id="span_force_country_code" style="color: blue;"></span>)&nbsp;<span id="span_force_phone" style="color: blue;"></span></p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmForceVerifyPhone()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection






















