
@extends('layouts.admin_app') 


@push('head')
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/u_calender.css') }}">	
    <style type="text/css">
		
		.nori_title{
			font-size: 20px;
		}

		#wrap {
			width: 1100px;
			margin: 0 auto;
			}
			
		#external-events {
			float: left;
			width: 150px;
			padding: 0 10px;
			text-align: left;
			}
			
		#external-events h4 {
			font-size: 16px;
			margin-top: 0;
			padding-top: 1em;
			}
			
		.external-event { /* try to mimick the look of a real event */
			margin: 10px 0;
			padding: 2px 4px;
			background: #3366CC;
			color: #fff;
			font-size: .85em;
			cursor: pointer;
			}
			
		#external-events p {
			margin: 1.5em 0;
			font-size: 11px;
			color: #666;
			}
			
		#external-events p input {
			margin: 0;
			vertical-align: middle;
			}

		#calendar {
	/* 		float: right; */
	        margin: 0 auto;
			width: 900px;
			background-color: #FFFFFF;
			  border-radius: 6px;
	        box-shadow: 0 1px 2px #C3C3C3;
			-webkit-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
	-moz-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
	box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
			}


    </style>

@endpush


@push('script')
	<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
	<script src="{{ asset('js/admin/u_common.js') }}"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>	
  	<script src="{{ asset('js/u_calender.js') }}"></script>	
	<script type="text/javascript">

			
		$(document).ready(function() {
		　		
		  setTimeout(function(){
		      //nori_show_calender();
		      getDividendRates();
		  },500);
			
		});


		// ＊＊ 取得目前已存倍率 ＊＊
		var m_calender_title_array = [];
		function getDividendRates(){

		  m_calender_title_array = [];
		  p_ApiMgr_admin.getDividendRates(

		        function (result) {
		        	//console.log('aaaa = '+ result['aaaa']);
		        	for (var i = result.length - 1; i >= 0; i--) {
		        		
		        		push_title(
		        			      result[i].year
		        				  , result[i].month
		        				  , result[i].day
		        				  , result[i].rate
		        				  );
		        	}
		            nori_show_calender();

		                        
		        },this,
		        function (error) {},this
		  );

		  return false;
		}




		function addDividendRates(input_year, input_month, input_day, input_content){

			  p_ApiMgr_admin.addDividendRates(
			        	input_year,
			        	input_month,
			        	input_day,
			        	input_content,
			        function (result) {
			        	// 後端會加入一筆資料, 然後重吐全部回來
			        	//console.log(result);
			        	m_calender_title_array = [];
			        	for (var i = result.length - 1; i >= 0; i--) {
			        		
			        		push_title(
			        			      result[i].year
			        				  , result[i].month
			        				  , result[i].day
			        				  , result[i].rate
			        				  );
			        	}
			            nori_show_calender();

			                        
			        },this,
			        function (error) {},this
			  );

			
		//console.log(m_calender_title_array);
		}



		// ＊＊＊　把收到的倍率資訊　丟到陣列　秀出來　＊＊＊
		
		function push_title(input_year, input_month, input_day, input_content){

			var title_obj = {};
			title_obj.title = '<span class="nori_title">'+input_content+'%</span>';
			title_obj.start = new Date(input_year, input_month, input_day);


			m_calender_title_array.push(title_obj);

			// 先檢查有沒有重複的那天, 有的畫覆蓋過去
			// var is_titled = false;
			// for (var i = m_calender_title_array.length - 1; i >= 0; i--) {
			// 	var _day   = m_calender_title_array[i].start.getDate();
			// 	var _month = m_calender_title_array[i].start.getMonth();
			// 	var _year  = m_calender_title_array[i].start.getFullYear();

			// 	if(_day == input_day && _month == input_month && _year == input_year){
			// 		m_calender_title_array[i] = title_obj;
			// 		is_titled = true;
			// 		break;
			// 	}
			// }

			// // 沒有重複的日期, 才新寫入
			// if(!is_titled){
			// 	m_calender_title_array.push(title_obj);
			// }
			
			//console.log(m_calender_title_array);
		}


		// ＊＊＊　日曆顯示　＊＊＊
		function nori_show_calender(){

				$('#calendar').html('');
			    var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				
				/*  className colors
				
				className: default(transparent), important(red), chill(pink), success(green), info(blue)
				
				*/		
				
				  
				/* initialize the external events
				-----------------------------------------------------------------*/
			
				$('#external-events div.external-event').each(function() {
					
					// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
					// it doesn't need to have a start or end
					var eventObject = {
						title: $.trim($(this).text()) // use the element's text as the event title
					};
		
					// store the Event Object in the DOM element so we can get to it later
					$(this).data('eventObject', eventObject);
					
					// make the event draggable using jQuery UI
					$(this).draggable({
						zIndex: 999,
						revert: true,      // will cause the event to go back to its
						revertDuration: 0  //  original position after the drag
					});
					
				});
			
			
				/* initialize the calendar
				-----------------------------------------------------------------*/
				
				var calendar =  $('#calendar').fullCalendar({
					header: {
						left: 'title',
						center: 'month',
						right: 'prev,next today'
					},
					editable: true,
					firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
					selectable: true,
					defaultView: 'month',
					
					axisFormat: 'h:mm',
					columnFormat: {
		                month: 'ddd',    // Mon
		                week: 'ddd d', // Mon 7
		                day: 'dddd M/d',  // Monday 9/7
		                agendaDay: 'dddd d'
		            },
		            titleFormat: {
		                month: 'MMMM yyyy', // September 2009
		                week: "MMMM yyyy", // September 2009
		                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
		            },
					allDaySlot: false,
					selectHelper: true,
					select: function(start, end, allDay) {
						
						// 如果日期比明天小 return  date start
						if(Date.parse(date+1).valueOf() > Date.parse(start).valueOf()){
							console.log('日期小於今日');
							return;
						}

						var title = prompt(start.toLocaleDateString()+' 請輸入倍率數字');
						if (title) {
							//console.log('留言: 開始時間 start = '+start.toLocaleDateString()+' , 留言內容 = '+title);
							var _local_day = start.getDate();
							var _local_month = start.getMonth();
							var _local_year = start.getFullYear();
							addDividendRates(_local_year,_local_month,_local_day,title);
							//nori_show_calender();
							// calendar.fullCalendar('renderEvent',
							// 	{
							// 		title: title,
							// 		start: start,
							// 		end: end,
							// 		allDay: allDay
							// 	},
							// 	true // make the event "stick"
							// );
							
						}
						//calendar.fullCalendar('unselect');
					},
					droppable: true, // this allows things to be dropped onto the calendar !!!
					drop: function(date, allDay) { // this function is called when something is dropped
						
						// retrieve the dropped element's stored Event Object
						var originalEventObject = $(this).data('eventObject');

						// we need to copy it, so that multiple events don't have a reference to the same object
						var copiedEventObject = $.extend({}, originalEventObject);
						
						// assign it the date that was reported
						copiedEventObject.start = date;
						copiedEventObject.allDay = allDay;
						
						// render the event on the calendar
						// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
						$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
						
						// is the "remove after drop" checkbox checked?
						if ($('#drop-remove').is(':checked')) {
							// if so, remove the element from the "Draggable Events" list
							$(this).remove();
						}
						
					},
					
					events: 
						m_calender_title_array
						// {
						// 	title: '<span class="nori_title">1.2</span>',
						// 	start: new Date(y, m, 1)
						// },
						// {
						// 	title: '<span class="nori_title">1.8</span>',
						// 	start: new Date(y, m, 15)
						// },					

					,			
				});

		}



	</script>

@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('admin.dividend_setting')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('admin.dividend_setting')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<!-- 日曆 -->
<div id='wrap' style="position: relative;left: -80px;">
	<div id='calendar'></div>

	<div style='clear:both'></div>
</div>


@endsection






















