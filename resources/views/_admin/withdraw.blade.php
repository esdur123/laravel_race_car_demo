
@extends('layouts.admin_app') 


@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<style type="text/css">
		.userinfo_mid_table, .userinfo_mid_table tr,.userinfo_mid_table tr td{
			text-align: center;
			border:1px #ddd solid;
			font-size: 20px;
		}

		.userinfo_mid_table{
			width: 100%;
		}

		.userinfo_mid_table tr td{
			width: 50%;
		}

		input[type=radio] {
			width:0.6em;
			height:0.6em;
		   
		}
		
	</style>
@endpush


@push('script')
	<script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
	<script src="{{ asset('js/admin/u_common.js') }}"></script>
	<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/u_my_utility.js') }}"></script>
	<script type="text/javascript">

		var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
		var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
		var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
		var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
		var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
		var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
		var m_lang_processing = "{{ Lang::get('table.processing')}}";
		var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
		var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
		var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
		var m_lang_info = "{{ Lang::get('table.info')}}";

		var m_lang_order_down = "{{ Lang::get('user.order_down')}}";
		var m_lang_cancel_order = "{{ Lang::get('user.cancel_order')}}";
		var m_lang_paid_already = "{{ Lang::get('user.paid_already')}}";
		var m_lang_contact = "{{ Lang::get('user.contact')}}";
		var m_lang_cancel = "{{ Lang::get('user.cancel')}}";
		var m_lang_payment_received = "{{ Lang::get('user.payment_received')}}";
		var m_lang_payment_paid = "{{ Lang::get('user.payment_paid')}}";
		var m_lang_seller_name = "{{ Lang::get('user.seller_name')}}";
		var m_lang_buyer_name = "{{ Lang::get('user.buyer_name')}}";		
		var m_lang_paid_complete = "{{ Lang::get('user.paid_complete')}}";
		var m_lang_receive_complete = "{{ Lang::get('user.receive_complete')}}";
		var m_lang_deal_done = "{{ Lang::get('user.deal_done')}}";
		var m_lang_buyer_cancel = "{{ Lang::get('user.buyer_cancel')}}";
		var m_lang_seller_cancel = "{{ Lang::get('user.seller_cancel')}}";

		var m_lang_buyer_info = "{{ Lang::get('admin.buyer_info')}}";
		var m_lang_seller_info = "{{ Lang::get('admin.seller_info')}}";
		var m_lang_deal_record = "{{ Lang::get('admin.deal_record')}}";
		var m_lang_options = "{{ Lang::get('admin.options')}}";
		var m_lang_nickname = "{{ Lang::get('admin.nickname')}}";
		var m_lang_phone = "{{ Lang::get('admin.phone')}}";
		var m_lang_bank_code = "{{ Lang::get('admin.bank_code')}}";
		var m_lang_back_account = "{{ Lang::get('admin.back_account')}}";
		var m_lang_buyer_confirmed = "{{ Lang::get('admin.buyer_confirmed')}}";		
		var m_lang_seller_confirmed = "{{ Lang::get('admin.seller_confirmed')}}";
		var m_lang_member_id = "{{ Lang::get('admin.member_info')}}";
		var m_lang_order_amount = "{{ Lang::get('admin.order_amount')}}";
		var m_lang_dealed = "{{ Lang::get('admin.dealed')}}";
		var m_lang_on_trade = "{{ Lang::get('admin.on_trade')}}";
		var m_lang_none_record = "{{ Lang::get('admin.none_record')}}";	
		var m_lang_open = "{{ Lang::get('user.open')}}";			

	    var m_lang_withdraw_result_waiting      = "{{ Lang::get('user.withdraw_result_waiting')}}";
	    var m_lang_withdraw_result_deal         = "{{ Lang::get('user.withdraw_result_deal')}}";
	    var m_lang_withdraw_result_user_cancel  = "{{ Lang::get('user.withdraw_result_user_cancel')}}";
	    var m_lang_withdraw_result_admin_cancel = "{{ Lang::get('user.withdraw_result_admin_cancel')}}";
	    var m_lang_withdraw_result_accepting = "{{ Lang::get('user.withdraw_result_accepting')}}";
	    var m_lang_cancel_withdraw = "{{ Lang::get('user.cancel_withdraw')}}";
	    var m_lang_withdraw_user_info = "{{ Lang::get('user.withdraw_user_info')}}";




		// ready-jobs
		$(function(){


		// $(".radio-inline") // select the radio by its id
		//     .change(function(){ // bind a function to the change event
		//         if( $(this).is(":checked") ){ // check if the radio is checked
		//             var val = $(this).val(); // retrieve the value
		//             console.log(val);
		//         }
		//     });

			$('input[type=radio][name=optradio]').change(function() {
				//alert(this.value); 在change的時候 傳datatable後面多一個值 然後reload
				reload_datatable();
			});		    
			//$('input[name=optradio]:checked').val()



		    setTimeout(function(){
		        get_all_withdraw_datatable();

		    },500);

		})
		          

		// ＊＊ 重新讀取datatable ＊＊
		function reload_datatable(){
	         $('#table_withdraw').DataTable().destroy();
	         get_all_withdraw_datatable();	
		}

		  // ＊＊ 跳頁至branch ＊＊
		  function openRecommenderTree(obj){

		      var attr_id = $(obj).attr("attr_id");
		      location.href = "/admin/manage/recommend/"+attr_id.toString();

		  }


	    // ＊＊ 取得-交易中列表 ＊＊
		var m_buy_datatable = null;
		function get_all_withdraw_datatable(){

		    var ajax_url = '/api/v1/admin/withdraw/info/'+$('input[name=optradio]:checked').val();  // TransactionController@getOrder
		    m_buy_datatable = $('#table_withdraw').DataTable({
		             "language": {
		                  "info":m_lang_info,
		                  "sLengthMenu": m_lang_show_n_result,
		                  "search": m_lang_keyword,  
		                  "processing": m_lang_processing,
		                  "loadingRecords": m_lang_loadingRecords,
		                  "zeroRecords": m_lang_show_zeroRecords,
		                  "infoEmpty": m_lang_infoEmpty,
		                  "infoPostFix": "",
		                 "oPaginate": {
		                   "sFirst": m_lang_first_page,
		                   "sPrevious": m_lang_pre_page,
		                   "sNext": m_lang_next_page,
		                   "sLast": m_lang_last_page,
		                 },
		             },
		            "processing": true,
		            "lengthChange": false,
		            "pagingType": "simple_numbers",
		            "searching": false,   
		            "destroy": true,                      
		            "columns": [	 
  		                {
		                    'data': 'id',
		                    'render': function (data, type, row, meta) {

		                        return data;
		                    }
		                },
		                {
		                    'data': 'created_at',
		                    'render': function (data, type, row, meta) {
		                        m_datatime = data.date;
		                        m_datatime = m_datatime.toString();
		                        m_datatime = m_datatime.substring(0, m_datatime.length-7);
		                        return m_datatime;
		                    }
		                },
		                {
		                    'data': 'amount',
		                    'render': function (data, type, row, meta) {
		                                  
		                        return data;
		                    }
		                },
		                {
		                    'data': 'id',
		                    'render': function (data, type, row, meta) {
		                        return '10';
		                    }
		                },
		                {
		                    'data': 'amount',
		                    'render': function (data, type, row, meta) {
		                    	var real_amount = data - 10;

		                        return real_amount;
		                    }
		                },
		                {
		                    'data': 'amount',
		                    'render': function (data, type, row, meta) {
		                    	var option_id          = row['id'];
		                    	var option_amount      = row['amount'] - 10;
		                    	var option_user_id     = row['user_id'];

                                data = '<button type="button" class="btn btn-default" option_id="'+option_id+'" option_user_id="'+option_user_id+'" option_amount="'+option_amount+'" onclick="get_usr_info(this)">'+m_lang_withdraw_user_info+'</button>';

		                        return data;
		                    }
		                },		                
		                {
		                    'data': 'state',
		                    'render': function (data, type, row, meta) {

                            var datatime = row['updated_at'].date;
                            datatime = datatime.toString();
                            datatime = datatime.substring(0, datatime.length-7);		                    	
	
                            if(data == 'Waiting') {
                                data = m_lang_withdraw_result_waiting;
                            }

                            if(data == 'Deal') {
                                data = '<span style="color:blue;">'+m_lang_withdraw_result_deal+'</span><br />'+datatime;
                            }

                            if(data == 'User-Canceled') {
                                data = m_lang_withdraw_result_user_cancel+'<br />'+datatime;
                            }

                            if(data == 'Admin-Canceled') {
                                data = m_lang_withdraw_result_admin_cancel;
                            }

                            if(data == 'Accepting') {
                                data = m_lang_withdraw_result_accepting;
                            }



		                        return data;
		                    }
		                },
		                {
		                    'data': 'commit', 
		                    'render': function (data, type, row, meta, aaa) {
		                    	var option_id          = row['id'];
	                            if(row['state'] == 'Waiting') {
		                            	
	                                data = '<button type="button" option_id="'+option_id+'" class="btn btn-warning" onclick="click_accepting(this)">'+m_lang_withdraw_result_accepting+'</button>';
	                            }

		                        return data;
		                    }
		                },
		                {
		                    'data': 'commit', 
		                    'render': function (data, type, row, meta, aaa) {
		                    	var option_id = row['id'];
	                            if(row['state'] == 'Accepting') {
		                            	
	                                data = '<button type="button" option_id="'+option_id+'" class="btn btn-info" onclick="click_dealed(this)">'+m_lang_withdraw_result_deal+'</button>';
	                            }

		                        return data;
		                    }
		                },		                
		            ],
		            "serverSide": true,
		            "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
		    })

		  return false;
		}


	    // ＊＊ 取消交易 modal ＊＊
	    var m_cancel_trading_id = 0;
		function openCancelTradingModal(obj){
			m_cancel_trading_id = $(obj).attr('attr_trading_id');

    		$('#cancelTradingModal').modal('toggle');

    		return false;
		}




		var m_dealed_id = 0;
		function click_dealed(obj){
			m_dealed_id = $(obj).attr('option_id');
			$("#span_dealed_id").text(m_dealed_id);			
			$("#confirm_dealed_modal").modal('toggle');
		}


		function confirm_dealed(){
			
			$('#confirm_dealed_modal').modal('hide');
		    p_ApiMgr_admin.dealWithdraw(
		        m_dealed_id,                           
		        function (result) {	
		         	 console.log(result);	        	
		             reload_datatable();	
		        },this,
		        function (error) {
		             reload_datatable();	
		        },this
		    );	    

		}

		
		var m_accepting_id = 0;
		function click_accepting(obj){
			m_accepting_id = $(obj).attr('option_id');
			$("#span_accepting_id").text(m_accepting_id);			
			$("#confirm_accepting_modal").modal('toggle');
		}

		function confirm_accepting(){
			
			$('#confirm_accepting_modal').modal('hide');

			//alert('確認提領完成 '+m_accepting_id);
		    p_ApiMgr_admin.acceptingWithdraw(
		        m_accepting_id,                           
		        function (result) {	
		         	 console.log(result);	        	
		             reload_datatable();	
		        },this,
		        function (error) {
		             reload_datatable();	
		        },this
		    );	    

		}
		

	    // ＊＊ 取得提領資料 ＊＊
		function get_usr_info(obj){

			var id = $(obj).attr('option_id');
			var user_id = $(obj).attr('option_user_id');
			var amount = $(obj).attr('option_amount');
			var rmb = 4 * amount;

			$("#span_withdraw_id").text('#'+id);
			$("#span_withdraw_point").text(amount);
			$("#span_withdraw_rmb").text(rmb);

			$('#userinfoModal').modal('toggle');
			$("#span_withdraw_bank").text(user_id);

		    p_ApiMgr_admin.getUserAccountInfo(
		        user_id,                           
		        function (result) {

	            var obj = JSON.parse(result);
	            //var bank_info = obj.bank_code + '<br />' + obj.bank_branch + '<br />' + obj.bank_account;
	            $("#span_withdraw_bank_code").text(obj.bank_code);			
	            $("#span_withdraw_bank_branch").text(obj.bank_branch);		
	            $("#span_withdraw_bank_account").text(obj.bank_account);		
	            $("#span_withdraw_nickname").text(obj.nickname);		
	            $("#span_withdraw_country_code").text(obj.country_code);	
	            $("#span_withdraw_phone").text(obj.phone);	

		        },this,
		        function (error) {},this
		    );


    		return false;

		}



	    // ＊＊ 確認交易 modal ＊＊
		function confirmCancelTrading(obj){

    		$('#cancelTradingModal').modal('hide');

		    p_ApiMgr_admin.cancelTrading(
		        m_cancel_trading_id,                           
		        function (result) {

		          //alert('修改成功');
		          location.reload();
		        },this,
		        function (error) {},this
		    );


    		return false;

		}

	    // ＊＊ 打開-取消委託 modal ＊＊
	    var m_cancel_order_id = 0;
		function openCancelOrderModal(obj){
			m_cancel_order_id = $(obj).attr('attr_trading_id');

    		$('#cancelOrderModal').modal('toggle');

    		return false;
		}


	    // ＊＊ 確認-取消委託 modal ＊＊
		function confirmCancelOrder(obj){

    		$('#cancelOrderModal').modal('hide');

		    p_ApiMgr_admin.cancelOrder(
		        m_cancel_order_id,                           
		        function (result) {

		          //alert('修改成功');
		          location.reload();
		        },this,
		        function (error) {},this
		    );


    		return false;

		}



	</script>

@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.page_withdraw')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.page_withdraw')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">
	<div style="font-size: 20px;">
		<label class="radio-inline"><input type="radio" value ="All" name="optradio" checked>{{ Lang::get('user.show_all')}}</label>
		<label class="radio-inline"><input type="radio" value ="Accepting" name="optradio">{{ Lang::get('user.withdraw_result_accepting')}}</label>
		<label class="radio-inline"><input type="radio" value ="Deal" name="optradio">{{ Lang::get('user.withdraw_result_deal')}}</label>		
	</div>





<hr align="left" width="20%" class="site_map_hr">

	<!-- Sell DataTable -->
	<div class="container">

		<div class="hide_overflow" style="overflow:auto;"> 
		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		        <table id="table_withdraw" class="display" style="width:100%">
		            <thead>
		                <tr>		   		              
		                	<th>#</th>          	                  
                            <th>{{ Lang::get('user.datetime')}}</th>                        
                            <th>{{ Lang::get('user.point')}}</th>
                            <th>{{ Lang::get('user.fee')}}</th>
                            <th>{{ Lang::get('user.true_get')}}</th>                                                        
                            <th>{{ Lang::get('user.withdraw_user_info')}}</th>   
                            <th>{{ Lang::get('user.state')}}</th>                                   
                            <th>{{ Lang::get('user.table_options')}}</th>   	                    
                      		<th>{{ Lang::get('user.table_options')}}</th>   
		                    <!-- 已配對完 撤回 -->
		                </tr>
		            </thead>
		            <tbody>
		                <tr>		       
		                	<td></td>    
		                	<td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>                
							<td></td>		                    
							<td></td>		
		                </tr>
		            </tbody>
		        </table>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br />
			</div>
		  </div>
		</div>
	</div>


  <br />
<!--   <hr align="center" width="85%" class="site_map_hr"> -->


<!--  end overflow -->


<!-- cancel trading Modal -->
<div id="cancelTradingModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.confirm_cancel_trading')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_cancel')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmCancelTrading()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div>
    </div>

  </div>
</div>


<!-- cancel trading Modal -->
<div id="cancelOrderModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.confirm_cancel_order')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_cancel')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmCancelOrder()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div>
    </div>

  </div>
</div>


<!-- userinfo Modal -->
<div id="userinfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.withdraw_user_info')}}</h4>
      </div>
      <div class="modal-body">

		<table class="userinfo_mid_table">
			<tr>
				<td>{{ Lang::get('user.withdraw_number')}}</td>				
				<td><span id="span_withdraw_id" style="color: blue;">{{ Lang::get('user.get_info')}}</span></td>
			</tr>
			<tr>
				<td>{{ Lang::get('user.true_get')}}</td>
				<td><span id="span_withdraw_point" style="color: blue;">{{ Lang::get('user.get_info')}}</span></td>
			</tr>			
			<tr>
				<td>{{ Lang::get('user.nickname')}}</td>
				<td><span id="span_withdraw_nickname" style="color: blue;">{{ Lang::get('user.get_info')}}</span></td>
			</tr>
			<tr>
				<td>{{ Lang::get('user.phone')}}</td>				
				<td>
					<span id="span_withdraw_country_code" style="color: blue;">{{ Lang::get('user.get_info')}}</span><br />
					<span id="span_withdraw_phone" style="color: blue;">{{ Lang::get('user.get_info')}}</span>
				</td>
			</tr>			
			<tr>
				<td>{{ Lang::get('user.bank_info')}}</td>
				<td>
					<span id="span_withdraw_bank_code" style="color: blue;">{{ Lang::get('user.get_info')}}</span><br />
					<span id="span_withdraw_bank_branch" style="color: blue;">{{ Lang::get('user.get_info')}}</span><br />
					<span id="span_withdraw_bank_account" style="color: blue;">{{ Lang::get('user.get_info')}}</span><br />
				</td>
			</tr>

			<tr>
				<td>{{ Lang::get('user.withdraw_amount')}}</td>
				<td><span id="span_withdraw_rmb" style="color: red;">{{ Lang::get('user.get_info')}}</span></td>
			</tr>															
		</table>


      </div>
      <div class="modal-footer">     	
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div>
    </div>

  </div>
</div>



<!-- cancel trading Modal -->
<div id="confirm_accepting_modal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.withdraw_result_accepting')}}</h4>
      </div>
      <div class="modal-body">
        <p>#&nbsp;<span id="span_accepting_id" style="color: blue;"></span></p>
        <p>{{ Lang::get('user.withdraw_chage_state_accepting')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirm_accepting()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<div id="confirm_dealed_modal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.withdraw_result_deal')}}</h4>
      </div>
      <div class="modal-body">
        <p>#&nbsp;<span id="span_dealed_id" style="color: blue;"></span></p>
        <p>{{ Lang::get('user.withdraw_chage_state_deal')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirm_dealed()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





@endsection






















