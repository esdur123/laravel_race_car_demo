
@extends('layouts.admin_app') 


@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@endpush


@push('script')
	<script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
	<script src="{{ asset('js/admin/u_common.js') }}"></script>
	<script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/u_my_utility.js') }}"></script>
	<script type="text/javascript">

		var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
		var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
		var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
		var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
		var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
		var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
		var m_lang_processing = "{{ Lang::get('table.processing')}}";
		var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
		var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
		var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
		var m_lang_info = "{{ Lang::get('table.info')}}";

		var m_lang_order_down = "{{ Lang::get('user.order_down')}}";
		var m_lang_cancel_order = "{{ Lang::get('user.cancel_order')}}";
		var m_lang_paid_already = "{{ Lang::get('user.paid_already')}}";
		var m_lang_contact = "{{ Lang::get('user.contact')}}";
		var m_lang_cancel = "{{ Lang::get('user.cancel')}}";
		var m_lang_payment_received = "{{ Lang::get('user.payment_received')}}";
		var m_lang_payment_paid = "{{ Lang::get('user.payment_paid')}}";
		var m_lang_seller_name = "{{ Lang::get('user.seller_name')}}";
		var m_lang_buyer_name = "{{ Lang::get('user.buyer_name')}}";		
		var m_lang_paid_complete = "{{ Lang::get('user.paid_complete')}}";
		var m_lang_receive_complete = "{{ Lang::get('user.receive_complete')}}";
		var m_lang_deal_done = "{{ Lang::get('user.deal_done')}}";
		var m_lang_buyer_cancel = "{{ Lang::get('user.buyer_cancel')}}";
		var m_lang_seller_cancel = "{{ Lang::get('user.seller_cancel')}}";

		var m_lang_buyer_info = "{{ Lang::get('admin.buyer_info')}}";
		var m_lang_seller_info = "{{ Lang::get('admin.seller_info')}}";
		var m_lang_deal_record = "{{ Lang::get('admin.deal_record')}}";
		var m_lang_options = "{{ Lang::get('admin.options')}}";
		var m_lang_nickname = "{{ Lang::get('admin.nickname')}}";
		var m_lang_phone = "{{ Lang::get('admin.phone')}}";
		var m_lang_bank_code = "{{ Lang::get('admin.bank_code')}}";
		var m_lang_back_account = "{{ Lang::get('admin.back_account')}}";
		var m_lang_buyer_confirmed = "{{ Lang::get('admin.buyer_confirmed')}}";		
		var m_lang_seller_confirmed = "{{ Lang::get('admin.seller_confirmed')}}";
		var m_lang_member_id = "{{ Lang::get('admin.member_info')}}";
		var m_lang_order_amount = "{{ Lang::get('admin.order_amount')}}";
		var m_lang_dealed = "{{ Lang::get('admin.dealed')}}";
		var m_lang_on_trade = "{{ Lang::get('admin.on_trade')}}";
		var m_lang_none_record = "{{ Lang::get('admin.none_record')}}";	
		var m_lang_open = "{{ Lang::get('user.open')}}";			

		// ready-jobs
		$(function(){

		    setTimeout(function(){
		        getTradingInfo();
		        getOrders();
		    },500);

		})

		                   
	    // ＊＊ 取得-委託列表 ＊＊
		var m_order_datatable = null;
		function getOrders(){


		    var ajax_url = '/api/v1/orders/by/admin';  // '/orders/by/admin@getOrder
		    m_order_datatable = $('#table_orders').DataTable({
		             "language": {
		                  "info":m_lang_info,
		                  "sLengthMenu": m_lang_show_n_result,
		                  "search": m_lang_keyword,  
		                  "processing": m_lang_processing,
		                  "loadingRecords": m_lang_loadingRecords,
		                  "zeroRecords": m_lang_show_zeroRecords,
		                  "infoEmpty": m_lang_infoEmpty,
		                  "infoPostFix": "",
		                 "oPaginate": {
		                   "sFirst": m_lang_first_page,
		                   "sPrevious": m_lang_pre_page,
		                   "sNext": m_lang_next_page,
		                   "sLast": m_lang_last_page,
		                 },
		             },
		            "processing": true,
		            "lengthChange": false,
		            "pagingType": "simple_numbers",
		            "searching": false,   
		            "destroy": true,                      
		            "columns": [	           
		                {
		                    'data': 'created_at',
		                    'render': function (data, type, row, meta) {
		                        var my_datatime = data.date;
		                        my_datatime = my_datatime.toString();
		                        my_datatime = my_datatime.substring(0, my_datatime.length-7);
		                        return my_datatime;		                    	

		                    }
		                },
		                {
		                    'data': 'user_id',
		                    'render': function (data, type, row, meta) {
		                     // var new_url = '<a href="/admin/manage/account/'+data.toString()+'" ><span style="color:blue;">'+m_lang_open+'</span></a>';
	                      //    return new_url;

	                         var return_data = '<button attr_id="'+ data +'" type="button" class="btn btn-primary" onclick="openRecommenderTree(this)">'+m_lang_open+'</button>'
	                         return return_data;

		                    }
		                },
		                {
		                    'data': 'total',
		                    'render': function (data, type, row, meta) {

		                        return data;
		                    }
		                },
		                {
		                    'data': 'deal',
		                    'render': function (data, type, row, meta) {

		                        return data;
		                    }
		                },
		                {
		                    'data': 'trading',
		                    'render': function (data, type, row, meta) {


		                        return data;
		                    }
		                },
		                {
		                    'data': 'id',
		                    'render': function (data, type, row, meta) {

		                          data = '<button attr_trading_id="'+data+'" type="button" class="btn btn-warning" onclick="openCancelOrderModal(this)">'+m_lang_cancel+'</button>';
		                          return data;
		                    }
		                }		                

		            ],
		            "serverSide": true,
		            "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
		    })

		  return false;
		}



		  // ＊＊ 跳頁至branch ＊＊
		  function openRecommenderTree(obj){

		      var attr_id = $(obj).attr("attr_id");
		      location.href = "/admin/manage/recommend/"+attr_id.toString();

		  }



	    // ＊＊ 取得-交易中列表 ＊＊
		var m_buy_datatable = null;
		function getTradingInfo(){

		    var m_id = '';
		    var m_nickname = '';
		    var m_country_code = '';
		    var m_phone = '';
		    var m_bank_code = '';
		    var m_bank_branch = '';
		    var m_bank_account = '';
		    var m_datatime = '';
		    var m_point = '';
		    var m_trading_id = '';

		    var ajax_url = '/api/v1/txn/trading/by/admin';  // TransactionController@getOrder
		    m_buy_datatable = $('#table_trading').DataTable({
		             "language": {
		                  "info":m_lang_info,
		                  "sLengthMenu": m_lang_show_n_result,
		                  "search": m_lang_keyword,  
		                  "processing": m_lang_processing,
		                  "loadingRecords": m_lang_loadingRecords,
		                  "zeroRecords": m_lang_show_zeroRecords,
		                  "infoEmpty": m_lang_infoEmpty,
		                  "infoPostFix": "",
		                 "oPaginate": {
		                   "sFirst": m_lang_first_page,
		                   "sPrevious": m_lang_pre_page,
		                   "sNext": m_lang_next_page,
		                   "sLast": m_lang_last_page,
		                 },
		             },
		            "processing": true,
		            "lengthChange": false,
		            "pagingType": "simple_numbers",
		            "searching": false,   
		            "destroy": true,                      
		            "columns": [	           
		                {
		                    'data': 'created_at',
		                    'render': function (data, type, row, meta) {
		                        m_datatime = data.date;
		                        m_datatime = m_datatime.toString();
		                        m_datatime = m_datatime.substring(0, m_datatime.length-7);
		                        return m_datatime;
		                    }
		                },

		                {
		                    'data': 'amount',
		                    'render': function (data, type, row, meta) {
		                                  
		                        return data;
		                    }
		                },
		                {
		                    'data': 'seller_info',
		                    'render': function (data, type, row, meta) {
		                        var array_info = data.split(",");
                        	    var id = array_info[0];
	                            var nickname = array_info[1];
	                            var country_code = array_info[2];
	                            var phone = array_info[3];
	                            var bank_code = array_info[4];
	                            var bank_branch = array_info[5];
	                            var bank_account = array_info[6];
		                        if(array_info.length == 7){
		                            id = array_info[0];
		                            nickname = array_info[1];
		                            country_code = array_info[2];
		                            phone = array_info[3];
		                            bank_code = array_info[4];
		                            bank_branch = array_info[5];
		                            bank_account = array_info[6];
		                        }

		                        var return_data = m_lang_nickname+': '+nickname+'<br />'+m_lang_phone+': (+'+country_code+') '+phone;
		                        return_data    += '<br />'+m_lang_bank_code+': '+bank_code;
								return_data    += '<br />'+m_lang_back_account+': '+bank_account;
		                        return return_data;
		                    }
		                },
		                {
		                    'data': 'buyer_info',
		                    'render': function (data, type, row, meta) {
		                        var array_info = data.split(",");
                        	    var id = array_info[0];
	                            var nickname = array_info[1];
	                            var country_code = array_info[2];
	                            var phone = array_info[3];
	                            var bank_code = array_info[4];
	                            var bank_branch = array_info[5];
	                            var bank_account = array_info[6];
		                        if(array_info.length == 7){
		                            id = array_info[0];
		                            nickname = array_info[1];
		                            country_code = array_info[2];
		                            phone = array_info[3];
		                            bank_code = array_info[4];
		                            bank_branch = array_info[5];
		                            bank_account = array_info[6];
		                        }

		                        var return_data = m_lang_nickname+': '+nickname+'<br />'+m_lang_phone+': (+'+country_code+') '+phone;
		                        return_data    += '<br />'+m_lang_bank_code+': '+bank_code;
								return_data    += '<br />'+m_lang_back_account+': '+bank_account;
		                        return return_data;
		                    }
		                },
		                {
		                    'data': 'confirm_value',
		                    'render': function (data, type, row, meta) {
		                        var confirm_value = data;
  		                        // var _confirm_value = '';
		                        var _confirm_date = '';
		                        var confirm_array = data.split(",");

		                        if(confirm_array.length == 1) {
		                          _confirm_value = confirm_array[0];
		                          if(_confirm_value=='none'){
		                          	_confirm_value = m_lang_none_record;
		                          }
		                        } else if(confirm_array.length == 2) {
		                          _confirm_value = confirm_array[0];
		                          _confirm_date =  confirm_array[1];
		                          if(_confirm_value=='buyer-confirm'){
		                          	_confirm_value = m_lang_buyer_confirmed;
		                          }		
		                          if(_confirm_value=='seller-confirm'){
		                          	_confirm_value = m_lang_seller_confirmed;
		                          }		  		                                                    
		                        } else {
		                          return '-';
		                        }

		                        return _confirm_value+'<br />'+_confirm_date;
		                    }
		                },
		                {
		                    'data': 'confirm_value',
		                    'render': function (data, type, row, meta, aaa) {
		                        m_trading_id = row['id'];
		           
		                          data = '<button attr_trading_id="'+m_trading_id+'" type="button" class="btn btn-warning" onclick="openCancelTradingModal(this)">'+m_lang_cancel+'</button>';
		                       
		                        return data;
		                    }
		                }
		            ],
		            "serverSide": true,
		            "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
		    })

		  return false;
		}


	    // ＊＊ 取消交易 modal ＊＊
	    var m_cancel_trading_id = 0;
		function openCancelTradingModal(obj){
			m_cancel_trading_id = $(obj).attr('attr_trading_id');

    		$('#cancelTradingModal').modal('toggle');

    		return false;
		}


	    // ＊＊ 確認交易 modal ＊＊
		function confirmCancelTrading(obj){

    		$('#cancelTradingModal').modal('hide');

		    p_ApiMgr_admin.cancelTrading(
		        m_cancel_trading_id,                           
		        function (result) {

		          //alert('修改成功');
		          location.reload();
		        },this,
		        function (error) {},this
		    );


    		return false;

		}

	    // ＊＊ 打開-取消委託 modal ＊＊
	    var m_cancel_order_id = 0;
		function openCancelOrderModal(obj){
			m_cancel_order_id = $(obj).attr('attr_trading_id');

    		$('#cancelOrderModal').modal('toggle');

    		return false;
		}


	    // ＊＊ 確認-取消委託 modal ＊＊
		function confirmCancelOrder(obj){

    		$('#cancelOrderModal').modal('hide');

		    p_ApiMgr_admin.cancelOrder(
		        m_cancel_order_id,                           
		        function (result) {

		          //alert('修改成功');
		          location.reload();
		        },this,
		        function (error) {},this
		    );


    		return false;

		}



	</script>

@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.system_trade')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('user.system_trade')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


	<!-- Sell DataTable -->
	<div class="container">
		<h2>{{ Lang::get('admin.trading_list')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		        <table id="table_trading" class="display" style="width:100%">
		            <thead>
		                <tr>		   		                	                  
		                    <th>{{ Lang::get('user.datetime')}}</th>	                	      		                    
		                    <th>{{ Lang::get('user.point')}}</th>
							<th>{{ Lang::get('admin.buyer_info')}}</th> 
							<th>{{ Lang::get('admin.seller_info')}}</th> 		                    
		                    <th>{{ Lang::get('admin.deal_record')}}</th>  
		                    <th>{{ Lang::get('admin.options')}}</th> 	                    
		                    <!-- 已配對完 撤回 -->
		                </tr>
		            </thead>
		            <tbody>
		                <tr>		           
		                	<td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>                
		                </tr>
		            </tbody>
		        </table>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br />
			</div>
		  </div>
		</div>
	</div>


  <br />
<!--   <hr align="center" width="85%" class="site_map_hr"> -->

	<!-- Order DataTable -->
	<div class="container">
		<h2>{{ Lang::get('admin.ordering_list')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		        <table id="table_orders" class="display" style="width:100%">
		            <thead>
		                <tr>
		                    <th>{{ Lang::get('user.datetime')}}</th>	                	
		                    <th>{{ Lang::get('user.member')}}</th>
		                    <th>{{ Lang::get('admin.order_amount')}}</th> 
		                    <th>{{ Lang::get('admin.dealed')}}</th>  
		                    <th>{{ Lang::get('admin.on_trade')}}</th>
		                    <th>{{ Lang::get('admin.options')}}</th>		                    
		                    <!-- 已配對完 撤回 -->
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                	<td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				<br /><br /><br /><br /><br />
			</div>
		  </div>
		</div>
	</div>

<!--  end overflow -->


<!-- cancel trading Modal -->
<div id="cancelTradingModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.confirm_cancel_trading')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_cancel')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmCancelTrading()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div>
    </div>

  </div>
</div>


<!-- cancel trading Modal -->
<div id="cancelOrderModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.confirm_cancel_order')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_cancel')}}</p>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmCancelOrder()">{{ Lang::get('user.confirm')}}</button>      	
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div>
    </div>

  </div>
</div>



@endsection






















