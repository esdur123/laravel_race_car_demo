
@extends('layouts.admin_front_app') 


@push('head')
<style type="text/css">



</style>

@endpush


@push('script')



<script type="text/javascript">
	
	// ready-jobs
	$(function(){

	  //$("#admin_login").on("submit", click_admin_login);
      $("#admin_login").on("submit", click_admin_login);
      $('#admin_login_modal').modal('toggle');

      $("#li_logout").hide(); 
      $("#nav_show_login").css("opacity","1");
      $("#tag_logout").on("click", openLoginModal);
   //    $('#admin_login_modal').on('hidden.bs.modal', function () {
   // 		$('#admin_login_modal').modal('toggle');
	  // })

	  $('#admin_login_modal').on('hidden.bs.modal', function () {

	  	if(!m_is_login){
	  		$('#admin_login_modal').modal('toggle');
	  	}

	  })


	})


	// 按下login
	var m_is_login = false;
	function click_admin_login(){
		 
		  var _account = $("#admin_account").val();
		  var _pw = $("#admin_password").val();

		  // 都不為空白才能輸入
		  if( !(_account != '' && _pw != '')){
	   		return false;
		  }
		 
		  // 連續按login
		  if(m_is_login){
		  	return false;
		  }
		  m_is_login = true;

		  p_ApiMgr_admin.login(
		        _account,
		        _pw,
		        function (result) {
		        
		            var obj = JSON.parse(result);
		            var id = obj.id;
		          console.log("登入成功");
		          location.href = "/admin/control";
		        },this,
		        function (error) {
		        	m_is_login = false;
		        },this
		  );

	  return false;
	}


  // ＊＊ login modal ＊＊    
  function openLoginModal(){

        $('#admin_login_modal').modal('toggle');        
        return false;
  }


  function pageMgrAccount(){
      openLoginModal();
  }


  function pageMgrBranch(){
      openLoginModal();
  }

  function pageMgrRecommend(){
      openLoginModal();
  }

  function pageTrade(){
      openLoginModal();
  }

  function pageControl(){
      openLoginModal();
  }

  function pageInfo(){
      openLoginModal();
  }



</script>


@endpush


@section('content')


	<!-- Modal -->
	<div id="admin_login_modal" class="modal fade" role="dialog">
	  <div class="modal-dialog " style="max-width: 98vw!important;" >

	    <!-- Modal content-->
	    <div class="modal-content">

		    <div class="modal-body" align="center">
				<h4 class="modal-title" style="text-align: center;">{{ Lang::get('admin.admin_login')}}</h4>
				<br />
				<form id="admin_login">
					<div class="form-group">
					  <input type="text" class="form-control" id="admin_account" placeholder="{{ Lang::get('home.account')}}" style="width: 80%;background-color : #F5F5F5; ">
					</div>

					<div class="form-group">
					<input type="password" class="form-control" id="admin_password" placeholder="{{ Lang::get('home.pw')}}"  style="width: 80%;background-color : #F5F5F5; ">
					</div>
					<br />
					<div>
					  <button type="submit" class="btn btn-warning" style="width: 80%;">{{ Lang::get('home.login')}}</button>  
					</div>
				</form>

		    </div>
	      <div class="modal-footer">
	      	<br />
	        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
	      </div>
	    </div>

	  </div>
	</div>



@endsection






















