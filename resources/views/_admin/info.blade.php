
@extends('layouts.admin_app') 


@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">      
    <style type="text/css">
	    .switch_group {
            width: 64px;
            height: 28px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            cursor: pointer;
        }
    </style>

@endpush


@push('script')
  <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
  <script src="{{ asset('js/admin/u_common.js') }}"></script>

<script type="text/javascript">
	
	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";
	var m_lang_update_succeed = "{{ Lang::get('user.update_succeed')}}";
	
	// ready-jobs
	$(function(){
//daily_verify_message_Modal

		// $( "#select_daily_verify_message" ).change(function() {
		//     showModalDailyMsg();
		  
		// });

		//checkIsAllLimitPay();
		//checkDailyVerifyMessageLimit();

	   	setTimeout(function(){
		     
		},500);

	})



	// ＊＊ 修改密碼 Modal ＊＊
	function UpdateAdminPassword(){

	  $('#passwordModal').modal('toggle');
	  return false;
	}


	// ＊＊ 修改密碼 ＊＊
	function confirmUpdateAdminPassword(){

	  p_ApiMgr_admin.UpdateAdminPassword(
	          $("#old_pw").val(),
	          $("#new_pw").val(),
	          $("#new_pw2").val(),	          
	        function (result) {
	            //console.log(result);
	            alert(m_lang_update_succeed);
	            $("#old_pw").val('');
	            $("#new_pw").val('');
	            $("#new_pw2").val('');
	        },this,
	        function (error) {},this
	  );

	  return false;
	}

</script>


@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('admin.admin_setting')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('admin.admin_setting')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

	<!-- Account DataTable -->
	<div class="container">

		<h2>{{ Lang::get('admin.pw_setting')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
						
				  <!-- change pw -->
				  <div id="div_update_pw">
				  	{{ Lang::get('user.old_pw')}} &nbsp; <span style="color: #BBB;">{{ Lang::get('user.first_login_pw')}}</span>
				  	<input type="password" class="form-control" id="old_pw" maxlength="16">
				  	<br />
				  	{{ Lang::get('user.new_pw')}}
				  	<input type="password" class="form-control" id="new_pw" maxlength="16">
				  	<br />
				  	{{ Lang::get('user.new_pw2')}}
				  	<input type="password" class="form-control" id="new_pw2" maxlength="16">
				  	<br />
					<button type="button" class="btn btn-success" onclick="UpdateAdminPassword()">{{ Lang::get('user.confirm_update')}}</button>			  			  	
				  </div>

			    </div>
			  </div>

			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
					<br /><br /><br /><br /><br />
				</div>
			  </div>
		</div>
	</div>


<!-- passwordModal -->
<div id="passwordModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <p>{{ Lang::get('user.update_user_info')}}</p>

      </div>
      <div class="modal-body">
        <p>{{ Lang::get('user.confirm_update')}}</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmUpdateAdminPassword()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection






















