
@extends('layouts.admin_app') 


@push('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">      
    <style type="text/css">
	    .switch_group {
            width: 64px;
            height: 28px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            cursor: pointer;
        }
    </style>

@endpush


@push('script')
  <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
  <script src="{{ asset('js/admin/u_common.js') }}"></script>

<script type="text/javascript">
	
	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

	// ready-jobs
	$(function(){
//daily_verify_message_Modal

		$( "#select_daily_verify_message" ).change(function() {
		    showModalDailyMsg();
		  
		});

		checkIsAllLimitWithdraw();
		checkDailyVerifyMessageLimit();

	   	setTimeout(function(){
		     
		},500);

	})


	// ＊＊ 按下-切換額度 ＊＊
	function clickswitchLimitedWithdraw(){

	    $("#confirmSwitchPayLimitModal").modal('toggle');


	    return false;
	}


	// ＊＊ 切換每日額度改變時 ＊＊
	function showModalDailyMsg(){

		$("#span_daily_msg_max").text($( "#select_daily_verify_message" ).val());
	    $("#daily_verify_message_Modal").modal('toggle');
			//alert( $( "#select_daily_verify_message" ).val() );

	    return false;
	}

	
	// ＊＊ 取得 - 每日驗證簡訊上限 ＊＊
	function checkDailyVerifyMessageLimit(){


	    p_ApiMgr_admin.getDailyVerifyMessageLimit(
	          function (result) {
	          	var limited_value = result['value'];

	          	$("#select_daily_verify_message").val(limited_value);
	          	//$( "#select_daily_verify_message" ).val(result['value']),
	          },this,
	          function (error) {},this
	    );


	    return false;		
	}


	// ＊＊ 確認 - 修改每日驗證簡訊上限 ＊＊
	function confirmChangeDailyMessageLimit(){
		$("#daily_verify_message_Modal").modal('hide');

		// 更改每日簡訊上限

		//alert('confirmChangeDailyMessageLimit');
	    p_ApiMgr_admin.updateDailyMessageLimit(
	    	  $( "#select_daily_verify_message" ).val(),
	          function (result) {
	          	//alert('修改成功');
	          },this,
	          function (error) {},this
	    );

	    return false;		
	}


	// ＊＊ 允許儲值 ＊＊
	var m_is_limited_pay = 0;
	function showLimitedPayIcon(value){
		//console.log(value);
	    m_is_limited_pay = value;
	    if(m_is_limited_pay == 0){
	      $("#switch_max_pay").attr("src","{{ asset('img/switch_on.png') }}"); 
	    }

	    if(m_is_limited_pay == 1){
	      $("#switch_max_pay").attr("src","{{ asset('img/switch_off.png') }}"); 
	    }

	    return false;
	}


	// ＊＊ 切換額度開關 ＊＊
	function confirmSwitchPayLimitModal(){

	    if(m_is_limited_pay==0) {
	      m_is_limited_pay = 1;
	    } else {
	      m_is_limited_pay = 0;
	    }


	    //alert(m_is_limited_pay);

	    // 等等做這個api
	    p_ApiMgr_admin.switchLimitedWithdraw(
	          m_is_limited_pay,                              
	          function (result) {
	            //alert('修改成功');
	            m_is_limited_pay = parseInt(result);
	            //alert(result);
	            showLimitedPayIcon(m_is_limited_pay);	            
	          },this,
	          function (error) {},this
	    );

	    return false;
	}


	// ＊＊ 取得 控制資訊 ＊＊
	function checkIsAllLimitWithdraw(){

	    p_ApiMgr_admin.checkIsAllLimitWithdraw(
	          function (result) {
	            //var obj = JSON.parse(result);
	            //alert(result['is_tagged']);
	            //console.log(obj.account);
	            showLimitedPayIcon(result['is_tagged']);

	          },this,
	          function (error) {},this
	    );

	    return false;
	}



</script>


@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.main_page')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
<!-- 			&nbsp;>&nbsp;{{ Lang::get('admin.sys_control')}} -->
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


	<!-- Account DataTable -->
	<div class="container">
		
		<div class="hide_overflow" style="overflow:auto;"> 
			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
				  <h2>{{ Lang::get('admin.setting')}}</h2>
	              <table border="0">
	              　<tr>
	              　  <td>
	                   <h5>{{ Lang::get('admin.allow_withdraw')}}</h5>  <!-- 今日儲值額度已滿 --> 
	                  </td>

	              　  <td style="width: 50px;">
	                    &nbsp; 
	                  </td> 

	              　  <td>
	                    <h5>{{ Lang::get('admin.daily_phone_verify_max')}}</h5>  <!-- 今日儲值額度已滿 --> 
	                  </td>

	              　</tr>

	              	<tr>
  	              　  <td>
	                    <div>
	                      <img id="switch_max_pay" class="switch_group" onclick="clickswitchLimitedWithdraw()" src="{{ asset('img/switch_none.png')}}">            
	                    </div>  
	                  </td> 

	              　  <td style="width: 50px;">
	                    &nbsp; 
	                  </td> 

	              　  <td>
	              		<br />
	                    <div>	                      
							<div class="form-group">
							  <select class="form-control" id="select_daily_verify_message">
							    <option value="3" >3</option>
							    <option value="5" >5</option>
							    <option value="10">10</option>
							  </select>
							</div>

	                    </div>  
	                  </td> 
	              	</tr>
	              </table>
			    </div>	
			  </div> <!-- end row -->

			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
					<br /><br /><br /><br /><br />
				</div>
			  </div>
		</div>
	</div>





<!-- Save User Info Modal -->
<div id="confirmSwitchPayLimitModal" class="modal fade" role="dialog">
  <div class="modal-dialog " style="max-width: 98vw!important;" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.allow_withdraw')}}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin.confirm_switch')}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmSwitchPayLimitModal()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Save User Info Modal -->
<div id="daily_verify_message_Modal" class="modal fade" role="dialog" >
  <div class="modal-dialog " style="max-width: 50vw!important;" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin.switch_daily_verify_message')}}</h4>
      </div>
      <div class="modal-body">
        <span>{{ Lang::get('admin.daily_verify_message_value')}}</span>&nbsp;<span id="span_daily_msg_max" style="color: blue;"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"  onclick="confirmChangeDailyMessageLimit()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@endsection






















