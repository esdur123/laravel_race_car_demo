
@extends('layouts.admin_app') 


@push('head')
	<link rel="stylesheet" href="{{ asset('css/u_google_chart.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

	<style type="text/css">

	    table {
	        border-collapse: inherit;
	    }

	    .google-visualization-orgchart-node {
	        border: inherit !important;
	        -webkit-box-shadow: inherit !important;
	        -moz-box-shadow: inherit !important;
	        background-color: inherit !important;
	        background: inherit !important;
	    }



	</style>

@endpush


@push('script')

	<script src="{{ asset('js/u_loader.js') }}"></script>
	<script src="{{ asset('js/u_tree_google_org_chart.js') }}"></script>
	<script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
	<script src="{{ asset('js/admin/u_common.js') }}"></script>

<script type="text/javascript">
	var m_lang_set = "{{ Lang::get('user.set')}}";
	var m_lang_performance_total = "{{ Lang::get('user.performance_total')}}";		
	var m_lang_performance_L = "{{ Lang::get('user.performance_L')}}";
	var m_lang_performance_R = "{{ Lang::get('user.performance_R')}}";	
	
    var m_recommend_id = '{{ $id }}'; 
	// ready-jobs
	$(function(){

	    setTimeout(function(){
	          getRecommenderSpecific();
	          $("#small_man_lv0").text(C_SET_LEVEL_0);
	          $("#small_man_lv1").text(C_SET_LEVEL_1);
	          $("#small_man_lv2").text(C_SET_LEVEL_2);
	    },500);
	})



	// ＊＊ 取得branch tree ＊＊
	function getRecommenderSpecific(){

	    p_ApiMgr_admin.getRecommenderSpecific(
	      m_recommend_id,
	      function (result) {
	        //console.log(result['tree']); 

	        m_tree_obj = JSON.parse(result['tree']); // 先轉m_tree_obj    

	        // // // 用callback 呼叫 OrganizationTreeByObjAndDraw_Recommend
	        google.charts.load('current', {packages:["orgchart"], 'callback': OrganizationTreeByObjAndDraw_AdminRecommend}); 
	        m_tree_mapdata = []; // 陣列清空  

	      },this,
	      function (error) {},this
	    );

	  return false;
	}


</script>

@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('user.org_recommend')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
      		&nbsp;>&nbsp;{{ Lang::get('admin.manager_members')}}      
			&nbsp;>&nbsp;{{ Lang::get('user.org_recommend')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">

<!-- <div class="hide_overflow" style="overflow:auto;">  -->

<!-- 	&::-webkit-scrollbar-thumb -->
	<!-- Main -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!-- 顯示小人配套 -->
	        <div >        	
	        	<i class="fas fa-user" style="font-size:20px;color:#ccc;"></i>&nbsp;<span id="small_man_lv0" style="font-size:18px;font-weight: normal;"></span>
	        	&nbsp;&nbsp;
	        	<i class="fas fa-user" style="font-size:20px;color:#77DDFF;"></i>&nbsp;<span id="small_man_lv1" style="font-size:18px;font-weight: normal;"></span>
	        	&nbsp;&nbsp;
	        	<i class="fas fa-user" style="font-size:20px;color:#FFB7DD;"></i>&nbsp;<span id="small_man_lv2" style="font-size:18px;font-weight: normal;"></span>
	        </div>
			<br />
	        
			<!-- 組織圖 -->
			<div class="nori_overflow">
	        	<div id="show_tree_div"></div>
	        </div>

		</div>

	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
			<br /><br /><br /><br /><br />
		</div>
	</div>
</div>




@endsection






















