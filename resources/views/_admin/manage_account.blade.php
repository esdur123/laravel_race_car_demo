
@extends('layouts.admin_app') 


@push('head')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@endpush


@push('script')
  <script src="{{ asset('js/u_jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/admin/u_api_mgr_admin.js') }}"></script>
  <script src="{{ asset('js/admin/u_common.js') }}"></script>

<script type="text/javascript">
	
	var m_lang_next_page = "{{ Lang::get('table.next_page')}}";
	var m_lang_pre_page = "{{ Lang::get('table.pre_page')}}";
	var m_lang_first_page = "{{ Lang::get('table.first_page')}}";
	var m_lang_last_page = "{{ Lang::get('table.last_page')}}";
	var m_lang_show_n_result = "{{ Lang::get('table.show_n_result')}}";
	var m_lang_keyword = "{{ Lang::get('table.keyword')}}";
	var m_lang_processing = "{{ Lang::get('table.processing')}}";
	var m_lang_loadingRecords = "{{ Lang::get('table.loadingRecords')}}";
	var m_lang_show_zeroRecords = "{{ Lang::get('table.zeroRecords')}}";
	var m_lang_infoEmpty = "{{ Lang::get('table.infoEmpty')}}";
	var m_lang_info = "{{ Lang::get('table.info')}}";

	var m_lang_srecommender_register = "{{ Lang::get('user.recommender_register')}}";
	var m_lang_max_left_max_right = "{{ Lang::get('user.max_left_max_right')}}";
	var m_lang_no_recommender = "{{ Lang::get('user.no_recommender')}}";
	var m_lang_select = "{{ Lang::get('user.select')}}";
	var m_lang_confirm_null_upline = "{{ Lang::get('admin.register_new_null_upline')}}";




	// ready-jobs
	$(function(){

		showRegisterSetSelect();

	   	setTimeout(function(){
			GetOrgBranchs();    
		},500);

	})


	// ＊＊ 按下 新版註冊 ＊＊
	function clickRegisterNew(){

          if($("#input_account_2").val() == '') return false;
          if($("#input_nickname_2").val() == '') return false;
          if($("#select_country_code_2").val() == '') return false;
          if($("#input_phone_2").val() == '') return false;
          if($("#select_user_level_2").val() == '') return false;

          if($("#input_recommend_account_2").val() == ''){
          	$("#span_new_register_upline").text(m_lang_confirm_null_upline);
          }

          if($("#select_user_level_2").val() == 0){
          	$("#span_new_register_set").text('提示: 配套為0');
          }

          $("#new_register_modal").modal();

	  return false;
	}


	// ＊＊ 確認 新版註冊 ＊＊
	function confirmRegisterNew(){

		$('#modal_register').modal('hide');
		$('#new_register_modal').modal('hide');
          // if($("#input_account_2").val() == '') return false;
          // if($("#input_nickname_2").val() == '') return false;
          // if($("#select_country_code_2").val() == '') return false;
          // if($("#input_phone_2").val() == '') return false;
          // if($("#select_user_level_2").val() == '') return false;

         //alert("123");
	      p_ApiMgr_admin.registerNew(	      	
	      	
			$('#input_recommend_account_2').val(),
	        $('#input_account_2').val(),
	        $('#input_nickname_2').val(),
	        $('#select_country_code_2').val(),
	        $('#input_phone_2').val(),
	        $('#select_user_level_2').val(),

	      function (result) {
	        //console.log(result);
	        alert('註冊成功');
	        location.reload();
	      },this,
	      function (error) {},this
	    );

	  return false;
	}


	// ＊＊ 無上線註冊 ＊＊
	function registerNoUpline(){

          if($("#input_account_3").val() == '') return false;
          if($("#input_nickname_3").val() == '') return false;
          if($("#select_country_code_3").val() == '') return false;
          if($("#input_phone_3").val() == '') return false;

	      p_ApiMgr_admin.registerNoUpline(	      	

	        $('#input_account_3').val(),
	        $('#input_nickname_3').val(),
	        $('#select_country_code_3').val(),
	        $('#input_phone_3').val(),

	      function (result) {
	        console.log(result);
	        alert('註冊成功');
	        location.reload();
	      },this,
	      function (error) {},this
	    );

	  return false;
	}


	// ＊＊ 極左極右註冊 ＊＊
	function registerMaxLeftMaxRight(){

          if($("#input_recommend_account_2").val() == '') return false;
          if($("#select_max_2").val() == '') return false;
          if($("#input_account_2").val() == '') return false;
          if($("#input_nickname_2").val() == '') return false;
          if($("#select_country_code_2").val() == '') return false;
          if($("#input_phone_2").val() == '') return false;
          if($("#select_user_level_2").val() == '') return false;

	      p_ApiMgr_admin.registerMaxLeftMaxRight(	      	
	        $('#input_recommend_account_2').val(),	      	
	        $('#select_max_2').val(),
	        $('#input_account_2').val(),
	        $('#input_nickname_2').val(),
	        $('#select_country_code_2').val(),
	        $('#input_phone_2').val(),
	        $('#select_user_level_2').val(), 
	      function (result) {
	        console.log(result);
	        alert('註冊成功');
	        location.reload();
	      },this,
	      function (error) {},this
	    );

	  return false;
	}


	// ＊＊ 註冊選擇配套的select - html ＊＊
	function showRegisterSetSelect(){
		var _html = '<select class="form-control" id="select_user_level_2"><option disabled selected value> -- '+m_lang_select+' --</option><option value="0">'+C_SET_LEVEL_0+'</option><option value="1">'+C_SET_LEVEL_1+'</option><option value="2">'+C_SET_LEVEL_2+'</option></select>';
		$("#select_sets_2").html(_html); 		
	}


	// ＊＊ 取得branch datatable ＊＊
	function GetOrgBranchs(){

	  var ajax_url = '/api/v1/user/accounts/by/admin';

	    var m_user_id = 0;
	    var m_country_code = '';
	    $('#table_accounts').DataTable({
	             "language": {
	                "info":m_lang_info,
	                "sLengthMenu": m_lang_show_n_result,
               	    "search": m_lang_keyword,	 
	                "processing": m_lang_processing,
	                "loadingRecords": m_lang_loadingRecords,
	                "zeroRecords": m_lang_show_zeroRecords,
	                "infoEmpty": m_lang_infoEmpty,
	                "infoPostFix": "",

	                "oPaginate": {
	                   "sFirst": m_lang_first_page,
	                   "sPrevious": m_lang_pre_page,
	                   "sNext": m_lang_next_page,
	                   "sLast": m_lang_last_page,                  
	                 },
	             },
	            "processing": true,
	            "lengthChange": false,
	            "pagingType": "simple_numbers",
	            "searching": true,     
	            "destroy": true,          
	            "columns": [	      
	                {
	                    'data': 'country_code',
	                    'render': function (data, type, row, meta) {
	                        m_user_id = row['id'];
	                        m_country_code = data;
	                        return m_user_id;
	                    }
	                },
	                {
	                    'data': 'account',
	                    'render': function (data, type, row, meta) {
	                    	var new_url = '<a href="/admin/manage/account/'+m_user_id.toString()+'" ><span style="color:blue;">'+data.toString()+'</span></a>';
	                        return new_url;
	                    }
	                },
	                {'data': 'nickname' },	                	                
	                {
	                    'data': 'phone',
	                    'render': function (data, type, row, meta) {
	                    	var full_phone = '(+'+m_country_code+') '+data;

	                        return full_phone;
	                    }
	                }

	            ],
	            "serverSide": true,
	            "ajax": p_ApiMgr_admin.getDataTableAjax('GET', ajax_url)
	    })


	  return false;
	}

	var m_walletId = 0;
	var m_user_Id = 0;

	// 秀表格
	function getWalletID(obj){

	  m_walletId = $(obj).attr("attr_wallet_id");
	  m_user_Id = $(obj).attr("attr_user_id");

	  $("#account_id").text(m_user_Id);
	  return false;
	}



	// ＊＊ 按下-建立新帳號 ＊＊
	function clickRegister(){

	    $("#modal_register").modal('toggle');


	    return false;
	}



	function RecyclePoint(){

	  var point =  $("#input_recyclePt").val();
	  
	  //alert('wallet_id = '+walletId+', point = '+point);

	    p_ApiMgr_admin.recyclePoint(
	          m_walletId,
	          m_user_Id,
	          point,
	          function (result) {
	            //console.log(result);
	            alert('回收成功');
	            location.reload();
	          },this,
	          function (error) {},this
	    );

	    return false;
	}


	function logout(){

	  //alert('wallet_id = '+walletId+', point = '+point);

	    p_ApiMgr_admin.logout(

	          function (result) {
	            //console.log(result);
	           location.href = "/admin";         
	            // location.reload();
	          },this,
	          function (error) {},this
	    );

	    return false;
	}


      // ＊＊ 推薦註冊 ＊＊
      function registerFromRecommender(){
          if($("#signup_recommender").val() == '') return false;
          if($("#signup_account").val() == '') return false;
          if($("#signup_nickname").val() == '') return false;
          if($("#signup_phone").val() == '') return false;
          if($("#signup_pw").val() == '') return false;
          if($("#signup_pw2").val() == '') return false;

          // alert($("#signup_recommender").val()+','+$("#signup_account").val()+','+$("#signup_nickname").val()+','+$("#select_country_code").val()+','+$("#signup_phone").val()+','+$("#signup_pw").val()+','+$("#signup_pw2").val());

          p_ApiMgr_admin.registerFromRecommender(
            $("#signup_recommender").val(),
            $("#signup_account").val(),
            $("#signup_nickname").val(),
            $("#select_country_code").val(),
            $("#signup_phone").val(),
            $("#signup_pw").val(),
            $("#signup_pw2").val(),
            function (result) {
            
              alert('註冊成功');
              location.reload();

            },this,
            function (error) {},this
          );

        return false;
      }

</script>



@endpush


@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
		<div style="display:inline-block">
			<h2>{{ Lang::get('admin.member_account')}}</h2>
		</div>
		<div class="web_map">	
			&nbsp;&nbsp;&nbsp;{{ Lang::get('user.main_page')}}
			&nbsp;>&nbsp;{{ Lang::get('admin.manager_members')}}
			&nbsp;>&nbsp;{{ Lang::get('admin.member_account')}}
		</div>
	</div>
</div>

<hr align="left" width="20%" class="site_map_hr">


	<!-- Account DataTable -->
	<div class="container">
	 	<button  onclick="clickRegister()" type="button" class="btn btn-success">{{ Lang::get('admin.create_new_user_account')}}</button>
	    <br /><br />


		<h2>{{ Lang::get('admin.account_list')}}</h2>
		<div class="hide_overflow" style="overflow:auto;"> 
			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
			        <table id="table_accounts" class="display" style="width:100%">
			            <thead>
			                <tr>
			  
			                    <th>#</th>	                	
			                    <th>{{ Lang::get('user.account')}}</th>
								<th>{{ Lang::get('user.nickname')}}</th> 
								<th>{{ Lang::get('user.phone')}}</th> 			                    
			                    <!-- 已配對完 撤回 -->
			                </tr>
			            </thead>
			            <tbody>
			                <tr>			             
			                	<td></td>
			                    <td></td>
			                	<td></td>
			                    <td></td>			                    
			                </tr>
			            </tbody>
			        </table>
			    </div>
			  </div>

			  <div class="row">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">      		
					<br /><br /><br /><br /><br />
				</div>
			  </div>
		</div>
	</div>




  <!-- Modal 註冊 -->
  <div id="modal_register" class="modal fade"  role="dialog">
    <div class="modal-dialog" style="max-width: 98vw;">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-title" style="font-weight: bold;">{{ Lang::get('user.add_account')}}</h5>         
        </div>
        <div class="modal-body">

				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#home">{{ Lang::get('user.register')}}</a></li>
<!-- 				  <li><a data-toggle="tab" href="#menu1">{{ Lang::get('user.max_left_max_right')}}</a></li>
				  <li><a data-toggle="tab" href="#menu2">{{ Lang::get('user.no_recommender')}}</a></li> -->
				</ul>

				<div class="tab-content">
				  <div id="home" class="tab-pane fade in active">



					<h3>{{ Lang::get('user.register')}}</h3>

	   				<div class="form-group">					
						<label for="input_recommend_account_2">{{ Lang::get('user.recommend_acc')}}</label>
						<input type="text" class="form-control" id="input_recommend_account_2"  placeholder="{{ Lang::get('user.recommend_acc')}}" >
						

						<br />
						<label for="input_account_2">{{ Lang::get('user.account')}}</label>
						<input type="text" class="form-control" id="input_account_2"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

						<br />
						<label for="input_nickname_2">{{ Lang::get('user.nickname')}}</label>
						<input type="text" class="form-control" id="input_nickname_2"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

						<br />
						<label for="select_country_code_2">{{ Lang::get('user.country_code')}}</label>
						<select class="form-control" id="select_country_code_2">
							@include('components.country_code')
						</select>

						<br />
						<label for="input_phone_2">{{ Lang::get('user.phone')}}</label>
						<input type="text" class="form-control" id="input_phone_2"  placeholder="09xxxxxxxx" maxlength="10">

						<br />
						<label for="select_user_level_2">{{ Lang::get('user.set')}}</label>
						<div id = "select_sets_2"></div>
					
					</div>

		            <div class="form-group " align="center">
		              <br />
		              <button id="btn_register_2" onclick="clickRegisterNew()" type="button" class="btn btn-success" style="width: 100%;">{{ Lang::get('user.register')}}</button>
		            </div>





















				  </div>


				  <!-- 極左極右 -->
<!-- 				  <div id="menu1" class="tab-pane fade">
				    <h3>{{ Lang::get('user.max_left_max_right')}}</h3>

	   				<div class="form-group">					
						<label for="input_recommend_account_2">{{ Lang::get('user.recommend_acc')}}</label>
						<input type="text" class="form-control" id="input_recommend_account_2"  placeholder="{{ Lang::get('user.recommend_acc')}}" >
						
						<br />
						<label for="select_max_2">{{ Lang::get('user.set_branch')}}</label>
							<select class="form-control" id="select_max_2">
							<option value="L">{{ Lang::get('user.max_l')}}</option>
							<option value="R">{{ Lang::get('user.max_r')}}</option>
						</select>

						<br />
						<label for="input_account_2">{{ Lang::get('user.account')}}</label>
						<input type="text" class="form-control" id="input_account_2"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

						<br />
						<label for="input_nickname_2">{{ Lang::get('user.nickname')}}</label>
						<input type="text" class="form-control" id="input_nickname_2"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

						<br />
						<label for="select_country_code_2">{{ Lang::get('user.country_code')}}</label>
						<select class="form-control" id="select_country_code_2">
							@include('components.country_code')
						</select>

						<br />
						<label for="input_phone_2">{{ Lang::get('user.phone')}}</label>
						<input type="text" class="form-control" id="input_phone_2"  placeholder="09xxxxxxxx" maxlength="10">

						<br />
						<label for="select_user_level_2">{{ Lang::get('user.set')}}</label>
						<div id = "select_sets_2"></div>
					
					</div>

		            <div class="form-group " align="center">
		              <br />
		              <button id="btn_register_2" onclick="registerMaxLeftMaxRight()" type="button" class="btn btn-success" style="width: 100%;">{{ Lang::get('user.max_left_max_right_register')}}</button>
		            </div>

				  </div> -->


				  <!-- 無上線 -->
<!-- 				  <div id="menu2" class="tab-pane fade">
				    <h3>{{ Lang::get('user.no_recommender')}}</h3>


	   				<div class="form-group">									

						<label for="input_account_3">{{ Lang::get('user.account')}}</label>
						<input type="text" class="form-control" id="input_account_3"  placeholder="{{ Lang::get('user.account')}}" maxlength="16">

						<br />
						<label for="input_nickname_3">{{ Lang::get('user.nickname')}}</label>
						<input type="text" class="form-control" id="input_nickname_3"  placeholder="{{ Lang::get('user.nickname')}}" maxlength="16">

						<br />
						<label for="select_country_code_3">{{ Lang::get('user.country_code')}}</label>
						<select class="form-control" id="select_country_code_3">
							@include('components.country_code')
						</select>

						<br />
						<label for="input_phone_3">{{ Lang::get('user.phone')}}</label>
						<input type="text" class="form-control" id="input_phone_3"  placeholder="09xxxxxxxx" maxlength="10">
					
					</div>

		            <div class="form-group " align="center">
		              <br />
		              <button id="btn_register_3" onclick="registerNoUpline()" type="button" class="btn btn-success" style="width: 100%;">{{ Lang::get('user.no_recommender_register')}}</button>
		            </div>


				  </div> -->
				</div>





        </div> 

        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-success" onclick="confirmRegister()">{{ Lang::get('user.confirm')}}</button>&nbsp; -->
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('user.cancel')}}</button>
        </div>
      </div>
      
    </div>
  </div>



<!-- Modal -->
<div id="new_register_modal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 98vw;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('user.register')}}</h4>
      </div>
      <div class="modal-body">
      	<span>{{ Lang::get('user.register_confirm')}}</span><br /><br />
        <span id="span_new_register_upline" style="color: red;"></span><br /><br />
        <span id="span_new_register_set" style="color: blue;"></span>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="confirmRegisterNew()">{{ Lang::get('user.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection






















